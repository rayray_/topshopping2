webpackJsonp([44],{

/***/ 138:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrecoCinemaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PrecoCinemaPage = (function () {
    function PrecoCinemaPage(navCtrl, navParams, http, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.storage = storage;
        this.searching = true;
    }
    PrecoCinemaPage.prototype.PrecoCinema = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + "jsonPrecosCinemaMobile.json?nocache=" + nocache).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
            _this.precos = data.lista;
            _this.searching = false;
            if (_this.precos == 0) {
                _this.someAreaCinema = true;
            }
            else {
                _this.someAreaCinema = false;
            }
        });
    };
    PrecoCinemaPage.prototype.ionViewDidLoad = function () {
        this.PrecoCinema();
        console.log('ionViewDidLoad PrecoCinemaPage');
    };
    return PrecoCinemaPage;
}());
PrecoCinemaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-preco-cinema',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/preco-cinema/preco-cinema.html"*/'\n\n\n\n\n\n<ion-content >\n\n  <div class="alignCenter">\n\n    <div class="price-container clearfix">\n\n        <div class="subHeader">\n\n          <p class="Title">Preços</p>\n\n          <p class="Chamada">Confira os preços disponíveis</p>\n\n        </div>\n\n\n\n      <div *ngIf="searching" class="spinner-container">\n\n        <ion-spinner></ion-spinner>\n\n        <p>Carregando dados...</p>\n\n      </div>\n\n      <div class="price clearfix" *ngFor="let p of precos">\n\n            <div class="label" [innerHTML]="p.nome.str.lang_1"></div>\n\n            <div class="form" [innerHTML]="p.descricao.str.lang_1"></div>\n\n      </div>\n\n    </div>\n\n    <div class="semConexao" *ngIf="(precos)==\'\'||precos==\'\'">\n\n      <img src="assets/iconSmile.png" class="iconeimg" alt="">\n\n        <p class="AindaNaoFavorito">Aguarde! </p>\n\n        <p>Ainda não há preços disponíveis.</p>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/preco-cinema/preco-cinema.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], PrecoCinemaPage);

//# sourceMappingURL=preco-cinema.js.map

/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CampanhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__campanha_cadastro_campanha_cadastro__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CampanhaPage = (function () {
    function CampanhaPage(navCtrl, navParams, service, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.CampanhaCadastroPage = __WEBPACK_IMPORTED_MODULE_4__campanha_cadastro_campanha_cadastro__["a" /* CampanhaCadastroPage */];
        this.Dominio = this.service.config.defaultDomain;
        this.campanha = this.navParams.get("campanha");
    }
    CampanhaPage.prototype.IrParaCadastro = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__campanha_cadastro_campanha_cadastro__["a" /* CampanhaCadastroPage */], {
            cadastrocampanha: this.campanha
        });
    };
    CampanhaPage.prototype.Compartilhar = function () {
        var titulo = this.campanha[0].Titulo;
        var desc = this.campanha[0].Descricao;
        console.log(this.campanha[0].Imagem, 'imgs');
        this.socialSharing.share(titulo + ' #AppPartageBetim', desc, this.campanha[0].Imagem).then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
        });
    };
    CampanhaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CampanhaPage');
    };
    return CampanhaPage;
}());
CampanhaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-campanha',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/campanha/campanha.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton">\n\n      <h1>Campanha</h1>\n\n\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="alignCenter">\n\n    \n\n  <div class="InternPadrao">\n\n    <div class="titulolazer clearfix">\n\n      <h1 [innerHTML]="campanha[0].Titulo"></h1>\n\n      <div class="icons">\n\n        <button ion-button (click)="Compartilhar()">\n\n          <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n        </button>\n\n      </div>\n\n    </div>\n\n    <p [innerHTML]="campanha[0].Descricao"></p>\n\n\n\n\n\n    <img src="{{ campanha[0].Imagem }}" *ngIf="campanha[0].Imagem!=\'\'" alt="">\n\n    <button ion-button class="buttonCadastre" *ngIf="campanha[0].AceitaCadastro==true && campanha[0].NueroInscritosDisponivel!=0" (click)="IrParaCadastro()">  \n\n      Quero me cadastrar\n\n  </button>\n\n\n\n  <button ion-button class="buttonCadastre" *ngIf="campanha[0].NueroInscritosDisponivel==0" style="background: #cacaca;\n\n  border: none;\n\n  box-shadow: none;">  \n\n   Inscrições Encerradas! \n\n</button>\n\n  </div>\n\n</div>\n\n</ion-content>\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/campanha/campanha.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]])
], CampanhaPage);

//# sourceMappingURL=campanha.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CampanhaCadastroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_moment__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_angular2_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var CampanhaCadastroPage = (function () {
    function CampanhaCadastroPage(navCtrl, navParams, _alertCtrl, service, moment, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._alertCtrl = _alertCtrl;
        this.service = service;
        this.moment = moment;
        this.http = http;
        this.form = {
            Nome: '',
            Email: '',
            Telefone: '',
            Celular: '',
            CPF: '',
            RG: '',
            DatadeNascimento: '',
            nomeArquivo: '',
            Texto: '',
            DataNascimento: ''
        };
        this.Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
        this.maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        this.maskcel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
        this.masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        this.maskcpfnovo = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /[0-9]/];
        this.campanha = this.navParams.get("cadastrocampanha");
        console.log(this.campanha, 'cadastro');
    }
    CampanhaCadastroPage.prototype.ValidaForm = function () {
        if (this.campanha[0].HabilitarNome == 2 && !this.form.Nome) {
            return "Nome";
        }
        else if (this.campanha[0].HabilitarEmail == 2 && !this.form.Email) {
            return "E-mail";
        }
        else if (this.campanha[0].HabilitarDataNascimento == 2 && !this.form.DataNascimento) {
            return "Data de Nascimento";
        }
        else if (this.campanha[0].HabilitarTelefone == 2 && !this.form.Telefone) {
            return "Telefone";
        }
        else if (this.campanha[0].HabilitarCelular == 2 && !this.form.Celular) {
            return "Celular";
        }
        else if (this.campanha[0].HabilitarCPF == 2 && !this.form.CPF) {
            return "CPF";
        }
        else if (this.campanha[0].HabilitarRG == 2 && !this.form.RG) {
            return "RG";
        }
        else if (this.campanha[0].HabilitarArquivo == 2 && !this.form.Arquivo) {
            return "Arquivo";
        }
        else if (this.campanha[0].HabilitarTexto == 2 && !this.form.Texto) {
            return "Descrição";
        }
        return true;
    };
    CampanhaCadastroPage.prototype.changeFile = function (arquivo) {
        this.form.nomeArquivo = arquivo.path[0].files[0].name;
        this.form.Arquivo = arquivo.path[0].files[0];
        var fotoSize = 0;
        fotoSize = arquivo.path[0].files[0].size / 1024;
        if (fotoSize > 4096) {
            // $scope.alertNativo();
            var alert_1 = this._alertCtrl.create({
                title: '<img src="assets/icon/error.png" /> Erro',
                subTitle: "O tamanho máximo da foto é de 4M.",
                buttons: ['OK'],
                cssClass: 'alertCustomCssError'
            });
            alert_1.present();
        }
    };
    ;
    CampanhaCadastroPage.prototype.enviar = function () {
        var _this = this;
        var str = this.form.DataNascimento;
        var res = str.split('/');
        var dataTratada = res[1] + '/' + res[0] + '/' + res[2];
        console.log(dataTratada);
        // this.form.DataNascimento = moment(this.form.DataNascimento).format("MM-DD-YYYY HH:mm");
        // console.log(this.form.DataNascimento);
        var mensagem = this.ValidaForm();
        if (mensagem == true) {
            if (this.campanha[0].HabilitarArquivo != 0) {
                var formData = new FormData();
                formData.append("Arquivo", this.form.Arquivo);
                return this.http.post(this.service.config.defaultDomain + '/api/Mobile/UploadFile?ClienteCod=' + this.service.config.idShopping + '&Area=campanhas', formData).map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    if (data.Sucesso != false) {
                        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        });
                        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
                        var body = "Nome=" + _this.form.Nome + "&Email=" + _this.form.Email + "&Telefone=" + _this.form.Telefone + "&Celular=" + _this.form.Celular + "&CPF=" + _this.form.CPF + "&RG=" + _this.form.RG + "&Texto=" + _this.form.Texto + "&ClienteCod=" + _this.service.config.idShopping + "&IdCampanha=" + _this.campanha[0].Id + "&Arquivo=" + data.Arquivo + "&DataNascimento=" + dataTratada;
                        return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/ParticiparCampanha2', body, { headers: headers }).map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            console.log(data);
                            if (data.Sucess != false) {
                                _this.Sucesso = true;
                            }
                            else {
                                _this._alertCtrl.create({
                                    title: _this.service.config.name,
                                    subTitle: data.Descricao,
                                    buttons: [{ text: 'Ok' }]
                                }).present();
                            }
                        }, function (err) {
                            console.log("ERROR!: ", err);
                            //  this.loader.dismiss();ion
                            _this._alertCtrl.create({
                                title: _this.service.config.name,
                                subTitle: err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', ''),
                                buttons: [{ text: 'Ok' }]
                            }).present();
                            // this.loader.dismiss()
                        });
                    }
                    else {
                        _this._alertCtrl.create({
                            title: _this.service.config.name,
                            subTitle: data.Descricao,
                            buttons: [{ text: 'Ok' }]
                        }).present();
                    }
                });
            }
            else {
                var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/x-www-form-urlencoded'
                });
                var body = "Nome=" + this.form.Nome + "&Email=" + this.form.Email + "&Telefone=" + this.form.Telefone + "&Celular=" + this.form.Celular + "&CPF=" + this.form.CPF + "&RG=" + this.form.RG + "&Texto=" + this.form.Texto + "&ClienteCod=" + this.service.config.idShopping + "&IdCampanha=" + this.campanha[0].Id + "&Arquivo=" + '' + "&DataNascimento=" + this.form.DataNascimento;
                return this.http.post(this.service.config.defaultDomain + '/api/Mobile/ParticiparCampanha2', body, { headers: headers }).map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    console.log(data);
                    if (data.Sucess != false) {
                        _this.Sucesso = true;
                    }
                    else {
                        _this._alertCtrl.create({
                            title: _this.service.config.name,
                            subTitle: data.Descricao,
                            buttons: [{ text: 'Ok' }]
                        }).present();
                    }
                }, function (err) {
                    console.log("ERROR!: ", err);
                    //  this.loader.dismiss();ion
                    _this._alertCtrl.create({
                        title: _this.service.config.name,
                        subTitle: err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', ''),
                        buttons: [{ text: 'Ok' }]
                    }).present();
                    // this.loader.dismiss()
                });
            }
        }
        else {
            this._alertCtrl.create({
                title: this.service.config.name,
                subTitle: "O campo " + mensagem + " é obrigatório",
                buttons: [{ text: 'Ok' }]
            }).present();
        }
    };
    CampanhaCadastroPage.prototype.irParaLogin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    CampanhaCadastroPage.prototype.irParaHome = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
    };
    CampanhaCadastroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CampanhaCadastroPage');
    };
    return CampanhaCadastroPage;
}());
CampanhaCadastroPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-campanha-cadastro',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/campanha-cadastro/campanha-cadastro.html"*/'<ion-header class="BarHeader">\n\n    <ion-navbar class="Nobackground">\n\n      <ion-title class="titleButton">\n\n        <h1>cadastro</h1>\n\n      </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n\n\n\n\n<ion-content >\n\n    <div class="alignCenter"  [hidden]="Sucesso">\n\n        <div class="subHeader">\n\n            <p class="Title">{{campanha[0].Titulo}}</p>\n\n            <p class="Chamada">Preencha os dados abaixo para se cadastrar.</p>\n\n          </div>\n\n      <div class="InternPadrao">\n\n        <form class="FormCadastre">\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarNome==1 || campanha[0].HabilitarNome==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarNome==2 ? \'*Nome\': \'Nome\' }}" [(ngModel)]="form.Nome" name="Nome" type="text">\n\n            </ion-item>\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarEmail==1 || campanha[0].HabilitarEmail==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarEmail==2 ? \'*E-mail\': \'E-mail\' }}"  [(ngModel)]="form.Email" name="Email" type="text">\n\n            </ion-item>\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarDataNascimento==1 || campanha[0].HabilitarDataNascimento==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarDataNascimento==2 ? \'*Data de Nascimento\': \'Data de Nascimento\' }}" [textMask]="{mask: maskdate}" name="DataNascimento" type="text" [(ngModel)]="form.DataNascimento" >\n\n            </ion-item>\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarTelefone==1 || campanha[0].HabilitarTelefone==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarTelefone==2 ? \'*Telefone\': \'Telefone\' }}"  [(ngModel)]="form.Telefone" [textMask]="{mask: masktel}" name="Telefone" type="tel">\n\n            </ion-item>\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarCelular==1 || campanha[0].HabilitarCelular==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarCelular==2 ? \'*Celular\': \'Celular\' }}"  [(ngModel)]="form.Celular" name="Celular"  [textMask]="{mask: maskcel}" type="tel">\n\n            </ion-item>\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarCPF==1 || campanha[0].HabilitarCPF==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarCPF==2 ? \'*CPF\': \'CPF\' }}"  [(ngModel)]="form.CPF" name="CPF" [textMask]="{mask: maskcpfnovo}" type="tel">\n\n            </ion-item>\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarRG==1 || campanha[0].HabilitarRG==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarRG==2 ? \'*RG\': \'RG\' }}"  [(ngModel)]="form.RG" name="RG" type="tel">\n\n            </ion-item>              \n\n            <div *ngIf="campanha[0].HabilitarArquivo==1 || campanha[0].HabilitarArquivo==2" class="fileUpload">\n\n                <span>Escolher arquivo</span>\n\n                <input type="file" id="arquivo" name="nomeArquivo" value="OK" class="upload" (change)="changeFile($event)"/>\n\n              <p *ngIf="form.nomeArquivo">{{ form.nomeArquivo }}</p>\n\n                <p [hidden]="form.nomeArquivo">Nenhum arquivo</p> \n\n            </div>\n\n            <ion-item class="nopaddingLeft" *ngIf="campanha[0].HabilitarTexto==1 || campanha[0].HabilitarTexto==2">\n\n              <input class="input" placeholder="{{campanha[0].HabilitarTexto==2 ? \'*Descrição\': \'Descrição\' }}" name="RG" \n\n              [(ngModel)]="form.Texto" type="tel">\n\n            </ion-item>  \n\n            <button ion-button class="buttonEnviar" (click)="enviar()">Cadastrar</button>\n\n        </form>\n\n      </div>\n\n  </div>\n\n  <div class="MsgOk"  *ngIf="Sucesso">\n\n    <div class="alignCenter">\n\n      <img src="assets/imgOk.png" class="imgOk" alt="">\n\n      <p class="DescMsg">Cadastro realizado com sucesso!</p>\n\n      <button class="ButtonVoltarHome" (click)="irParaHome()">VOLTAR PARA A HOME</button>\n\n    </div>\n\n  </div>\n\n<!-- \n\n  <div class="alignCenter" *ngIf="Token==undefined">\n\n      <p class="semConexao">Você precisa estar logado para acessar essa área.</p>\n\n      <button class="buttonIrparalogin" (click)="irParaLogin()" >\n\n        Ir para Login\n\n      </button>\n\n    </div> -->\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/campanha-cadastro/campanha-cadastro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_6_angular2_moment__["MomentModule"],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
], CampanhaCadastroPage);

//# sourceMappingURL=campanha-cadastro.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CadastrePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__esquecisenha_esquecisenha__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_native_storage__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_facebook__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_facebook_facebook__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var CadastrePage = (function () {
    function CadastrePage(navCtrl, navParams, _alertCtrl, _loadingCtrl, service, http, Facebook, NativeStorage, facebookService, events, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.service = service;
        this.http = http;
        this.Facebook = Facebook;
        this.NativeStorage = NativeStorage;
        this.facebookService = facebookService;
        this.events = events;
        this.fb = fb;
        this.Formulario = false;
        this.Sucesso = false;
        this.form = {
            Nome: '',
            Email: '',
            Telefone: '',
            Sexo: '',
            EstadoCivil: '',
            Senha: '',
            ConfirmaSenha: '',
            Facebook: ''
        };
        this.maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        this.masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        this.selectOptions = {
            title: 'Selecione o sexo',
        };
        this.SelectEstadoCivil = {
            title: 'Estado Civil'
        };
        this.HomePage = __WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */];
        this.EsquecisenhaPage = __WEBPACK_IMPORTED_MODULE_5__esquecisenha_esquecisenha__["a" /* EsquecisenhaPage */];
        this.name = '';
        this.AparecerAlert = 'Apareça';
        this.profileFB = this.navParams.get("profile");
        console.log(this.profileFB);
        if (this.profileFB != undefined) {
            this.form.Nome = this.profileFB.name;
            if (this.profileFB.email != undefined) {
                this.form.Email = this.profileFB.email;
            }
            this.form.Facebook = this.profileFB.id;
        }
        this.CadastroForm = fb.group({
            'name': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(3)]],
            'email': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, this.EmailValidacao.bind(this)]],
            'Telefone': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, this.TelefoneValidacao.bind(this)]],
            'Sexo': ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            'EstadoCivil': ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required],
            'DataNascimento': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, this.DataNascimentoValidacao.bind(this)]],
            'Senha': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(5)]],
            'ConfirmarSenha': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(5)]],
        });
    }
    CadastrePage.prototype.NomeValidacao = function (control) {
        if (!control.value.match("^[a-zA-Z ,.'-]+$")) {
            return { invalidName: true };
        }
    };
    CadastrePage.prototype.EmailValidacao = function (control) {
        if (!(control.value.match('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'))) {
            return { invalidEmail: true };
        }
    };
    CadastrePage.prototype.TelefoneValidacao = function (control) {
        if (control.value !== '') {
            if (!control.value.match('\\(?\\d{2}\\)?-? *\\d{4}-? *-?\\d{4}')) {
                return { invalidPhone: true };
            }
        }
    };
    CadastrePage.prototype.DataNascimentoValidacao = function (control) {
        if (control.value !== '') {
            if (!control.value.match('[0-9]{2}\/[0-9]{2}\/[0-9]{4}$')) {
                return { invalidPhone: true };
            }
        }
    };
    CadastrePage.prototype.finalizar = function () {
        var _this = this;
        var loader = this._loadingCtrl.create({
            content: 'Aguarde...',
            dismissOnPageChange: true
        });
        loader.present();
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "Nome=" + this.form.Nome + "&Email=" + this.form.Email + "&Facebook=" + this.form.Facebook + "&Telefone=" + this.form.Telefone + "&Sexo=" + this.form.Sexo + "&EstadoCivil=" + this.form.EstadoCivil + "&Senha=" + this.form.Senha + "&Origem=" + 'Aplicativo' + "&ClienteCod=" + this.service.config.idShopping;
        return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPAdd2', body, { headers: headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.Sucess != false) {
                _this.Formulario = true,
                    loader.dismiss(),
                    _this.Sucesso = true,
                    console.log(data);
                _this.Sucesso = true;
                return false;
            }
            else {
                _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: data.Descricao,
                    buttons: [{ text: 'Ok' }]
                }).present();
            }
        }, function (err) {
            console.log("ERROR!: ", err);
            //  this.loader.dismiss();ion
            var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
            if (mensagem.indexOf('encontra cadastrado') != -1) {
                _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: "Esse e-mail já se encontra cadastrado. Informe outro e-mail ou <span (click)='EsqueciPage()'>clique aqui</span> caso tenha esquecido sua senha.",
                    buttons: [{ text: 'Ok' }]
                }).present();
                loader.dismiss();
            }
            else {
                _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: mensagem,
                    buttons: [{ text: 'Ok' }]
                }).present();
                loader.dismiss();
            }
        });
    };
    CadastrePage.prototype.createUser2 = function () {
        var _this = this;
        setTimeout(function () {
            console.log('User Login');
            var profile = JSON.parse(localStorage.getItem('profile'));
            _this.events.publish('user:Cadastre', profile, Date.now());
        }, 1000);
    };
    CadastrePage.prototype.EsqueciPage = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__esquecisenha_esquecisenha__["a" /* EsquecisenhaPage */]);
    };
    CadastrePage.prototype.PegarDadosFb = function () {
        var _this = this;
        var permissions = new Array();
        var nav = this.navCtrl;
        permissions = ['public_profile', 'user_friends', 'email'];
        this.Facebook.login(permissions).then(function (response) {
            var userId = response.authResponse.userID;
            var params = new Array();
            _this.Facebook.api("/me?fields=name,gender,email,birthday", params)
                .then(function (profile) {
                profile.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
                localStorage.setItem('profile', JSON.stringify(profile));
                this.form.Facebook = userId;
            });
            _this.createUser2();
        }, function (error) {
            console.log(error);
        });
        this.getDadosFb();
    };
    // infofb = JSON.parse(localStorage.getItem('profile'));
    CadastrePage.prototype.getDadosFb = function () {
        var _this = this;
        this.events.subscribe('user:Cadastre', function (profile) {
            _this.form.Nome = profile.name;
            if (profile.email != undefined) {
                _this.form.Email = profile.email;
            }
            _this.form.Facebook = profile.id;
        });
    };
    CadastrePage.prototype.VoltarHomeLogado = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "Email=" + this.form.Email + "&Senha=" + this.form.Senha + "&ClienteCod=" + this.service.config.idShopping;
        return this.http.post(this.service.config.defaultDomain + '/api/Mobile/Login', body, { headers: headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Logado = true;
            localStorage.setItem("cacheUsuario", JSON.stringify(data));
            localStorage.removeItem('profile');
            _this.events.publish('user:Login', data, _this.Logado);
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {
                AparecerAlert: 'Apareça'
            });
            console.log('caiuno if');
        }, function (err) {
            console.log("ERROR!: ", err);
        });
    };
    CadastrePage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.Facebook.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.InfosFb = res;
            _this.http.get(_this.service.config.defaultDomain + '/api/Mobile/VerificaFacebook?FacebookId=' + _this.InfosFb.id + '&ClienteCod=' + _this.service.config.idShopping).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                if (data.Success == true) {
                    var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/x-www-form-urlencoded'
                    });
                    var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* RequestOptions */]({ headers: headers });
                    var body = "Facebook=" + _this.InfosFb.id + "&ClienteCod=" + _this.service.config.idShopping;
                    return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        _this.Logado = true;
                        localStorage.setItem("cacheUsuario", JSON.stringify(data));
                        localStorage.removeItem('profile');
                        _this.events.publish('user:Login', data, _this.Logado);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {
                            AparecerAlert: 'Apareça'
                        });
                    });
                }
                else {
                    var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/x-www-form-urlencoded'
                    });
                    var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* RequestOptions */]({ headers: headers });
                    var body = "Nome=" + _this.InfosFb.name + "&Email=" + _this.InfosFb.email + "&Facebook=" + _this.InfosFb.id + "&Origem=" + 'Aplicativo' + "&ClienteCod=" + _this.service.config.idShopping;
                    return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPAdd2', body, { headers: headers }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        console.log(data);
                        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        });
                        var options = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["d" /* RequestOptions */]({ headers: headers });
                        var body = "Facebook=" + _this.InfosFb.id + "&ClienteCod=" + _this.service.config.idShopping;
                        return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            _this.Logado = true;
                            localStorage.setItem("cacheUsuario", JSON.stringify(data));
                            localStorage.removeItem('profile');
                            _this.events.publish('user:Login', data, _this.Logado);
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {
                                AparecerAlert: 'Apareça'
                            });
                        });
                    }, function (err) {
                        console.log("ERROR!: ", err);
                    });
                }
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    CadastrePage.prototype.LogarFb = function () {
        var _this = this;
        this.Facebook.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
            }
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    // myForm:FormGroup;
    CadastrePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CadastrePage');
    };
    CadastrePage.prototype.ionViewWillLeave = function () {
        localStorage.removeItem('profile');
    };
    return CadastrePage;
}());
CadastrePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-cadastre',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/cadastre/cadastre.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton"> \n\n      <h1>Cadastro</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n<div [hidden]="Formulario">\n\n  <div class="subHeader">\n\n    <p class="Title">Cadastro</p>\n\n    <p class="Chamada">Informe os dados abaixo para se cadastrar</p>\n\n    <button class="ButtonloginfB" ion-button (click)="LogarFb()">\n\n        <img src="assets/icofb.png" alt="">LOGIN COM FACEBOOK</button>\n\n\n\n    <form [formGroup]="CadastroForm"  class="FormCadastre">\n\n      <ion-item class="nopaddingLeft">\n\n        <ion-input class="input" placeholder="Nome" formControlName="name" [(ngModel)]="form.Nome" name="Nome" type="text" ></ion-input>\n\n      </ion-item>\n\n     <ion-item class="nopaddingLeft">\n\n        <ion-input class="input" placeholder="E-mail" formControlName="email" [(ngModel)]="form.Email" name="Email" type="text" ></ion-input>\n\n      </ion-item>\n\n      <ion-item class="nopaddingLeft">\n\n        <input [textMask]="{mask: masktel}" class="input" placeholder="Telefone"  name="Telefone" [(ngModel)]="form.Telefone" formControlName="Telefone" type="tel" >\n\n      </ion-item>\n\n      <ion-item class="nopaddingLeft">\n\n       <ion-select class="input select" [(ngModel)]="form.Sexo" name="Sexo" multiple="false" cancelText="Cancelar" okText="Ok" [selectOptions]="selectOptions" placeholder="Sexo" formControlName="Sexo" >\n\n        <ion-option value="1">Feminino</ion-option>\n\n        <ion-option value="2" >Masculino</ion-option>\n\n      </ion-select>\n\n      </ion-item>\n\n     <ion-item class="nopaddingLeft">\n\n        <ion-select class="input select" [(ngModel)]="form.EstadoCivil" name="Sexo" multiple="false" cancelText="Cancelar" okText="Ok" [selectOptions]="SelectEstadoCivil" formControlName="EstadoCivil" placeholder="Estado Civil">\n\n            <ion-option value="Solteiro">Solteiro</ion-option>\n\n            <ion-option value="Casado" >Casado</ion-option>\n\n            <ion-option value="Separado" >Separado</ion-option>\n\n            <ion-option value="Viúvo" >Viúvo</ion-option>\n\n          </ion-select>\n\n       </ion-item>\n\n     <ion-item class="nopaddingLeft">\n\n        <input class="input" placeholder="Data de Nascimento" [textMask]="{mask: maskdate}" name="DataNascimento" type="tel" formControlName="DataNascimento" >\n\n      </ion-item>\n\n       <ion-item class="nopaddingLeft">\n\n        <ion-input class="input" placeholder="Senha" [(ngModel)]="form.Senha" formControlName="Senha" name="Senha" type="password" ></ion-input>\n\n      </ion-item>\n\n      <ion-item class="nopaddingLeft">\n\n        <ion-input class="input"  [(ngModel)]="form.ConfirmaSenha"  placeholder="Confirmar Senha" type="password" name="confirmasenha" formControlName="ConfirmarSenha" ></ion-input>\n\n      </ion-item> \n\n      <button ion-button class="buttonEnviar" (click)="finalizar()">Cadastrar</button>\n\n    </form>\n\n  </div>\n\n</div>\n\n\n\n<div class="MsgOk" *ngIf="Sucesso">\n\n  <img src="assets/imgOk.png" class="imgOk" alt="">\n\n  <p class="DescMsg">Cadastro realizado com sucesso!</p>\n\n  <button class="ButtonVoltarHome" ion-button (click)="VoltarHomeLogado()">VOLTAR PARA A HOME</button>\n\n</div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/cadastre/cadastre.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_9__providers_facebook_facebook__["a" /* FacebookService */]],
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_7__ionic_native_facebook__["a" /* Facebook */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_native_storage__["a" /* NativeStorage */],
        __WEBPACK_IMPORTED_MODULE_9__providers_facebook_facebook__["a" /* FacebookService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
], CadastrePage);

//# sourceMappingURL=cadastre.js.map

/***/ }),

/***/ 143:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutorialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TutorialPage = (function () {
    function TutorialPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.Header = false;
        this.Footer = true;
        this.HomePage = __WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */];
    }
    TutorialPage.prototype.ionViewWillEnter = function () {
        document.getElementById("ion-header").style.display = "none";
    };
    TutorialPage.prototype.ionViewWillLeave = function () {
        document.getElementById("ion-header").style.display = "block";
    };
    TutorialPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TutorialPage');
    };
    TutorialPage.prototype.irparaHome = function () {
        localStorage.setItem('Primeiravez', 'true');
    };
    return TutorialPage;
}());
TutorialPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-tutorial',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/tutorial/tutorial.html"*/'<ion-content>\n\n<img src="assets/logoTutorial2.png" class="logoTutorial" alt="">\n\n<ion-slides class="contentprehome" pager>\n\n  <ion-slide class="slider-slide">\n\n      <h1>Olha quem<br>está de cara nova</h1><img src="assets/iconConteudo.png" >\n\n      <h1>O APP do<br>TopShopping</h1>\n\n      <p>Vem conferir!\n\n          </p>\n\n    </ion-slide>\n\n  <!-- <ion-slide class="slider-slide">\n\n    <h1>favoritos</h1><img src="assets/tutorial/icon-favorito.png" >\n\n    <p>Gostou do que viu e leu? Adicione aos favoritos e acesse rapidamente depois.</p>\n\n  </ion-slide>\n\n  <ion-slide class="slider-slide">\n\n    <h1>localização</h1><img src="assets/tutorial/icon-localizacao.png" >\n\n    <p>Quer saber informações sobre horário de funcionamento e como chegar? Aqui você terá tudo isso e muito mais.</p>\n\n  </ion-slide> -->\n\n  \n\n</ion-slides>\n\n<h2 class="buttonPrehome" [navPush]="HomePage" (click)="irparaHome()">Entrar</h2>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/tutorial/tutorial.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], TutorialPage);

//# sourceMappingURL=tutorial.js.map

/***/ }),

/***/ 144:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FeedPage = (function () {
    function FeedPage(navCtrl, navParams, http, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.Pagina = 0;
        this.Feed = [];
        this.searching = true;
        this.DataFormatada = [];
    }
    FeedPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.SocialFeed();
            refresher.complete();
        }, 2000);
    };
    FeedPage.prototype.SocialFeed = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        this.http.get(this.service.config.defaultDomain + "/api/mobile/RedesSociaisList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Feed = data.lista,
                _this.Dominio = _this.service.config.defaultDomain;
            _this.searching = false;
            if (_this.Feed.length == 0) {
                _this.NaoDisponivel = true;
            }
            else {
                _this.NaoDisponivel = false;
            }
            // for (var i = 0; i < this.Feed.length; i++) {
            //  this.DataFormatada.push(
            //   this.Feed[i].dataPublicacao.replace(':','h'))
            //   console.log(this.DataFormatada, 'primeirofor')
            // }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    FeedPage.prototype.CountPaginacao = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/RedesSociaisList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.lista;
            if (_this.Data != '') {
                for (var i = 0; i < 5; i++) {
                    _this.Feed.push(_this.Data[i]);
                }
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    FeedPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            _this.CountPaginacao();
            // }
            console.log('Async operation has ended');
            infiniteScroll.complete();
        }, 500);
    };
    FeedPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FeedPage');
        this.SocialFeed();
    };
    return FeedPage;
}());
FeedPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-feed',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/feed/feed.html"*/'<ion-content class="BoxFiquePorDentro">\n\n\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n      <ion-refresher-content></ion-refresher-content>\n\n    </ion-refresher>\n\n    <div class="alignCenter">\n\n      <div *ngIf="searching" class="spinner-container">\n\n        <ion-spinner></ion-spinner>\n\n        <p>Carregando dados...</p>\n\n      </div>\n\n\n\n    <div class="semConexao" *ngIf="NaoDisponivel"> Não há posts disponíveis. </div>\n\n    <div class="repeatfeedsocial" *ngFor="let f of Feed">\n\n      <div class="headsocial">\n\n        <div class="iconsocial">\n\n          <img src="assets/Fb_socialfeed.png" *ngIf="f.TipoRedeSocial == \'Facebook\'">\n\n          <img src="assets/In_socialfeed.png" *ngIf="f.TipoRedeSocial == \'Instagram\'">\n\n          <img src="assets/Tw_socialfeed.png" *ngIf="f.TipoRedeSocial != \'Facebook\' && f.TipoRedeSocial != \'Instagram\'">\n\n        </div>\n\n        <p>{{ f.dataPublicacao }}</p>\n\n      </div>\n\n      <img [hidden]="f.Picture==\'\' || f.Picture==null" src="{{f.Picture}}">\n\n      <p>{{ f.Mensagem }}</p>\n\n    </div>\n\n    <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n      <ion-infinite-scroll-content loadingSpinner="bubbles"\n\n      loadingText="Carregando mais dados...">></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n  </div>\n\n    \n\n  </ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/feed/feed.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */]])
], FeedPage);

//# sourceMappingURL=feed.js.map

/***/ }),

/***/ 145:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiquepordentroPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__fiquepordentrointerna_fiquepordentrointerna__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FiquepordentroPage = (function () {
    function FiquepordentroPage(navCtrl, navParams, http, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.storage = storage;
        this.Dominio = this.service.config.defaultDomain;
        this.Pagina = 0;
        this.rootNavCtrl = navParams.get('rootNavCtrl');
    }
    FiquepordentroPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.Pagina = 0;
            _this.FiquePorDentro();
            refresher.complete();
        }, 2000);
    };
    FiquepordentroPage.prototype.FiquePorDentro = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5", { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Novidades = data.lista,
                _this.Dominio = _this.service.config.defaultDomain,
                _this.storage.set('Novidades', _this.Novidades);
            if (_this.Novidades.length == 0) {
                _this.NaoDisponivel = true;
            }
            else {
                _this.NaoDisponivel = false;
            }
            // console.log(this.Novidades, 'Novidade')
        }, function (err) {
            console.log('deuruim', err);
            _this.storage.get('Novidades').then(function (val) {
                console.log('Your age is', val);
                _this.Novidades = val;
            });
        });
    };
    FiquepordentroPage.prototype.CountPaginacaoFiquePorDentro = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.lista;
            if (_this.Data != undefined) {
                for (var i = 0; i < 6; i++) {
                    _this.Novidades.push(_this.Data[i]);
                }
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    FiquepordentroPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        // console.log('Begin async operation');
        setTimeout(function () {
            if (_this.Novidades != undefined) {
                // console.log('Async operation has ended');
                _this.CountPaginacaoFiquePorDentro();
                infiniteScroll.complete();
            }
            else {
                ('travo');
            }
        }, 500);
    };
    FiquepordentroPage.prototype.IrParaInterna = function ($event, nov) {
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_4__fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */], {
            novidade: nov
        });
    };
    FiquepordentroPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FiquepordentroPage');
        this.FiquePorDentro();
    };
    return FiquepordentroPage;
}());
FiquepordentroPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-fiquepordentro',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/fiquepordentro/fiquepordentro.html"*/'<ion-content class="BoxFiquePorDentro">\n\n\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n      <ion-refresher-content></ion-refresher-content>\n\n    </ion-refresher>\n\n  <div class="alignCenter">\n\n    <div class="HomeFique" *ngFor="let nov of Novidades" (click)="IrParaInterna($event, nov)">\n\n      <div class="areaflexHomeFique" *ngIf="nov!=undefined">\n\n        <div class="Areaimg">\n\n          <img src="{{Dominio}}/{{nov.midia.arquivos.lang_1.arquivo.url}}">\n\n        </div>\n\n        <div class="Desc">\n\n          <p class="Tit" [innerHTML]="nov.nome.str.lang_1 | truncate : 50 : \'...\'"></p>\n\n          <p class="txt">{{ nov.chamada.str.lang_1 | truncate : 50 : "..." }}</p>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Carregando mais dados...">></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/fiquepordentro/fiquepordentro.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], FiquepordentroPage);

//# sourceMappingURL=fiquepordentro.js.map

/***/ }),

/***/ 146:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EnquetesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__cupons_interna_cupons_interna__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EnquetesPage = (function () {
    function EnquetesPage(navCtrl, navParams, http, _alertCtrl, _loadingCtrl, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.service = service;
        this.loader = this._loadingCtrl.create({
            content: 'Aguarde...'
        });
        this.Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
        this.primeiraparte = false;
        this.Sucesso = false;
        this.irparacupom = function (localNavCtrl, $event, idCupom) {
            var _this = this;
            if (localNavCtrl === void 0) { localNavCtrl = true; }
            this.idCupom = $event;
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
            headers.append('Authorization', this.Token.Token);
            this.http.get(this.service.config.defaultDomain + "/api/Mobile/GetCupomPromocional/?CodCupom=" + this.idCupom, { "headers": headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                _this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_6__cupons_interna_cupons_interna__["a" /* CuponsInternaPage */], {
                    c: data.Cupons[0]
                });
            }, function (err) {
                console.log('deuruim enquete', err);
            });
        };
        this.rootNavCtrl = navParams.get('rootNavCtrl');
    }
    EnquetesPage.prototype.EnqueteUsuario = function () {
        var _this = this;
        this.Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Authorization', this.Token.Token);
        this.http.get(this.service.config.defaultDomain + "/api/Mobile/GetEnqueteUsuario", { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.enquete = data.Enquetes,
                _this.verEnquete = data.Sucess;
            _this.Dominio = _this.service.config.defaultDomain;
            console.log(_this.verEnquete);
            localStorage.setItem("cachePesquisa", JSON.stringify(data));
            localStorage.setItem("enqueteRespondida", JSON.stringify(data));
            _this.enquetes = JSON.parse(localStorage.getItem("cachePesquisa"));
            //listagem de respostas
            _this.respostas = [];
            if (_this.enquetes.Enquetes != null) {
                for (var i = 0; i < _this.enquetes.Enquetes[0].EnqueteOpcoes.length; i++) {
                    _this.respostas[i] = {
                        Id: _this.enquetes.Enquetes[0].EnqueteOpcoes[i]["Id"],
                        Resposta: _this.enquetes.Enquetes[0].EnqueteOpcoes[i]["Resposta"],
                        Ordem: i,
                        model: false,
                        checked: false
                    };
                }
            }
        }, function (err) {
            console.log('deuruim enquete', err);
        });
    };
    EnquetesPage.prototype.selecionaCheck = function (checkbox) {
        for (var i = 0; i < this.respostas.length; i++) {
            if (checkbox.Ordem != i) {
                this.respostas[i].model = false;
                this.respostas[i].checked = false;
            }
            else {
                this.respostas[i].checked = true;
            }
        }
    };
    EnquetesPage.prototype.responder = function () {
        for (var i = 0; i < this.respostas.length; i++) {
            if (this.respostas[i].model == true && this.respostas[i].checked == true) {
                this.respostaSelecionada = this.respostas[i];
            }
        }
        // console.log(this.respostaSelecionada);
        if (this.respostaSelecionada != undefined) {
            this.finalizar();
        }
        else {
            this._alertCtrl.create({
                title: this.service.config.name,
                subTitle: 'Escolha uma resposta',
                buttons: [{ text: 'Ok' }]
            }).present();
        }
    };
    ;
    EnquetesPage.prototype.finalizar = function () {
        var _this = this;
        this.loader.present();
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json',
            'Authorization': this.Token.Token
        });
        var body = "ClienteCod=" + this.Token.codCliente + "&IdCadastroAPP=" + this.Token.codUsuario + "&IdEnqueteOpcao=" + this.respostaSelecionada.Id;
        return this.http.post(this.service.config.defaultDomain + '/api/Mobile/ResponderEnquete?' + body, body, { headers: headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.Sucess != false) {
                // console.log(data)
                _this.CupomSouN = data;
                _this.primeiraparte = false;
                _this.Sucesso = true;
            }
            else
                _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: data.Descricao,
                    buttons: [{ text: 'Ok' }]
                }).present();
            _this.loader.dismiss();
            return false;
        }, function (err) {
            console.log("ERROR!: ", err);
            //  this.loader.dismiss();ion
            _this._alertCtrl.create({
                title: _this.service.config.name,
                subTitle: 'Erro',
                buttons: [{ text: 'Ok' }]
            }).present();
            _this.loader.dismiss();
        });
    };
    EnquetesPage.prototype.irParaLogin = function (localNavCtrl) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    EnquetesPage.prototype.irparahome = function (localNavCtrl) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */]);
    };
    EnquetesPage.prototype.ionViewDidLoad = function () {
        if (this.Token != undefined) {
            this.EnqueteUsuario();
        }
    };
    return EnquetesPage;
}());
EnquetesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-enquetes',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/enquetes/enquetes.html"*/'<ion-content class="BoxFiquePorDentro">\n\n  <div class="alignCenter"  *ngFor="let f of enquete">\n\n    <div class="containerFaleConosco" [hidden]="Sucesso==true || primeiraparte">\n\n      <div class="pinicial">\n\n        <p *ngIf="f.IdCupom == 0">Participe de nossa enquete. É rápido e fácil.</p>\n\n        <p *ngIf="f.IdCupom != 0">Responda essa enquete para liberar um cupom de desconto. </p>\n\n      </div>\n\n\n\n      <div class="info-shopping" [innerHTML]="f.Pergunta">\n\n      </div>\n\n\n\n      <ion-item *ngFor="let resposta of respostas">\n\n        <ion-label>{{ resposta.Resposta }}</ion-label>\n\n        <ion-checkbox [(ngModel)]="resposta.model" (click)="selecionaCheck(resposta)"></ion-checkbox>\n\n      </ion-item>\n\n\n\n      <button ion-item class="btnEnviar" (click)="responder()">Responder</button>\n\n    </div>\n\n  </div>\n\n  <div class="semConexao" *ngIf="verEnquete==false" style="margin-top: 28%;">\n\n    <p>Por enquanto não existem novas enquetes. Aguarde. :)</p>\n\n    <button  class="butttonnovo" (click)="irparahome()">\n\n      Voltar para home\n\n    </button>\n\n  </div>\n\n  <div class="container" *ngIf="Sucesso==true">\n\n    <div class="msgenviada">\n\n      <img src="assets/imgOk.png" alt="" class="ok">\n\n      <p>Enquete respondida com sucesso!</p>\n\n      <div *ngFor="let f of enquete">\n\n        <button ion-button *ngIf="f.IdCupom != 0" class="butttonnovo" (click)="irparacupom($event, f.IdCupom)">\n\n          Cupom liberado, clique e acesse\n\n        </button>\n\n      <button ion-button *ngIf="f.IdCupom == 0" class="butttonnovo2" (click)="irparahome()">\n\n        Voltar para home\n\n      </button>\n\n      </div>\n\n    </div>\n\n  </div>\n\n\n\n  <div class="alignCenter" *ngIf="Token==undefined">\n\n      <p class="semConexao">Você precisa estar logado para acessar essa área.</p>\n\n      <button class="buttonIrparalogin" (click)="irParaLogin()" >\n\n          Ir para Login\n\n        </button>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/enquetes/enquetes.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */]])
], EnquetesPage);

//# sourceMappingURL=enquetes.js.map

/***/ }),

/***/ 147:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VitrinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__vitrine_interna_vitrine_interna__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var VitrinePage = (function () {
    function VitrinePage(navCtrl, navParams, http, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.filter = false;
        this.Pagina = 0;
        this.allStores = [];
        this.searching = true;
        this.rootNavCtrl = navParams.get('rootNavCtrl');
    }
    VitrinePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.Vitrine();
            refresher.complete();
        }, 2000);
    };
    VitrinePage.prototype.Vitrine = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.defaultDomain + "/api/mobile/VitrineList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6').map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Dominio = _this.service.config.defaultDomain;
            // console.log(this.vitrine)
            _this.allStores = data.lista;
            _this.vitrine = data.lista;
            _this.searching = false;
            if (_this.vitrine.length == 0) {
                _this.vitrineProduto = true;
            }
        }, function (err) {
            console.log('deuruim vitrine', err);
        });
    };
    VitrinePage.prototype.CountPaginacao = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/VitrineList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.lista;
            if (_this.Data != undefined) {
                for (var i = 0; i < 6; i++) {
                    _this.vitrine.push(_this.Data[i]);
                }
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    VitrinePage.prototype.IrParaInterna = function (localNavCtrl, event, v) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_4__vitrine_interna_vitrine_interna__["a" /* VitrineInternaPage */], {
            v: event
        });
    };
    VitrinePage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            if (_this.vitrine != undefined) {
                _this.CountPaginacao();
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }
            else {
                console.log('travo');
            }
        }, 500);
    };
    VitrinePage.prototype.AbrirFiltro = function () {
        if (!this.filter) {
            this.filter = true;
        }
        else {
            this.filter = false;
        }
    };
    VitrinePage.prototype.Filtrar = function () {
        var auxStores = [];
        console.log('filtro', this.BuscaVitrine);
        if (this.BuscaVitrine != "") {
            for (var i = 0; i < this.allStores.length; i++) {
                if (this.allStores[i].nome.str.lang_1.toUpperCase().indexOf(this.BuscaVitrine.toUpperCase()) > -1) {
                    auxStores.push(this.allStores[i]);
                }
                else {
                    this.vitrine = auxStores;
                    this.filter = false;
                }
            }
        }
        else {
            auxStores = this.allStores;
        }
        this.vitrine = auxStores;
        this.filter = false;
        if (this.vitrine.length == 0) {
            this.nadaEncontrado = true;
        }
        else {
            this.nadaEncontrado = false;
        }
    };
    VitrinePage.prototype.ionViewDidLoad = function () {
        this.Vitrine();
    };
    return VitrinePage;
}());
VitrinePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-vitrine',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/vitrine/vitrine.html"*/'<ion-content class="BoxFiquePorDentro" [class.filter]="filter">\n\n  <style>\n\n    .barVitrine{\n\n      background: #d9d9d9!important\n\n    }\n\n    .BoxFiquePorDentro{\n\n      background: #d9d9d9!important      \n\n    }\n\n    .HomeFique{\n\n      background: #ededed!important;\n\n    }\n\n    .alignCenter{\n\n      margin-top: 0%;\n\n    }\n\n    </style>\n\n  <div class="barVitrine bar-subheader noPadding">\n\n    <h2 class="title ng-binding" style="pointer-events: none;    pointer-events: none;\n\n    font-family: \'Rubik\';\n\n    color: #00417f;\n\n    font-size: 19px;\n\n    font-weight: 500;\n\n    margin-bottom: 5px;">Vitrine Virtual</h2>\n\n    <div class="filtroInterno" (click)="AbrirFiltro()"></div>\n\n  </div>\n\n  <div class="alignCenter">\n\n      <div class="setafiltro" *ngIf="filter">\n\n        <img src="assets/icon/set_filtro.png" alt="">\n\n      </div>\n\n\n\n  <div class="form" *ngIf="filter" >\n\n    <button class="btn-filter" (click)="Filtrar($event)"></button>\n\n    <input [(ngModel)]="BuscaVitrine"  placeholder="Digite o nome do produto..." type="text">\n\n  </div>\n\n\n\n</div>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n\n\n  <div class="alignCenter">\n\n  <div class="container-stores" [hidden]="filter" style="padding-top: 2%;">\n\n    <div *ngIf="searching" class="spinner-container">\n\n      <ion-spinner></ion-spinner>\n\n      <p>Carregando dados...</p>\n\n    </div>\n\n    <div class="vitrine-virtual">\n\n      <div class="HomeFique" *ngFor="let v of vitrine ">\n\n        <div *ngIf="v!=undefined" class="areaflexHomeFique" (click)="IrParaInterna($event, v)">\n\n          <div class="Areaimg">\n\n            <img *ngIf="v.midia.arquivos.lang_1.arquivo.url!=\'\'" src="{{ Dominio }}/{{v.midia.arquivos.lang_1.arquivo.url}}" style="border-radius: 5px;"/>\n\n          </div>\n\n          <div class="Desc">\n\n            <p class="Tit ">{{v.nome.str.lang_1}}</p>\n\n            <p class="txt" *ngIf="v.associacao!=0">{{v.associacao[0].nome}}</p>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n    <div class="semConexao" *ngIf="nadaEncontrado">\n\n      <p>Nenhum item corresponde à sua procura.</p>\n\n    </div>\n\n      <div class="semConexao" *ngIf="vitrineProduto">\n\n          <img src="assets/iconSmile.png" class="iconeimg" alt=""><br>\n\n          \n\n        Não há produtos disponíveis. </div>\n\n  </div>\n\n</div>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Carregando mais dados...">></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/vitrine/vitrine.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */]])
], VitrinePage);

//# sourceMappingURL=vitrine.js.map

/***/ }),

/***/ 148:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CuponsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cupons_interna_cupons_interna__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CuponsPage = (function () {
    function CuponsPage(navCtrl, navParams, http, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.Pagina = 0;
        this.Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
        this.LoginPage = __WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */];
        this.rootNavCtrl = navParams.get('rootNavCtrl');
    }
    CuponsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.Cupons();
            refresher.complete();
        }, 2000);
    };
    CuponsPage.prototype.Cupons = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Authorization', this.Token.Token);
        this.http.get(this.service.config.defaultDomain + "/api/Mobile/CuponsPromocionaisList2?Pagina=" + this.Pagina + "&Items=5", { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.ListaCupons = data.Cupons,
                _this.Dominio = _this.service.config.defaultDomain;
        }, function (err) {
            console.log('deuruim', err);
        });
    };
    CuponsPage.prototype.CountPaginacaoCupons = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Authorization', this.Token.Token);
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/Mobile/CuponsPromocionaisList2?Pagina=" + this.Pagina + "&Items=5", { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.Cupons;
            if (_this.Data != undefined) {
                for (var i = 0; i < 5; i++) {
                    _this.ListaCupons.push(_this.Data[i]);
                }
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    CuponsPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            if (_this.ListaCupons != undefined) {
                _this.CountPaginacaoCupons();
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }
            else {
                console.log('travo');
            }
        }, 500);
    };
    CuponsPage.prototype.IrParaInterna = function (localNavCtrl, event, c) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_5__cupons_interna_cupons_interna__["a" /* CuponsInternaPage */], {
            c: event
        });
    };
    CuponsPage.prototype.irParaLogin = function (localNavCtrl, event, s) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_4__login_login__["a" /* LoginPage */]);
    };
    CuponsPage.prototype.ionViewDidLoad = function () {
        if (this.Token != undefined) {
            this.Cupons();
            console.log('VeioTOken');
        }
        console.log('Veio no didload');
    };
    return CuponsPage;
}());
CuponsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-cupons',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/cupons/cupons.html"*/'<ion-content class="BoxFiquePorDentro">\n\n\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n  <div class="alignCenter">\n\n    <div *ngFor="let c of ListaCupons">\n\n      <div class="HomeFique" *ngIf="c!=undefined" (click)="IrParaInterna($event, c)">\n\n        <div class="areaflexHomeFique">\n\n          <div class="Areaimg">\n\n            <img src="{{c.Imagem}}">\n\n          </div>\n\n          <div class="Desc">\n\n            <p class="Tit" [innerHTML]="c.Titulo"></p>\n\n            <p class="txt">{{ c.Descricao | truncate : 50 : "..." }}</p>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n\n\n  <div class="alignCenter" *ngIf="ListaCupons==undefined">\n\n    <p class="semConexao">Você precisa estar logado para acessar essa área.</p>\n\n    <button class="buttonIrparalogin" (click)="irParaLogin()">\n\n      Ir para Login\n\n    </button>\n\n  </div>\n\n  <div class="semConexao" *ngIf="ListaCupons!=undefined && (ListaCupons)==\'\'||ListaCupons==\'\'">\n\n\n\n    <img src="assets/iconSmile.png" class="iconeimg" alt="">\n\n    <p class="AindaNaoFavorito">Aguarde! </p>\n\n    <p>Não existem cupons disponíveis no momento.</p>\n\n  </div>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Carregando mais dados...">></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/cupons/cupons.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */]])
], CuponsPage);

//# sourceMappingURL=cupons.js.map

/***/ }),

/***/ 149:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OshoppingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OshoppingPage = (function () {
    function OshoppingPage(navCtrl, navParams, http, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
    }
    OshoppingPage.prototype.InfoShopping = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json?nocache=" + nocache).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            var paginas = data.lista;
            var filtrarPage = [];
            for (var i = 0; i < paginas.length; i++) {
                if (paginas[i].texto == 'TopShopping') {
                    filtrarPage.push(paginas[i]);
                    //  this.Segmentos=segmentolojas;
                    console.log(filtrarPage, 'entro fi');
                }
                _this.item = filtrarPage;
            }
            _this.Dominio = _this.service.config.defaultDomain;
            console.log(_this.item[0]);
            console.log(_this.Dominio);
        }, function (err) {
            console.log('deuruim lojas', err);
        });
    };
    OshoppingPage.prototype.ionViewDidLoad = function () {
        this.InfoShopping();
        console.log('ionViewDidLoad OshoppingPage');
    };
    return OshoppingPage;
}());
OshoppingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-oshopping',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/oshopping/oshopping.html"*/'<ion-content class="BoxFiquePorDentro" >\n\n  \n\n  <div *ngIf="item">\n\n    <div class="image-shopping">\n\n      <img src="{{Dominio}}/{{item[0].midia.arquivos.lang_1.imagem.url}}" />\n\n    </div>\n\n    <div class="container">\n\n      <div class="info-shopping">\n\n        <div class="nome" [innerHTML]="item[0].titulo.str.lang_1"></div>\n\n        <div class="content-shopping clearfix">\n\n          <div [innerHTML]="item[0].link_texto" class="texto-shopping"></div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/oshopping/oshopping.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */]])
], OshoppingPage);

//# sourceMappingURL=oshopping.js.map

/***/ }),

/***/ 150:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaservicosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__servicosinterna_servicosinterna__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ListaservicosPage = (function () {
    function ListaservicosPage(navCtrl, navParams, http, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.rootNavCtrl = navParams.get('rootNavCtrl');
    }
    ListaservicosPage.prototype.ListServicos = function () {
        var _this = this;
        this.http.get(this.service.config.jsonDomain + "jsonservicos.json").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.servicos = data.lista;
            _this.Dominio = _this.service.config.defaultDomain;
            console.log(_this.servicos);
        }, function (err) {
            console.log('deuruim listaservicos', err);
        });
    };
    ListaservicosPage.prototype.IrParaInterna = function (localNavCtrl, event, s) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_4__servicosinterna_servicosinterna__["a" /* ServicosinternaPage */], {
            s: event
        });
    };
    ListaservicosPage.prototype.ionViewDidLoad = function () {
        this.ListServicos();
        // console.log('ionViewDidLoad Vit  rinePage');
    };
    return ListaservicosPage;
}());
ListaservicosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-listaservicos',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/listaservicos/listaservicos.html"*/'<ion-content class="BoxFiquePorDentro">\n\n  <div class="alignCenter">\n\n    <div class="subHeader">\n\n      <p class="Title">Nossos Serviços</p>\n\n      <p class="Chamada">Confira os principais serviços que oferecemos</p>\n\n    </div>\n\n    <div class="container-stores" ng-hide="filter" style="padding-top: 2%;">\n\n        <!-- <div class="servicoslist"> -->\n\n        <div class="HomeFique" *ngFor="let s of servicos" (click)="IrParaInterna($event, s)">\n\n          <div class="areaflexHomeFique">\n\n          <div class="Areaimg">\n\n            <img src="{{Dominio}}/{{s.midia.arquivos.lang_1.arquivo.url}}" />\n\n          </div>\n\n          <div class="Desc">\n\n            <p class="Tit ">{{s.nome.str.lang_1}}</p>\n\n            <p class="txt" *ngIf="s.lucs!=0">{{ s.lucs[0].piso.nome.str.lang_1}}</p>\n\n          </div>\n\n        </div>\n\n        </div>\n\n        <!-- </div> -->\n\n      </div>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/listaservicos/listaservicos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */]])
], ListaservicosPage);

//# sourceMappingURL=listaservicos.js.map

/***/ }),

/***/ 151:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EstacionamentoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var EstacionamentoPage = (function () {
    function EstacionamentoPage(navCtrl, navParams, http, service, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.events = events;
        this.Estacionamento = [];
        this.setores = [];
        this.vagas = [];
        this.sliderOptions = {
            pager: true,
            autoHeight: true,
        };
    }
    EstacionamentoPage.prototype.GetParking = function () {
        var _this = this;
        this.http.get(this.service.config.jsonDomain + "jsonEstacionamento.json").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Estacionamento = data.lista,
                // console.log(this.Estacionamento)
                _this.setores = [];
            _this.setores.push(_this.Estacionamento[0].setores);
            _this.vagas = [];
            _this.vagas.push(_this.Estacionamento[0].setores[0].vagas);
            _this.AndarSelecionado = _this.setores;
            _this.SetorSelecionado = _this.SetorSelecionado;
            _this.listsetores = _this.setores;
            // console.log(this.listsetores, 'setores')
            _this.listvagas = _this.vagas[0];
            console.log(_this.listvagas, 'vagas');
            var employee = [];
            employee.push(_this.Estacionamento[0].local);
            _this.AndarSelecionado = employee[0];
            // console.log( this.AndarSelecionado)
            var employee2 = [];
            employee2.push(_this.listsetores[0][0].setor);
            _this.SetorSelecionado = employee2[0];
            var employee3 = [];
            employee3.push(_this.listsetores[0][0].vagas);
            _this.Vagaselecionada = employee3[0][0];
            console.log(_this.Vagaselecionada);
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    EstacionamentoPage.prototype.slideChanged = function () {
        var currentIndex1 = this.slides1.getActiveIndex();
        //     console.log('Selected', selectedValue);
        this.setores = [];
        this.setores.push(this.Estacionamento[currentIndex1].setores);
        this.AndarSelecionado = currentIndex1;
        this.listsetores = this.setores;
        console.log(this.listsetores, 'setores');
        // this.listvagas=this.vagas;
        // console.log(this.listvagas,'vagas')
        var employee = [];
        employee.push(this.Estacionamento[currentIndex1].local);
        this.AndarSelecionado = employee[0];
    };
    EstacionamentoPage.prototype.slideChanged2 = function () {
        var currentIndex2 = this.slides2.getActiveIndex();
        this.Vagaselecionada = currentIndex2;
        var employee = [];
        employee.push(this.listsetores[0][currentIndex2]);
        this.SetorSelecionado = employee[0].setor;
        this.listvagas = employee[0].vagas;
        localStorage.setItem("setorrr", JSON.stringify(this.SetorSelecionado));
    };
    EstacionamentoPage.prototype.slideChanged3 = function () {
        var currentIndex2 = this.slides3.getActiveIndex();
        this.vagas = [];
        this.Vagaselecionada2 = currentIndex2;
        var employee = [];
        employee.push(this.listvagas[currentIndex2]);
        this.Vagaselecionada = employee[0];
        localStorage.setItem("vaga", JSON.stringify(this.Vagaselecionada));
        console.log(this.Vagaselecionada);
    };
    EstacionamentoPage.prototype.PrevSlide = function () {
        this.slides1.slidePrev();
        // console.log(proximo, 'proximo')
    };
    EstacionamentoPage.prototype.ProximoSlide = function () {
        this.slides1.slideNext();
        // console.log(proximo, 'proximo')
    };
    EstacionamentoPage.prototype.PrevSlide2 = function () {
        this.slides2.slidePrev();
        // console.log(proximo, 'proximo')
    };
    EstacionamentoPage.prototype.ProximoSlide2 = function () {
        this.slides2.slideNext();
        // console.log(proximo, 'proximo')
    };
    EstacionamentoPage.prototype.PrevSlide3 = function () {
        this.slides3.slidePrev();
        // console.log(proximo, 'proximo')
    };
    EstacionamentoPage.prototype.ProximoSlide3 = function () {
        this.slides3.slideNext();
        // console.log(proximo, 'proximo')
    };
    EstacionamentoPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.ArmazenamentoEstacionamento = JSON.parse(window.localStorage.getItem('vaga-selecionada'));
        if (this.ArmazenamentoEstacionamento != undefined) {
            this.Salvo = true;
        }
        this.events.subscribe('user:estacionamento', function (AreaEstacionamento) {
            _this.ArmazenamentoEstacionamento = AreaEstacionamento;
            console.log(_this.ArmazenamentoEstacionamento);
        });
    };
    EstacionamentoPage.prototype.ionViewDidLoad = function () {
        this.GetParking();
    };
    EstacionamentoPage.prototype.SalvarVaga = function () {
        var AreaEstacionamento = {
            andar: this.AndarSelecionado,
            setor: this.SetorSelecionado,
            vaga: this.Vagaselecionada
        };
        this.Salvo = true;
        this.events.publish('user:estacionamento', AreaEstacionamento);
        localStorage.setItem("vaga-selecionada", JSON.stringify(AreaEstacionamento));
    };
    EstacionamentoPage.prototype.limparVaga = function () {
        localStorage.setItem("vaga-selecionada", JSON.stringify(null));
        this.Salvo = false;
    };
    return EstacionamentoPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('Slides1'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], EstacionamentoPage.prototype, "slides1", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('Slides2'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], EstacionamentoPage.prototype, "slides2", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('Slides3'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], EstacionamentoPage.prototype, "slides3", void 0);
EstacionamentoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-estacionamento',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/estacionamento/estacionamento.html"*/'<ion-content class="BoxFiquePorDentro">\n\n  <!-- <div class="BoxGuardarVaga" [hidden]="Salvo">\n\n    <div class="row">\n\n      <p class="TitleR">Setor</p>\n\n      <ion-select class="selectNew" [(ngModel)]="form.Andar" name="Andar" multiple="false" cancelText="Cancelar" okText="Ok" [selectOptions]="selectOptions" (ionChange)="onSelectChange($event)">\n\n        <ion-option  *ngFor="let e of Estacionamento; let i=index" value="{{i}}">{{e.local}}</ion-option>\n\n      </ion-select>\n\n    </div>\n\n\n\n\n\n    <div class="row"  *ngIf="listsetores!=\'\'">\n\n      <p class="TitleR">Local</p>\n\n      <ion-select class="selectNew" [(ngModel)]="form.Setor" name="Setor" multiple="false" cancelText="Cancelar" okText="Ok" [selectOptions]="selectOptions2"  *ngFor="let setores of listsetores"  (ionChange)="onSelectChange2($event)">\n\n        <ion-option  *ngFor="let s of setores; let i=index" value="{{i}}">{{s.setor}}</ion-option>\n\n      </ion-select>\n\n    </div>\n\n    <div class="row" *ngIf="listvagas!=\'\'">\n\n      <p class="TitleR">Vaga</p>\n\n\n\n      <ion-select class="selectNew" [(ngModel)]="form.Vaga" name="Vaga" multiple="false" cancelText="Cancelar" okText="Ok" [selectOptions]="selectOptions3"   (ionChange)="onSelectChange3($event)">\n\n          <ion-option  *ngFor="let v of listvagas; let i=index" value="{{v}}">{{v}}</ion-option>\n\n        </ion-select>\n\n    </div>\n\n  </div>\n\n  \n\n   <button class="buttonsalvar"  [hidden]="Salvo" ion-button (click)="presentActionSheet()">\n\n    <img src="assets/carrinhobutton.png" alt=""> \n\n    Salvar Vaga</button>\n\n\n\n  <div class="parte3" *ngIf="Salvo">\n\n    <div class="vaga-selecionada clearfix">\n\n      <img src="assets/carrinho.png" alt="">\n\n      <div class="local">{{ ArmazenamentoEstacionamento.andar }}</div>\n\n      <div class="setorSelect">Setor - {{ ArmazenamentoEstacionamento.setor }}\n\n        <br>\n\n        Vaga - {{ArmazenamentoEstacionamento.vaga}}\n\n      </div>\n\n    </div>\n\n    <button ion-button class="buttonsalvar" (click)="limparVaga()">\n\n      <img src="assets/limparvaga.png" alt="" />\n\n      Limpar Vaga\n\n    </button>\n\n   <div class="data-vaga">\n\n      Salvo em {{selecionado.data}}\n\n    </div> \n\n  </div>\n\n    -->\n\n\n\n  <div class="BoxGuardarVaga" [hidden]="Salvo">\n\n    <div class="row">\n\n      <p class="TitleR">Setor</p>\n\n\n\n      <div class="left" (click)="PrevSlide()">\n\n        <ion-icon name="ios-arrow-back"></ion-icon>\n\n      </div>\n\n      <div class="right"  (click)="ProximoSlide()">\n\n        <ion-icon name="ios-arrow-forward" end></ion-icon>\n\n      </div>\n\n      <ion-slides pager #Slides1 (ionSlideDidChange)="slideChanged(e)" class="SliderAndar">\n\n        <ion-slide *ngFor="let e of Estacionamento; let i=index">\n\n          <p [innerHTML]="e.local"></p>\n\n        </ion-slide>\n\n      </ion-slides>\n\n    </div>\n\n    <div class="row"  *ngIf="listsetores!=\'\'">\n\n      <p class="TitleR">Local</p>\n\n    \n\n      <div class="left" (click)="PrevSlide2()">\n\n        <ion-icon name="ios-arrow-back"></ion-icon>\n\n      </div>\n\n      <div class="right"  (click)="ProximoSlide2()">\n\n          <ion-icon name="ios-arrow-forward"></ion-icon>\n\n      </div>\n\n      <ion-slides pager #Slides2 class="SliderAndar" *ngFor="let setores of listsetores" (ionSlideDidChange)="slideChanged2(s)" >\n\n        <ion-slide *ngFor="let s of setores">\n\n          <p [innerHTML]="s.setor"></p>\n\n        </ion-slide>\n\n      </ion-slides>\n\n    </div>\n\n    <div class="row" *ngIf="(listvagas)!=\'\'||listvagas!=\'\'">\n\n      <p class="TitleR">Vaga</p>\n\n      \n\n        <div class="left" (click)="PrevSlide3()">\n\n          <ion-icon name="ios-arrow-back"></ion-icon>\n\n        </div>\n\n        <div class="right"  (click)="ProximoSlide3()">\n\n            <ion-icon name="ios-arrow-forward"></ion-icon>\n\n        </div>\n\n        <ion-slides pager #Slides3 class="SliderAndar" (ionSlideDidChange)="slideChanged3(v)" >\n\n          <ion-slide *ngFor="let v of listvagas">\n\n            <p [innerHTML]="v"></p>\n\n          </ion-slide>\n\n        </ion-slides>\n\n    </div>\n\n  </div>\n\n  \n\n   <button class="buttonsalvar"  [hidden]="Salvo" ion-button (click)="SalvarVaga()">\n\n    <img src="assets/carrinhobutton.png" alt=""> \n\n    Salvar Vaga</button>\n\n\n\n  <div class="parte3" *ngIf="Salvo">\n\n    <div class="vaga-selecionada clearfix">\n\n      <img src="assets/carrinho.png" alt="">\n\n      <div class="local">{{ ArmazenamentoEstacionamento.andar }}</div>\n\n      <div class="setorSelect">Setor - {{ ArmazenamentoEstacionamento.setor }}<br><span *ngIf="(listvagas)!=\'\'||listvagas!=\'\'">Vaga - {{ ArmazenamentoEstacionamento.vaga }}</span></div>\n\n      <!-- <div class="setorSelect"></div> -->\n\n    </div>\n\n    <button ion-button class="buttonsalvar" (click)="limparVaga()">\n\n      <img src="assets/limparvaga.png" alt="" />\n\n      Limpar Vaga\n\n    </button>\n\n    <!-- <div class="data-vaga">\n\n      Salvo em {{selecionado.data}}\n\n    </div> -->\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/estacionamento/estacionamento.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
], EstacionamentoPage);

//# sourceMappingURL=estacionamento.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__servicosinterna_servicosinterna__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filmes_interna_filmes_interna__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lojas_interna_lojas_interna__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__vitrine_interna_vitrine_interna__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__gastronomia_interna_gastronomia_interna__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__fiquepordentrointerna_fiquepordentrointerna__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__lazer_interna_lazer_interna__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__cupons_interna_cupons_interna__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FavoritosPage = (function () {
    function FavoritosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.openFavorite = function (f) {
            var rote = '';
            switch (f.areaLike) {
                case 'Guia de Lojas':
                    //     localStorage.setItem('store', JSON.stringify(f))
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__lojas_interna_lojas_interna__["a" /* LojasInternaPage */], {
                        loja: f
                    });
                    break;
                case 'Serviços':
                    //     localStorage.setItem('service', JSON.stringify(f))
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__servicosinterna_servicosinterna__["a" /* ServicosinternaPage */], {
                        s: f
                    });
                    break;
                case 'Cinema':
                    //     localStorage.setItem('movie', JSON.stringify(f))
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__filmes_interna_filmes_interna__["a" /* FilmesInternaPage */], {
                        f: f
                    });
                    break;
                case 'Vitrine Virtual':
                    //     localStorage.setItem('vitrine', JSON.stringify(f))
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__vitrine_interna_vitrine_interna__["a" /* VitrineInternaPage */], {
                        v: f
                    });
                    break;
                case 'Gastronomia':
                    //     localStorage.setItem('gastronomy', JSON.stringify(f))
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__gastronomia_interna_gastronomia_interna__["a" /* GastronomiaInternaPage */], {
                        gastr: f
                    });
                    break;
                case 'Fique Por Dentro':
                    //     localStorage.setItem('fiquepordentro', JSON.stringify(f))
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */], {
                        novidade: f
                    });
                    break;
                case 'Entretenimento':
                    //     localStorage.setItem('entretenimentoSelect', JSON.stringify(f))
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__lazer_interna_lazer_interna__["a" /* LazerInternaPage */], {
                        c: f
                    });
                    break;
                case 'Cupom':
                    localStorage.setItem('cupomSelect', JSON.stringify(f));
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__cupons_interna_cupons_interna__["a" /* CuponsInternaPage */], {
                        c: f
                    });
                    break;
            }
        };
        this.getFavorites();
    }
    FavoritosPage.prototype.getFavorites = function () {
        var favorites = [];
        var areas = ['servico', 'cinema', 'lojas', 'vitrine', 'gastronomia', 'fiquepordentro', 'entretenimento', 'cupom'];
        for (var i = 0; i < areas.length; i++) {
            var item = JSON.parse(localStorage.getItem(areas[i] + '-like'));
            if (item == null)
                item = [];
            var area = '';
            item.map(function (elem) {
                switch (areas[i]) {
                    case 'lojas':
                        area = 'Guia de Lojas';
                        break;
                    case 'servico':
                        area = 'Serviços';
                        break;
                    case 'lojas':
                        area = 'Guia de Lojas';
                        break;
                    case 'cinema':
                        area = 'Cinema';
                        break;
                    case 'vitrine':
                        area = 'Vitrine Virtual';
                        break;
                    case 'gastronomia':
                        area = 'Gastronomia';
                        break;
                    case 'fiquepordentro':
                        area = 'Fique Por Dentro';
                        break;
                    case 'entretenimento':
                        area = 'Entretenimento';
                        break;
                    case 'cupom':
                        area = 'Cupom';
                        break;
                }
                elem.areaLike = area;
            });
            favorites = favorites.concat(item);
        }
        this.favorites = favorites;
    };
    FavoritosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FavoritosPage');
    };
    return FavoritosPage;
}());
FavoritosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-favoritos',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/favoritos/favoritos.html"*/'<ion-header class="BarHeader">\n\n    <ion-navbar class="Nobackground">\n\n      <button ion-button menuToggle class="buttonmenu">\n\n          <img src="assets/icon/icoMenu.png" alt="">\n\n        </button>\n\n      <ion-title class="titleButton">\n\n        <h1>Favoritos</h1>\n\n      </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n\n\n<ion-content class="BoxFiquePorDentro">\n\n  <div class="alignCenter">\n\n    <div class="subHeader">\n\n      <p class="Title">Conteúdo Salvo</p>\n\n      <p class="Chamada">Confira seus conteúdos favoritados</p>\n\n      \n\n    </div>\n\n    <div class="HomeFique" *ngFor="let f of favorites" (click)="openFavorite(f)">\n\n      <div class="areaflexHomeFique">\n\n        <div class="Areaimg">\n\n          <img src="assets/icon/icohearth.png">\n\n        </div>\n\n        <div class="Desc">\n\n          <p class="Tit" [innerHTML]="f.areaLike"></p>\n\n          <p class="txt">{{ f.nome.str.lang_1}}</p>\n\n        </div>\n\n      </div>\n\n    </div>\n\n    <div class="semConexao" *ngIf="(favorites).length==0||favorites.length==0">\n\n      <img src="assets/iconSmile.png" class="iconeimg" alt="">\n\n        <p class="AindaNaoFavorito">Você ainda não favoritou</p>\n\n        <p>Marque os conteúdos que mais gostar como favorito e acesse de forma rápida nessa área.</p>\n\n     </div>\n\n  </div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/favoritos/favoritos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], FavoritosPage);

//# sourceMappingURL=favoritos.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HorariosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HorariosPage = (function () {
    function HorariosPage(navCtrl, navParams, http, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
    }
    HorariosPage.prototype.InfoShopping = function () {
        var _this = this;
        this.http.get(this.service.config.jsonDomain + "jsonHorariosMobile.json").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.horarios = data.lista;
            _this.Dominio = _this.service.config.defaultDomain;
            console.log(_this.horarios);
        }, function (err) {
            console.log('deuruim lojas', err);
        });
    };
    HorariosPage.prototype.ionViewDidLoad = function () {
        this.InfoShopping();
        console.log('ionViewDidLoad OshoppingPage');
    };
    return HorariosPage;
}());
HorariosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-horarios',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/horarios/horarios.html"*/'<ion-content class="BoxFiquePorDentro">\n\n  <div class="alignCenter">\n\n  <div class="container">\n\n    <div class="info-shopping" >\n\n        <div class="nome">Segunda a Sábado</div>\n\n    <div class="content-shopping clearfix">\n\n        <div  class="texto-shopping">\n\n          <p>Lojas: 10h às 22h<br>\n\n              Alimentação e Lazer: 10h às 22h</p>\n\n        </div>\n\n    </div>\n\n  </div>\n\n  <div class="info-shopping" >\n\n      <div class="nome">Feriados</div>\n\n  <div class="content-shopping clearfix">\n\n      <div  class="texto-shopping">\n\n        <p>Lojas: 13h às 21h (opcional)<br>\n\n            Alimentação e Lazer: 12h às 22h</p>\n\n      </div>\n\n  </div>\n\n</div>\n\n<div class="info-shopping" >\n\n    <div class="nome">Domingos</div>\n\n<div class="content-shopping clearfix">\n\n    <div  class="texto-shopping">\n\n      <p>Lojas: 13h às 21h<br>\n\n          Alimentação e Lazer: 12h às 22h</p>\n\n    </div>\n\n</div>\n\n</div>\n\n</div>\n\n</div>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/horarios/horarios.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */]])
], HorariosPage);

//# sourceMappingURL=horarios.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComochegarPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(470);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__ = __webpack_require__(471);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ComochegarPage = (function () {
    function ComochegarPage(navCtrl, navParams, geolocation, googleMaps, service, _loadingCtrl, _alertCtrl, http, viewCtrl, diagnostic) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.geolocation = geolocation;
        this.googleMaps = googleMaps;
        this.service = service;
        this._loadingCtrl = _loadingCtrl;
        this._alertCtrl = _alertCtrl;
        this.http = http;
        this.viewCtrl = viewCtrl;
        this.diagnostic = diagnostic;
        this.endereco = this.service.config.endereco;
        this.loadJson = false;
        this.getPositionAtual = false;
        this.tracarRota = function () {
            var _this = this;
            this.loading = this._loadingCtrl.create({
                content: ' Aguarde...',
                dismissOnPageChange: true
            });
            this.loading.present();
            this.http.get(this.service.config.jsonDomain + "jsoncomochegar.json").map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.jsonCentral = data,
                    _this.Dominio = _this.service.config.defaultDomain;
                console.log(_this.jsonCentral);
                _this.locAtual(_this.jsonCentral);
            }, function (err) {
                console.log('deuruim', err);
            });
        };
    }
    ComochegarPage.prototype.ionViewDidLoad = function () {
    };
    ComochegarPage.prototype.locAtual = function (jsonreceb) {
        var _this = this;
        var _alert = this._alertCtrl.create({
            'title': this.service.config.name,
            'message': 'Para traçar a rota, ative sua localização.',
            buttons: ['OK']
        });
        this.diagnostic.requestLocationAuthorization()
            .then(function (state) {
            _this.diagnostic.isLocationAvailable()
                .then(function (state) {
                if (state == true) {
                    // if (state == this.diagnostic.bluetoothState.POWERED_ON){
                    var options = { timeout: 120000, enableHighAccuracy: true };
                    navigator.geolocation.getCurrentPosition(function (resp) {
                        console.log(resp);
                        resp.coords.latitude;
                        resp.coords.longitude;
                        console.log(resp);
                        _this.lat = _this.jsonCentral.lista[0].latitude;
                        _this.log = _this.jsonCentral.lista[0].longitude;
                        _this.end = _this.jsonCentral.lista[0].descricao.str.lang_1;
                        var enderecoParse = _this.end.split(' ').join('+');
                        //var urlRota = "https://www.google.com.br/maps/dir/@lat_o,@lon_o/@end/@@lat_d,@lon_d,10z/?hl=pt-BR";
                        var LatAtual = resp.coords.latitude;
                        var LongAtual = resp.coords.longitude;
                        _this.montandorota = "https://www.google.com.br/maps/dir/" + LatAtual + "," + LongAtual + "/" + enderecoParse + "/@" + _this.lat + "," + _this.log + "/?hl=pt-BR";
                        window.open(_this.montandorota, '_system', 'location=no');
                        console.log(_this.montandorota);
                        _this.loading.dismiss();
                    }, function (error) {
                        _this.loading.dismiss();
                        console.log(error);
                    }, options);
                }
                else {
                    _this.loading.dismiss();
                    _alert.present();
                }
            });
        });
    };
    ComochegarPage.prototype.RotaComoChegar = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        this.http.get(this.service.config.jsonDomain + "jsoncomochegar.json", { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Rota = data,
                _this.Dominio = _this.service.config.defaultDomain;
            console.log(_this.Rota);
        }, //this.product = JSON.stringify(data),
        function (//this.product = JSON.stringify(data),
            err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    return ComochegarPage;
}());
ComochegarPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-comochegar',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/comochegar/comochegar.html"*/'<ion-content class="BoxFiquePorDentro">\n\n  <div class="alignCenter">\n\n    <div class="areapesquisa">\n\n        <div class="boxContatoEndereco" [hidden]="Sucesso">\n\n            <div class="icon-info">\n\n                <img src="assets/faleconosco/iconAddress.png" alt="" />\n\n            </div>\n\n            <div class="descr_info">\n\n                <p>ENDEREÇO</p>\n\n                <address [innerHTML]="endereco"></address>\n\n            </div>\n\n        </div>\n\n      <input type="text" class="input" placeholder="Origem" (click)="tracarRota()">\n\n      <div class="buttonenviar" (click)="tracarRota()">Traçar Rota</div>\n\n    </div>\n\n    <!-- <div id="map"></div> -->\n\n  </div>\n\n    <img src="assets/imgMapa.png" alt="">\n\n  </ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/comochegar/comochegar.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* GoogleMaps */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__["a" /* Diagnostic */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_native_google_maps__["a" /* GoogleMaps */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_diagnostic__["a" /* Diagnostic */]])
], ComochegarPage);

//# sourceMappingURL=comochegar.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContatoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ContatoPage = (function () {
    function ContatoPage(navCtrl, navParams, http, service, _alertCtrl, _loadingCtrl, fb) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.fb = fb;
        this.loader = this._loadingCtrl.create({
            content: 'Aguarde...',
            dismissOnPageChange: true
        });
        this.Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
        this.HomePage = __WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */];
        this.Sucesso = false;
        this.form2 = {
            nome: '',
            email: '',
            assunto: '',
            mensagem: ''
        };
        this.InfoContato();
        if (this.Token != undefined) {
            this.form2 = {
                nome: this.Token.nomeUsuario,
                email: this.Token.emailUsuario,
                assunto: '',
                mensagem: ''
            };
        }
        else {
            this.form2 = {
                nome: '',
                email: '',
                assunto: '',
                mensagem: ''
            };
        }
        this.rootNavCtrl = navParams.get('rootNavCtrl');
        this.ContatoForm = fb.group({
            'nome': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(3)]],
            'email': ['', [this.EmailValidacao.bind(this)]],
            'assunto': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(3)]],
            'mensagem': ['', [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["Validators"].minLength(3)]],
        });
    }
    ContatoPage.prototype.EmailValidacao = function (control) {
        if (!(control.value.match('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'))) {
            return { invalidEmail: true };
        }
    };
    ContatoPage.prototype.ValidaForm = function () {
        if (!this.form2.nome) {
            return "Nome";
        }
        else if (this.ContatoForm.controls.email.invalid) {
            return "E-mail";
        }
        else if (!this.form2.assunto) {
            return "Assunto";
        }
        else if (!this.form2.mensagem) {
            return "Mensagem";
        }
        return true;
    };
    ContatoPage.prototype.Enviar = function () {
        var _this = this;
        var mensagem = this.ValidaForm();
        if (mensagem == true) {
            var body = "pemail=" + this.form2.email + "&pmensagem=" + this.form2.mensagem + "&pnome=" + this.form2.nome + "&porigem=AppShopping&ptelefone=00000000";
            return this.http.get(this.service.config.importacaoDomain + '?' + body, body).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                _this.Formularios = data;
                _this.Dominio = _this.service.config.defaultDomain;
                // this.loader.dismiss()
                _this.Sucesso = true;
                console.log(_this.Formularios);
            });
        }
        else {
            this._alertCtrl.create({
                title: this.service.config.name,
                subTitle: "O campo " + mensagem + " é obrigatório",
                buttons: [{ text: 'Ok' }]
            }).present();
            console.log('erro');
        }
    };
    ContatoPage.prototype.InfoContato = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Headers */]();
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json?nocache=" + nocache).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            // this.item2 = data.lista[0].link_texto;
            var paginas = data.lista;
            var filtrarPage = [];
            for (var i = 0; i < paginas.length; i++) {
                if (paginas[i].texto == 'ENTRE EM CONTATO') {
                    filtrarPage.push(paginas[i]);
                    //  this.Segmentos=segmentolojas;
                    // console.log(filtrarPage, 'entro fi')
                }
                _this.item3 = filtrarPage;
                _this.item3 = _this.item3[0].link_texto;
                console.log(_this.item3);
            }
            _this.Dominio = _this.service.config.defaultDomain;
        }, function (err) {
            console.log('deuruim lojas', err);
        });
    };
    ContatoPage.prototype.irparaHome = function (localNavCtrl, event, s) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */]);
    };
    ContatoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContatoPage');
    };
    return ContatoPage;
}());
ContatoPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-contato',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/contato/contato.html"*/'<ion-content class="BoxFiquePorDentro">\n\n    <div class="alignCenter">\n\n        <div class="container" [hidden]="Sucesso">\n\n            <div class="info-shopping">\n\n                <div class="nome">Fale Conosco</div>\n\n                <div class="content-shopping clearfix">\n\n                    <form [formGroup]="ContatoForm" class="FormCadastre">\n\n                        <ion-item class="nopaddingLeft">\n\n                            <ion-input class="input" formControlName="nome" placeholder="Seu Nome" [(ngModel)]="form2.nome" type="text" name="nome"></ion-input>\n\n                        </ion-item>\n\n                        <ion-item class="nopaddingLeft">\n\n                            <ion-input class="input" formControlName="email" placeholder="Seu E-mail" [(ngModel)]="form2.email" type="text" required="true"></ion-input>\n\n                        </ion-item>\n\n                        <ion-item class="nopaddingLeft">\n\n                            <ion-input class="input" formControlName="assunto" placeholder="Assunto" [(ngModel)]="form2.assunto" type="text"></ion-input>\n\n                        </ion-item>\n\n                        <ion-item class="nopaddingLeft">\n\n                            <textarea name="mensagem" formControlName="mensagem" placeholder="Digite aqui sua mensagem" [(ngModel)]="form2.mensagem" id="" cols="30" rows="10" class="input textarea"></textarea>\n\n                        </ion-item>\n\n                        <button ion-button class="buttonEnviar" ng-disabled="ContatoForm.$invalid" (click)="Enviar()">Enviar</button>\n\n                    </form>\n\n                </div>\n\n            </div>\n\n        </div>\n\n        <div class="boxContatoEndereco" [hidden]="Sucesso">\n\n            <!-- <div class="icon-info">\n\n                <img src="assets/iconPhone.png" alt="">\n\n            </div> -->\n\n            <div class="descr_info">\n\n                <p>CONTATO</p>\n\n                <address [innerHTML]="item3">\n\n                </address>\n\n            </div>\n\n        </div>\n\n\n\n        <div class="MsgOk" *ngIf="Sucesso">\n\n            <img src="assets/imgOk.png" class="imgOk" alt="">\n\n            <p class="DescMsg">Enviado com sucesso!</p>\n\n            <button class="ButtonVoltarHome" (click)="irparaHome()">VOLTAR PARA A HOME</button>\n\n        </div>\n\n\n\n    </div>\n\n\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/contato/contato.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_2__angular_forms__["FormBuilder"]])
], ContatoPage);

//# sourceMappingURL=contato.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Page1Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var Page1Page = (function () {
    function Page1Page(navCtrl, navParams, http, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
    }
    Page1Page.prototype.InfoShopping = function () {
        var _this = this;
        this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.page1 = data.lista;
            _this.Dominio = _this.service.config.defaultDomain;
            console.log(_this.page1, 'Pagina 1');
        }, function (err) {
            console.log('deuruim lojas', err);
        });
    };
    Page1Page.prototype.ionViewDidLoad = function () {
        this.InfoShopping();
        console.log('ionViewDidLoad Page1Page');
    };
    return Page1Page;
}());
Page1Page = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-page1',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/page1/page1.html"*/'<ion-content class="BoxFiquePorDentro">\n\n  <div class="alignCenter" *ngIf="page1?.length">\n\n    <div class="image-shopping">\n\n      <img src="{{Dominio}}/{{page1[1].midia.arquivos.lang_1.imagem.url}}" />\n\n    </div>\n\n    <div class="container">\n\n      <div class="info-shopping">\n\n        <div class="nome" [innerHTML]="page1[1].titulo.str.lang_1"></div>\n\n        <div class="content-shopping clearfix">\n\n          <div [innerHTML]="page1[1].link_texto" class="texto-shopping"></div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n  \n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/page1/page1.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */]])
], Page1Page);

//# sourceMappingURL=page1.js.map

/***/ }),

/***/ 157:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Page2Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Page2Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Page2Page = (function () {
    function Page2Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Page2Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Page2Page');
    };
    return Page2Page;
}());
Page2Page = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-page2',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/page2/page2.html"*/'<!--\n\n  Generated template for the Page2Page page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>page2</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/page2/page2.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], Page2Page);

//# sourceMappingURL=page2.js.map

/***/ }),

/***/ 158:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Page3Page; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the Page3Page page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var Page3Page = (function () {
    function Page3Page(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    Page3Page.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Page3Page');
    };
    return Page3Page;
}());
Page3Page = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-page3',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/page3/page3.html"*/'<!--\n\n  Generated template for the Page3Page page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <ion-title>page3</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/page3/page3.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], Page3Page);

//# sourceMappingURL=page3.js.map

/***/ }),

/***/ 159:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__notificacoes_interna_notificacoes_interna__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NotificacoesPage = (function () {
    function NotificacoesPage(navCtrl, navParams, http, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.storage = storage;
    }
    NotificacoesPage.prototype.ViewDetails = function ($event, m) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__notificacoes_interna_notificacoes_interna__["a" /* NotificacoesInternaPage */], {
            msgInter: m
        });
    };
    NotificacoesPage.prototype.ListMensagens = function () {
        var _this = this;
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        var headers = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["a" /* Headers */]({
            'Authorization': Token.Token
        });
        var options = new __WEBPACK_IMPORTED_MODULE_5__angular_http__["d" /* RequestOptions */]({ headers: headers });
        this.http.get(this.service.config.defaultDomain + "/api/mobile/MensagensList?Clientecod=" + this.service.config.idShopping, { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.MsgsList = data.Mensagens,
                _this.Dominio = _this.service.config.defaultDomain,
                _this.storage.set('Mensagens', _this.MsgsList);
            if (_this.MsgsList.length == 0) {
                _this.NaoDisponivel = true;
            }
            else {
                _this.NaoDisponivel = false;
            }
            console.log(_this.MsgsList, 'Novidade');
        }, function (err) {
            console.log('deuruim', err);
            _this.storage.get('Mensagens').then(function (val) {
                console.log('Your age is', val);
                _this.MsgsList = val;
            });
        });
    };
    NotificacoesPage.prototype.ionViewDidLoad = function () {
        this.ListMensagens();
        console.log('ionViewDidLoad NotificacoesPage');
    };
    NotificacoesPage.prototype.ionViewWillLeave = function () {
        this.ListMensagens();
    };
    NotificacoesPage.prototype.ionViewCanEnter = function () {
        this.ListMensagens();
    };
    return NotificacoesPage;
}());
NotificacoesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-notificacoes',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/notificacoes/notificacoes.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n        <img src="assets/icon/icoMenu.png" alt="">\n\n      </button>\n\n    <ion-title class="titleButton">\n\n      <h1>notificações</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <div class="alignCenter">\n\n      <div class="subHeader">\n\n          <p class="Chamada">Confira abaixo as notificações.</p>\n\n      </div>\n\n    <div class="BoxFiquePorDentro" >\n\n      <div class="HomeFique" *ngFor="let m of MsgsList" (click)="ViewDetails($event, m)">\n\n        <div class="iconpositive" *ngIf="m.Nova==false"></div>\n\n        <div class="iconnegative" *ngIf="m.Nova==true"></div>\n\n        <div class="areaflexHomeFique">\n\n          <div class="Desc">\n\n            <p class="Tit">{{m.Titulo}}</p>\n\n          </div>\n\n        </div>\n\n      </div>\n\n      <div class="semConexao" *ngIf="(MsgsList)==\'\'||MsgsList==\'\'">\n\n          \n\n              <img src="assets/iconSmile.png" class="iconeimg" alt="" style="    width: 35%;\n\n              margin: 15% 0 3%;">\n\n              <p class="AindaNaoFavorito">Aguarde! </p>\n\n              <p>Não existem notificações.</p>\n\n            </div>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/notificacoes/notificacoes.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */]])
], NotificacoesPage);

//# sourceMappingURL=notificacoes.js.map

/***/ }),

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuscaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__servicosinterna_servicosinterna__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filmes_interna_filmes_interna__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lojas_interna_lojas_interna__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__gastronomia_interna_gastronomia_interna__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__fiquepordentrointerna_fiquepordentrointerna__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__lazer_interna_lazer_interna__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_service_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var BuscaPage = (function () {
    function BuscaPage(navCtrl, navParams, events, _alertCtrl, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this._alertCtrl = _alertCtrl;
        this.service = service;
        this.openDetails = function (l) {
            switch (l.categoria) {
                case "Acontece":
                    // localStorage.setItem('fiquedentro',JSON.stringify(l));
                    // $state.go('app.fiquepordentroDetails');
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */], {
                        novidade: l
                    });
                    break;
                case "Filmes":
                    // localStorage.setItem('movie',JSON.stringify(l));
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__filmes_interna_filmes_interna__["a" /* FilmesInternaPage */], {
                        f: l
                    });
                    break;
                case "Alimentação":
                    // localStorage.setItem('gastronomy',JSON.stringify(l));
                    // $state.go('app.gastronomiainterna');
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__gastronomia_interna_gastronomia_interna__["a" /* GastronomiaInternaPage */], {
                        gastr: l
                    });
                    break;
                case "Lazer":
                    // localStorage.setItem('gastronomy',JSON.stringify(l));
                    // $state.go('app.gastronomiainterna');
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__lazer_interna_lazer_interna__["a" /* LazerInternaPage */], {
                        c: l
                    });
                    break;
                case "Serviços":
                    // localStorage.setItem('gastronomy',JSON.stringify(l));
                    // $state.go('app.gastronomiainterna');
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__servicosinterna_servicosinterna__["a" /* ServicosinternaPage */], {
                        s: l
                    });
                    break;
                case "Lojas":
                    // localStorage.setItem('store',JSON.stringify(l));
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__lojas_interna_lojas_interna__["a" /* LojasInternaPage */], {
                        loja: l
                    });
                default:
            }
        };
        this.txtValue = localStorage.getItem('campoPesquisa');
        var resultadoLoja = JSON.parse(localStorage.getItem('resultadoDaBuscaHome'));
        var fieldLoja = '';
        if (resultadoLoja != undefined) {
            for (var k = 0; k < resultadoLoja.length; k++) {
                if (resultadoLoja[k].categoria == "Filmes") {
                    var urlPipocao = resultadoLoja[k].midia.arquivos.lang_1.arquivo.url;
                    resultadoLoja[k].midia.arquivos.lang_1.arquivo.url = urlPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
                }
            }
        }
        var allStores = resultadoLoja;
        this.stores = allStores;
        if (allStores.length == 0) {
            this.nadaEncontrado = true;
            // this._alertCtrl.create({
            //   title: this.service.config.name,
            //   subTitle: 'Nada Encontrado',
            //   buttons: [{ text: 'Ok' }]
            // }).present();
        }
        else {
            this.nadaEncontrado = false;
        }
        this.quantidade = this.stores.length > 0 ? 10 : 0;
        this.storesQntd = allStores.length;
    }
    BuscaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BuscaPage');
    };
    return BuscaPage;
}());
BuscaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-busca',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/busca/busca.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n        <img src="assets/icon/icoMenu.png" alt="">\n\n      </button>\n\n    <ion-title class="titleButton">\n\n      <h1>busca</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div class="alignCenter">\n\n  <p class="semConexao" style=" text-align: left; margin: 8% 0 3%;" [hidden]="nadaEncontrado">Busca por "{{txtValue}}"</p>\n\n  <div class="BoxFiquePorDentro" >\n\n      <div class="HomeFique" *ngFor="let loja of stores" (click)="openDetails(loja)">\n\n        <div class="areaflexHomeFique">\n\n          <div class="Desc">\n\n            <p class="txt">{{loja.categoria}}</p>\n\n            <p class="Tit">{{loja.nome.str.lang_1}}</p>\n\n          </div>\n\n      </div>\n\n      \n\n    </div>\n\n    <div class="semConexao" style="margin: 8% auto 3%" *ngIf="nadaEncontrado">\n\n        <p>Nenhum resultado encontrado!</p>\n\n      </div>\n\n  </div>\n\n</div>\n\n</ion-content>\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/busca/busca.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_8__providers_service_service__["a" /* ServiceProvider */]])
], BuscaPage);

//# sourceMappingURL=busca.js.map

/***/ }),

/***/ 169:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 169;

/***/ }),

/***/ 211:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/acontece/acontece.module": [
		517,
		43
	],
	"../pages/busca/busca.module": [
		544,
		42
	],
	"../pages/cadastre/cadastre.module": [
		507,
		41
	],
	"../pages/campanha-cadastro/campanha-cadastro.module": [
		509,
		40
	],
	"../pages/campanha/campanha.module": [
		510,
		39
	],
	"../pages/comochegar/comochegar.module": [
		535,
		38
	],
	"../pages/contato/contato.module": [
		536,
		37
	],
	"../pages/cupons-interna/cupons-interna.module": [
		515,
		36
	],
	"../pages/cupons/cupons.module": [
		520,
		35
	],
	"../pages/enquetes/enquetes.module": [
		516,
		34
	],
	"../pages/esquecisenha/esquecisenha.module": [
		506,
		33
	],
	"../pages/estacionamento/estacionamento.module": [
		531,
		32
	],
	"../pages/favoritos/favoritos.module": [
		533,
		31
	],
	"../pages/feed/feed.module": [
		513,
		30
	],
	"../pages/filmes-interna/filmes-interna.module": [
		503,
		29
	],
	"../pages/filmes/filmes.module": [
		504,
		28
	],
	"../pages/fiquepordentro/fiquepordentro.module": [
		514,
		27
	],
	"../pages/fiquepordentrointerna/fiquepordentrointerna.module": [
		505,
		26
	],
	"../pages/gastronomia-interna/gastronomia-interna.module": [
		524,
		25
	],
	"../pages/gastronomia/gastronomia.module": [
		525,
		24
	],
	"../pages/home/home.module": [
		511,
		23
	],
	"../pages/horarios/horarios.module": [
		534,
		22
	],
	"../pages/lazer-interna/lazer-interna.module": [
		526,
		21
	],
	"../pages/lazer/lazer.module": [
		527,
		20
	],
	"../pages/list-filmes/list-filmes.module": [
		502,
		19
	],
	"../pages/list-shopping/list-shopping.module": [
		540,
		18
	],
	"../pages/listaservicos/listaservicos.module": [
		530,
		17
	],
	"../pages/login/login.module": [
		508,
		16
	],
	"../pages/lojas-interna/lojas-interna.module": [
		522,
		15
	],
	"../pages/lojas/lojas.module": [
		523,
		14
	],
	"../pages/meuperfil/meuperfil.module": [
		541,
		13
	],
	"../pages/notificacoes-interna/notificacoes-interna.module": [
		542,
		12
	],
	"../pages/notificacoes/notificacoes.module": [
		543,
		11
	],
	"../pages/ofertas/ofertas.module": [
		521,
		10
	],
	"../pages/oshopping/oshopping.module": [
		528,
		9
	],
	"../pages/page1/page1.module": [
		537,
		8
	],
	"../pages/page2/page2.module": [
		538,
		7
	],
	"../pages/page3/page3.module": [
		539,
		6
	],
	"../pages/preco-cinema/preco-cinema.module": [
		501,
		5
	],
	"../pages/servicos/servicos.module": [
		532,
		4
	],
	"../pages/servicosinterna/servicosinterna.module": [
		529,
		3
	],
	"../pages/tutorial/tutorial.module": [
		512,
		2
	],
	"../pages/vitrine-interna/vitrine-interna.module": [
		518,
		1
	],
	"../pages/vitrine/vitrine.module": [
		519,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 211;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__filmes_filmes__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__filmes_interna_filmes_interna__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__fiquepordentrointerna_fiquepordentrointerna__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_list_filmes_list_filmes__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__campanha_campanha__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var HomePage = (function () {
    function HomePage(navCtrl, service, http, viewCtrl, _alertCtrl, navParams, events, storage, platform) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.http = http;
        this.viewCtrl = viewCtrl;
        this._alertCtrl = _alertCtrl;
        this.navParams = navParams;
        this.events = events;
        this.storage = storage;
        this.platform = platform;
        this.Usuario = localStorage.getItem("cacheUsuario");
        this.Dominio = this.service.config.defaultDomain;
        this.Pagina = 0;
        this.irParaInterna = function ($event, banner) {
            var fiquepordentro = [];
            if (banner.tipo == "Campanha") {
                for (var i = 0; i < this.Campanha.length; i++) {
                    console.log(this.Campanha[i].Id);
                    if (banner.cod == this.Campanha[i].Id) {
                        fiquepordentro.push(this.Campanha[i]);
                        // console.log('banner')
                        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__campanha_campanha__["a" /* CampanhaPage */], {
                            campanha: fiquepordentro
                        });
                    }
                    else {
                        console.log('Não Achou');
                    }
                }
            }
            else {
                for (var i = 0; i < this.Novidades.length; i++) {
                    if (banner.tipo == "Floater" && banner.url != null) {
                        window.open(banner.url, '_system', 'location=yes');
                    }
                    if (banner.cod == this.Novidades[i].cod) {
                        fiquepordentro.push(this.Novidades[i]);
                        // console.log('banner')
                        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */], {
                            novidade: fiquepordentro
                        });
                    }
                    else {
                        console.log('Não Achou');
                    }
                }
                // }
            }
        };
        this.config = {
            slidesPerView: 3.5,
            paginationClickable: true,
            spaceBetween: 15,
            freeMode: true
        };
        this.slideOptions = {
            initialSlide: 0,
            loop: false,
            direction: 'horizontal',
            pager: true,
            speed: 800
        };
        this.Sliders();
        this.CinemaCartazes();
        this.FiquePorDentro();
        this.Campanhas();
    }
    HomePage.prototype.showAlertLoginSucesso = function () {
        var alert = this._alertCtrl.create({
            title: '<img src="assets/icon/confirmation.png" /> Bem Vindo!',
            subTitle: 'Olá ' + this.Usuario.nomeUsuario + ', você está logado!',
            buttons: ['OK'],
            cssClass: 'alertCustomCss'
        });
        alert.present();
    };
    HomePage.prototype.doRefresh = function (refresher) {
        var _this = this;
        setTimeout(function () {
            _this.Sliders();
            _this.Pagina = 0;
            _this.FiquePorDentro();
            refresher.complete();
        }, 2000);
    };
    HomePage.prototype.Sliders = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + "jsonfloatersMobile.json?nocache=" + nocache, { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.banners = data.lista,
                _this.Dominio = _this.service.config.defaultDomain;
            // console.log(this.banners, 'Banner')
            _this.storage.set('banners', _this.banners);
        }, //this.product = JSON.stringify(data),
        function (//this.product = JSON.stringify(data),
            err) {
            // stop disconnect watch
            console.log('deuruim', err);
            _this.storage.get('banners').then(function (val) {
                console.log('Your age is', val);
                _this.banners = val;
            });
        } //this.status.error = JSON.stringify(err)
        );
    };
    HomePage.prototype.CinemaCartazes = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + "jsonfilmes.json?nocache=" + nocache, { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Filmes = data.lista;
            if (_this.Filmes == 0) {
                _this.someAreaCinema = true;
            }
            else {
                _this.someAreaCinema = false;
            }
            _this.CartazLink = _this.service.config.pipocaoCartazHome;
            // console.log(this.Filmes),
            // console.log(this.CartazLink)
            _this.storage.set('Filmes', _this.Filmes);
            for (var i = 0; i < data.lista.length; i++) {
                var findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
                var changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
                data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
            }
        }, //this.product = JSON.stringify(data),
        function (//this.product = JSON.stringify(data),
            err) {
            // stop disconnect watch
            console.log('deuruim', err);
            _this.storage.get('Filmes').then(function (val) {
                console.log('Your age is', val);
                _this.Filmes = val;
            });
        });
    };
    HomePage.prototype.FiquePorDentro = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5", { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Novidades = data.lista,
                _this.Dominio = _this.service.config.defaultDomain,
                _this.storage.set('Novidades', _this.Novidades);
            // console.log(this.Novidades, 'Novidade')
        }, function (err) {
            // stop disconnect watch
            console.log('deuruim', err);
            _this.storage.get('Novidades').then(function (val) {
                console.log('Your age is', val);
                _this.Novidades = val;
            });
        });
    };
    HomePage.prototype.Campanhas = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        this.http.get(this.service.config.defaultDomain + "/api/mobile/CampanhaList?ClienteCod=" + this.service.config.idShopping, { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Campanha = data.Campanhas,
                _this.Dominio = _this.service.config.defaultDomain;
            // console.log(this.Campanha, 'Campanha')
        }, function (err) {
            console.log('deuruim', err);
        });
    };
    HomePage.prototype.CountPaginacaoFiquePorDentro = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.lista;
            if (_this.Data != undefined) {
                for (var i = 0; i < 5; i++) {
                    _this.Novidades.push(_this.Data[i]);
                }
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    HomePage.prototype.DetailsFiquePorDentro = function ($event, fique) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */], {
            novidade: fique
        });
    };
    HomePage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        // console.log('Begin async operation');
        setTimeout(function () {
            if (_this.Novidades != undefined) {
                // console.log('Async operation has ended');
                _this.CountPaginacaoFiquePorDentro();
                infiniteScroll.complete();
            }
            else {
                ('travo');
            }
        }, 500);
    };
    HomePage.prototype.IrListaDeFilmes = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__filmes_filmes__["a" /* FilmesPage */]);
    };
    HomePage.prototype.isLogged = function () {
        console.log('passou na funcao');
        if (this.Usuario != undefined) {
            console.log('entronotrue');
            this.Logado = true;
        }
        else {
            console.log('entronofalse');
            this.Logado = false;
            // return false;
        }
    };
    HomePage.prototype.IrParaFilmes = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_list_filmes_list_filmes__["a" /* ListFilmesPage */]);
    };
    HomePage.prototype.IrParaInternaDeOutroFilme = function (event, filme) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__filmes_interna_filmes_interna__["a" /* FilmesInternaPage */], {
            f: filme
        });
    };
    HomePage.prototype.ionViewWillEnter = function () {
        this.viewCtrl.showBackButton(false);
        document.getElementById("AparecerHome").style.display = "block";
        document.getElementById("NaoAparecer").style.display = "none";
    };
    HomePage.prototype.ionViewWillLeave = function () {
        document.getElementById("AparecerHome").style.display = "none";
        document.getElementById("NaoAparecer").style.display = "block";
    };
    HomePage.prototype.ionViewLoaded = function () {
        // localStorage.removeItem('resultadoDaBuscaHome')
    };
    return HomePage;
}());
HomePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-home',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/home/home.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n          <img src="assets/icon/icoMenuHome.png" alt="">\n\n        </button>\n\n    <ion-title>\n\n      <img src="assets/logo.png" alt="" class="logoCliente">\n\n    <!-- <div class="buttonpesquisa">\n\n    </div> -->\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n    <ion-refresher-content></ion-refresher-content>\n\n  </ion-refresher>\n\n  <ion-slides pager class="SliderHome">\n\n    <ion-slide *ngFor="let banner of banners" (click)="irParaInterna($event, banner)">\n\n      <img-loader useImg src="{{Dominio}}/{{banner.midia.arquivos.lang_1.arquivo.url}}" alt="" style="width: 100%;"></img-loader>\n\n    </ion-slide>\n\n  </ion-slides>\n\n\n\n  <div class="AreaCinemaHome" *ngIf="someAreaCinema!=true">\n\n    <div class="headerTitle">\n\n      <div class="alignCenter flex">\n\n        <div class="camptitledesc">\n\n          <p class="title">\n\n            Cinema\n\n          </p>\n\n          <p class="descT">Confira nossa programação</p>\n\n        </div>\n\n        <div class="buttonVerM" (click)="IrParaFilmes()">ver mais</div>\n\n      </div>\n\n    </div>\n\n    <swiper [config]="config">\n\n      <div class="swiper-wrapper">\n\n        <div class="swiper-slide" *ngFor="let filme of Filmes" (click)="IrParaInternaDeOutroFilme($event, filme)">\n\n          <img-loader useImg src="{{filme.midia.arquivos.lang_1.arquivo.url}}"></img-loader>\n\n        </div>\n\n      </div>\n\n    </swiper>\n\n  </div>\n\n\n\n\n\n  <div class="AreaFiqueporDentro">\n\n    <div class="headerTitle">\n\n      <div class="alignCenter flex">\n\n        <div class="camptitledesc nowidth">\n\n          <p class="title">\n\n            Fique por dentro\n\n          </p>\n\n          <p class="descT">Confira tudo o que acontece no shopping</p>\n\n        </div>\n\n      </div>\n\n    </div>\n\n    <div class="alignCenter">\n\n      <div *ngFor="let fique of Novidades" (click)="DetailsFiquePorDentro($event, fique)">\n\n        <div class="HomeFique" *ngIf="fique!=undefined">\n\n          <div class="areaflexHomeFique">\n\n            <div class="Areaimg">\n\n              <img-loader useImg src="{{Dominio}}/{{fique.midia.arquivos.lang_1.arquivo.url}}" style="height: 115px;"></img-loader>\n\n            </div>\n\n            <div class="Desc">\n\n              <p class="Tit" [innerHTML]="fique.nome.str.lang_1 | truncate : 35 : \'...\'" ></p>\n\n              <p class="txt" [innerHTML]="fique.chamada.str.lang_1 | truncate : 50 : \'...\'"></p>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Carregando mais dados...">></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n</ion-content>\n\n\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/home/home.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */],
        __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */]])
], HomePage);

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 26:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SharedService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SharedService = (function () {
    function SharedService(http) {
        this.http = http;
        this.checkLike = function (area, objeto) {
            // gera o nome do localStorage padrao area-like
            var localStorageName = area + "-like";
            //busca os likes da area
            var likes = JSON.parse(localStorage.getItem(localStorageName));
            if (likes == null)
                likes = [];
            //verifica se o objeto em questão ja foi curtido
            // let like
            var curtiu = __WEBPACK_IMPORTED_MODULE_3_lodash__["filter"](likes, function (like) {
                return like.cod == objeto.cod;
            });
            if (curtiu == null)
                curtiu = [];
            //retorna true se ja foi curtido e false se não
            return curtiu.length > 0;
        };
        this.setLike = function (area, objeto) {
            var localStorageName = area + "-like";
            //busca os likes da area
            var likes = JSON.parse(localStorage.getItem(localStorageName));
            //se nao existir nenhum like ele gera um array vazio
            if (likes == null)
                likes = [];
            //verifica se o objeto em questão ja foi curtido
            var curtiu = __WEBPACK_IMPORTED_MODULE_3_lodash__["filter"](likes, function (like) {
                return like.cod == objeto.cod;
            });
            if (curtiu == null)
                curtiu = [];
            //se ja curtiu ele ira descurtir, senao ira curtir
            if (curtiu.length > 0) {
                likes = __WEBPACK_IMPORTED_MODULE_3_lodash__["difference"](likes, curtiu);
            }
            else {
                likes.push(objeto);
            }
            //salva no localStorage de curtidas da area
            localStorage.setItem(localStorageName, JSON.stringify(likes));
        };
        console.log('Hello SharedServiceProvider Provider');
    }
    return SharedService;
}());
SharedService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], SharedService);

//# sourceMappingURL=shared-service.js.map

/***/ }),

/***/ 38:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiquepordentrointernaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var FiquepordentrointernaPage = (function () {
    function FiquepordentrointernaPage(navCtrl, navParams, service, socialSharing, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.SharedService = SharedService;
        this.Dominio = this.service.config.defaultDomain;
        this.novidade = this.navParams.get("novidade");
        console.log(this.novidade);
        this.vota = this.SharedService.checkLike("fiquepordentro", this.novidade);
        console.log(this.vota, 'vota');
    }
    FiquepordentrointernaPage.prototype.likeVote = function (novidade) {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("fiquepordentro", novidade);
    };
    FiquepordentrointernaPage.prototype.abrirExterno = function (link) {
        window.open(link, '_system', 'location=no');
    };
    FiquepordentrointernaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.novidade.nome.str.lang_1 + this.service.config.HashTag + ' www.topshopping.com.br', this.subject, this.Dominio + '/' + this.novidade.midia.arquivos.lang_1.arquivo.url).then(function () {
        }).catch(function () {
        });
    };
    FiquepordentrointernaPage.prototype.ionViewDidLoad = function () {
    };
    return FiquepordentrointernaPage;
}());
FiquepordentrointernaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-fiquepordentrointerna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/fiquepordentrointerna/fiquepordentrointerna.html"*/'\n\n<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton"> \n\n      <h1>Fique por dentro</h1>\n\n      \n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content >\n\n<div class="InternPadrao">\n\n  <div class="alignCenter">\n\n    <div class="titulolazer clearfix">\n\n        <h1 [innerHTML]="novidade.nome.str.lang_1"></h1>\n\n        <div class="icons">\n\n          <button ion-button (click)="likeVote(novidade)">\n\n            <img src="assets/icon/iconLikeFilmes.png"  [hidden]="vota" alt="">\n\n            <img src="assets/icon/iconLikeFilmes_ativ.png"   *ngIf="vota" alt="">\n\n          </button>\n\n          <button ion-button (click)="Compartilhar()">\n\n            <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n          </button>\n\n        </div>  \n\n      </div>\n\n      <p class="data" > {{ novidade.categorias[0].nome.str.lang_1 }} - {{novidade.data_cadastro}}</p> \n\n      <p [innerHTML]="novidade.descricao.str.lang_1"></p>\n\n      <a *ngIf="novidade.url"><p (click)=abrirExterno(novidade.url) style="color: #07C;">Saiba Mais</p></a>\n\n  </div>\n\n  <img src="{{Dominio}}/{{novidade.midia.arquivos.lang_1.arquivo.url}}" alt=""> \n\n</div>\n\n</ion-content>\n\n\n\n<app-footer selector="page-fiquepordentrointerna"></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/fiquepordentrointerna/fiquepordentrointerna.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__["a" /* SharedService */]])
], FiquepordentrointernaPage);

//# sourceMappingURL=fiquepordentrointerna.js.map

/***/ }),

/***/ 396:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BuscaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__service_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BuscaProvider = (function () {
    function BuscaProvider(http, service, events, _alertCtrl, _loadingCtrl) {
        var _this = this;
        this.http = http;
        this.service = service;
        this.events = events;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.Pagina = 0;
        this.loader = this._loadingCtrl.create({
            content: 'Aguarde...',
            dismissOnPageChange: true
        });
        this.JsonLojas();
        this.JsonGastronomia();
        this.JsonDestaque();
        this.JsonServicos();
        this.JsonLazer();
        this.JsonFilme();
        this.events.subscribe('user:Pesquisa', function (FieldBusca, page) {
            // console.log(FieldBusca, page, 'tA DENTRO DO CONSTRUCTOR')
            var auxList = [];
            var store;
            var nadaEncontrado;
            var txtBusca;
            if (FieldBusca == '') {
                _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: 'Digite o que procura.',
                    buttons: [{ text: 'Ok' }]
                }).present();
            }
            FieldBusca;
            if (_this.lojas != undefined && _this.filmes && _this.gastronomia != undefined && _this.destaque != undefined) {
                for (var z = 0; z < _this.lojas.length; z++) {
                    _this.lojas[z].categoria = "Lojas";
                    store = _this.lojas[z];
                    if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
                        auxList.push(store);
                    }
                }
                for (var z = 0; z < _this.servicos.length; z++) {
                    _this.servicos[z].categoria = "Serviços";
                    store = _this.servicos[z];
                    if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
                        auxList.push(store);
                    }
                }
                for (var z = 0; z < _this.lazer.length; z++) {
                    _this.lazer[z].categoria = "Lazer";
                    store = _this.lazer[z];
                    if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
                        auxList.push(store);
                    }
                }
                for (var v = 0; v < _this.gastronomia.length; v++) {
                    _this.gastronomia[v].categoria = "Alimentação";
                    store = _this.gastronomia[v];
                    if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
                        auxList.push(store);
                    }
                }
                for (var b = 0; b < _this.destaque.length; b++) {
                    _this.destaque[b].categoria = "Acontece";
                    store = _this.destaque[b];
                    if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
                        auxList.push(store);
                    }
                }
                for (var h = 0; h < _this.filmes.length; h++) {
                    _this.filmes[h].categoria = "Filmes";
                    store = _this.filmes[h];
                    if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
                        auxList.push(store);
                    }
                }
                if (auxList.toString() == "") {
                    console.log('veiovazio');
                    // localStorage.setItem('campoPesquisa', 'Não Encontrou '+FieldBusca);
                    nadaEncontrado = true;
                    localStorage.setItem('resultadoDaBuscaHome', JSON.stringify(auxList));
                    localStorage.setItem('campoPesquisa', FieldBusca);
                }
                else {
                    localStorage.setItem('resultadoDaBuscaHome', JSON.stringify(auxList));
                    nadaEncontrado = false;
                    localStorage.setItem('campoPesquisa', FieldBusca);
                    return true;
                }
            }
            else {
                console.log('Nao Carrego os Jsons');
                // localStorage.setItem('campoPesquisa', 'Não Encontrou '+FieldBusca);
                return false;
            }
        });
        console.log('Hello BuscaProvider Provider');
    }
    BuscaProvider.prototype.JsonLojas = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=1000").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.lojas = data.lista;
        });
    };
    BuscaProvider.prototype.JsonGastronomia = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6').map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.gastronomia = data.lista;
        });
    };
    BuscaProvider.prototype.JsonDestaque = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + 'jsonDestaquesMobile.json?nocache=' + nocache).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.destaque = data.lista;
        });
    };
    BuscaProvider.prototype.JsonLazer = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        // this.http.get(this.service.config.jsonDomain + 'jsonEntretenimento.json?nocache=' + nocache).map(res => res.json())
        this.http.get(this.service.config.defaultDomain + "/api/mobile/EntretenimentoList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.lazer = data.lista;
        });
    };
    BuscaProvider.prototype.JsonServicos = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + 'jsonservicos.json?nocache=' + nocache).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.servicos = data.lista;
        });
    };
    BuscaProvider.prototype.JsonFilme = function () {
        var _this = this;
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + "jsonfilmes.json?nocache=" + nocache).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.filmes = data.lista;
            _this.CartazLink = _this.service.config.pipocaoCartazHome;
            for (var i = 0; i < data.lista.length; i++) {
                var findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
                var changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
                data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
            }
        }, function (err) {
            console.log('deuruim lojas', err);
        });
    };
    return BuscaProvider;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["o" /* Nav */])
], BuscaProvider.prototype, "nav", void 0);
BuscaProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* Events */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* LoadingController */]])
], BuscaProvider);

//# sourceMappingURL=busca.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(420);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListFilmesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__filmes_filmes__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__preco_cinema_preco_cinema__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic2_super_tabs__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ListFilmesPage = (function () {
    function ListFilmesPage(navCtrl, navParams, http, service, superTabsCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.superTabsCtrl = superTabsCtrl;
        this.FilmesPage = __WEBPACK_IMPORTED_MODULE_2__filmes_filmes__["a" /* FilmesPage */];
        this.PrecoCinemaPage = __WEBPACK_IMPORTED_MODULE_3__preco_cinema_preco_cinema__["a" /* PrecoCinemaPage */];
    }
    ListFilmesPage.prototype.onTabSelect = function (ev) {
        console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
    };
    ListFilmesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ListFilmesPage');
    };
    return ListFilmesPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_6_ionic2_super_tabs__["a" /* SuperTabs */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic2_super_tabs__["a" /* SuperTabs */])
], ListFilmesPage.prototype, "superTabs", void 0);
ListFilmesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-list-filmes',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/list-filmes/list-filmes.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n        <img src="assets/icon/icoMenu.png" alt="">\n\n      </button>\n\n    <ion-title class="titleButton">\n\n        <h1>Cinema</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  \n\n  <super-tabs>\n\n    <super-tab  [root]="FilmesPage" title="Filmes" id="filme"></super-tab>\n\n    <super-tab [root]="PrecoCinemaPage" title="Preços" id="preco"></super-tab>\n\n  </super-tabs>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/list-filmes/list-filmes.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_6_ionic2_super_tabs__["b" /* SuperTabsController */]])
], ListFilmesPage);

//# sourceMappingURL=list-filmes.js.map

/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilmesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filmes_interna_filmes_interna__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var FilmesPage = (function () {
    function FilmesPage(navCtrl, navParams, http, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.storage = storage;
        this.searching = true;
        this.Pagina = 0;
        this.rootNavCtrl = navParams.get('rootNavCtrl');
    }
    FilmesPage.prototype.FilmesList = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/FilmesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5').map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.filmes = data.lista;
            _this.searching = false;
            _this.filmes = data.lista,
                _this.CartazLink = _this.service.config.pipocaoCartazHome;
            // console.log(this.filmes)
            // console.log(this.CartazLink)
            if (_this.filmes == 0) {
                _this.someAreaCinema = true;
            }
            else {
                _this.someAreaCinema = false;
            }
            _this.storage.set('filmes', _this.filmes);
            for (var i = 0; i < data.lista.length; i++) {
                var findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
                var changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
                data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
            }
        }, function (err) {
            console.log('deuruim lojas', err);
            _this.searching = false;
            _this.storage.get('filmes').then(function (val) {
                console.log('Your age is', val);
                _this.filmes = val;
            });
        });
    };
    FilmesPage.prototype.CountPaginacao = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Headers */]();
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/FilmesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.lista;
            for (var i = 0; i < 5; i++) {
                _this.filmes.push(_this.Data[i]);
            }
            _this.CartazLink = _this.service.config.pipocaoCartazHome;
            // console.log(this.filmes)
            // console.log(this.CartazLink)
            for (var i_1 = 0; i_1 < data.lista.length; i_1++) {
                var findPipocao = data.lista[i_1].midia.arquivos.lang_1.arquivo.url;
                var changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
                data.lista[i_1].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    FilmesPage.prototype.IrParaInterna = function (event, f) {
        this.rootNavCtrl.push(__WEBPACK_IMPORTED_MODULE_3__filmes_interna_filmes_interna__["a" /* FilmesInternaPage */], {
            f: f
        });
    };
    FilmesPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            if (_this.filmes != undefined) {
                console.log('Async operation has ended');
                _this.CountPaginacao();
                infiniteScroll.complete();
            }
            else {
                ('travo');
            }
        }, 500);
    };
    FilmesPage.prototype.ionViewDidLoad = function () {
        this.FilmesList();
        // console.log('ionViewDidLoad FilmesPage');
    };
    return FilmesPage;
}());
FilmesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-filmes',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/filmes/filmes.html"*/'<!-- <ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n          <img src="assets/icon/icoMenu.png" alt="">\n\n        </button>\n\n    <ion-title class="titleButton">\n\n      <h1>filmes</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header> -->\n\n\n\n<ion-content>\n\n\n\n  <div class="alignCenter">\n\n    <div class="subHeader">\n\n      <p class="Title">Programação</p>\n\n      <p class="Chamada">Filmes em cartaz neste momento</p>\n\n    </div>\n\n    <div *ngIf="searching" class="spinner-container">\n\n        <ion-spinner></ion-spinner>\n\n        <p>Carregando dados...</p>\n\n      </div>\n\n    <div class="HomeFique"  [hidden]="filmes==undefined" *ngFor="let f of filmes" (click)="IrParaInterna($event, f)">\n\n      <div class="areaflexHomeFique" *ngIf="f!=undefined">\n\n        <div class="Areaimg">\n\n          <img-loader useImg src="{{f.midia.arquivos.lang_1.arquivo.url}}" alt="" style="width: 100%;" ></img-loader>\n\n        </div>\n\n        <div class="Desc">\n\n          <p class="Tit" [innerHTML]="f.nome.str.lang_1"></p>\n\n          <p class="txt">{{ f.genero.str.lang_1 }} / {{f.tempo}} min</p>\n\n          <p class="txt">{{ f.censura.str.lang_1 }} Anos</p>\n\n          <p class="legendado" *ngIf="f.tipo==\'DUB\'">Dublado {{f.tecnologia}}</p>\n\n          <p class="legendado" *ngIf="f.tipo==\'LEG\'">Legendado {{f.tecnologia}}</p>\n\n          <p class="legendado" *ngIf="f.tipo==\'DUB e LEG\'">DUB e LEG {{f.tecnologia}}</p>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n  \n\n  <div class="semConexao" *ngIf="(filmes)==\'\'||filmes==\'\'">\n\n    <img src="assets/iconSmile.png" class="iconeimg" alt="">\n\n      <p class="AindaNaoFavorito">Aguarde! </p>\n\n      <p>Em breve você poderá conferir a nova programação.</p>\n\n   </div>\n\n\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n      <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Carregando mais dados..."></ion-infinite-scroll-content>\n\n    </ion-infinite-scroll>\n\n</ion-content>\n\n\n\n<!-- <app-footer></app-footer> -->'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/filmes/filmes.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], FilmesPage);

//# sourceMappingURL=filmes.js.map

/***/ }),

/***/ 420:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(492);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tutorial_tutorial__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_cadastre_cadastre__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_esquecisenha_esquecisenha__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_acontece_acontece__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_feed_feed__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_fiquepordentro_fiquepordentro__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_fiquepordentrointerna_fiquepordentrointerna__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_enquetes_enquetes__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_ofertas_ofertas__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_cupons_cupons__ = __webpack_require__(148);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_cupons_interna_cupons_interna__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_vitrine_vitrine__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_vitrine_interna_vitrine_interna__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_lojas_lojas__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_lojas_interna_lojas_interna__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_gastronomia_gastronomia__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_gastronomia_interna_gastronomia_interna__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_lazer_lazer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_lazer_interna_lazer_interna__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_list_filmes_list_filmes__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_preco_cinema_preco_cinema__ = __webpack_require__(138);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_filmes_filmes__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_filmes_interna_filmes_interna__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_oshopping_oshopping__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_servicos_servicos__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_list_shopping_list_shopping__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_listaservicos_listaservicos__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_estacionamento_estacionamento__ = __webpack_require__(151);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_servicosinterna_servicosinterna__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_favoritos_favoritos__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_horarios_horarios__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_comochegar_comochegar__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_contato_contato__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_meuperfil_meuperfil__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_notificacoes_notificacoes__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_notificacoes_interna_notificacoes_interna__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_page1_page1__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_page2_page2__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_page3_page3__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_campanha_campanha__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_campanha_cadastro_campanha_cadastro__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__pages_busca_busca__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__ionic_native_status_bar__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_50__ionic_native_splash_screen__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_51__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_52_ng2_truncate__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53_angular2_text_mask__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_53_angular2_text_mask___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_53_angular2_text_mask__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_54__ionic_native_facebook__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_55__providers_facebook_facebook__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_56__ionic_native_native_storage__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_57_ionic2_super_tabs__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_58__angular_forms__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_59__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60_angular2_useful_swiper__ = __webpack_require__(499);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_60_angular2_useful_swiper___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_60_angular2_useful_swiper__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_61__providers_busca_busca__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_62__providers_shared_service_shared_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_63__ionic_native_network__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_64__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_65__components_footer_footer__ = __webpack_require__(500);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_66__ionic_native_push__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_67__ionic_native_local_notifications__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68_angular2_moment__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_68_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_68_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_69_ionic_image_loader__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






































































var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_tutorial_tutorial__["a" /* TutorialPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_cadastre_cadastre__["a" /* CadastrePage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_esquecisenha_esquecisenha__["a" /* EsquecisenhaPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_acontece_acontece__["a" /* AcontecePage */], __WEBPACK_IMPORTED_MODULE_11__pages_feed_feed__["a" /* FeedPage */], __WEBPACK_IMPORTED_MODULE_12__pages_fiquepordentro_fiquepordentro__["a" /* FiquepordentroPage */], __WEBPACK_IMPORTED_MODULE_14__pages_enquetes_enquetes__["a" /* EnquetesPage */], __WEBPACK_IMPORTED_MODULE_13__pages_fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_ofertas_ofertas__["a" /* OfertasPage */], __WEBPACK_IMPORTED_MODULE_18__pages_vitrine_vitrine__["a" /* VitrinePage */], __WEBPACK_IMPORTED_MODULE_19__pages_vitrine_interna_vitrine_interna__["a" /* VitrineInternaPage */], __WEBPACK_IMPORTED_MODULE_16__pages_cupons_cupons__["a" /* CuponsPage */], __WEBPACK_IMPORTED_MODULE_17__pages_cupons_interna_cupons_interna__["a" /* CuponsInternaPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_lojas_lojas__["a" /* LojasPage */], __WEBPACK_IMPORTED_MODULE_21__pages_lojas_interna_lojas_interna__["a" /* LojasInternaPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_gastronomia_gastronomia__["a" /* GastronomiaPage */], __WEBPACK_IMPORTED_MODULE_23__pages_gastronomia_interna_gastronomia_interna__["a" /* GastronomiaInternaPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_lazer_lazer__["a" /* LazerPage */], __WEBPACK_IMPORTED_MODULE_25__pages_lazer_interna_lazer_interna__["a" /* LazerInternaPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_list_filmes_list_filmes__["a" /* ListFilmesPage */], __WEBPACK_IMPORTED_MODULE_28__pages_filmes_filmes__["a" /* FilmesPage */], __WEBPACK_IMPORTED_MODULE_29__pages_filmes_interna_filmes_interna__["a" /* FilmesInternaPage */], __WEBPACK_IMPORTED_MODULE_27__pages_preco_cinema_preco_cinema__["a" /* PrecoCinemaPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_oshopping_oshopping__["a" /* OshoppingPage */], __WEBPACK_IMPORTED_MODULE_32__pages_list_shopping_list_shopping__["a" /* ListShoppingPage */], __WEBPACK_IMPORTED_MODULE_37__pages_horarios_horarios__["a" /* HorariosPage */], __WEBPACK_IMPORTED_MODULE_38__pages_comochegar_comochegar__["a" /* ComochegarPage */], __WEBPACK_IMPORTED_MODULE_39__pages_contato_contato__["a" /* ContatoPage */], __WEBPACK_IMPORTED_MODULE_43__pages_page1_page1__["a" /* Page1Page */], __WEBPACK_IMPORTED_MODULE_44__pages_page2_page2__["a" /* Page2Page */], __WEBPACK_IMPORTED_MODULE_45__pages_page3_page3__["a" /* Page3Page */],
            __WEBPACK_IMPORTED_MODULE_31__pages_servicos_servicos__["a" /* ServicosPage */], __WEBPACK_IMPORTED_MODULE_33__pages_listaservicos_listaservicos__["a" /* ListaservicosPage */], __WEBPACK_IMPORTED_MODULE_34__pages_estacionamento_estacionamento__["a" /* EstacionamentoPage */], __WEBPACK_IMPORTED_MODULE_35__pages_servicosinterna_servicosinterna__["a" /* ServicosinternaPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_favoritos_favoritos__["a" /* FavoritosPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_campanha_campanha__["a" /* CampanhaPage */], __WEBPACK_IMPORTED_MODULE_47__pages_campanha_cadastro_campanha_cadastro__["a" /* CampanhaCadastroPage */],
            __WEBPACK_IMPORTED_MODULE_48__pages_busca_busca__["a" /* BuscaPage */],
            __WEBPACK_IMPORTED_MODULE_40__pages_meuperfil_meuperfil__["a" /* MeuperfilPage */],
            __WEBPACK_IMPORTED_MODULE_41__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */], __WEBPACK_IMPORTED_MODULE_42__pages_notificacoes_interna_notificacoes_interna__["a" /* NotificacoesInternaPage */],
            __WEBPACK_IMPORTED_MODULE_65__components_footer_footer__["a" /* Footer */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                mode: 'md'
            }, {
                links: [
                    { loadChildren: '../pages/preco-cinema/preco-cinema.module#PrecoCinemaPageModule', name: 'PrecoCinemaPage', segment: 'preco-cinema', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/list-filmes/list-filmes.module#ListFilmesPageModule', name: 'ListFilmesPage', segment: 'list-filmes', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/filmes-interna/filmes-interna.module#FilmesInternaPageModule', name: 'FilmesInternaPage', segment: 'filmes-interna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/filmes/filmes.module#FilmesPageModule', name: 'FilmesPage', segment: 'filmes', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/fiquepordentrointerna/fiquepordentrointerna.module#FiquepordentrointernaPageModule', name: 'FiquepordentrointernaPage', segment: 'fiquepordentrointerna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/esquecisenha/esquecisenha.module#EsquecisenhaPageModule', name: 'EsquecisenhaPage', segment: 'esquecisenha', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/cadastre/cadastre.module#CadastrePageModule', name: 'CadastrePage', segment: 'cadastre', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/campanha-cadastro/campanha-cadastro.module#CampanhaCadastroPageModule', name: 'CampanhaCadastroPage', segment: 'campanha-cadastro', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/campanha/campanha.module#CampanhaPageModule', name: 'CampanhaPage', segment: 'campanha', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/home/home.module#HomePageModule', name: 'HomePage', segment: 'home', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/feed/feed.module#FeedPageModule', name: 'FeedPage', segment: 'feed', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/fiquepordentro/fiquepordentro.module#FiquepordentroPageModule', name: 'FiquepordentroPage', segment: 'fiquepordentro', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/cupons-interna/cupons-interna.module#CuponsInternaPageModule', name: 'CuponsInternaPage', segment: 'cupons-interna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/enquetes/enquetes.module#EnquetesPageModule', name: 'EnquetesPage', segment: 'enquetes', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/acontece/acontece.module#AcontecePageModule', name: 'AcontecePage', segment: 'acontece/:type', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/vitrine-interna/vitrine-interna.module#VitrineInternaPageModule', name: 'VitrineInternaPage', segment: 'vitrine-interna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/vitrine/vitrine.module#VitrinePageModule', name: 'VitrinePage', segment: 'vitrine', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/cupons/cupons.module#CuponsPageModule', name: 'CuponsPage', segment: 'cupons', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/ofertas/ofertas.module#OfertasPageModule', name: 'OfertasPage', segment: 'ofertas', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/lojas-interna/lojas-interna.module#LojasInternaPageModule', name: 'LojasInternaPage', segment: 'lojas-interna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/lojas/lojas.module#LojasPageModule', name: 'LojasPage', segment: 'lojas', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/gastronomia-interna/gastronomia-interna.module#GastronomiaInternaPageModule', name: 'GastronomiaInternaPage', segment: 'gastronomia-interna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/gastronomia/gastronomia.module#GastronomiaPageModule', name: 'GastronomiaPage', segment: 'gastronomia', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/lazer-interna/lazer-interna.module#LazerInternaPageModule', name: 'LazerInternaPage', segment: 'lazer-interna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/lazer/lazer.module#LazerPageModule', name: 'LazerPage', segment: 'lazer', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/oshopping/oshopping.module#OshoppingPageModule', name: 'OshoppingPage', segment: 'oshopping', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/servicosinterna/servicosinterna.module#ServicosinternaPageModule', name: 'ServicosinternaPage', segment: 'servicosinterna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/listaservicos/listaservicos.module#ListaservicosPageModule', name: 'ListaservicosPage', segment: 'listaservicos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/estacionamento/estacionamento.module#EstacionamentoPageModule', name: 'EstacionamentoPage', segment: 'estacionamento', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/servicos/servicos.module#ServicosPageModule', name: 'ServicosPage', segment: 'servicos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/favoritos/favoritos.module#FavoritosPageModule', name: 'FavoritosPage', segment: 'favoritos', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/horarios/horarios.module#HorariosPageModule', name: 'HorariosPage', segment: 'horarios', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/comochegar/comochegar.module#ComochegarPageModule', name: 'ComochegarPage', segment: 'comochegar', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/contato/contato.module#ContatoPageModule', name: 'ContatoPage', segment: 'contato', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/page1/page1.module#Page1PageModule', name: 'Page1Page', segment: 'page1', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/page2/page2.module#Page2PageModule', name: 'Page2Page', segment: 'page2', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/page3/page3.module#Page3PageModule', name: 'Page3Page', segment: 'page3', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/list-shopping/list-shopping.module#ListShoppingPageModule', name: 'ListShoppingPage', segment: 'list-shopping', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/meuperfil/meuperfil.module#MeuperfilPageModule', name: 'MeuperfilPage', segment: 'meuperfil', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/notificacoes-interna/notificacoes-interna.module#NotificacoesInternaPageModule', name: 'NotificacoesInternaPage', segment: 'notificacoes-interna', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/notificacoes/notificacoes.module#NotificacoesPageModule', name: 'NotificacoesPage', segment: 'notificacoes', priority: 'low', defaultHistory: [] },
                    { loadChildren: '../pages/busca/busca.module#BuscaPageModule', name: 'BuscaPage', segment: 'busca', priority: 'low', defaultHistory: [] }
                ]
            }),
            __WEBPACK_IMPORTED_MODULE_51__angular_http__["c" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_52_ng2_truncate__["a" /* TruncateModule */],
            __WEBPACK_IMPORTED_MODULE_58__angular_forms__["FormsModule"],
            __WEBPACK_IMPORTED_MODULE_53_angular2_text_mask__["TextMaskModule"],
            __WEBPACK_IMPORTED_MODULE_57_ionic2_super_tabs__["c" /* SuperTabsModule */],
            __WEBPACK_IMPORTED_MODULE_58__angular_forms__["ReactiveFormsModule"],
            __WEBPACK_IMPORTED_MODULE_60_angular2_useful_swiper__["SwiperModule"],
            __WEBPACK_IMPORTED_MODULE_68_angular2_moment__["MomentModule"],
            __WEBPACK_IMPORTED_MODULE_64__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
            __WEBPACK_IMPORTED_MODULE_69_ionic_image_loader__["b" /* IonicImageLoader */].forRoot(),
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_5__pages_tutorial_tutorial__["a" /* TutorialPage */],
            __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
            __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_8__pages_cadastre_cadastre__["a" /* CadastrePage */],
            __WEBPACK_IMPORTED_MODULE_9__pages_esquecisenha_esquecisenha__["a" /* EsquecisenhaPage */],
            __WEBPACK_IMPORTED_MODULE_10__pages_acontece_acontece__["a" /* AcontecePage */], __WEBPACK_IMPORTED_MODULE_11__pages_feed_feed__["a" /* FeedPage */], __WEBPACK_IMPORTED_MODULE_12__pages_fiquepordentro_fiquepordentro__["a" /* FiquepordentroPage */], __WEBPACK_IMPORTED_MODULE_14__pages_enquetes_enquetes__["a" /* EnquetesPage */], __WEBPACK_IMPORTED_MODULE_13__pages_fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */],
            __WEBPACK_IMPORTED_MODULE_15__pages_ofertas_ofertas__["a" /* OfertasPage */], __WEBPACK_IMPORTED_MODULE_18__pages_vitrine_vitrine__["a" /* VitrinePage */], __WEBPACK_IMPORTED_MODULE_19__pages_vitrine_interna_vitrine_interna__["a" /* VitrineInternaPage */], __WEBPACK_IMPORTED_MODULE_16__pages_cupons_cupons__["a" /* CuponsPage */], __WEBPACK_IMPORTED_MODULE_17__pages_cupons_interna_cupons_interna__["a" /* CuponsInternaPage */],
            __WEBPACK_IMPORTED_MODULE_20__pages_lojas_lojas__["a" /* LojasPage */], __WEBPACK_IMPORTED_MODULE_21__pages_lojas_interna_lojas_interna__["a" /* LojasInternaPage */],
            __WEBPACK_IMPORTED_MODULE_22__pages_gastronomia_gastronomia__["a" /* GastronomiaPage */], __WEBPACK_IMPORTED_MODULE_23__pages_gastronomia_interna_gastronomia_interna__["a" /* GastronomiaInternaPage */],
            __WEBPACK_IMPORTED_MODULE_24__pages_lazer_lazer__["a" /* LazerPage */], __WEBPACK_IMPORTED_MODULE_25__pages_lazer_interna_lazer_interna__["a" /* LazerInternaPage */],
            __WEBPACK_IMPORTED_MODULE_26__pages_list_filmes_list_filmes__["a" /* ListFilmesPage */], __WEBPACK_IMPORTED_MODULE_28__pages_filmes_filmes__["a" /* FilmesPage */], __WEBPACK_IMPORTED_MODULE_29__pages_filmes_interna_filmes_interna__["a" /* FilmesInternaPage */], __WEBPACK_IMPORTED_MODULE_27__pages_preco_cinema_preco_cinema__["a" /* PrecoCinemaPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_list_shopping_list_shopping__["a" /* ListShoppingPage */], __WEBPACK_IMPORTED_MODULE_30__pages_oshopping_oshopping__["a" /* OshoppingPage */], __WEBPACK_IMPORTED_MODULE_37__pages_horarios_horarios__["a" /* HorariosPage */], __WEBPACK_IMPORTED_MODULE_38__pages_comochegar_comochegar__["a" /* ComochegarPage */], __WEBPACK_IMPORTED_MODULE_39__pages_contato_contato__["a" /* ContatoPage */], __WEBPACK_IMPORTED_MODULE_43__pages_page1_page1__["a" /* Page1Page */], __WEBPACK_IMPORTED_MODULE_44__pages_page2_page2__["a" /* Page2Page */], __WEBPACK_IMPORTED_MODULE_45__pages_page3_page3__["a" /* Page3Page */],
            __WEBPACK_IMPORTED_MODULE_31__pages_servicos_servicos__["a" /* ServicosPage */], __WEBPACK_IMPORTED_MODULE_33__pages_listaservicos_listaservicos__["a" /* ListaservicosPage */], __WEBPACK_IMPORTED_MODULE_34__pages_estacionamento_estacionamento__["a" /* EstacionamentoPage */], __WEBPACK_IMPORTED_MODULE_35__pages_servicosinterna_servicosinterna__["a" /* ServicosinternaPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_favoritos_favoritos__["a" /* FavoritosPage */],
            __WEBPACK_IMPORTED_MODULE_46__pages_campanha_campanha__["a" /* CampanhaPage */], __WEBPACK_IMPORTED_MODULE_47__pages_campanha_cadastro_campanha_cadastro__["a" /* CampanhaCadastroPage */],
            __WEBPACK_IMPORTED_MODULE_48__pages_busca_busca__["a" /* BuscaPage */],
            __WEBPACK_IMPORTED_MODULE_40__pages_meuperfil_meuperfil__["a" /* MeuperfilPage */],
            __WEBPACK_IMPORTED_MODULE_41__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */], __WEBPACK_IMPORTED_MODULE_42__pages_notificacoes_interna_notificacoes_interna__["a" /* NotificacoesInternaPage */],
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_49__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_50__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_54__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_57_ionic2_super_tabs__["c" /* SuperTabsModule */],
            __WEBPACK_IMPORTED_MODULE_55__providers_facebook_facebook__["a" /* FacebookService */],
            __WEBPACK_IMPORTED_MODULE_56__ionic_native_native_storage__["a" /* NativeStorage */],
            __WEBPACK_IMPORTED_MODULE_57_ionic2_super_tabs__["b" /* SuperTabsController */],
            __WEBPACK_IMPORTED_MODULE_61__providers_busca_busca__["a" /* BuscaProvider */],
            __WEBPACK_IMPORTED_MODULE_62__providers_shared_service_shared_service__["a" /* SharedService */],
            __WEBPACK_IMPORTED_MODULE_63__ionic_native_network__["a" /* Network */],
            __WEBPACK_IMPORTED_MODULE_59__ionic_native_social_sharing__["a" /* SocialSharing */],
            __WEBPACK_IMPORTED_MODULE_65__components_footer_footer__["a" /* Footer */],
            __WEBPACK_IMPORTED_MODULE_66__ionic_native_push__["a" /* Push */],
            __WEBPACK_IMPORTED_MODULE_67__ionic_native_local_notifications__["a" /* LocalNotifications */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilmesInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__filmes_filmes__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_list_filmes_list_filmes__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var FilmesInternaPage = FilmesInternaPage_1 = (function () {
    function FilmesInternaPage(navCtrl, navParams, service, _loadingCtrl, socialSharing, http, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this._loadingCtrl = _loadingCtrl;
        this.socialSharing = socialSharing;
        this.http = http;
        this.SharedService = SharedService;
        this.arrDias = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'];
        this.dataHoje = new Date();
        this.dia = this.dataHoje.getDay();
        this.diaH = this.arrDias[this.dia].valueOf();
        this.dtHoje = this.diaH + ',';
        this.FilmesPage = __WEBPACK_IMPORTED_MODULE_3__filmes_filmes__["a" /* FilmesPage */];
        this.TemIngresso = this.service.config.IngressoOnline;
        this.config = {
            slidesPerView: 3.5,
            paginationClickable: true,
            spaceBetween: 15,
            freeMode: true
        };
        this.Dominio = this.service.config.defaultDomain;
        this.Filme = this.navParams.get("f");
        console.log(this.Filme);
        this.Salas();
        this.CinemaCartazes();
        this.dtHoje;
        console.log(this.Filme);
        this.vota = this.SharedService.checkLike("cinema", this.Filme);
        console.log(this.vota, 'vota');
    }
    FilmesInternaPage.prototype.likeVote = function () {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("cinema", this.Filme);
    };
    FilmesInternaPage.prototype.Salas = function () {
        this.Filme;
        var SALA = this.Filme.salas;
        var jsonLimpo = [];
        var arrDias = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'];
        var dataHoje = new Date();
        var dia = dataHoje.getDay();
        var diaH = arrDias[dia].valueOf();
        var dtHoje = diaH + ',';
        for (var x = 0; x < SALA.length; x++) {
            if (jsonLimpo.find(function (data) { return data.sala == SALA[x].sala; }) == null) {
                jsonLimpo.push({ sala: SALA[x].sala, horario: SALA[x].horas });
                //  console.log('---------');
                //  console.log(SALA[x]);
            }
            else {
                var elem = jsonLimpo.find(function (data) { return data.sala == SALA[x].sala; });
                elem.horario = elem.horario.concat(SALA[x].horas);
            }
        }
        jsonLimpo.forEach(function (e) {
            var DiaDasemana = dtHoje;
            // console.log(DiaDasemana)
            e.horario.forEach(function (p) {
                if (p.dias == DiaDasemana) {
                    // p.hora = p.hora.split(",");
                    p.hora = p.hora.toString().split(',');
                    // console.log(p.hora);
                    // console.log(dtHoje);
                }
                ;
            });
        });
        this.listHorarios = jsonLimpo;
        // console.log(this.listHorarios, 'Listahorarios')
    };
    FilmesInternaPage.prototype.CinemaCartazes = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        this.http.get(this.service.config.jsonDomain + "jsonfilmes.json", { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Cartazeslist = data.lista,
                _this.CartazLink = _this.service.config.pipocaoCartazHome;
            for (var i = 0; i < data.lista.length; i++) {
                var findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
                var changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
                data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
            }
        }, //this.product = JSON.stringify(data),
        function (//this.product = JSON.stringify(data),
            err) {
            console.log('deuruim', err);
        });
    };
    FilmesInternaPage.prototype.Compartilhar = function () {
        var loader = this._loadingCtrl.create({
            content: 'Aguarde...',
            dismissOnPageChange: true
        });
        loader.present();
        this.socialSharing.share(this.Filme.nome.str.lang_1 + this.service.config.HashTag + ' www.topshopping.com.br', this.subject, this.Filme.midia.arquivos.lang_1.arquivo.url).then(function () {
            loader.dismiss();
        }).catch(function () {
            loader.dismiss();
        });
    };
    FilmesInternaPage.prototype.IrParaFilmes = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_list_filmes_list_filmes__["a" /* ListFilmesPage */]);
    };
    FilmesInternaPage.prototype.IrListaDeFilmes = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__filmes_filmes__["a" /* FilmesPage */]);
    };
    FilmesInternaPage.prototype.IrParaInternaDeOutroFilme = function (event, f) {
        this.navCtrl.remove(1);
        this.navCtrl.push(FilmesInternaPage_1, {
            f: f
        });
    };
    FilmesInternaPage.prototype.LinkIngresso = function () {
        window.open(this.service.config.LinkIngresso, '_system', 'location=no');
    };
    FilmesInternaPage.prototype.SlideResize = function () {
        this.slides1.resize();
    };
    FilmesInternaPage.prototype.ionViewDidLoad = function () {
    };
    return FilmesInternaPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('SlideFilmes'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["u" /* Slides */])
], FilmesInternaPage.prototype, "slides1", void 0);
FilmesInternaPage = FilmesInternaPage_1 = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-filmes-interna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/filmes-interna/filmes-interna.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton">\n\n      <h1>filmes</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n  <div class="alignCenter">\n\n    <div class="InternaFilme">\n\n      <h1>{{Filme.nome.str.lang_1}}</h1>\n\n\n\n      <div class="BaseFilme">\n\n        <div class="Cartaz">\n\n          <img src="{{Filme.midia.arquivos.lang_1.arquivo.url}}" alt="" style="width: 100%;">\n\n        </div>\n\n\n\n        <div class="InfoFilme">\n\n          <p><strong>Titulo Original:</strong><br> {{Filme.nome.str.lang_1}}</p>\n\n          <p><strong>Duração:</strong> {{Filme.tempo}} min</p>\n\n          <p><strong>Gênero:</strong> {{Filme.genero.str.lang_1}}</p>\n\n          <p><strong>Direção:</strong> {{Filme.diretor.str.lang_1}}</p>\n\n          <p><strong>Elenco:</strong> {{Filme.elenco.str.lang_1}}</p>\n\n\n\n\n\n          <div class="icons">\n\n          <button ion-button class="likecinema">\n\n            <img src="assets/icon/iconLikeFilmes.png" (click)="likeVote()" [hidden]="vota" alt="">\n\n            <img src="assets/icon/iconLikeFilmes_ativ.png"  (click)="likeVote()" *ngIf="vota" alt="">\n\n          </button>\n\n          <button ion-button (click)="Compartilhar()">\n\n            <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n          </button>\n\n\n\n          </div>\n\n        </div>\n\n      </div>\n\n\n\n\n\n      <div class="SectionHorarios">\n\n        <h1>Sessões:</h1>\n\n\n\n        <div class="listHorarios">\n\n          <div class="itemhorario" *ngFor="let l of listHorarios">\n\n            <p class="nomesala">{{l.sala}}</p>\n\n            <ng-container *ngFor="let h of l.horario">\n\n              <div class="boxall" *ngIf="h.dias == dtHoje">\n\n                <div class="boxHorario" *ngFor="let c of h.hora">{{c}} <span *ngIf="h.dublado==true">DUB</span> <span *ngIf="h.dublado==false">LEG</span> {{h.tecnologia}}</div>\n\n              </div>\n\n            </ng-container>\n\n          </div>\n\n        </div>\n\n      </div>\n\n      <div class="SectionHorarios sinopsearea">\n\n        <h1>Sinopse:</h1>\n\n        <p [innerHTML]="Filme.descricao.str.lang_1"></p>\n\n\n\n        <div class="buttonIngresso" *ngIf="TemIngresso" (click)="LinkIngresso()">\n\n          <img src="assets/icon/icoIngressoW.png" alt="">\n\n          <span>Comprar ingresso</span>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n    <div class="AreaCinemaHome">\n\n      <div class="headerTitle">\n\n        <div class="alignCenter flex">\n\n          <div class="camptitledesc">\n\n            <p class="title">\n\n              Programação\n\n            </p>\n\n            <p class="descT">Confira outros filmes em cartaz</p>\n\n          </div>\n\n          <div class="buttonVerM" (click)="IrParaFilmes()">ver mais</div>\n\n        </div>\n\n      </div>\n\n      <swiper [config]="config">\n\n        <div class="swiper-wrapper">\n\n          <div class="swiper-slide" *ngFor="let f of Cartazeslist" (click)="IrParaInternaDeOutroFilme($event, f)">\n\n              <img src="{{f.midia.arquivos.lang_1.arquivo.url}}">\n\n          </div>\n\n        </div>\n\n      </swiper>\n\n    </div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/filmes-interna/filmes-interna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_5__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_7__providers_shared_service_shared_service__["a" /* SharedService */]])
], FilmesInternaPage);

var FilmesInternaPage_1;
//# sourceMappingURL=filmes-interna.js.map

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cadastre_cadastre__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__esquecisenha_esquecisenha__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_facebook_facebook__ = __webpack_require__(72);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var LoginPage = (function () {
    function LoginPage(navCtrl, navParams, viewCtrl, _alertCtrl, _loadingCtrl, service, http, Facebook, facebookService, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.service = service;
        this.http = http;
        this.Facebook = Facebook;
        this.facebookService = facebookService;
        this.events = events;
        this.form = {
            Email: '',
            Senha: '',
            Facebook: ''
        };
        this.HomePage = __WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */];
        this.CadastrePage = __WEBPACK_IMPORTED_MODULE_3__cadastre_cadastre__["a" /* CadastrePage */];
        this.EsquecisenhaPage = __WEBPACK_IMPORTED_MODULE_5__esquecisenha_esquecisenha__["a" /* EsquecisenhaPage */];
        this.AparecerAlert = 'Apareça';
    }
    LoginPage.prototype.logar = function () {
        var _this = this;
        var loader = this._loadingCtrl.create({
            content: 'Aguarde...',
            dismissOnPageChange: true
        });
        // this.loader.present().then(() => {
        loader.present();
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "Email=" + this.form.Email + "&Senha=" + this.form.Senha + "&ClienteCod=" + this.service.config.idShopping;
        return this.http.post(this.service.config.defaultDomain + '/api/Mobile/Login', body, { headers: headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            if (data.Sucess != false) {
                _this.Logado = true;
                localStorage.setItem("cacheUsuario", JSON.stringify(data));
                localStorage.removeItem('profile');
                _this.events.publish('user:Login', data, _this.Logado);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {
                    AparecerAlert: 'Apareça',
                });
                console.log('caiuno if');
            }
            else {
                var alert_1 = _this._alertCtrl.create({
                    title: '<img src="assets/icon/error.png" /> Erro',
                    subTitle: data.descricao,
                    buttons: ['OK'],
                    cssClass: 'alertCustomCssError'
                });
                alert_1.present();
                console.log('caiu no else');
                loader.dismiss();
                return false;
            }
        }, function (err) {
            console.log("ERROR!: ", err);
            var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
            var alert = _this._alertCtrl.create({
                title: '<img src="assets/icon/error.png" /> Erro',
                subTitle: mensagem,
                buttons: ['OK'],
                cssClass: 'alertCustomCssError'
            });
            alert.present();
            loader.dismiss();
        });
        // })
    };
    LoginPage.prototype.getUserDetail = function (userid) {
        var _this = this;
        this.Facebook.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(function (res) {
            console.log(res);
            _this.InfosFb = res;
            _this.http.get(_this.service.config.defaultDomain + '/api/Mobile/VerificaFacebook?FacebookId=' + _this.InfosFb.id + '&ClienteCod=' + _this.service.config.idShopping).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                if (data.Success == true) {
                    var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/x-www-form-urlencoded'
                    });
                    var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
                    var body = "Facebook=" + _this.InfosFb.id + "&ClienteCod=" + _this.service.config.idShopping;
                    return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        _this.Logado = true;
                        localStorage.setItem("cacheUsuario", JSON.stringify(data));
                        localStorage.removeItem('profile');
                        _this.events.publish('user:Login', data, _this.Logado);
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {
                            AparecerAlert: 'Apareça'
                        });
                    });
                }
                else {
                    var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                        'Content-Type': 'application/x-www-form-urlencoded'
                    });
                    var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
                    var body = "Nome=" + _this.InfosFb.name + "&Email=" + _this.InfosFb.email + "&Facebook=" + _this.InfosFb.id + "&Origem=" + 'Aplicativo' + "&ClienteCod=" + _this.service.config.idShopping;
                    return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPAdd2', body, { headers: headers }).map(function (res) { return res.json(); })
                        .subscribe(function (data) {
                        console.log(data);
                        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                            'Content-Type': 'application/x-www-form-urlencoded'
                        });
                        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
                        var body = "Facebook=" + _this.InfosFb.id + "&ClienteCod=" + _this.service.config.idShopping;
                        return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            _this.Logado = true;
                            localStorage.setItem("cacheUsuario", JSON.stringify(data));
                            localStorage.removeItem('profile');
                            _this.events.publish('user:Login', data, _this.Logado);
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__home_home__["a" /* HomePage */], {
                                AparecerAlert: 'Apareça'
                            });
                        });
                    }, function (err) {
                        console.log("ERROR!: ", err);
                    });
                }
            });
        })
            .catch(function (e) {
            console.log(e);
        });
    };
    LoginPage.prototype.LogarFb = function () {
        var _this = this;
        this.Facebook.login(['public_profile', 'user_friends', 'email'])
            .then(function (res) {
            if (res.status === "connected") {
                _this.getUserDetail(res.authResponse.userID);
            }
            else {
            }
        })
            .catch(function (e) { return console.log('Error logging into Facebook', e); });
    };
    LoginPage.prototype.getLoginFacebook = function () {
        var loader = this._loadingCtrl.create({
            content: 'Aguarde...',
            dismissOnPageChange: true
        });
        // let infofb = JSON.parse(localStorage.getItem('profile'));
        loader.present();
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad LoginPage');    
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        this.viewCtrl.showBackButton(false);
    };
    LoginPage.prototype.ionViewWillLeave = function () {
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-login',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/login/login.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n      <img src="assets/icon/icoMenu.png" alt="">\n\n    </button>\n\n    <ion-title class="titleButton"> \n\n      <h1>login</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div class="subHeader">\n\n    <p class="Title">Entrar</p>\n\n    <p class="Chamada">Efetue seu login com os dados abaixo:</p>\n\n  </div>\n\n  <div class="FormLogin">\n\n    <ion-item class="nopaddingLeft">\n\n      <input class="input" [(ngModel)]="form.Email" placeholder="Seu e-mail" type="email" >\n\n      <!-- <ion-input class="input" [(ngModel)]="form.Email" placeholder="Seu e-mail" type="email" ></ion-input> -->\n\n    </ion-item>\n\n    <ion-item class="nopaddingLeft">\n\n      <input class="input" [(ngModel)]="form.Senha" placeholder="Sua senha" type="password">\n\n     \n\n    </ion-item>\n\n\n\n    <button ion-button class="buttonEnviar" (click)="logar()">Entrar</button>\n\n  </div>\n\n\n\n  <p class="ouEsqueciMinhaSenha" [navPush]="EsquecisenhaPage">ESQUECI MINHA SENHA</p>\n\n\n\n  <button ion-button class="ButtonloginfB" (click)="LogarFb()">\n\n    <img src="assets/icofb.png" alt="">\n\n    login com facebook</button>\n\n  <button ion-button class="ButtonCadastrese" [navPush]="CadastrePage">cadastre-se</button>\n\n  \n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/login/login.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_8__providers_facebook_facebook__["a" /* FacebookService */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_7__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_facebook__["a" /* Facebook */],
        __WEBPACK_IMPORTED_MODULE_8__providers_facebook_facebook__["a" /* FacebookService */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LojasInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LojasInternaPage = (function () {
    function LojasInternaPage(navCtrl, navParams, service, socialSharing, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.SharedService = SharedService;
        this.Dominio = this.service.config.defaultDomain;
        this.loja = this.navParams.get("loja");
        console.log(this.loja);
        this.vota = this.SharedService.checkLike("lojas", this.loja);
        console.log(this.vota, 'vota');
    }
    LojasInternaPage.prototype.likeVote = function (loja) {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("lojas", loja);
    };
    LojasInternaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.loja.nome.str.lang_1 + this.service.config.HashTag + ' www.topshopping.com.br', this.subject, this.loja.midia.arquivos.lang_1.arquivo.url).then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
            console.log('caiu no erro');
        });
    };
    LojasInternaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LojasInternaPage');
    };
    return LojasInternaPage;
}());
LojasInternaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-lojas-interna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lojas-interna/lojas-interna.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton">\n\n      <h1>Loja</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <div class="logo-store" *ngIf="loja.midia.arquivos.lang_1.arquivo.url!=\'\'">\n\n      <div class="logo">\n\n        <img src="{{loja.midia.arquivos.lang_1.arquivo.url}}" />\n\n      </div>\n\n    </div>\n\n    <div class="alignCenter">\n\n        \n\n      <div class="titulolazer clearfix">\n\n        <h1 [innerHTML]="loja.nome.str.lang_1"></h1>\n\n        <div class="icons">\n\n          \n\n          <button ion-button >\n\n            <img src="assets/icon/iconLikeFilmes.png" (click)="likeVote(loja)" [hidden]="vota" alt="">\n\n            <img src="assets/icon/iconLikeFilmes_ativ.png"  (click)="likeVote(loja)" *ngIf="vota" alt="">\n\n          </button>\n\n          <button ion-button (click)="Compartilhar()">\n\n          <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n        </button>\n\n        </div>\n\n      </div>\n\n\n\n      <p class="TitleInfo bold">{{loja.categorias[0].nome.str.lang_1}}</p>\n\n\n\n      <p class="othersTextos" [hidden]="loja.telefones[0].valor==\'\'">{{loja.telefones[0].nome.str.lang_1}} {{loja.telefones[0].valor}}</p>\n\n      <!-- <p class="othersTextos" [hidden]="loja.emails[0].valor==\'\'||loja.emails[0]==undefined">E-mail: {{loja.emails[0}}</p> -->\n\n      <p class="othersTextos" [hidden]="!loja.url[0].url">Site: {{ loja.url[0].url }}</p>\n\n      <p class="othersTextos" *ngIf="loja.lucs!=0">{{loja.lucs[0].piso.nome.str.lang_1}} </p>\n\n      <p class="othersTextos description" [innerHTML]="loja.descricao.str.lang_1"></p>\n\n\n\n      <img *ngIf="loja.midia.arquivos.lang_1.arquivo_lista!=undefined" src="{{loja.midia.arquivos.lang_1.arquivo_list}}" alt="">\n\n  </div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lojas-interna/lojas-interna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__["a" /* SharedService */]])
], LojasInternaPage);

//# sourceMappingURL=lojas-interna.js.map

/***/ }),

/***/ 46:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GastronomiaInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GastronomiaInternaPage = (function () {
    function GastronomiaInternaPage(navCtrl, navParams, service, socialSharing, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.SharedService = SharedService;
        this.Dominio = this.service.config.defaultDomain;
        this.gastronomia = this.navParams.get("gastr");
        console.log(this.gastronomia);
        this.vota = this.SharedService.checkLike("gastronomia", this.gastronomia);
        console.log(this.vota, 'vota');
    }
    GastronomiaInternaPage.prototype.likeVote = function (gastronomia) {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("gastronomia", gastronomia);
    };
    GastronomiaInternaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GastronomiaInternaPage');
    };
    GastronomiaInternaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.gastronomia.nome.str.lang_1 + this.service.config.HashTag + ' www.topshopping.com.br', this.subject, this.gastronomia.midia.arquivos.lang_1.arquivo.url).then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
        });
    };
    return GastronomiaInternaPage;
}());
GastronomiaInternaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-gastronomia-interna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/gastronomia-interna/gastronomia-interna.html"*/'\n\n<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton"> \n\n      <h1>alimentação</h1>\n\n      \n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <div class="logo-store" *ngIf="gastronomia.midia.arquivos.lang_1.arquivo.url!=\'\'">\n\n    <div class="logo">\n\n      <img src="{{gastronomia.midia.arquivos.lang_1.arquivo.url}}"/>\n\n    </div>\n\n</div>\n\n<div class="alignCenter">\n\n  <div class="titulolazer clearfix">\n\n    <h1 [innerHTML]="gastronomia.nome.str.lang_1"></h1>\n\n    <div class="icons">\n\n      <button>\n\n        <img src="assets/icon/iconLikeFilmes.png" (click)="likeVote(gastronomia)" [hidden]="vota" alt="">\n\n        <img src="assets/icon/iconLikeFilmes_ativ.png"  (click)="likeVote(gastronomia)" *ngIf="vota" alt="">\n\n      </button>\n\n        <button (click)="Compartilhar()">\n\n        <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n      </button>\n\n\n\n    </div>\n\n  </div>\n\n\n\n  <p class="TitleInfo bold">{{gastronomia.categorias[0].nome.str.lang_1}}</p>\n\n\n\n  <p class="othersTextos" [hidden]="gastronomia.telefones[0].valor==\'\'">{{gastronomia.telefones[0].nome.str.lang_1}} {{gastronomia.telefones[0].valor}}</p>\n\n  <!-- <p class="othersTextos" [hidden]="loja.emails[0].valor==\'\'||loja.emails[0]==undefined">E-mail: {{loja.emails[0}}</p> -->\n\n  <p class="othersTextos" [hidden]="!gastronomia.url[0].url">Site: {{ gastronomia.url[0].url }}</p>\n\n  <p class="othersTextos" *ngIf="gastronomia.lucs.length>0">{{gastronomia.lucs[0].piso.nome.str.lang_1}} </p>\n\n  <p class="othersTextos" [innerHTML]="gastronomia.descricao.str.lang_1"></p>\n\n\n\n  <img *ngIf="gastronomia.midia.arquivos.lang_1.arquivo_lista!=undefined" src="{{gastronomia.midia.arquivos.lang_1.arquivo_list}}" alt="">\n\n</div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>\n\n\n\n\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/gastronomia-interna/gastronomia-interna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__["a" /* SharedService */]])
], GastronomiaInternaPage);

//# sourceMappingURL=gastronomia-interna.js.map

/***/ }),

/***/ 462:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 222,
	"./af.js": 222,
	"./ar": 223,
	"./ar-dz": 224,
	"./ar-dz.js": 224,
	"./ar-kw": 225,
	"./ar-kw.js": 225,
	"./ar-ly": 226,
	"./ar-ly.js": 226,
	"./ar-ma": 227,
	"./ar-ma.js": 227,
	"./ar-sa": 228,
	"./ar-sa.js": 228,
	"./ar-tn": 229,
	"./ar-tn.js": 229,
	"./ar.js": 223,
	"./az": 230,
	"./az.js": 230,
	"./be": 231,
	"./be.js": 231,
	"./bg": 232,
	"./bg.js": 232,
	"./bm": 233,
	"./bm.js": 233,
	"./bn": 234,
	"./bn.js": 234,
	"./bo": 235,
	"./bo.js": 235,
	"./br": 236,
	"./br.js": 236,
	"./bs": 237,
	"./bs.js": 237,
	"./ca": 238,
	"./ca.js": 238,
	"./cs": 239,
	"./cs.js": 239,
	"./cv": 240,
	"./cv.js": 240,
	"./cy": 241,
	"./cy.js": 241,
	"./da": 242,
	"./da.js": 242,
	"./de": 243,
	"./de-at": 244,
	"./de-at.js": 244,
	"./de-ch": 245,
	"./de-ch.js": 245,
	"./de.js": 243,
	"./dv": 246,
	"./dv.js": 246,
	"./el": 247,
	"./el.js": 247,
	"./en-au": 248,
	"./en-au.js": 248,
	"./en-ca": 249,
	"./en-ca.js": 249,
	"./en-gb": 250,
	"./en-gb.js": 250,
	"./en-ie": 251,
	"./en-ie.js": 251,
	"./en-nz": 252,
	"./en-nz.js": 252,
	"./eo": 253,
	"./eo.js": 253,
	"./es": 254,
	"./es-do": 255,
	"./es-do.js": 255,
	"./es-us": 256,
	"./es-us.js": 256,
	"./es.js": 254,
	"./et": 257,
	"./et.js": 257,
	"./eu": 258,
	"./eu.js": 258,
	"./fa": 259,
	"./fa.js": 259,
	"./fi": 260,
	"./fi.js": 260,
	"./fo": 261,
	"./fo.js": 261,
	"./fr": 262,
	"./fr-ca": 263,
	"./fr-ca.js": 263,
	"./fr-ch": 264,
	"./fr-ch.js": 264,
	"./fr.js": 262,
	"./fy": 265,
	"./fy.js": 265,
	"./gd": 266,
	"./gd.js": 266,
	"./gl": 267,
	"./gl.js": 267,
	"./gom-latn": 268,
	"./gom-latn.js": 268,
	"./gu": 269,
	"./gu.js": 269,
	"./he": 270,
	"./he.js": 270,
	"./hi": 271,
	"./hi.js": 271,
	"./hr": 272,
	"./hr.js": 272,
	"./hu": 273,
	"./hu.js": 273,
	"./hy-am": 274,
	"./hy-am.js": 274,
	"./id": 275,
	"./id.js": 275,
	"./is": 276,
	"./is.js": 276,
	"./it": 277,
	"./it.js": 277,
	"./ja": 278,
	"./ja.js": 278,
	"./jv": 279,
	"./jv.js": 279,
	"./ka": 280,
	"./ka.js": 280,
	"./kk": 281,
	"./kk.js": 281,
	"./km": 282,
	"./km.js": 282,
	"./kn": 283,
	"./kn.js": 283,
	"./ko": 284,
	"./ko.js": 284,
	"./ky": 285,
	"./ky.js": 285,
	"./lb": 286,
	"./lb.js": 286,
	"./lo": 287,
	"./lo.js": 287,
	"./lt": 288,
	"./lt.js": 288,
	"./lv": 289,
	"./lv.js": 289,
	"./me": 290,
	"./me.js": 290,
	"./mi": 291,
	"./mi.js": 291,
	"./mk": 292,
	"./mk.js": 292,
	"./ml": 293,
	"./ml.js": 293,
	"./mr": 294,
	"./mr.js": 294,
	"./ms": 295,
	"./ms-my": 296,
	"./ms-my.js": 296,
	"./ms.js": 295,
	"./my": 297,
	"./my.js": 297,
	"./nb": 298,
	"./nb.js": 298,
	"./ne": 299,
	"./ne.js": 299,
	"./nl": 300,
	"./nl-be": 301,
	"./nl-be.js": 301,
	"./nl.js": 300,
	"./nn": 302,
	"./nn.js": 302,
	"./pa-in": 303,
	"./pa-in.js": 303,
	"./pl": 304,
	"./pl.js": 304,
	"./pt": 305,
	"./pt-br": 306,
	"./pt-br.js": 306,
	"./pt.js": 305,
	"./ro": 307,
	"./ro.js": 307,
	"./ru": 308,
	"./ru.js": 308,
	"./sd": 309,
	"./sd.js": 309,
	"./se": 310,
	"./se.js": 310,
	"./si": 311,
	"./si.js": 311,
	"./sk": 312,
	"./sk.js": 312,
	"./sl": 313,
	"./sl.js": 313,
	"./sq": 314,
	"./sq.js": 314,
	"./sr": 315,
	"./sr-cyrl": 316,
	"./sr-cyrl.js": 316,
	"./sr.js": 315,
	"./ss": 317,
	"./ss.js": 317,
	"./sv": 318,
	"./sv.js": 318,
	"./sw": 319,
	"./sw.js": 319,
	"./ta": 320,
	"./ta.js": 320,
	"./te": 321,
	"./te.js": 321,
	"./tet": 322,
	"./tet.js": 322,
	"./th": 323,
	"./th.js": 323,
	"./tl-ph": 324,
	"./tl-ph.js": 324,
	"./tlh": 325,
	"./tlh.js": 325,
	"./tr": 326,
	"./tr.js": 326,
	"./tzl": 327,
	"./tzl.js": 327,
	"./tzm": 328,
	"./tzm-latn": 329,
	"./tzm-latn.js": 329,
	"./tzm.js": 328,
	"./uk": 330,
	"./uk.js": 330,
	"./ur": 331,
	"./ur.js": 331,
	"./uz": 332,
	"./uz-latn": 333,
	"./uz-latn.js": 333,
	"./uz.js": 332,
	"./vi": 334,
	"./vi.js": 334,
	"./x-pseudo": 335,
	"./x-pseudo.js": 335,
	"./yo": 336,
	"./yo.js": 336,
	"./zh-cn": 337,
	"./zh-cn.js": 337,
	"./zh-hk": 338,
	"./zh-hk.js": 338,
	"./zh-tw": 339,
	"./zh-tw.js": 339
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 462;

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LazerInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LazerInternaPage = (function () {
    function LazerInternaPage(navCtrl, navParams, service, socialSharing, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.SharedService = SharedService;
        this.Dominio = this.service.config.defaultDomain;
        this.firstParam = this.navParams.get("c");
        console.log(this.firstParam);
        this.vota = this.SharedService.checkLike("entretenimento", this.firstParam);
        console.log(this.vota, 'vota');
    }
    LazerInternaPage.prototype.likeVote = function (lazer) {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("entretenimento", lazer);
    };
    LazerInternaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.firstParam.nome.str.lang_1 + this.service.config.HashTag + ' www.topshopping.com.br', this.firstParam, this.firstParam.midia.arquivos.lang_1.arquivo_list[0].url).then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
        });
    };
    LazerInternaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LazerInternaPage');
    };
    return LazerInternaPage;
}());
LazerInternaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-lazer-interna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lazer-interna/lazer-interna.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton"> \n\n      <h1>lazer</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="scrolllazer">\n\n  <div class="alignCenter">\n\n    \n\n  <div class="InternPadrao" >\n\n      <div class="titulo-vitrine clearfix">\n\n          <h1>{{ firstParam.nome.str.lang_1 }}</h1>\n\n          \n\n          <div class="icons">\n\n      \n\n              \n\n            <button ion-button >\n\n              <img src="assets/icon/iconLikeFilmes.png" (click)="likeVote(firstParam)" [hidden]="vota" alt="">\n\n              <img src="assets/icon/iconLikeFilmes_ativ.png"  (click)="likeVote(firstParam)" *ngIf="vota" alt="">\n\n            </button>\n\n            <button ion-button  (click)="Compartilhar()">\n\n            <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n          </button>\n\n          </div>\n\n        </div>\n\n        <!-- <img [hidden]="firstParam.midia.arquivos.lang_1.arquivo.mine_type==\'unknown/unknown\'" src="{{firstParam.midia.arquivos.lang_1.arquivo.url}}">\n\n        <img *ngIf="firstParam.midia.arquivos.lang_1.arquivo.mine_type==\'unknown/unknown\'" src="{{firstParam.midia.arquivos.lang_1.arquivo_list[0].url}}"> -->\n\n        <img [hidden]="firstParam.midia.arquivos.lang_1.arquivo_list.length!=0" src="{{firstParam.midia.arquivos.lang_1.arquivo.url}}">\n\n        <img *ngIf="firstParam.midia.arquivos.lang_1.arquivo_list.length!=0" src="{{firstParam.midia.arquivos.lang_1.arquivo_list[0].url}}">\n\n    <p [innerHTML]="firstParam.descricao.str.lang_1"></p>\n\n    \n\n    </div>\n\n  </div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lazer-interna/lazer-interna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__["a" /* SharedService */]])
], LazerInternaPage);

//# sourceMappingURL=lazer-interna.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicosinternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ServicosinternaPage = (function () {
    function ServicosinternaPage(navCtrl, navParams, service, socialSharing, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.SharedService = SharedService;
        this.Dominio = this.service.config.defaultDomain;
        this.servico = this.navParams.get("s");
        console.log(this.servico);
        this.vota = this.SharedService.checkLike("servico", this.servico);
        console.log(this.vota, 'vota');
    }
    ServicosinternaPage.prototype.likeVote = function (servico) {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("servico", servico);
    };
    ServicosinternaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.servico.nome.str.lang_1, 'Aplicativo Partage São Gonçalo').then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
        });
    };
    ServicosinternaPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ServicosinternaPage');
    };
    return ServicosinternaPage;
}());
ServicosinternaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-servicosinterna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/servicosinterna/servicosinterna.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton">\n\n      <h1>serviços</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <div class="center">\n\n    <div class="titulolazer clearfix">\n\n      <div class="iconServico">\n\n          <img src="{{Dominio}}/{{servico.midia.arquivos.lang_1.arquivo.url}}" alt=""></div>\n\n      <h1 [innerHTML]="servico.nome.str.lang_1"></h1>\n\n\n\n      <div class="icons">\n\n        <button ion-button >\n\n          <img src="assets/icon/iconLikeFilmes.png" (click)="likeVote(servico)" [hidden]="vota" alt="">\n\n          <img src="assets/icon/iconLikeFilmes_ativ.png"  (click)="likeVote(servico)" *ngIf="vota" alt="">\n\n        </button>\n\n        <button ion-button  (click)="Compartilhar()">\n\n          <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n        </button>\n\n        </div>\n\n    </div>\n\n\n\n\n\n    <p class="othersTextos" [hidden]="servico.telefones[0].valor==null">{{servico.telefones[0].nome.str.lang_1}} {{servico.telefones[0].valor}}</p>\n\n    <!-- <p class="othersTextos" *ngIf="servico.lucs!=0">{{servico.lucs[0].piso.nome.str.lang_1}} </p> -->\n\n    <p class="othersTextos" [innerHTML]="servico.descricao.str.lang_1"></p>\n\n\n\n    <!-- <img class="FotoServico" *ngIf="servico.midia.arquivos.lang_1.arquivo_list!=\'\'" src="{{Dominio}}/{{servico.midia.arquivos.lang_1.arquivo_list[0].url}}" alt=""> -->\n\n  </div>\n\n</ion-content>\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/servicosinterna/servicosinterna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__["a" /* SharedService */]])
], ServicosinternaPage);

//# sourceMappingURL=servicosinterna.js.map

/***/ }),

/***/ 492:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(394);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_tutorial_tutorial__ = __webpack_require__(143);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_login_login__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_acontece_acontece__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_ofertas_ofertas__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_lojas_lojas__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_gastronomia_gastronomia__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_lazer_lazer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_list_filmes_list_filmes__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_servicos_servicos__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_favoritos_favoritos__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_list_shopping_list_shopping__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_meuperfil_meuperfil__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_notificacoes_notificacoes__ = __webpack_require__(159);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_notificacoes_interna_notificacoes_interna__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_busca_busca__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__providers_shared_service_shared_service__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__providers_busca_busca__ = __webpack_require__(396);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_app_availability__ = __webpack_require__(493);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_in_app_browser__ = __webpack_require__(494);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_network__ = __webpack_require__(397);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_push__ = __webpack_require__(398);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_local_notifications__ = __webpack_require__(399);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30_ionic_image_loader__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





















// import { PushProvider } from '../providers/push/push';










var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, service, appAvailability, iab, events, busca, sharedService, sanitization, _alertCtrl, network, element, cdRef, push, http, imageLoaderConfig, localNotifications
        // private localNotification: PhonegapLocalNotification
    ) {
        var _this = this;
        this.platform = platform;
        this.service = service;
        this.appAvailability = appAvailability;
        this.iab = iab;
        this.events = events;
        this.busca = busca;
        this.sharedService = sharedService;
        this.sanitization = sanitization;
        this._alertCtrl = _alertCtrl;
        this.network = network;
        this.element = element;
        this.cdRef = cdRef;
        this.push = push;
        this.http = http;
        this.imageLoaderConfig = imageLoaderConfig;
        this.localNotifications = localNotifications;
        // @ViewChild('Like') likey: ElementRef;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_tutorial_tutorial__["a" /* TutorialPage */];
        this.HomePage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.LoginPage = __WEBPACK_IMPORTED_MODULE_6__pages_login_login__["a" /* LoginPage */];
        this.OfertasPage = __WEBPACK_IMPORTED_MODULE_8__pages_ofertas_ofertas__["a" /* OfertasPage */];
        this.AcontecePage = __WEBPACK_IMPORTED_MODULE_7__pages_acontece_acontece__["a" /* AcontecePage */];
        this.LojasPage = __WEBPACK_IMPORTED_MODULE_9__pages_lojas_lojas__["a" /* LojasPage */];
        this.GastronomiaPage = __WEBPACK_IMPORTED_MODULE_10__pages_gastronomia_gastronomia__["a" /* GastronomiaPage */];
        this.LazerPage = __WEBPACK_IMPORTED_MODULE_11__pages_lazer_lazer__["a" /* LazerPage */];
        this.ListFilmesPage = __WEBPACK_IMPORTED_MODULE_12__pages_list_filmes_list_filmes__["a" /* ListFilmesPage */];
        this.ServicosPage = __WEBPACK_IMPORTED_MODULE_13__pages_servicos_servicos__["a" /* ServicosPage */];
        this.FavoritosPage = __WEBPACK_IMPORTED_MODULE_14__pages_favoritos_favoritos__["a" /* FavoritosPage */];
        this.ListShoppingPage = __WEBPACK_IMPORTED_MODULE_15__pages_list_shopping_list_shopping__["a" /* ListShoppingPage */];
        this.MeuperfilPage = __WEBPACK_IMPORTED_MODULE_16__pages_meuperfil_meuperfil__["a" /* MeuperfilPage */];
        this.NotificacoesPage = __WEBPACK_IMPORTED_MODULE_17__pages_notificacoes_notificacoes__["a" /* NotificacoesPage */];
        this.disconnect = false;
        this.frasemenu = this.service.config.FraseMenu;
        this.page = __WEBPACK_IMPORTED_MODULE_19__pages_busca_busca__["a" /* BuscaPage */];
        this.VerificaLogin();
        imageLoaderConfig.enableSpinner(true);
        // set the maximum concurrent connections to 10
        imageLoaderConfig.setConcurrency(10000);
        this.network.onDisconnect().subscribe(function () {
            _this.disconnect = true;
            _this.onDisconnect();
        });
        // this.sanitization=sanitization;
        var Primeiravez = JSON.parse(localStorage.getItem('Primeiravez'));
        if (Primeiravez == 'true' || Primeiravez != undefined) {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        }
        else {
            this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_tutorial_tutorial__["a" /* TutorialPage */];
        }
        this.events.subscribe('user:Login', function (data, Logado, AparecerAlert) {
            _this.Usuario = data,
                _this.Logado = Logado,
                _this.infoPerfil();
            console.log(_this.Logado, 'status logado');
            //LOGIN PELO FACEBOOK CAI NO 3
            if (_this.Usuario.fbUsuario != null && _this.Usuario.fotoUsuario != null) {
                _this.ModeFace = false;
                _this.ModeFace2 = false;
                _this.ModoFace3 = false;
                console.log("caiu 1");
            }
            else if (_this.Usuario.fbUsuario == null && _this.Usuario.fotoUsuario == null) {
                _this.ModeFace = false;
                _this.ModeFace2 = true;
                _this.ModoFace3 = true;
                console.log("caiu 2");
            }
            else if (_this.Usuario.fbUsuario != null && _this.Usuario.fotoUsuario == null) {
                _this.ModeFace = true;
                _this.ModeFace2 = false;
                _this.ModoFace3 = true;
                console.log("caiu 3");
            }
            else if (_this.Usuario.fbUsuario == null && _this.Usuario.fotoUsuario != null) {
                _this.ModeFace = false;
                _this.ModeFace2 = false;
                _this.ModoFace3 = false;
                console.log("caiu 4");
            }
            _this.MsgsNaoLidas();
            setInterval(function () {
                _this.MsgsNaoLidas();
            }, 5000);
            _this.showAlertLoginSucesso();
            _this.initPushNotification();
            // this.AtrelandoUsuarioAoPush()
        });
        this.events.subscribe('FotoPerfil', function (response) {
            _this.infoPerfil();
        });
        platform.ready().then(function () {
            _this.initPushNotification();
            splashScreen.hide();
            // this.push;
            if (_this.platform.is('ios')) {
                statusBar.hide();
            }
            else {
                statusBar.show();
            }
        });
        this.events.subscribe('user:MensagemLida', function (MsgsList) {
            _this.MsgsNaoLidas();
        });
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        this.VerificaLogin();
        if (Token) {
            // this.MsgsNaoLidas() 
            this.infoPerfil();
            setInterval(function () {
                _this.MsgsNaoLidas();
            }, 5000);
        }
        else {
            console.log('To Deslogado');
        }
    }
    MyApp.prototype.Deslogar = function (page) {
        var _this = this;
        localStorage.removeItem('cacheUsuario');
        this.events.unsubscribe('user:Login', function (data, Logado) {
            console.log('Login', data, Logado);
            _this.Usuario = data,
                _this.Logado = Logado;
        });
        this.Logado = false;
        this.nav.setRoot(page);
    };
    MyApp.prototype.initPushNotification = function () {
        var _this = this;
        if (!this.platform.is('cordova')) {
            console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
            return;
        }
        var options = {
            android: {
                senderID: this.service.config.SenderID
            },
            ios: {
                alert: 'true',
                badge: false,
                sound: 'true'
            },
            windows: {}
        };
        var pushObject = this.push.init(options);
        pushObject.on('registration').subscribe(function (data) {
            console.log('device token -> ' + data.registrationId);
            localStorage.setItem('Token', JSON.stringify(data.registrationId));
            var plataforma = '';
            if (navigator.userAgent.match(/Android/i)) {
                plataforma = 'android';
            }
            else {
                plataforma = 'ios';
            }
            // this.http.get(this.service.config.defaultDomain + '/Admin/PushRegister/Index/'+this.service.config.idShopping+'?token='+data.registrationId+'&plataforma='+plataforma).map(res => res.json())
            if (!_this.Usuario) {
                _this.http.get(_this.service.config.defaultDomain + ':128/Admin/PushRegister/Index/' + _this.service.config.idShopping + '?token=' + data.registrationId + '&plataforma=' + plataforma).map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    console.log(data, "retorno do token");
                }, function (err) {
                    // stop disconnect watch
                    console.log('deuruim', err);
                });
            }
            else {
                _this.http.get(_this.service.config.defaultDomain + ':128/Admin/PushRegister/Index/' + _this.service.config.idShopping + '?token=' + data.registrationId + '&plataforma=' + plataforma + '&usuarioid=' + _this.Usuario.codUsuario).map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    console.log(data, "retorno do token");
                }, function (err) {
                    // stop disconnect watch
                    console.log('deuruim', err);
                });
            }
        });
        pushObject.on('notification').subscribe(function (notificacao) {
            _this.Token = JSON.parse(localStorage.getItem('cacheUsuario'));
            console.log('message -> ' + notificacao);
            [];
            if (notificacao.additionalData.foreground == true) {
                _this.localNotifications.schedule({
                    title: _this.service.config.name,
                    text: notificacao.message,
                    icon: 'ic_notifications',
                    smallIcon: 'ic_notification_small',
                    id: notificacao.additionalData.MensagemCod
                });
                _this.localNotifications.on("click", function (notification, state) {
                    if (!_this.Token) {
                        _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */]);
                    }
                    else {
                        console.log('clique do push', notification, state);
                        _this.http.get(_this.service.config.defaultDomain + "/api/mobile/MensagensList?ClienteCod=" + _this.service.config.idShopping + '&MensagemCod=' + notification.id, { "headers": new __WEBPACK_IMPORTED_MODULE_27__angular_http__["a" /* Headers */]({ 'Authorization': _this.Token.Token }) }).map(function (res) { return res.json(); })
                            .subscribe(function (data) {
                            _this.MensagemInterna = data.Mensagens[0];
                            // this.navCtrl.remove(1)
                            console.log(data, 'InternaNotificacao');
                            _this.nav.push(__WEBPACK_IMPORTED_MODULE_18__pages_notificacoes_interna_notificacoes_interna__["a" /* NotificacoesInternaPage */], {
                                msgInter: _this.MensagemInterna
                            });
                        });
                    }
                });
            }
            else if (notificacao.additionalData.foreground == false) {
                // if(!this.Token){
                _this.http.get(_this.service.config.defaultDomain + "/api/mobile/MensagensList?ClienteCod=" + _this.service.config.idShopping + '&MensagemCod=' + notificacao.additionalData.MensagemCod, { "headers": new __WEBPACK_IMPORTED_MODULE_27__angular_http__["a" /* Headers */]({ 'Authorization': _this.Token.Token }) }).map(function (res) { return res.json(); })
                    .subscribe(function (data) {
                    _this.MensagemInterna = data.Mensagens[0];
                    console.log(data, 'InternaNotificacao');
                    _this.nav.push(__WEBPACK_IMPORTED_MODULE_18__pages_notificacoes_interna_notificacoes_interna__["a" /* NotificacoesInternaPage */], {
                        msgInter: _this.MensagemInterna
                    });
                    console.log('Push notification clicked', _this.MensagemInterna);
                });
                // }
            }
        });
        pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin' + error); });
    };
    MyApp.prototype.onDisconnect = function () {
        console.log('Sem conexão com internet. O conteúdo mais recente não poderá ser exibido. :-(');
        this._alertCtrl.create({
            // title: this.service.config.name,
            // subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
            // buttons: [{ text: 'Ok' }]
            title: '<img src="assets/icon/error.png" />' + this.service.config.name,
            subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
            buttons: ['OK'],
            cssClass: 'alertCustomCssError'
        }).present();
        this.VerificaLogin();
    };
    MyApp.prototype.showAlertLoginSucesso = function () {
        var alert = this._alertCtrl.create({
            title: '<img src="assets/icon/confirmation.png" /> Bem Vindo!',
            subTitle: 'Olá ' + this.Usuario.nomeUsuario + ', você está logado!',
            buttons: ['OK'],
            cssClass: 'alertCustomCss'
        });
        alert.present();
    };
    MyApp.prototype.VerificaLogin = function () {
        if (this.service.isLogged()) {
            this.Logado = true;
            this.Usuario = JSON.parse(window.localStorage.getItem('cacheUsuario'));
        }
        else {
            this.Logado = false;
        }
    };
    MyApp.prototype.pushPage = function (page) {
        this.nav.push(page).then(function (response) {
            console.log(response);
        }).catch(function (e) {
            console.log(e);
        });
    };
    MyApp.prototype.setPage = function (page) {
        var _this = this;
        this.nav.setRoot(page).then(function (response) {
            console.log(response);
            // this.events.publish('user:pagesClass', page, this.ClassAcontece);
            _this.Filtro = false;
        }).catch(function (e) {
            console.log(e);
        });
    };
    MyApp.prototype.setPage2 = function (page) {
        var _this = this;
        this.nav.setRoot(page).then(function (response) {
            // localStorage.setItem('1', 'contato')
            console.log(response);
            // this.events.publish('user:pagesClass', page, this.ClassAcontece);
            _this.events.publish('user:Pagina', 1);
            _this.Filtro = false;
        }).catch(function (e) {
            console.log(e);
        });
    };
    MyApp.prototype.CheckForApp = function (app) {
        // let app;
        console.log(app);
        if (this.platform.is('ios')) {
            if (app == 'facebook') {
                this.fbCheck('fb://', 'fb://profile/566270846722846');
            }
            else if (app == 'instagram') {
                this.instaCheck('instagram://');
            }
            else if (app == 'twitter') {
                this.twitterCheck('twitter://');
            }
            else if (app == 'youtube') {
                this.youtubeCheck('youtube://');
            }
            else if (app == "topshopping") {
                window.open('http://www.ehtopshopping.com.br/', '_system', 'location=no');
            }
        }
        else if (this.platform.is('android')) {
            if (app == 'facebook') {
                this.fbCheck('com.facebook.katana', 'fb://page/' + this.service.config.IdFacebook);
            }
            else if (app == 'instagram') {
                this.instaCheck('com.instagram.android');
            }
            else if (app == 'twitter') {
                this.twitterCheck('com.twitter.android');
            }
            else if (app == 'youtube') {
                this.youtubeCheck('com.youtube');
            }
            else if (app == "topshopping") {
                window.open('http://www.ehtopshopping.com.br/', '_system', 'location=no');
            }
        }
    };
    MyApp.prototype.fbCheck = function (facebook, urlscheme) {
        var _this = this;
        this.appAvailability.check(facebook)
            .then(function (yes) {
            console.log(facebook + ' is available');
            window.open(urlscheme, '_system', 'location=no');
        }, function (no) {
            console.log(facebook + ' is NOT available');
            window.open('https://www.facebook.com/' + _this.service.config.NomePagina, '_system', 'location=no');
            console.log("náo tem instalado");
        });
    };
    MyApp.prototype.instaCheck = function (instagram) {
        var _this = this;
        this.appAvailability.check(instagram)
            .then(function (yes) {
            console.log(instagram + ' is available');
            _this.appExistStatus = instagram + 'is available';
            window.open('instagram://user?username=' + _this.service.config.usernameInstagram, '_system', 'location=no');
        }, function (no) {
            console.log(instagram + ' is NOT available');
            window.open('https://www.instagram.com/' + _this.service.config.usernameInstagram, '_system', 'location=no');
        });
    };
    MyApp.prototype.youtubeCheck = function (youtube) {
        var _this = this;
        this.appAvailability.check(youtube)
            .then(function (yes) {
            console.log(youtube + ' is available');
            _this.appExistStatus = youtube + 'is available';
            window.open('youtube://' + _this.service.config.youtube, '_system', 'location=no');
        }, function (no) {
            console.log(youtube + ' is NOT available');
            window.open('https://www.youtube.com/' + _this.service.config.youtube, '_system', 'location=no');
        });
    };
    MyApp.prototype.twitterCheck = function (twitter) {
        var _this = this;
        this.appAvailability.check(this.app)
            .then(function (yes) {
            console.log(twitter + ' is available');
            _this.appExistStatus = _this.app + 'is available';
            {
                window.open('twitter://user?screen_name=' + _this.service.config.usernameTwitter, '_system', 'location=no');
            }
        }, function (no) {
            console.log("veio no else do availabity");
            window.open('https://twitter.com/' + _this.service.config.usernameTwitter, '_system', 'location=no');
            console.log("náo tem instalado");
        });
    };
    MyApp.prototype.MsgsNaoLidas = function () {
        var _this = this;
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        if (Token) {
            var headers = new __WEBPACK_IMPORTED_MODULE_27__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': Token.Token
            });
            var options = new __WEBPACK_IMPORTED_MODULE_27__angular_http__["d" /* RequestOptions */]({ headers: headers });
            // let body = `Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}&codMensagem=${this.Msg.Cod}`;
            return this.http.get(this.service.config.defaultDomain + '/api/Mobile/MensagensNaoLidas?ClienteCod=' + this.service.config.idShopping + '&codUsuario=' + Token.codUsuario, { headers: headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                // console.log(data)
                _this.MensagensDisponiveis = data;
                _this.numeroMsgs = _this.MensagensDisponiveis.NaoLidas;
            }, function (err) {
                console.log(err);
                // localStorage.removeItem('cacheUsuario')
                _this.Logado = false;
            });
        }
    };
    MyApp.prototype.onClick = function () {
        if (!this.Filtro) {
            this.Filtro = true;
        }
        else {
            this.Filtro = false;
        }
    };
    MyApp.prototype.BuscarPesquisa = function () {
        this.events.publish('user:Pesquisa', this.FieldBusca, this.page);
        this.busca;
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_19__pages_busca_busca__["a" /* BuscaPage */]);
        // this.nav.remove(-1)
        this.Filtro = false;
    };
    MyApp.prototype.infoPerfil = function () {
        var _this = this;
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        var headers = new __WEBPACK_IMPORTED_MODULE_27__angular_http__["a" /* Headers */]();
        headers.append('Authorization', Token.Token);
        this.http.get(this.service.config.defaultDomain + "/api/mobile/CadastrosAPPDetalhes?ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario, { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.meusdados = data,
                _this.FotoApp = _this.meusdados.fotoUsuario;
            if (_this.Usuario.fbUsuario != null && _this.meusdados.fotoUsuario != null) {
                _this.ModeFace = false;
                _this.ModeFace2 = false;
                _this.ModoFace3 = false;
            }
            else if (_this.Usuario.fbUsuario == null && _this.meusdados.fotoUsuario == null) {
                _this.ModeFace = false;
                _this.ModeFace2 = true;
                _this.ModoFace3 = true;
            }
            else if (_this.Usuario.fbUsuario != null && _this.meusdados.fotoUsuario == null) {
                _this.ModeFace = true;
                _this.ModeFace2 = false;
                _this.ModoFace3 = true;
            }
            else if (_this.Usuario.fbUsuario == null && _this.meusdados.fotoUsuario == null) {
                _this.ModeFace = false;
                _this.ModeFace2 = true;
                _this.ModoFace3 = true;
            }
        }, function (err) {
            console.log('deuruim', err);
            _this.VerificaLogin();
        });
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/app/app.html"*/'<ion-menu [content]="content">\n\n    <ion-header class="HeaderMenu">\n\n        <ion-toolbar>\n\n          <div class="AreaNaoCadastrou" *ngIf="!Logado">\n\n            <p class="title">\n\n              Ainda não se cadastrou?\n\n            </p>\n\n            <p class="descr">{{ frasemenu }}</p>\n\n             <div class="butentroucadastre"  (click)="pushPage(LoginPage)" menuClose >ENTRE OU CADASTRE-SE</div>\n\n             \n\n             \n\n          </div>\n\n          \n\n          <div class="AreaLogado" *ngIf="Logado==true">\n\n            <div class="headerBox" menuClose (click)="pushPage(MeuperfilPage)">\n\n                <div class="BoxPhoto" *ngIf="ModeFace" [ngStyle]="{\'background-image\': \'url(https://graph.facebook.com/\' + Usuario.fbUsuario + \'/\' + \'picture?type=large\' + \')\'}">\n\n                  </div>\n\n                  <div class="BoxPhoto" *ngIf="ModeFace2" style="background: url(\'assets/perfil.png\')"> </div>\n\n                  <div class="BoxPhoto" [hidden]="ModoFace3" [ngStyle]="{\'background-image\': \'url(\' + FotoApp + \'\' + \'\' + \')\'}">\n\n                   </div>\n\n\n\n              <div class="BoxName">\n\n                <p class="Name" >{{ Usuario.nomeUsuario }}</p>\n\n                <p class="Email"> {{ Usuario.emailUsuario}} </p>\n\n              </div>\n\n            </div>\n\n            \n\n              <div class="AreaNotificacoes" menuClose (click)="setPage(NotificacoesPage)">\n\n                <img src="assets/icon-sino.png" alt="">\n\n                <div class="QuantosNottem">\n\n                  <p class="tit1">Você possui:</p>\n\n                  <p class="numbernotifications">{{numeroMsgs}} novas notificações</p>\n\n                </div>\n\n            </div>\n\n          </div>\n\n        </ion-toolbar>\n\n    </ion-header>\n\n    <ion-content class="MenuScroll">\n\n        <ion-list>\n\n            <button ion-item menuClose (click)="setPage(HomePage)">\n\n              Home\n\n            </button>\n\n            <button ion-item menuClose (click)="setPage(AcontecePage)">\n\n              Acontece\n\n            </button>\n\n            <button ion-item menuClose (click)="setPage(FavoritosPage)">\n\n              Favoritos\n\n            </button>\n\n            <button ion-item menuClose (click)="setPage(OfertasPage)">\n\n              Ofertas\n\n            </button>\n\n            <button ion-item menuClose (click)="setPage(ServicosPage)">\n\n              Serviços\n\n            </button>\n\n            <button ion-item menuClose (click)="setPage(ListShoppingPage)">\n\n              O Shopping\n\n            </button>\n\n            <button ion-item *ngIf="Logado==true" (click)="Deslogar(LoginPage)" menuClose>\n\n              Sair\n\n            </button> \n\n\n\n\n\n            <div ion-item class="socialnav">\n\n              <p>Siga-nos</p>\n\n              <ul>\n\n                <li (click)="CheckForApp(\'facebook\')">\n\n                  <img src="assets/icon/fb.png" alt="">\n\n                </li>\n\n                <li (click)="CheckForApp(\'instagram\')">\n\n                  <img src="assets/icon/instagram.png" alt="">\n\n                </li>\n\n                <li  (click)="CheckForApp(\'twitter\')">\n\n                  <img src="assets/icon/tw.png" alt="">\n\n                </li>\n\n                <li (click)="CheckForApp(\'topshopping\')">\n\n                  <img src="assets/blog.png" alt="">\n\n                </li>\n\n              </ul>\n\n            </div>\n\n        </ion-list>\n\n    </ion-content>\n\n</ion-menu>\n\n<ion-header class="IconeBusca" id="ion-header">\n\n  <ion-navbar>\n\n      <button ion-item class="buttonpesquisa" [hidden]="Filtro" (click)="onClick()">\n\n      <img src="assets/search.png" id="NaoAparecer" alt="">     \n\n      <img id="AparecerHome" src="assets/searchHome.png">\n\n      </button>\n\n      \n\n     <button ion-item class="buttonpesquisa" *ngIf="Filtro" (click)="onClick()">\n\n      <img  alt="" src="assets/closeSeachHome.png" id="AparecerHome">\n\n      </button> \n\n  </ion-navbar>\n\n</ion-header>\n\n<div class="buscaLoja" *ngIf="Filtro">\n\n  <div class="form-busca">\n\n      <input type="text" name="name" [(ngModel)]="FieldBusca" placeholder="Palavra-chave">\n\n      <!-- <button type="button" name="button" (click)="BuscarPesquisa()" class="">OK</button> -->\n\n\n\n      <button class="btn-filter" (click)="BuscarPesquisa()">Buscar</button>\n\n  </div>\n\n</div>\n\n\n\n\n\n<ion-nav id="nav" #content [root]="rootPage"></ion-nav>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/app/app.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_23__ionic_native_app_availability__["a" /* AppAvailability */], __WEBPACK_IMPORTED_MODULE_24__ionic_native_in_app_browser__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_21__providers_shared_service_shared_service__["a" /* SharedService */], __WEBPACK_IMPORTED_MODULE_19__pages_busca_busca__["a" /* BuscaPage */]],
    }),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[m-ripple-effect]',
        host: {
            'tappable': '',
            'role': 'button',
            'style': 'position: relative; overflow: hidden'
        }
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_20__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_23__ionic_native_app_availability__["a" /* AppAvailability */],
        __WEBPACK_IMPORTED_MODULE_24__ionic_native_in_app_browser__["a" /* InAppBrowser */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */],
        __WEBPACK_IMPORTED_MODULE_22__providers_busca_busca__["a" /* BuscaProvider */],
        __WEBPACK_IMPORTED_MODULE_21__providers_shared_service_shared_service__["a" /* SharedService */],
        __WEBPACK_IMPORTED_MODULE_25__angular_platform_browser__["c" /* DomSanitizer */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_26__ionic_native_network__["a" /* Network */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"],
        __WEBPACK_IMPORTED_MODULE_28__ionic_native_push__["a" /* Push */],
        __WEBPACK_IMPORTED_MODULE_27__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_30_ionic_image_loader__["a" /* ImageLoaderConfig */],
        __WEBPACK_IMPORTED_MODULE_29__ionic_native_local_notifications__["a" /* LocalNotifications */]
        // private localNotification: PhonegapLocalNotification
    ])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 5:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServiceProvider = (function () {
    function ServiceProvider(http) {
        this.http = http;
        this.config = new function () {
            this.SenderID = '3985457908',
                this.shopping = "topshopping";
            this.idShopping = 19,
                //shopping teste
                // this.idShopping = 17,
                // this.shopping="shopping-de-teste",
                this.name = "TopShopping",
                this.telefone = "(21) 2667-1787",
                this.email = "sac@psbetim.com.br";
            this.site = "www.topshopping.com.br",
                this.IdFacebook = "558893457475582";
            this.NomePagina = "PartageShoppingBetim/";
            this.usernameInstagram = "partageshoppingbetim";
            this.usernameTwitter = "";
            // this.youtube="user/ShoppingMetropolitan",
            this.swarm = "",
                this.defaultDomain = "http://www.am4mall.com.br";
            // this.defaultDomainHomolog="http://www.am4mall.com.br:128";
            this.domain = this.defaultDomain + "/conteudo/clientes/",
                this.jsonDomain = this.domain + this.shopping + "/jsons/";
            this.contentDomain = this.domain;
            this.wiseitDomain = "";
            this.importacaoDomain = this.defaultDomain + "/Admin/Importacao/ImpFaleConosco/" + this.shopping;
            this.importacaoNovidades = this.defaultDomain + "/Admin/Importacao/ImpCadastros/am4";
            this.getImagesDomain = "http://www.am4mall.com.br";
            this.pipocaoCartazHome = "http://www.pipocao.com.br/images/cinema/340x480/";
            this.IngressoOnline = true;
            this.LinkIngresso = 'https://www.kinoplex.com.br/cinema/kinoplex-topshopping-1-a-6/49';
            this.FraseMenu = 'Cadastre-se e receba as últimas novidades do TopShopping!';
            this.HashTag = '#AppTopShopping';
            this.endereco = 'Av. Gov. Roberto Silveira, 540, Centro, Nova Iguaçu - RJ';
        };
        this.Usuario = JSON.parse(window.localStorage.getItem('cacheUsuario'));
    }
    ServiceProvider.prototype.isLogged = function () {
        if (this.Usuario != undefined) {
            return true;
        }
        else {
            return false;
        }
    };
    return ServiceProvider;
}());
ServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
], ServiceProvider);

//# sourceMappingURL=service.js.map

/***/ }),

/***/ 500:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Footer; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_acontece_acontece__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_lojas_lojas__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_gastronomia_gastronomia__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_lazer_lazer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_list_filmes_list_filmes__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Footer = (function () {
    function Footer(navCtrl, navParams, events) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.events = events;
        this.AcontecePage = __WEBPACK_IMPORTED_MODULE_2__pages_acontece_acontece__["a" /* AcontecePage */];
        this.LojasPage = __WEBPACK_IMPORTED_MODULE_3__pages_lojas_lojas__["a" /* LojasPage */];
        this.GastronomiaPage = __WEBPACK_IMPORTED_MODULE_4__pages_gastronomia_gastronomia__["a" /* GastronomiaPage */];
        this.LazerPage = __WEBPACK_IMPORTED_MODULE_5__pages_lazer_lazer__["a" /* LazerPage */];
        this.ListFilmesPage = __WEBPACK_IMPORTED_MODULE_6__pages_list_filmes_list_filmes__["a" /* ListFilmesPage */];
    }
    Footer.prototype.setPage = function (page) {
        this.navCtrl.setRoot(page).then(function (response) {
            console.log(response);
        }).catch(function (e) {
            console.log(e);
        });
    };
    Footer.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad Footer');
    };
    return Footer;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])('ClassAcontece'),
    __metadata("design:type", Object)
], Footer.prototype, "AcontecePage", void 0);
Footer = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'app-footer',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/components/footer/footer.html"*/'<ion-footer class="footerBarra footerrodape">\n\n  <button ion-item class="ItFotter ativaAcontece" (click)="setPage(AcontecePage)">\n\n    <img src="assets/icon/1.png" id="Acontece" class="first">\n\n    <img src="assets/icon/1_active.png" id="AconteceActive" class="first">\n\n    <p> Acontece </p>\n\n  </button>\n\n  <button ion-item class="ItFotter AtivaCinema" (click)="setPage(ListFilmesPage)" >\n\n    <img src="assets/icon/2.png" id="Cinema">\n\n    <img src="assets/icon/2_active.png" id="CinemaActive" alt="">\n\n    <p> Cinema </p>\n\n  </button>\n\n  <button ion-item class="ItFotter AtivaLazer" (click)="setPage(LazerPage)" >\n\n    <img src="assets/icon/3.png" id="Lazer" >\n\n    <img src="assets/icon/3_active.png" id="LazerActive"  alt="">\n\n    <p> lazer </p>\n\n  </button>\n\n  <button ion-item class="ItFotter AtivaLojas" (click)="setPage(LojasPage)" >\n\n    <img src="assets/icon/4.png" id="Lojas">\n\n    <img src="assets/icon/4_active.png" id="LojasActive" alt="">\n\n    <p> lojas </p>\n\n  </button>\n\n  <button ion-item class="ItFotter AtivaGastro" (click)="setPage(GastronomiaPage)">\n\n    <img src="assets/icon/5.png" class="last" id="Gastronomia">\n\n    <img src="assets/icon/5_active.png"  id="GastronomiaActive" class="last">\n\n    <p> alimentação </p>\n\n  </button>\n\n</ion-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/components/footer/footer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
], Footer);

//# sourceMappingURL=footer.js.map

/***/ }),

/***/ 59:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CuponsInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var CuponsInternaPage = (function () {
    function CuponsInternaPage(navCtrl, navParams, service, socialSharing, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.SharedService = SharedService;
        this.Dominio = this.service.config.defaultDomain;
        this.firstParam = this.navParams.get("c");
        console.log(this.firstParam);
        this.vota = this.SharedService.checkLike("lojas", this.firstParam);
        console.log(this.vota, 'vota');
    }
    CuponsInternaPage.prototype.likeVote = function (loja) {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("lojas", loja);
    };
    CuponsInternaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.firstParam.Titulo + this.service.config.HashTag + this.service.config.site, this.subject, this.firstParam.Imagem).then(function () {
            // Sharing via email is possible
            console.log('Entrou no Share TUDO!');
        }).catch(function () {
            console.log();
            // Sharing via email is not possible
        });
    };
    CuponsInternaPage.prototype.ionViewDidLoad = function () {
    };
    return CuponsInternaPage;
}());
CuponsInternaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-cupons-interna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/cupons-interna/cupons-interna.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton"> \n\n      <h1>Cupons</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div class="InternPadrao" >\n\n    <img src="{{firstParam.Imagem}}" alt="">\n\n      <p class="title"> {{firstParam.Titulo}}</p>\n\n      <p class="data">Válido até {{ firstParam.DataFim | date : \'dd/MM/yyyy\' }}</p>\n\n      <p innerHTML="">{{firstParam.Descricao}}</p>\n\n\n\n      <button ion-button class="buttoncompartilhar" (click)="Compartilhar()">\n\n        <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n        <p>Compartilhar</p>\n\n      </button>\n\n    </div>\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/cupons-interna/cupons-interna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__["a" /* SharedService */]])
], CuponsInternaPage);

//# sourceMappingURL=cupons-interna.js.map

/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AcontecePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__feed_feed__ = __webpack_require__(144);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__fiquepordentro_fiquepordentro__ = __webpack_require__(145);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__enquetes_enquetes__ = __webpack_require__(146);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic2_super_tabs__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_http__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var AcontecePage = (function () {
    function AcontecePage(navCtrl, service, viewCtrl, superTabsCtrl, http, navParams, events) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.viewCtrl = viewCtrl;
        this.superTabsCtrl = superTabsCtrl;
        this.http = http;
        this.navParams = navParams;
        this.events = events;
        this.showTitles = true;
        this.EnquetesPage = __WEBPACK_IMPORTED_MODULE_4__enquetes_enquetes__["a" /* EnquetesPage */];
        this.FeedPage = __WEBPACK_IMPORTED_MODULE_2__feed_feed__["a" /* FeedPage */];
        this.FiquepordentroPage = __WEBPACK_IMPORTED_MODULE_3__fiquepordentro_fiquepordentro__["a" /* FiquepordentroPage */];
        this.Pagina = 5;
        this.respostas = [];
    }
    AcontecePage.prototype.SocialFeed = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_7__angular_http__["a" /* Headers */]();
        this.http.get(this.service.config.defaultDomain + "/api/mobile/RedesSociaisList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Feed = data.lista,
                _this.Dominio = _this.service.config.defaultDomain;
            _this.searching = false;
            if (_this.Feed.length == 0) {
                _this.NaoDisponivel = true;
            }
            else {
                _this.NaoDisponivel = false;
            }
        }, function (err) {
            console.log('deuruim', err);
        });
    };
    AcontecePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AcontecePage');
        this.SocialFeed();
    };
    AcontecePage.prototype.onTabSelect = function (ev) {
    };
    return AcontecePage;
}());
AcontecePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])({
        segment: 'acontece/:type'
    }),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-acontece',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/acontece/acontece.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n        <img src="assets/icon/icoMenu.png" alt="">\n\n      </button>\n\n    <ion-title class="titleButton">\n\n      <h1>acontece</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n<super-tabs id="mainTabs" scrollTabs="true" selectedTabIndex="0"  [config]="{ sideMenu: \'left\' }" (tabSelect)="onTabSelect($event)">\n\n  <super-tab [root]="FiquepordentroPage" title="Fique por dentro" id="locationTab" style="" class="nopaddingmenu"></super-tab>\n\n  <super-tab [root]="FeedPage" title="Social Feed"  id="homeTab" [hidden]="Feed==undefined"></super-tab>\n\n  <super-tab [root]="EnquetesPage" id="Enquetes" [title]="showTitles? \'Enquetes\' : \'\'" [rootParams]="{parent : tabParent}"></super-tab>\n\n</super-tabs>\n\n</ion-content>\n\n\n\n\n\n<app-footer selector="page-acontece"></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/acontece/acontece.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_6__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["w" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_5_ionic2_super_tabs__["b" /* SuperTabsController */],
        __WEBPACK_IMPORTED_MODULE_7__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
], AcontecePage);

//# sourceMappingURL=acontece.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VitrineInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__ = __webpack_require__(26);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var VitrineInternaPage = (function () {
    function VitrineInternaPage(navCtrl, navParams, service, socialSharing, SharedService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.service = service;
        this.socialSharing = socialSharing;
        this.SharedService = SharedService;
        this.Dominio = this.service.config.defaultDomain;
        this.firstParam = this.navParams.get("v");
        console.log(this.firstParam);
        this.vota = this.SharedService.checkLike("vitrine", this.firstParam);
        console.log(this.vota, 'vota');
    }
    VitrineInternaPage.prototype.likeVote = function (vitrine) {
        this.vota = this.vota ? false : true;
        this.SharedService.setLike("vitrine", vitrine);
    };
    VitrineInternaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.firstParam.nome.str.lang_1 + this.service.config.HashTag + ' www.topshopping.com.br', this.subject, this.Dominio + '/' + this.firstParam.midia.arquivos.lang_1.arquivo.url).then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
        });
    };
    VitrineInternaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VitrineInternaPage');
    };
    return VitrineInternaPage;
}());
VitrineInternaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-vitrine-interna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/vitrine-interna/vitrine-interna.html"*/'\n\n<ion-header class="BarHeader">\n\n    <ion-navbar class="Nobackground">\n\n      <ion-title class="titleButton"> \n\n        <h1>produto</h1>\n\n        \n\n      </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n\n\n<ion-content>\n\n  <div class="alignCenter">\n\n    \n\n  <div class="titulo-vitrine clearfix">\n\n    <h1>{{ firstParam.nome.str.lang_1 }}</h1>\n\n    \n\n    <div class="icons">\n\n\n\n      <button ion-button >\n\n        <img src="assets/icon/iconLikeFilmes.png" (click)="likeVote(firstParam)" [hidden]="vota" alt="">\n\n        <img src="assets/icon/iconLikeFilmes_ativ.png"  (click)="likeVote(firstParam)" *ngIf="vota" alt="">\n\n      </button>\n\n      <button  ion-button (click)="Compartilhar()">\n\n      <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n    </button>\n\n    </div>\n\n  </div>\n\n  <div class="image-vitrine">\n\n      <img src="{{Dominio}}/{{firstParam.midia.arquivos.lang_1.arquivo.url}}" />\n\n  </div>\n\n\n\n  \n\n\n\n  <div class="descricao-vitrine">\n\n      <p [innerHTML]="firstParam.descricao.str.lang_1"></p>\n\n  </div>\n\n</div>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/vitrine-interna/vitrine-interna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_4__providers_shared_service_shared_service__["a" /* SharedService */]])
], VitrineInternaPage);

//# sourceMappingURL=vitrine-interna.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LojasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lojas_interna_lojas_interna__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LojasPage = (function () {
    function LojasPage(navCtrl, navParams, http, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.storage = storage;
        this.Pagina = 0;
        this.searching = true;
        this.categories = [];
        this.letras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0-9"];
        this.BuscaVitrine = '';
        this.CategoriaBusca = '';
        this.letraselecionada = '';
    }
    LojasPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.Lojas();
            refresher.complete();
        }, 2000);
    };
    LojasPage.prototype.Lojas = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=6").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.lojas = data.lista;
            _this.Dominio = _this.service.config.defaultDomain;
            _this.searching = false;
            _this.storage.set('lojas', _this.lojas);
        }, function (err) {
            console.log('deuruim lojas', err);
            _this.searching = false;
            _this.storage.get('lojas').then(function (val) {
                console.log('Your age is', val);
                _this.lojas = val;
            });
        });
    };
    LojasPage.prototype.Segmentos = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/SegmentosList?codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            var segmentosgastro = data.lista;
            var segmentolojas = [];
            for (var i = 0; i < segmentosgastro.length; i++) {
                if (segmentosgastro[i].tipo.str.lang_1 != 'Gastronomia') {
                    segmentolojas.push(segmentosgastro[i]);
                    //  this.Segmentos=segmentolojas;
                    console.log(segmentolojas, 'entro fi');
                }
                _this.ListSegmentos = segmentolojas;
            }
            _this.storage.set('Segmentos', _this.ListSegmentos);
        }, function (err) {
            console.log('deuruim lojas', err);
            _this.storage.get('Segmentos').then(function (val) {
                console.log('Your age is', val);
                _this.ListSegmentos = val;
            });
        });
    };
    LojasPage.prototype.CountPaginacaoLojas = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.lista;
            if (_this.Data != undefined) {
                for (var i = 0; i < 6; i++) {
                    _this.lojas.push(_this.Data[i]);
                }
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    LojasPage.prototype.IrParaInterna = function ($event, loja) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__lojas_interna_lojas_interna__["a" /* LojasInternaPage */], {
            loja: loja
        });
    };
    LojasPage.prototype.AbrirFiltro = function () {
        if (!this.filter) {
            this.filter = true;
        }
        else {
            this.filter = false;
        }
    };
    LojasPage.prototype.Filtrar = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + "&Nome=" + this.BuscaVitrine + "&Letra=" + this.letraselecionada + "&CodSegmento=" + this.CategoriaBusca).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.lojas = data.lista;
            _this.filter = false;
            if (_this.lojas == '') {
                _this.nadaEncontrado = true;
            }
            else {
                _this.nadaEncontrado = false;
            }
        }, function (err) {
            _this.filter = false;
            console.log('deuruim lojas', err);
            _this.nadaEncontrado = true;
        });
    };
    LojasPage.prototype.listClick = function (event, newValue) {
        console.log(newValue);
        this.selectedItem = newValue;
        this.letraselecionada = newValue;
        this.Filtrar();
        this.selectedItem = '';
    };
    LojasPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            if (_this.lojas != undefined) {
                _this.CountPaginacaoLojas();
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }
            else {
                console.log('travo');
            }
        }, 500);
    };
    LojasPage.prototype.ionViewDidLoad = function () {
        this.Lojas();
        this.Segmentos();
        console.log('ionViewDidLoad LojasPage');
    };
    return LojasPage;
}());
LojasPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-lojas',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lojas/lojas.html"*/'<ion-header class="BarHeader">\n\n  <style>\n\n      .barVitrine{\n\n        background: #d9d9d9!important\n\n      }\n\n      .ColorBgLoja{\n\n        background: #d9d9d9!important      \n\n      }\n\n      .HomeFique{\n\n        background: #ededed!important;\n\n      }\n\n      </style>\n\n<ion-navbar class="Nobackground">\n\n  <button ion-button menuToggle class="buttonmenu">\n\n        <img src="assets/icon/icoMenu.png" alt="">\n\n      </button>\n\n  <ion-title class="titleButton">\n\n    <h1>Lojas</h1>\n\n  </ion-title>\n\n</ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content  class="ColorBgLoja" [class.filter]="filter">\n\n<ion-refresher (ionRefresh)="doRefresh($event)">\n\n  <ion-refresher-content></ion-refresher-content>\n\n</ion-refresher>\n\n\n\n<div class="barVitrine bar-subheader noPadding">\n\n  <div class="alignCenter flexcenter">\n\n    <!-- <h2 class="title ng-binding" style="pointer-events: none;">Guia de Lojas<br></h2> -->\n\n    <div class="subHeader">\n\n      <p class="Title">Guia de Lojas</p>\n\n      <p class="Chamada">Confira as lojas que você encontra aqui</p>\n\n    </div>\n\n    <div class="filtroInterno" (click)="AbrirFiltro()"></div>\n\n  </div>\n\n</div>\n\n\n\n<section style="background: #fff">\n\n<div class="alignCenter" *ngIf="filter">\n\n  <div class="setafiltro">\n\n    <img src="assets/icon/set_filtro.png" alt="">\n\n  </div>\n\n  <div class="form">\n\n    <button class="btn-filter" (click)="Filtrar($event)"></button>\n\n    <input [(ngModel)]="BuscaVitrine"  placeholder="Digite o nome da loja..." type="text">\n\n    \n\n    <p class="TitSelect">Selecione o segmento ou a inicial:</p>\n\n    <select [(ngModel)]="CategoriaBusca" (change)="Filtrar($event)" id="category">\n\n        <option value="">Todos</option>\n\n          <option *ngFor="let c of ListSegmentos" value="{{c.cod}}" >{{c.nome.str.lang_1}}</option>\n\n      </select>\n\n  </div>\n\n  <div class=\'letters\'>\n\n    <!-- <p>Busca por letra inicial</p> -->\n\n    <div>\n\n      <li *ngFor="let item of letras" [ngClass]="{\'active\': selectedItem == item}" (click)="listClick($event, item)">{{item}}</li>\n\n    </div>\n\n  </div>\n\n</div>\n\n</section>\n\n<div class="alignCenter">\n\n  <div class="container-stores" [hidden]="filter" >\n\n    <div *ngIf="searching" class="spinner-container">\n\n      <ion-spinner></ion-spinner>\n\n      <p>Carregando dados...</p>\n\n    </div>\n\n    <!-- <div class="stores"> -->\n\n      <div  class="HomeFique" *ngFor="let loja of lojas" (click)="IrParaInterna($event, loja)">\n\n        <div class="areaflexHomeFique">\n\n        <div class="Areaimg">\n\n          <img-loader useImg *ngIf="loja.midia.arquivos.lang_1.arquivo.url!=\'\'" src="{{loja.midia.arquivos.lang_1.arquivo.url}}" ></img-loader>\n\n\n\n          <img-loader useImg src="assets/sem-fotoLojas.png" *ngIf="!loja.midia.arquivos.lang_1.arquivo.url!=\'\'" alt=""></img-loader>\n\n        </div>\n\n        <div class="Desc">\n\n          <!-- <div class="vertAlight"> -->\n\n            <p class="Tit">{{loja.nome.str.lang_1}}</p>\n\n            <p class="txt" *ngIf="loja.categorias!=0">{{loja.categorias[0].nome.str.lang_1}}</p>\n\n            <p class="txt" *ngIf="loja.lucs!=0">{{loja.lucs[0].piso.nome.str.lang_1}}</p>\n\n          <!-- </div> -->\n\n        </div>\n\n      </div>\n\n      </div>\n\n    <!-- </div> -->\n\n    <div class="semConexao" *ngIf="nadaEncontrado">\n\n      <p>Nenhum item corresponde à sua procura.</p>\n\n    </div>\n\n  </div>\n\n</div>\n\n\n\n<ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n  <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Carregando mais dados...">></ion-infinite-scroll-content>\n\n</ion-infinite-scroll>\n\n</ion-content>\n\n\n\n\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lojas/lojas.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], LojasPage);

//# sourceMappingURL=lojas.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GastronomiaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__gastronomia_interna_gastronomia_interna__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GastronomiaPage = (function () {
    function GastronomiaPage(navCtrl, navParams, http, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.storage = storage;
        this.Pagina = 0;
        this.searching = true;
        this.categories = [];
        this.letras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0-9"];
        this.BuscaVitrine = '';
        this.CategoriaBusca = '';
        this.letraselecionada = '';
    }
    GastronomiaPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.Gastronomia();
            refresher.complete();
        }, 2000);
    };
    GastronomiaPage.prototype.Gastronomia = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6').map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.gastronomia = data.lista;
            _this.Dominio = _this.service.config.defaultDomain;
            _this.allStores = data.lista;
            _this.searching = false;
            _this.storage.set('gastronomia', _this.gastronomia);
        }, function (err) {
            console.log('deuruim lojas', err);
            _this.searching = false;
            _this.storage.get('gastronomia').then(function (val) {
                console.log('Your age is', val);
                _this.gastronomia = val;
            });
        });
    };
    GastronomiaPage.prototype.Segmentos = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/SegmentosGastronomiaList?codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Segmentos = data.lista;
            _this.Dominio = _this.service.config.defaultDomain;
            _this.searching = false;
            _this.storage.set('segmentosgastronomia', _this.Segmentos);
        }, function (err) {
            console.log('deuruim lojas', err);
            _this.storage.get('segmentosgastronomia').then(function (val) {
                console.log('Your age is', val);
                _this.Segmentos = val;
            });
        });
    };
    GastronomiaPage.prototype.CountPaginacaoGastronomia = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* Headers */]();
        this.Pagina++;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6', { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.Data = data.lista;
            for (var i = 0; i < 6; i++) {
                _this.gastronomia.push(_this.Data[i]);
            }
        }, function (err) {
            console.log('deuruim', err);
        } //this.status.error = JSON.stringify(err)
        );
    };
    GastronomiaPage.prototype.AbrirFiltro = function () {
        if (!this.filter) {
            this.filter = true;
        }
        else {
            this.filter = false;
        }
    };
    GastronomiaPage.prototype.Filtrar = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomainHomolog + "/api/mobile/GastronomiaList?codCliente=" + this.service.config.idShopping + "&Nome=" + this.BuscaVitrine + "&Letra=" + this.letraselecionada + "&CodSegmento=" + this.CategoriaBusca).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.gastronomia = data.lista;
            _this.filter = false;
            if (_this.gastronomia == '') {
                _this.nadaEncontrado = true;
            }
            else {
                _this.nadaEncontrado = false;
            }
        }, function (err) {
            _this.filter = false;
            console.log('deuruim lojas', err);
            _this.nadaEncontrado = true;
        });
    };
    GastronomiaPage.prototype.listClick = function (event, newValue) {
        console.log(newValue);
        this.selectedItem = newValue;
        this.letraselecionada = newValue;
        this.Filtrar();
        this.letraselecionada = '';
    };
    GastronomiaPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        console.log('Begin async operation');
        setTimeout(function () {
            if (_this.gastronomia != undefined) {
                _this.CountPaginacaoGastronomia();
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }
            else {
                ('travo');
            }
        }, 500);
    };
    GastronomiaPage.prototype.IrParaInterna = function ($event, g) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__gastronomia_interna_gastronomia_interna__["a" /* GastronomiaInternaPage */], {
            gastr: g
        });
    };
    GastronomiaPage.prototype.ionViewDidLoad = function () {
        this.Gastronomia();
        this.Segmentos();
        console.log('ionViewDidLoad GastronomiaPage');
    };
    return GastronomiaPage;
}());
GastronomiaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-gastronomia',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/gastronomia/gastronomia.html"*/'<ion-header class="BarHeader">\n\n    <style>\n\n        .barVitrine{\n\n          background: #d9d9d9!important\n\n        }\n\n        .ColorBgLoja{\n\n          background: #d9d9d9!important      \n\n        }\n\n        .HomeFique{\n\n          background: #ededed!important;\n\n        }\n\n        </style>\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n          <img src="assets/icon/icoMenu.png" alt="">\n\n        </button>\n\n    <ion-title class="titleButton">\n\n      <h1>alimentação</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content class="ColorBgLoja" [class.filter]="filter">\n\n  <div class="barVitrine bar-subheader noPadding">\n\n    <div class="alignCenter flexcenter">\n\n      <div class="subHeader">\n\n        <p class="Title">Onde comer</p>\n\n        <p class="Chamada">Conheça as diversas opções</p>\n\n      </div>\n\n      <div class="filtroInterno" (click)="AbrirFiltro()"></div>\n\n    </div>\n\n  </div>\n\n  <section style="background:#ffffff">\n\n    \n\n  <div class="alignCenter" *ngIf="filter">\n\n      <div class="setafiltro">\n\n        <img src="assets/icon/set_filtro.png" alt="">\n\n      </div>\n\n    <div class="form">\n\n      <button class="btn-filter" (click)="Filtrar($event)"></button>\n\n      <input [(ngModel)]="BuscaVitrine"  placeholder="Digite o nome da loja..." type="text">\n\n      <select [(ngModel)]="CategoriaBusca" (change)="Filtrar($event)">\n\n          <option value="">Todos</option>\n\n              <option *ngFor="let c of Segmentos" value="{{c.cod}}">{{c.nome.str.lang_1}}</option>\n\n          </select>\n\n    </div>\n\n    <div class=\'letters\'>\n\n      <!-- <p>Busca por letra inicial</p> -->\n\n      <div>\n\n        <li *ngFor="let item of letras" [ngClass]="{\'active\': selectedItem == item}" (click)="listClick($event, item)">{{item}}</li>\n\n      </div>\n\n    </div>\n\n  </div>\n\n\n\n  </section>\n\n  <div class="alignCenter">\n\n    <div class="container-stores" [hidden]="filter" style="padding-top: 2%;">\n\n      <div *ngIf="searching" class="spinner-container">\n\n        <ion-spinner></ion-spinner>\n\n        <p>Carregando dados...</p>\n\n      </div>\n\n      <div class="stores">\n\n        <div class="HomeFique" *ngFor="let g of gastronomia" (click)="IrParaInterna($event, g)">\n\n\n\n          <div class="areaflexHomeFique" *ngIf="g!=undefined">\n\n            <div class="Areaimg" >\n\n              <img-loader useImg *ngIf="g.midia.arquivos.lang_1.arquivo.url!=\'\'" src="{{g.midia.arquivos.lang_1.arquivo.url}}" ></img-loader>\n\n\n\n              <img src="assets/sem-fotoGastronomia.png" *ngIf="g.midia.arquivos.lang_1.arquivo.url==\'\'" alt="">\n\n            </div>\n\n            <div class="Desc">\n\n              <p class="Tit">{{g.nome.str.lang_1}}</p>\n\n              <!-- <p class="type">{{g.categorias[0].nome.str.lang_1}}</p>\n\n              <p class="floor">{{g.lucs[0].piso.nome.str.lang_1}}</p> -->\n\n              <p class="txt" *ngIf="g.categorias!=0">{{g.categorias[0].nome.str.lang_1}}</p>\n\n              <p class="txt" *ngIf="g.lucs!=0">{{g.lucs[0].piso.nome.str.lang_1}}</p>\n\n            </div>\n\n          </div>\n\n        </div>\n\n      </div>\n\n      <div class="semConexao" *ngIf="nadaEncontrado">\n\n        <p>Nenhum item corresponde à sua procura.</p>\n\n      </div>\n\n    </div>\n\n\n\n  </div>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Carregando mais dados..."></ion-infinite-scroll-content>\n\n  </ion-infinite-scroll>\n\n\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/gastronomia/gastronomia.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], GastronomiaPage);

//# sourceMappingURL=gastronomia.js.map

/***/ }),

/***/ 64:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LazerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__lazer_interna_lazer_interna__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LazerPage = (function () {
    function LazerPage(navCtrl, navParams, http, service, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.storage = storage;
        this.Dominio = this.service.config.defaultDomain;
        this.Pagina = 0;
        this.nomeShopping = this.service.config.name;
    }
    LazerPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        console.log('Begin async operation', refresher);
        setTimeout(function () {
            console.log('Async operation has ended');
            _this.Lazer();
            refresher.complete();
        }, 2000);
    };
    LazerPage.prototype.Lazer = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.defaultDomain + "/api/mobile/EntretenimentoList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5").map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.item = data.lista,
                _this.Dominio = _this.service.config.defaultDomain;
            console.log(_this.item);
            _this.storage.set('lazer', _this.item);
        }, function (err) {
            console.log('deuruim', err);
            _this.storage.get('lazer').then(function (val) {
                console.log('Your age is', val);
                _this.item = val;
            });
        });
    };
    LazerPage.prototype.IrParaInterna = function (localNavCtrl, event, c) {
        if (localNavCtrl === void 0) { localNavCtrl = true; }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__lazer_interna_lazer_interna__["a" /* LazerInternaPage */], {
            c: event
        });
    };
    LazerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LazerPage');
        this.Lazer();
    };
    return LazerPage;
}());
LazerPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-lazer',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lazer/lazer.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n          <img src="assets/icon/icoMenu.png" alt="">\n\n        </button>\n\n    <ion-title class="titleButton">\n\n      <h1>lazer</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content >\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n\n      <ion-refresher-content></ion-refresher-content>\n\n    </ion-refresher>\n\n  <div class="alignCenter">\n\n      <div class="subHeader">\n\n        <p class="Title">Venha se divertir</p>\n\n        <p class="Chamada">Confira as opções de entretenimento no {{nomeShopping}}.</p>\n\n      </div>\n\n    <div class="HomeFique" *ngFor="let l of item" (click)="IrParaInterna($event, l)">\n\n      <div class="areaflexHomeFique">\n\n        <div class="Areaimg">\n\n\n\n    <img [hidden]="l.midia.arquivos.lang_1.arquivo_list.length!=0" src="{{l.midia.arquivos.lang_1.arquivo.url}}">\n\n  <img *ngIf="l.midia.arquivos.lang_1.arquivo_list.length!=0" src="{{l.midia.arquivos.lang_1.arquivo_list[0].url}}">\n\n\n\n        </div>\n\n        <div class="Desc">\n\n          <p class="Tit" [innerHTML]="l.nome.str.lang_1"></p>\n\n          <p class="txt" [innerHTML]=" l.descricao.str.lang_1 | truncate : 50 : \'...\'"></p>\n\n        </div>\n\n      </div>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/lazer/lazer.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], LazerPage);

//# sourceMappingURL=lazer.js.map

/***/ }),

/***/ 72:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FacebookService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__ = __webpack_require__(58);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FacebookService = (function () {
    function FacebookService(fb) {
        this.fb = fb;
        this.APP_ID = 142735572987072;
    }
    FacebookService.prototype.init = function () {
        this.fb.browserInit(this.APP_ID, "v2.8");
    };
    return FacebookService;
}());
FacebookService = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_native_facebook__["a" /* Facebook */]])
], FacebookService);

//# sourceMappingURL=facebook.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EsquecisenhaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EsquecisenhaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
var EsquecisenhaPage = (function () {
    function EsquecisenhaPage(navCtrl, navParams, _alertCtrl, _loadingCtrl, service, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._alertCtrl = _alertCtrl;
        this._loadingCtrl = _loadingCtrl;
        this.service = service;
        this.http = http;
        this.loader = this._loadingCtrl.create({
            content: 'Aguarde...',
            dismissOnPageChange: true
        });
        this.HomePage = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.form = {
            Email: ''
        };
    }
    EsquecisenhaPage.prototype.enviarEmail = function () {
        var _this = this;
        this.loader.present();
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "Email=" + this.form.Email + "&ClienteCod=" + this.service.config.idShopping;
        if (this.form.Email == '') {
            this._alertCtrl.create({
                title: this.service.config.name,
                subTitle: 'O campo e-mail é obrigatório',
                buttons: [{ text: 'Ok' }]
            }).present();
            this.loader.dismiss();
        }
        else {
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/RecuperarSenha', body, { headers: headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                if (!data.sucess) {
                    _this._alertCtrl.create({
                        title: _this.service.config.name,
                        subTitle: data.Descricao,
                        buttons: [{ text: 'Ok' }]
                    }).present();
                }
                else {
                    _this.Sucesso = true;
                }
                _this.loader.dismiss();
                // this.navCtrl.setRoot(LoginPage)
                return false;
            }, function (err) {
                console.log("ERROR!: ", err._body);
                _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: 'O campo e-mail precisar ser válido',
                    buttons: [{ text: 'Ok' }]
                }).present();
                _this.loader.dismiss();
            });
        }
    };
    EsquecisenhaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EsquecisenhaPage');
    };
    return EsquecisenhaPage;
}());
EsquecisenhaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-esquecisenha',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/esquecisenha/esquecisenha.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton">\n\n      <h1>Esqueci Minha Senha</h1>\n\n\n\n      <!--<img src="assets/icon/logo.png" alt="" class="logoCliente">-->\n\n      <div class="buttonpesquisa">\n\n      </div>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <div class="subHeader" [hidden]="Sucesso">\n\n    <p class="Title">Meu Acesso</p>\n\n    <p class="Chamada">Para recuperar a sua senha, informe seu e-mail no campo abaixo.</p>\n\n  </div>\n\n  <div class="FormLogin" [hidden]="Sucesso">\n\n    <ion-item class="nopaddingLeft">\n\n      <input class="input" [(ngModel)]="form.Email" placeholder="Seu e-mail" type="">\n\n    </ion-item>\n\n    <button ion-button class="buttonEnviar" (click)="enviarEmail()">Enviar</button>\n\n  </div>\n\n\n\n\n\n  <div class="MsgOk" *ngIf="Sucesso">\n\n    <img src="assets/imgOk.png" class="imgOk" alt="">\n\n    <p class="DescMsg">Você receberá um e-mail com os passos para recuperar sua senha.</p>\n\n    <button class="ButtonVoltarHome" [navPush]="HomePage">VOLTAR PARA A HOME</button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/esquecisenha/esquecisenha.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
], EsquecisenhaPage);

//# sourceMappingURL=esquecisenha.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OfertasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__vitrine_vitrine__ = __webpack_require__(147);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__cupons_cupons__ = __webpack_require__(148);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var OfertasPage = (function () {
    function OfertasPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.VitrinePage = __WEBPACK_IMPORTED_MODULE_2__vitrine_vitrine__["a" /* VitrinePage */];
        this.CuponsPage = __WEBPACK_IMPORTED_MODULE_3__cupons_cupons__["a" /* CuponsPage */];
    }
    OfertasPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OfertasPage');
    };
    OfertasPage.prototype.onTabSelect = function (ev) {
        console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
    };
    return OfertasPage;
}());
OfertasPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-ofertas',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/ofertas/ofertas.html"*/'<ion-header class="BarHeader">\n\n    <ion-navbar class="Nobackground">\n\n      <button ion-button menuToggle class="buttonmenu">\n\n          <img src="assets/icon/icoMenu.png" alt="">\n\n        </button>\n\n      <ion-title class="titleButton">\n\n        <h1>ofertas</h1>\n\n      </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n\n\n<ion-content>\n\n  <super-tabs id="mainTabs" selectedTabIndex="0"  [config]="{ sideMenu: \'left\' }" (tabSelect)="onTabSelect($event)">\n\n      <super-tab [root]="CuponsPage" title="cupons"  id="homeTab"></super-tab>\n\n      <super-tab [root]="VitrinePage" title="vitrine virtual" id="locationTab" style="" class="nopaddingmenu"></super-tab>\n\n    </super-tabs>\n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/ofertas/ofertas.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], OfertasPage);

//# sourceMappingURL=ofertas.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServicosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__listaservicos_listaservicos__ = __webpack_require__(150);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__estacionamento_estacionamento__ = __webpack_require__(151);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ServicosPage = (function () {
    function ServicosPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ListaservicosPage = __WEBPACK_IMPORTED_MODULE_2__listaservicos_listaservicos__["a" /* ListaservicosPage */];
        this.EstacionamentoPage = __WEBPACK_IMPORTED_MODULE_3__estacionamento_estacionamento__["a" /* EstacionamentoPage */];
    }
    ServicosPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ServicosPage');
    };
    return ServicosPage;
}());
ServicosPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-servicos',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/servicos/servicos.html"*/'<ion-header class="BarHeader">\n\n    <ion-navbar class="Nobackground">\n\n      <button ion-button menuToggle class="buttonmenu">\n\n          <img src="assets/icon/icoMenu.png" alt="">\n\n        </button>\n\n      <ion-title class="titleButton">\n\n        <h1>serviços</h1>\n\n      </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n\n\n<ion-content>\n\n  <super-tabs id="mainTabs" selectedTabIndex="0"  [config]="{ sideMenu: \'left\' }">\n\n    <super-tab [root]="ListaservicosPage" title="serviços"  id="homeTab"></super-tab>\n\n    <super-tab [root]="EstacionamentoPage" title="Estacionamento" id="locationTab" style="" class="nopaddingmenu"></super-tab>\n\n  </super-tabs>\n\n  \n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/servicos/servicos.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */]])
], ServicosPage);

//# sourceMappingURL=servicos.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListShoppingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__oshopping_oshopping__ = __webpack_require__(149);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__horarios_horarios__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__comochegar_comochegar__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__contato_contato__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__page1_page1__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__page2_page2__ = __webpack_require__(157);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__page3_page3__ = __webpack_require__(158);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_ionic2_super_tabs__ = __webpack_require__(71);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var ListShoppingPage = (function () {
    function ListShoppingPage(navCtrl, navParams, http, service, superTabsCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.service = service;
        this.superTabsCtrl = superTabsCtrl;
        this.OshoppingPage = __WEBPACK_IMPORTED_MODULE_2__oshopping_oshopping__["a" /* OshoppingPage */];
        this.HorariosPage = __WEBPACK_IMPORTED_MODULE_3__horarios_horarios__["a" /* HorariosPage */];
        this.ComochegarPage = __WEBPACK_IMPORTED_MODULE_4__comochegar_comochegar__["a" /* ComochegarPage */];
        this.ContatoPage = __WEBPACK_IMPORTED_MODULE_5__contato_contato__["a" /* ContatoPage */];
        this.Page1Page = __WEBPACK_IMPORTED_MODULE_6__page1_page1__["a" /* Page1Page */];
        this.Page2Page = __WEBPACK_IMPORTED_MODULE_7__page2_page2__["a" /* Page2Page */];
        this.Page3Page = __WEBPACK_IMPORTED_MODULE_8__page3_page3__["a" /* Page3Page */];
        this.page0 = [];
        this.page1 = [];
        this.page2 = [];
        this.page3 = [];
        this.infosPages();
        this.superTabsCtrl.enableTabsSwipe(true);
    }
    ListShoppingPage.prototype.infosPages = function () {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_10__angular_http__["a" /* Headers */]();
        var geranum = Math.random() * 9999999;
        var nocache = Math.round(geranum);
        this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json?nocache=" + nocache).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.menus = data.lista;
            for (var z = 0; z < _this.menus.length; z++) {
                if (_this.menus[z] == _this.menus[0]) {
                    _this.page0.push(_this.menus[0].titulo.str.lang_1);
                    console.log(_this.page0, 'Pagina 0');
                }
                else {
                    console.log('Nao veio 0');
                    _this.page0 = undefined;
                }
                if (_this.menus[z] == _this.menus[1]) {
                    _this.page1.push(_this.menus[1].titulo.str.lang_1);
                    console.log(_this.page1, 'Pagina 1');
                }
                else {
                    console.log('nao veio 1');
                    _this.page2 = undefined;
                }
                if (_this.menus[z] == _this.menus[2]) {
                    _this.page2.push(_this.menus[2].titulo.str.lang_1);
                    console.log(_this.page2, 'Pagina 2');
                }
                else {
                    console.log('nao veio o 2');
                    _this.page2 = undefined;
                }
                if (_this.menus[z] == _this.menus[3]) {
                    _this.page3.push(_this.menus[3].titulo.str.lang_1);
                    console.log(_this.page3, 'Pagina 3');
                }
                else {
                    console.log('nao veio o 3');
                    _this.page3 = undefined;
                }
            }
        }, function (err) {
            console.log('deuruim lojas', err);
        });
    };
    ListShoppingPage.prototype.onTabSelect = function (ev) {
        console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
    };
    ListShoppingPage.prototype.ionViewDidLoad = function () {
    };
    return ListShoppingPage;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_11_ionic2_super_tabs__["a" /* SuperTabs */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_11_ionic2_super_tabs__["a" /* SuperTabs */])
], ListShoppingPage.prototype, "superTabs", void 0);
ListShoppingPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-list-shopping',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/list-shopping/list-shopping.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <button ion-button menuToggle class="buttonmenu">\n\n        <img src="assets/icon/icoMenu.png" alt="">\n\n      </button>\n\n    <ion-title class="titleButton">\n\n        <h1>O Shopping</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  \n\n  <super-tabs #SuperTabs id="mainTabs" scrollTabs="true" (tabSelect)="onTabSelect($event)" [config]="{ sideMenu: \'left\' }">\n\n    \n\n    <super-tab  [root]="OshoppingPage" title="O Shopping" id="sobre"></super-tab>\n\n    <super-tab [root]="ContatoPage" title="contato" id="contato"></super-tab>\n\n    <super-tab [root]="HorariosPage" title="horários" id="horarios"></super-tab>\n\n    <super-tab [root]="ComochegarPage" title="como chegar" id="comochegar"></super-tab>\n\n    <!-- <super-tab [root]="HorariosPage" title="horários" id="horarios"></super-tab>\n\n    <super-tab [root]="ComochegarPage" title="como chegar" id="comochegar"></super-tab> -->\n\n  </super-tabs>\n\n  \n\n</ion-content>\n\n\n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/list-shopping/list-shopping.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_10__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_9__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_11_ionic2_super_tabs__["b" /* SuperTabsController */]])
], ListShoppingPage);

//# sourceMappingURL=list-shopping.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MeuperfilPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_facebook_facebook__ = __webpack_require__(72);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_moment__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_angular2_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_angular2_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__ = __webpack_require__(472);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_base64__ = __webpack_require__(473);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_base64_to_gallery__ = __webpack_require__(474);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









// import { File } from '@ionic-native/file';
// import { Transfer, TransferObject } from '@ionic-native/transfer';
// import { FilePath } from '@ionic-native/file-path';



var MeuperfilPage = (function () {
    function MeuperfilPage(navCtrl, platform, navParams, _alertCtrl, facebookService, Facebook, events, service, storage, toastCtrl, moment, http, camera, 
        // private transfer: Transfer, 
        // private file: File, 
        // private filePath: FilePath, 
        loadingCtrl, actionSheetCtrl, base64, base64ToGallery) {
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this._alertCtrl = _alertCtrl;
        this.facebookService = facebookService;
        this.Facebook = Facebook;
        this.events = events;
        this.service = service;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.moment = moment;
        this.http = http;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.base64 = base64;
        this.base64ToGallery = base64ToGallery;
        this.lastImage = null;
        this.maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        this.masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        this.selectOptions = {
            title: 'Selecione o sexo',
        };
        this.SelectEstadoCivil = {
            title: 'Estado Civil'
        };
        this.Formulario = false;
        this.formSenha = {
            Senha: '',
            NovaSenha: '',
            ConfirmaSenha: ''
        };
        this.Usuario = JSON.parse(window.localStorage.getItem('cacheUsuario'));
        console.log(this.Usuario);
        this.infoPerfil();
    }
    MeuperfilPage.prototype.editarPerfil = function () {
        var DataFormatada2 = __WEBPACK_IMPORTED_MODULE_8_moment__(this.meusdados.datanascimentoUsuario).format("DD/MM/YYYY");
        console.log(this.meusdados);
        this.Formulario = true;
        this.form = {
            Nome: this.meusdados.nomelUsuario,
            Email: this.meusdados.emailUsuario,
            Telefone: this.meusdados.telefone,
            Sexo: this.meusdados.sexoUsuario,
            EstadoCivil: this.meusdados.estadocivilUsuario,
            datanascimento: DataFormatada2
        };
    };
    MeuperfilPage.prototype.changePassword = function () {
        this.alterarSenhar = true;
    };
    MeuperfilPage.prototype.atualizar = function () {
        var _this = this;
        var str = this.form.datanascimento;
        var res = str.split('/');
        var dataTratada = res[1] + '/' + res[0] + '/' + res[2];
        console.log(dataTratada);
        if (dataTratada == "undefined//undefined") {
            dataTratada = "";
            console.log(dataTratada);
            var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': Token.Token
            });
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
            var body = "Nome=" + this.form.Nome + "&Email=" + this.form.Email + "&Telefone=" + this.form.Telefone + "&Sexo=" + this.form.Sexo + "&EstadoCivil=" + this.form.EstadoCivil + "&DataNascimento=" + dataTratada + "&Origem=" + 'Aplicativo' + "&ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPEdit', body, { headers: headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: data.Descricao,
                    buttons: [{
                            text: 'Ok', handler: function () {
                                //TODO: Your logic here
                                // this.navCtrl.push(SomeComponent);
                                _this.Formulario = false,
                                    _this.infoPerfil();
                            }
                        }]
                });
                alerta.present();
            }, function (err) {
                console.log("ERROR!: ", err);
                console.log(err);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: err.Descricao,
                    buttons: [{ text: 'Ok' }],
                });
                alerta.present();
            });
        }
        else {
            var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': Token.Token
            });
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
            var body = "Nome=" + this.form.Nome + "&Email=" + this.form.Email + "&Telefone=" + this.form.Telefone + "&Sexo=" + this.form.Sexo + "&EstadoCivil=" + this.form.EstadoCivil + "&DataNascimento=" + dataTratada + "&Origem=" + 'Aplicativo' + "&ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPEdit', body, { headers: headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: data.Descricao,
                    buttons: [{
                            text: 'Ok', handler: function () {
                                //TODO: Your logic here
                                // this.navCtrl.push(SomeComponent);
                                _this.Formulario = false,
                                    _this.infoPerfil();
                            }
                        }]
                });
                alerta.present();
            }, function (err) {
                console.log("ERROR!: ", err);
                console.log(err);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: err.Descricao,
                    buttons: [{ text: 'Ok' }],
                });
                alerta.present();
            });
        }
    };
    MeuperfilPage.prototype.TrocarSenha = function () {
        var _this = this;
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': Token.Token
        });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        if (this.Usuario.fbUsuario == null) {
            var body = "SenhaAtual=" + this.formSenha.Senha + "&NovaSenha=" + this.formSenha.NovaSenha + "&ConfirmarSenha=" + this.formSenha.ConfirmaSenha + "&Origem=" + 'Aplicativo' + "&ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastroAppAlterarSenha', body, { headers: headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: data.Descricao,
                    buttons: [{
                            text: 'Ok', handler: function () {
                                _this.alterarSenhar = false,
                                    _this.infoPerfil();
                            }
                        }]
                });
                alerta.present();
            }, function (err) {
                var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
                console.log("ERROR!: ", err);
                console.log(err);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: mensagem,
                    buttons: [{ text: 'Ok' }],
                });
                alerta.present();
            });
        }
        else {
            var body = "NovaSenha=" + this.formSenha.NovaSenha + "&ConfirmarSenha=" + this.formSenha.ConfirmaSenha + "&Origem=" + 'Aplicativo' + "&ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastroAppAlterarSenha', body, { headers: headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: data.Descricao,
                    buttons: [{
                            text: 'Ok', handler: function () {
                                _this.alterarSenhar = false,
                                    _this.infoPerfil();
                            }
                        }]
                });
                alerta.present();
            }, function (err) {
                var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
                console.log("ERROR!: ", err);
                console.log(err);
                var alerta = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: mensagem,
                    buttons: [{ text: 'Ok' }],
                });
                alerta.present();
            });
        }
    };
    MeuperfilPage.prototype.createUser2 = function () {
        var _this = this;
        setTimeout(function () {
            console.log('User Login');
            var profile = JSON.parse(localStorage.getItem('profile'));
            _this.events.publish('user:PegarDados', profile, Date.now());
        }, 1000);
    };
    MeuperfilPage.prototype.facebook = function () {
        var _this = this;
        var permissions = new Array();
        var nav = this.navCtrl;
        permissions = ['public_profile', 'user_friends', 'email'];
        this.Facebook.login(permissions).then(function (response) {
            var userId = response.authResponse.userID;
            var params = new Array();
            _this.Facebook.api("/me?fields=name,gender,email,birthday", params)
                .then(function (profile) {
                profile.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
                localStorage.setItem('profile', JSON.stringify(profile));
                this.form.Facebook = userId;
            });
            _this.createUser2();
        }, function (error) {
            console.log(error);
        });
    };
    MeuperfilPage.prototype.VincularConta = function () {
        var _this = this;
        this.facebook();
        this.events.subscribe('user:PegarDados', function (profile) {
            var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
            headers.append('Authorization', Token.Token);
            headers.append('Content-Type', 'application/json');
            var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
            var body = {
                Origem: "Aplicativo",
                ClienteCod: _this.service.config.idShopping,
                UsuarioCod: Token.codUsuario,
                FacebookId: profile.id
            };
            return _this.http.post(_this.service.config.defaultDomain + '/api/Mobile/VincularFacebook', body, { headers: headers }).map(function (res) { return res.json(); })
                .subscribe(function (data) {
                console.log(data);
                var alert = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: data.Descricao,
                    buttons: [{ text: 'Ok', }]
                });
                alert.present();
            }, function (err) {
                console.log("ERROR!: ", err);
                var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
                var alert = _this._alertCtrl.create({
                    title: _this.service.config.name,
                    subTitle: mensagem,
                    buttons: [{ text: 'Ok' }]
                });
                alert.present();
            });
        });
    };
    MeuperfilPage.prototype.infoPerfil = function () {
        var _this = this;
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
        headers.append('Authorization', Token.Token);
        this.http.get(this.service.config.defaultDomain + "/api/mobile/CadastrosAPPDetalhes?ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario, { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.meusdados = data,
                _this.Dominio = _this.service.config.defaultDomain,
                _this.storage.set('meusdados', _this.meusdados);
            console.log(_this.meusdados, 'Meus Dados');
            _this.email = _this.meusdados.emailUsuario;
            _this.telefone = _this.meusdados.telUsuario;
            _this.sexo = _this.meusdados.sexoUsuario;
            _this.FotoApp = _this.meusdados.fotoUsuario;
            _this.facebooktem = _this.Usuario.fbUsuario;
            if (_this.sexo == '1') {
                _this.sexo = 'Feminino';
            }
            else if (_this.sexo == '2') {
                _this.sexo = 'Masculino';
            }
            else {
                _this.sexo = 'Sexo';
            }
            _this.estadocivil = _this.meusdados.estadocivilUsuario;
            if (_this.estadocivil == '1') {
                _this.estadocivil = 'Feminino';
            }
            else if (_this.estadocivil == '2') {
                _this.estadocivil = 'Masculino';
            }
            else if (_this.estadocivil == '3') {
                _this.estadocivil = 'Masculino';
            }
            else if (_this.estadocivil == '4') {
                _this.estadocivil = 'Masculino';
            }
            _this.datanascimento = _this.meusdados.datanascimentoUsuario;
            _this.datacadastro = _this.meusdados.datacadastroUsuario;
            if (_this.Usuario.fbUsuario != null && _this.meusdados.fotoUsuario != null) {
                _this.ModeFace = false;
                _this.ModeFace2 = false;
                _this.ModoFace3 = false;
            }
            else if (_this.Usuario.fbUsuario == null && _this.meusdados.fotoUsuario == null) {
                _this.ModeFace = false;
                _this.ModeFace2 = true;
                _this.ModoFace3 = true;
            }
            else if (_this.Usuario.fbUsuario != null && _this.meusdados.fotoUsuario == null) {
                _this.ModeFace = true;
                _this.ModeFace2 = false;
                _this.ModoFace3 = true;
            }
            else if (_this.Usuario.fbUsuario == null && _this.meusdados.fotoUsuario == null) {
                _this.ModeFace = false;
                _this.ModeFace2 = true;
                _this.ModoFace3 = true;
            }
        }, function (err) {
            console.log('deuruim', err);
            _this.storage.get('meusdados').then(function (val) {
                console.log('Your age is', val);
                _this.meusdados = val;
            });
        });
    };
    MeuperfilPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Escolha uma imagem',
            buttons: [
                {
                    text: 'Abrir Galeria',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Usar a Camera',
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancelar',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    };
    MeuperfilPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 50,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true,
            destinationType: this.camera.DestinationType.DATA_URL,
        };
        this.camera.getPicture(options).then(function (imagePath) {
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.ImagemBase64 = imagePath;
            }
            else {
                _this.ImagemBase64 = imagePath;
            }
            _this.uploadImage();
        }, function (err) {
            _this.presentToast('Erro ao selecionar a imagem');
        });
    };
    // Create a new name for the image
    MeuperfilPage.prototype.createFileName = function () {
        var d = new Date(), n = d.getTime(), newFileName = n + ".jpg";
        return newFileName;
    };
    // Copy the image to a local folder
    // private copyFileToLocalDir(namePath, currentName, newFileName) {
    //   this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
    //     this.lastImage = newFileName;
    //   }, error => {
    //     this.presentToast('Erro ao ler o arquivo.');
    //   });
    // }
    MeuperfilPage.prototype.presentToast = function (text) {
        var toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    // Always get the accurate path to your apps folder
    MeuperfilPage.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return cordova.file.dataDirectory + img;
        }
    };
    ;
    MeuperfilPage.prototype.uint8ToString = function (Uint8Array) {
        var i, length, out = '';
        for (i = 0, length = Uint8Array.length; i < length; i += 1) {
            out += String.fromCharCode(Uint8Array[i]);
        }
        return out;
    };
    MeuperfilPage.prototype.uploadImage = function () {
        // btoa(this.ImagemBase64)
        // var base64 = btoa(this.uint8ToString(this.ImagemBase64));
        var _this = this;
        this.loading = this.loadingCtrl.create({
            content: 'Aguarde...',
        });
        this.loading.present();
        var base64 = this.ImagemBase64;
        var data = new FormData();
        data.append("image_data", base64);
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        var url = "http://am4mall.com.br/api/mobile/CadastroAppUploadFoto";
        // const fileTransfer: TransferObject = this.transfer.create();
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = {
            ClienteCod: this.service.config.idShopping,
            codUsuario: Token.codUsuario,
            Base64: 'data:image/jpeg;base64,' + base64
        };
        // let body = `ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}&Base64=${'data:image/jpeg;base64,'+base64}`;
        return this.http.post('http://am4mall.com.br/api/mobile/CadastroAppUploadFoto', body, { headers: headers }).map(function (res) { return res.json(); })
            .subscribe(function (response) {
            console.log(response);
            // this.path=response;
            _this.infoPerfil();
            _this.loading.dismiss();
            _this.events.publish('FotoPerfil', response, Date.now());
        }, function (err) {
            console.log(err);
            _this.presentToast(err);
            _this.loading.dismiss();
        });
    };
    MeuperfilPage.prototype.ionViewDidLoad = function () {
    };
    return MeuperfilPage;
}());
MeuperfilPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-meuperfil',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/meuperfil/meuperfil.html"*/'<ion-header class="BarHeader">\n\n  <ion-navbar class="Nobackground">\n\n    <ion-title class="titleButton"> \n\n      <h1>Meu Perfil</h1>\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <div class="bgPerfil">\n\n    <div class="fotoPerfil" (click)="presentActionSheet()">\n\n      <div class="iconphoto">\n\n        <!-- <input type="file" accept="image/*"> -->\n\n      </div>\n\n\n\n         <!-- <img src="https://graph.facebook.com/{{Usuario.fbUsuario}}/picture?type=large" alt="" *ngIf="ModeFace" >  \n\n         <img src="assets/perfil.png" alt="" *ngIf="ModeFace2"> \n\n         <img src="{{FotoApp}}" alt="" [hidden]="ModoFace3"> -->\n\n          \n\n         <div class="ajustFoto" *ngIf="ModeFace" [ngStyle]="{\'background-image\': \'url(https://graph.facebook.com/\' + Usuario.fbUsuario + \'/\' + \'picture?type=large\' + \')\'}">\n\n         </div>\n\n         <div class="ajustFoto" *ngIf="ModeFace2" style="background: url(\'assets/perfil.png\')"> </div>\n\n         <div class="ajustFoto" [hidden]="ModoFace3" [ngStyle]="{\'background-image\': \'url(\' + FotoApp + \'\' + \'\' + \')\'}">    </div>\n\n\n\n\n\n         \n\n    </div>\n\n       <h1 class="clearfix ng-binding" ng-bind="infoperfil.Nome">{{ Usuario.nomeUsuario }}</h1> \n\n\n\n      <button class="editarperfil" ion-item [hidden]="Formulario" (click)="editarPerfil()">Editar meu perfil agora</button>\n\n  </div>\n\n  <div class="listmeuperfil" [hidden]="Formulario || alterarSenhar">\n\n    <div class="alignCenter">\n\n      <div class="boxupdatePerfil center">\n\n          <p class=" avisoperfil">Mantenha seus dados sempre atualizados e tenha uma experiência ainda mais exclusiva!</p>\n\n        </div>\n\n    </div>\n\n  <p class="campo bordertop">E-mail<br><span [innerHTML]="email"> </span></p>\n\n  <!-- <p class="campo" *ngIf="telefone!=null">Telefone<br><span> {{ MeusDados.telefone }} </span></p>   -->\n\n  <p class="campo" *ngIf="sexo!=\'Sexo\'" >Sexo<br><span [innerHTML]="sexo">  </span></p>\n\n  <p class="campo" *ngIf="estadocivil!=\'null\' ">Estado Civil<br><span [innerHTML]="estadocivil"> </span></p>\n\n  <p class="campo" *ngIf="datanascimento!=null">Data de Nascimento<br><span>{{datanascimento | amUtc | amDateFormat: \'DD/MM/YYYY\'}}\n\n    </span></p>\n\n  <div class="alignCenter marginForm">\n\n    <button ion-item class="ButtonloginfB" *ngIf="Usuario.fbUsuario==null" (click)="VincularConta()">Conecte com o Facebook</button>\n\n    <button ion-item class="buttonAlterar" (click)="changePassword()">\n\n      Alterar Senha\n\n    </button>\n\n  </div>\n\n</div>\n\n  <div class="alignCenter" *ngIf="Formulario">\n\n    <form  class="FormCadastre">\n\n      <ion-item class="nopaddingLeft">\n\n          <ion-input class="input" placeholder="Nome" [(ngModel)]="form.Nome" name="Nome" type="text" ></ion-input>\n\n        </ion-item>\n\n      <ion-item class="nopaddingLeft">\n\n          <ion-input class="input" placeholder="E-mail"  [(ngModel)]="form.Email" name="Email" type="text" ></ion-input>\n\n        </ion-item>\n\n        <ion-item class="nopaddingLeft">\n\n          <input [textMask]="{mask: masktel}" class="input" placeholder="Telefone"  name="Telefone" [(ngModel)]="form.Telefone" type="tel" >\n\n        </ion-item>\n\n        <ion-item class="nopaddingLeft">\n\n        <ion-select class="input select" [(ngModel)]="form.Sexo" name="Sexo" multiple="false" cancelText="Cancelar" okText="Ok" [selectOptions]="selectOptions" placeholder="{{sexo}}" >\n\n          <ion-option value="1">Feminino</ion-option>\n\n          <ion-option value="2" >Masculino</ion-option>\n\n        </ion-select>\n\n        </ion-item>\n\n      <ion-item class="nopaddingLeft">\n\n          <ion-select class="input select" [(ngModel)]="form.EstadoCivil" name="Sexo" multiple="false" cancelText="Cancel" okText="Ok" [selectOptions]="SelectEstadoCivil" placeholder="Estado Civil">\n\n              <ion-option value="Solteiro">Solteiro</ion-option>\n\n              <ion-option value="Casado" >Casado</ion-option>\n\n              <ion-option value="Separado" >Separado</ion-option>\n\n              <ion-option value="Viúvo" >Viúvo</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n      <ion-item class="nopaddingLeft">\n\n          <input class="input" placeholder="Data de Nascimento" [(ngModel)]="form.datanascimento" [textMask]="{mask: maskdate}" name="DataNascimento" type="tel" >\n\n        </ion-item>\n\n        <button ion-button (click)="atualizar()" class="buttonAlterar" >\n\n          Atualizar os Dados\n\n        </button>\n\n    </form>\n\n  </div>\n\n  <div class="alignCenter" *ngIf="alterarSenhar">\n\n    <form class="FormCadastre">\n\n        <ion-item class="nopaddingLeft" *ngIf="facebooktem==null">\n\n            <ion-input class="input" placeholder="Senha Atual" [(ngModel)]="formSenha.Senha" name="Senha" type="password" ></ion-input>\n\n          </ion-item>\n\n        <ion-item class="nopaddingLeft">\n\n          <ion-input class="input"  [(ngModel)]="formSenha.NovaSenha"  placeholder="Nova Senha" type="password" name="NovaSenha"  ></ion-input>\n\n        </ion-item> \n\n        <ion-item class="nopaddingLeft">\n\n          <ion-input class="input"  [(ngModel)]="formSenha.ConfirmaSenha"  placeholder="Confirmar Senha" type="password" name="ConfirmaSenha"  ></ion-input>\n\n        </ion-item> \n\n        <button ion-button (click)="TrocarSenha()" class="buttonAlterar">\n\n          Confirma senha\n\n        </button>\n\n    </form>\n\n  </div>\n\n\n\n\n\n</ion-content>\n\n<app-footer></app-footer>\n\n'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/meuperfil/meuperfil.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_5__providers_facebook_facebook__["a" /* FacebookService */], __WEBPACK_IMPORTED_MODULE_10__ionic_native_base64__["a" /* Base64 */], __WEBPACK_IMPORTED_MODULE_11__ionic_native_base64_to_gallery__["a" /* Base64ToGallery */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__["a" /* Camera */],],
    }),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({
        name: 'formatDate'
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["s" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__providers_facebook_facebook__["a" /* FacebookService */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_facebook__["a" /* Facebook */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */],
        __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["v" /* ToastController */],
        __WEBPACK_IMPORTED_MODULE_7_angular2_moment__["MomentModule"],
        __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_9__ionic_native_camera__["a" /* Camera */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* LoadingController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
        __WEBPACK_IMPORTED_MODULE_10__ionic_native_base64__["a" /* Base64 */],
        __WEBPACK_IMPORTED_MODULE_11__ionic_native_base64_to_gallery__["a" /* Base64ToGallery */]])
], MeuperfilPage);

//# sourceMappingURL=meuperfil.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotificacoesInternaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_service_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__lojas_lojas__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__lojas_interna_lojas_interna__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__acontece_acontece__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__fiquepordentrointerna_fiquepordentrointerna__ = __webpack_require__(38);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__list_shopping_list_shopping__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__home_home__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__meuperfil_meuperfil__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__gastronomia_gastronomia__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__gastronomia_interna_gastronomia_interna__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__filmes_filmes__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__servicos_servicos__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__servicosinterna_servicosinterna__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__lazer_lazer__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__lazer_interna_lazer_interna__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ofertas_ofertas__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__vitrine_interna_vitrine_interna__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






















var NotificacoesInternaPage = (function () {
    function NotificacoesInternaPage(navCtrl, socialSharing, navParams, service, http, storage, events) {
        this.navCtrl = navCtrl;
        this.socialSharing = socialSharing;
        this.navParams = navParams;
        this.service = service;
        this.http = http;
        this.storage = storage;
        this.events = events;
        this.openDetails = function ($event, l) {
            switch (this.Msg.CodConteudo) {
                case 1:
                    //Loja Lista
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__lojas_lojas__["a" /* LojasPage */]);
                    break;
                case 2:
                    //Loja Específica
                    console.log(this.lojas, 'lojainterna');
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__lojas_interna_lojas_interna__["a" /* LojasInternaPage */], {
                        loja: this.lojas
                    });
                    break;
                case 3:
                    // ListadeFique
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__acontece_acontece__["a" /* AcontecePage */]);
                    break;
                case 4:
                    //FiqueEspecifico
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_9__fiquepordentrointerna_fiquepordentrointerna__["a" /* FiquepordentrointernaPage */], {
                        novidade: this.novidade
                    });
                    break;
                case 5:
                    // Sobre
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__list_shopping_list_shopping__["a" /* ListShoppingPage */]);
                    break;
                case 6:
                    // Home
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_11__home_home__["a" /* HomePage */]);
                    break;
                case 7:
                    // Meu Perfil"
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_12__meuperfil_meuperfil__["a" /* MeuperfilPage */]);
                    break;
                case 8:
                    // Alimentação lista
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_13__gastronomia_gastronomia__["a" /* GastronomiaPage */]);
                    break;
                case 9:
                    // Alimentação Específico
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_14__gastronomia_interna_gastronomia_interna__["a" /* GastronomiaInternaPage */], {
                        gastr: this.gastronomia
                    });
                    break;
                case 10:
                    // Fale conosco
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__list_shopping_list_shopping__["a" /* ListShoppingPage */]);
                    break;
                case 11:
                    // Filme lista
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_15__filmes_filmes__["a" /* FilmesPage */]);
                    break;
                case 12:
                    // Filme Específico
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_15__filmes_filmes__["a" /* FilmesPage */], {
                        f: l
                    });
                    break;
                case 13:
                    // Como Chegar
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__list_shopping_list_shopping__["a" /* ListShoppingPage */]);
                    break;
                case 14:
                    // Contato
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__list_shopping_list_shopping__["a" /* ListShoppingPage */]);
                    break;
                case 15:
                    // Estacionamento
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_16__servicos_servicos__["a" /* ServicosPage */]);
                    break;
                case 16:
                    // Horarios
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__list_shopping_list_shopping__["a" /* ListShoppingPage */]);
                    break;
                case 17:
                    // Lazer lista
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_18__lazer_lazer__["a" /* LazerPage */]);
                    break;
                case 18:
                    // Lazer Específico
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_19__lazer_interna_lazer_interna__["a" /* LazerInternaPage */], {
                        c: this.lazer
                    });
                    break;
                case 19:
                    // O Shopping
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_10__list_shopping_list_shopping__["a" /* ListShoppingPage */]);
                    break;
                case 21:
                    // Servicos Lista
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_16__servicos_servicos__["a" /* ServicosPage */]);
                    break;
                case 22:
                    // Serviços Interna
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_17__servicosinterna_servicosinterna__["a" /* ServicosinternaPage */], {
                        s: this.servicos
                    });
                    break;
                case 23:
                    // Vitrine Lista
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_20__ofertas_ofertas__["a" /* OfertasPage */]);
                    break;
                case 24:
                    // Vitrine Especifica 
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_21__vitrine_interna_vitrine_interna__["a" /* VitrineInternaPage */], {
                        v: this.vitrine
                    });
                    break;
                case 25:
                    // Enquete 
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__acontece_acontece__["a" /* AcontecePage */]);
                    break;
                case 26:
                    // Enquete Especifica
                    this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_8__acontece_acontece__["a" /* AcontecePage */]);
                    break;
                case "Lojas":
                    // localStorage.setItem('store',JSON.stringify(l));
                    this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__lojas_interna_lojas_interna__["a" /* LojasInternaPage */], {
                        loja: l
                    });
                default:
            }
        };
        this.Msg = this.navParams.get("msgInter");
        console.log(this.Msg);
        switch (this.Msg.CodConteudo) {
            case 2:
                //Loja Específica
                this.LojaEspecifica();
                this.LazerEspecifico();
                console.log(this.lojas, 'lojas');
                break;
            case 18:
                // Lazer Interna
                this.LazerEspecifico();
                console.log(this.lazer, 'lazer');
                break;
            case 4:
                //FiqueEspecifico
                this.FiquePorDentroEspecifico();
                console.log(this.novidade, 'novidade');
                break;
            case 22:
                // Lazer Interna
                this.ServicosEspecifico();
                console.log(this.servicos, 'lazer');
                break;
            case 24:
                // Lazer Interna
                this.VitrineEspecifica();
                console.log(this.vitrine, 'vitrine');
                break;
            case 9:
                // Lazer Interna
                this.GastronomiaEspecifica();
                console.log(this.gastronomia, 'gastronomia');
            default:
        }
    }
    NotificacoesInternaPage.prototype.LerAmsg = function () {
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': Token.Token
        });
        var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var body = "Origem=" + 'Aplicativo' + "&ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario + "&codMensagem=" + this.Msg.Cod;
        return this.http.post(this.service.config.defaultDomain + '/api/Mobile/Lermensagem', body, { headers: headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    NotificacoesInternaPage.prototype.Compartilhar = function () {
        this.socialSharing.share(this.Msg.Titulo + this.service.config.HashTag, this.Msg.Imagem).then(function () {
            // Sharing via email is possible
        }).catch(function () {
            // Sharing via email is not possible
        });
    };
    NotificacoesInternaPage.prototype.LojaEspecifica = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?CodInterna=" + this.Msg.CodInterna + "&codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.lojas = data.lista[0];
        }, function (err) {
            console.log('deuruim lojas', err);
        });
    };
    NotificacoesInternaPage.prototype.LazerEspecifico = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/EntretenimentoList?CodInterna=" + this.Msg.CodInterna + "&codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.lazer = data.lista[0];
        }, function (err) {
            console.log('deuruim lazer', err);
        });
    };
    NotificacoesInternaPage.prototype.FiquePorDentroEspecifico = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?CodInterna=" + this.Msg.CodInterna + "&codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.novidade = data.lista[0];
        }, function (err) {
            console.log('deuruim fique', err);
        });
    };
    NotificacoesInternaPage.prototype.ServicosEspecifico = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/ServicosList?CodInterna=" + this.Msg.CodInterna + "&codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.servicos = data.lista[0];
        }, function (err) {
            console.log('deuruim serv', err);
        });
    };
    NotificacoesInternaPage.prototype.VitrineEspecifica = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/VitrineList?CodInterna=" + this.Msg.CodInterna + "&codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.vitrine = data.lista[0];
        }, function (err) {
            console.log('deuruim vitri', err);
        });
    };
    NotificacoesInternaPage.prototype.GastronomiaEspecifica = function () {
        var _this = this;
        this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?CodInterna=" + this.Msg.CodInterna + "&codCliente=" + this.service.config.idShopping).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.gastronomia = data.lista[0];
        }, function (err) {
            console.log('deuruim gastr', err);
        });
    };
    NotificacoesInternaPage.prototype.ListMensagens = function () {
        var _this = this;
        var Token = JSON.parse(localStorage.getItem('cacheUsuario'));
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Headers */]({
            'Authorization': Token.Token
        });
        var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["d" /* RequestOptions */]({ headers: headers });
        this.http.get(this.service.config.defaultDomain + "/api/mobile/MensagensList?Clientecod=" + this.service.config.idShopping, { "headers": headers }).map(function (res) { return res.json(); })
            .subscribe(function (data) {
            _this.MsgsList = data.Mensagens,
                _this.events;
            console.log(_this.MsgsList, 'Novidade');
        }, function (err) {
            console.log('deuruim', err);
        });
    };
    NotificacoesInternaPage.prototype.ionViewDidLoad = function () {
        this.LerAmsg();
        this.events.publish('user:MensagemLida', this.MsgsList);
        console.log('ionViewDidLoad NotificacoesInternaPage');
    };
    NotificacoesInternaPage.prototype.ionViewWillLeave = function () {
        this.ListMensagens();
    };
    return NotificacoesInternaPage;
}());
NotificacoesInternaPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* IonicPage */])(),
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-notificacoes-interna',template:/*ion-inline-start:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/notificacoes-interna/notificacoes-interna.html"*/'\n\n<ion-header class="BarHeader">\n\n    <ion-navbar class="Nobackground">\n\n      <ion-title class="titleButton"> \n\n        <h1>Notificação</h1>\n\n      </ion-title>\n\n    </ion-navbar>\n\n  </ion-header>\n\n  \n\n  <ion-content >\n\n  <div class="InternPadrao">\n\n      <img src="{{Msg.Imagem}}" alt="">\n\n    <div class="alignCenter">\n\n      \n\n      <div class="titulolazer clearfix">\n\n          <h1> {{Msg.Titulo}} </h1>\n\n          <div class="icons">\n\n            <!-- <button class="Naotem">\n\n              <img src="assets/icon/iconLikeFilmes.png" (click)="likeVote(novidade)" [hidden]="vota" alt="">\n\n              <img src="assets/icon/iconLikeFilmes_ativ.png"  (click)="likeVote(novidade)" *ngIf="vota" alt="">\n\n            </button>\n\n            <button (click)="Compartilhar()" ion-buttom>\n\n              <img src="assets/icon/icoCompartilhar.png" alt="" class="buttoncompartilharimg">\n\n            </button> -->\n\n          </div>  \n\n        </div>\n\n        <p [innerHTML]="Msg.Texto">\n\n        </p>\n\n        <button ion-item class="buttonIrParaInterna" (click)="openDetails(Msg.CodConteudo)"> Ir Para {{Msg.DescricaoConteudo}} </button>\n\n    </div>\n\n   \n\n  </div>\n\n  </ion-content>\n\n  \n\n<app-footer></app-footer>'/*ion-inline-end:"/Users/usuario/Desktop/projetos/topshopping2/src/pages/notificacoes-interna/notificacoes-interna.html"*/,
        providers: [__WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */]]
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_social_sharing__["a" /* SocialSharing */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["r" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_2__providers_service_service__["a" /* ServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Events */]])
], NotificacoesInternaPage);

//# sourceMappingURL=notificacoes-interna.js.map

/***/ })

},[402]);
//# sourceMappingURL=main.js.map