
import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides,Events } from 'ionic-angular';

import { Http, Response, Headers } from '@angular/http';
import { ServiceProvider } from '../../providers/service/service';
@IonicPage()
@Component({
  selector: 'page-estacionamento',
  templateUrl: 'estacionamento.html',
})

export class EstacionamentoPage {
  
  @ViewChild('Slides1') slides1: Slides;
  @ViewChild('Slides2') slides2: Slides;
  @ViewChild('Slides3') slides3: Slides;
  features1: any
  features2: any
  features3: any
  ArmazenamentoEstacionamento:any;
  Estacionamento:Array<any>=[]
  AndarSelecionado
  
  setores:Array<any>=[]
  listsetores
  SetorSelecionado
  vagas:Array<any>=[]
  listvagas
  constructor(
    public navCtrl: NavController, public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    public events: Events,
  ) {
  }
  
  
  GetParking(){
    this.http.get(this.service.config.jsonDomain+"jsonEstacionamento.json").map(res => res.json())
    .subscribe(
    data => {
       this.Estacionamento=data.lista,
      // console.log(this.Estacionamento)
      this.setores=[];  
      this.setores.push(this.Estacionamento[0].setores);
      this.vagas=[];  
      this.vagas.push(this.Estacionamento[0].setores[0].vagas);

      this.AndarSelecionado=this.setores
      this.SetorSelecionado=this.SetorSelecionado
  
      this.listsetores=this.setores;
      // console.log(this.listsetores, 'setores')
      this.listvagas=this.vagas[0]
      
      console.log(this.listvagas, 'vagas')

      let employee = []
      
      employee.push(this.Estacionamento[0].local);
      this.AndarSelecionado=employee[0];
      // console.log( this.AndarSelecionado)


      let employee2 = []
      
      employee2.push(this.listsetores[0][0].setor);
      this.SetorSelecionado=employee2[0];
      
      
      let employee3 = []
      
      employee3.push(this.listsetores[0][0].vagas);
      this.Vagaselecionada=employee3[0][0];
      console.log(this.Vagaselecionada);



     
    }, 
    err =>  {
      console.log('deuruim', err)
    }//this.status.error = JSON.stringify(err)
    );
  } 
  
  slideChanged() {
    let currentIndex1 = this.slides1.getActiveIndex();
 //     console.log('Selected', selectedValue);
    this.setores=[];  
    this.setores.push(this.Estacionamento[currentIndex1].setores);
    this.AndarSelecionado=currentIndex1
    this.listsetores=this.setores;
    console.log(this.listsetores,'setores')
    // this.listvagas=this.vagas;
    // console.log(this.listvagas,'vagas')

    let employee = []
    
    employee.push(this.Estacionamento[currentIndex1].local);
    this.AndarSelecionado=employee[0];
  }
  Vagaselecionada
  slideChanged2() {
    let currentIndex2 = this.slides2.getActiveIndex();
  
      this.Vagaselecionada=currentIndex2
  
      let employee = []
      
      employee.push(this.listsetores[0][currentIndex2]);
      this.SetorSelecionado=employee[0].setor;
      this.listvagas=employee[0].vagas;
   localStorage.setItem("setorrr",JSON.stringify(this.SetorSelecionado));
      
  }
  Vagaselecionada2
 
  
  slideChanged3() {
    let currentIndex2 = this.slides3.getActiveIndex();
        this.vagas=[]
    
        this.Vagaselecionada2=currentIndex2
        
    
        let employee = []
        

        employee.push(this.listvagas[currentIndex2]);
        this.Vagaselecionada=employee[0];
   localStorage.setItem("vaga",JSON.stringify(this.Vagaselecionada));
        
        console.log( this.Vagaselecionada)
  }
  sliderOptions = {
    pager: true,
    autoHeight: true,
    
    }


    PrevSlide(){
      this.slides1.slidePrev();
          // console.log(proximo, 'proximo')
        }
      
        ProximoSlide(){
      this.slides1.slideNext();
          // console.log(proximo, 'proximo')
        }
        PrevSlide2(){
      this.slides2.slidePrev();
          // console.log(proximo, 'proximo')
        }
      
        ProximoSlide2(){
      this.slides2.slideNext();
          // console.log(proximo, 'proximo')
        }
        PrevSlide3(){
      this.slides3.slidePrev();
          // console.log(proximo, 'proximo')
        }
      
        ProximoSlide3(){
      this.slides3.slideNext();
          // console.log(proximo, 'proximo')
        }

  ionViewWillEnter() {
    this.ArmazenamentoEstacionamento = JSON.parse(window.localStorage.getItem('vaga-selecionada'));
    if(this.ArmazenamentoEstacionamento!=undefined){
      this.Salvo=true;
    }
      this.events.subscribe('user:estacionamento', (AreaEstacionamento) => {
      this.ArmazenamentoEstacionamento=AreaEstacionamento;
      console.log( this.ArmazenamentoEstacionamento)
      
    })
 
  }
  ionViewDidLoad() {
    this.GetParking()
  }

  Salvo
  
  SalvarVaga(){
  const AreaEstacionamento={
    andar: this.AndarSelecionado,
    setor: this.SetorSelecionado,
    vaga: this.Vagaselecionada
  }
    this.Salvo=true;
    this.events.publish('user:estacionamento', AreaEstacionamento);
   localStorage.setItem("vaga-selecionada",JSON.stringify(AreaEstacionamento));
  }

  limparVaga(){
   localStorage.setItem("vaga-selecionada",JSON.stringify(null));
   this.Salvo=false;
  }
}
