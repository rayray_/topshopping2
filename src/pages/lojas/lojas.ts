import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';
import { LojasInternaPage } from '../lojas-interna/lojas-interna'
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-lojas',
  templateUrl: 'lojas.html',
})
export class LojasPage {

  Token
  Dominio
  lojas
  Pagina = 0
  Data
  ListSegmentos
  allStores
  searching = true;
  categories = []
  letras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0-9"]
  BuscaVitrine: string = ''
  CategoriaBusca:string = ''
  letraselecionada:string = ''
  activeletra
  nadaEncontrado

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private storage: Storage, ) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.Lojas()
      refresher.complete();
    }, 2000);
  }

  Lojas() {
    this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=6").map(res => res.json())
      .subscribe(
      data => {
        this.lojas = data.lista;
        this.Dominio = this.service.config.defaultDomain;
        this.searching = false;
        this.storage.set('lojas', this.lojas);
      },
      err => {
        console.log('deuruim lojas', err);
        this.searching=false;
        this.storage.get('lojas').then((val) => {
          console.log('Your age is', val);
          this.lojas=val
        });
      }
      );
  }

  Segmentos() {
    this.http.get(this.service.config.defaultDomain + "/api/mobile/SegmentosList?codCliente=" + this.service.config.idShopping).map(res => res.json())
      .subscribe(
      data => {
         
        let segmentosgastro=data.lista;
        
        let segmentolojas=[]
        for (var i = 0; i < segmentosgastro.length; i++) {
          if (segmentosgastro[i].tipo.str.lang_1!='Gastronomia') {
              segmentolojas.push(segmentosgastro[i]);
              //  this.Segmentos=segmentolojas;
          console.log(segmentolojas, 'entro fi')
          
          }
        this.ListSegmentos=segmentolojas;
        }
        this.storage.set('Segmentos', this.ListSegmentos);
      },
      err => {
        console.log('deuruim lojas', err);
        this.storage.get('Segmentos').then((val) => {
          console.log('Your age is', val);
          this.ListSegmentos=val
        });
      }
      );
  }

  CountPaginacaoLojas() {
    var headers = new Headers();
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6', { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Data = data.lista
        if (this.Data != undefined) {
          for (var i = 0; i < 6; i++) {
            this.lojas.push(this.Data[i]);
          }
        }
      },
      err => {
        console.log('deuruim', err)
      }//this.status.error = JSON.stringify(err)
      );
  }
  IrParaInterna($event, loja) {
    this.navCtrl.push(LojasInternaPage, {
      loja: loja
    });
  }
  filter
  AbrirFiltro() {
    if (!this.filter) {
      this.filter = true
    } else {
      this.filter = false;
    }
  }

  Filtrar() {
    this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + "&Nome=" + this.BuscaVitrine + "&Letra=" + this.letraselecionada + "&CodSegmento=" + this.CategoriaBusca).map(res => res.json())
      .subscribe(
      data => {
        this.lojas = data.lista;
        this.filter = false;
        if (this.lojas == '') {
          this.nadaEncontrado = true;
        }else{

          this.nadaEncontrado = false;
        }
      },
      err => {
        this.filter = false;
        console.log('deuruim lojas', err);
        this.nadaEncontrado = true;
      }
      );
  }
  selectedItem
  listClick(event, newValue) {
    console.log(newValue);
    this.selectedItem = newValue;
    this.letraselecionada=newValue;
    this.Filtrar()

    this.selectedItem=''
  }


  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      if (this.lojas != undefined) {
        this.CountPaginacaoLojas()
        console.log('Async operation has ended');
        infiniteScroll.complete();
      } else {
        console.log('travo')
      }
    }, 500);
  }
  ionViewDidLoad() {
    this.Lojas()
    this.Segmentos()
    console.log('ionViewDidLoad LojasPage');
  }

}