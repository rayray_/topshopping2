import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { FilmesInternaPage } from '../filmes-interna/filmes-interna';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-filmes',
  templateUrl: 'filmes.html',
})
export class FilmesPage {
  filmes
  CartazLink
  Token
  searching=true
  Pagina=0
  Data
  
  rootNavCtrl: NavController;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private storage: Storage,) {
    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }
  someAreaCinema
  FilmesList() {
    this.http.get(this.service.config.defaultDomain + "/api/mobile/FilmesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5').map(res => res.json())
      .subscribe(
      data => {
        this.filmes = data.lista;
        this.searching=false;
        this.filmes=data.lista,
        this.CartazLink=this.service.config.pipocaoCartazHome
        // console.log(this.filmes)
        // console.log(this.CartazLink)
        if(this.filmes==0){
          this.someAreaCinema=true
        }else{
          this.someAreaCinema=false
        }
        this.storage.set('filmes', this.filmes);
        for(let i = 0; i < data.lista.length; i++) {
          let findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
          let changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
          data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
        }
      },
      err => {
        console.log('deuruim lojas', err);
        this.searching=false;
        this.storage.get('filmes').then((val) => {
          console.log('Your age is', val);
          this.filmes=val
        });
      }
      );
  }

  

  CountPaginacao() {
    var headers = new Headers();
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain + "/api/mobile/FilmesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5', { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Data = data.lista
          for (var i = 0; i < 5; i++) {
            this.filmes.push(this.Data[i]);
          }
        this.CartazLink=this.service.config.pipocaoCartazHome
        // console.log(this.filmes)
        // console.log(this.CartazLink)
  
        for(let i = 0; i < data.lista.length; i++) {
          let findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
          let changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
          data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
        }
        
      },
      err => {
        console.log('deuruim', err)
      }//this.status.error = JSON.stringify(err)
      );
  }
  IrParaInterna( event, f) {
      this.rootNavCtrl.push(FilmesInternaPage,{
        f: f
      });
  }
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      if(this.filmes!=undefined){
      console.log('Async operation has ended');
      this.CountPaginacao()
      infiniteScroll.complete();
    }else{
      ('travo')
    }
    
    }, 500);
  }
  ionViewDidLoad() {
      this.FilmesList()
    // console.log('ionViewDidLoad FilmesPage');
  }

}
