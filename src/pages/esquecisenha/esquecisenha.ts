import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HomePage } from '../home/home'
import { ServiceProvider } from '../../providers/service/service';
/**
 * Generated class for the EsquecisenhaPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-esquecisenha',
  templateUrl: 'esquecisenha.html',
})
export class EsquecisenhaPage {
  loader = this._loadingCtrl.create({
    content: 'Aguarde...',
    dismissOnPageChange: true
  });
  HomePage = HomePage
  form = {
    Email: ''
  }
  Sucesso;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public http: Http, ) {
  }


  enviarEmail() {
    this.loader.present();
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });

    let body = `Email=${this.form.Email}&ClienteCod=${this.service.config.idShopping}`;

    if (this.form.Email == '') {
      this._alertCtrl.create({
        title: this.service.config.name,
        subTitle: 'O campo e-mail é obrigatório',
        buttons: [{ text: 'Ok' }]
      }).present();
      this.loader.dismiss()
    } else {
      return this.http.post(this.service.config.defaultDomain + '/api/Mobile/RecuperarSenha', body, { headers: headers }).map(res => res.json())
        .subscribe((data) => {
          console.log(data)

          if (!data.sucess) {
            this._alertCtrl.create({
              title: this.service.config.name,
              subTitle: data.Descricao,
              buttons: [{ text: 'Ok' }]
            }).present();

          }
          else {
            this.Sucesso = true;
          }
          this.loader.dismiss();
          // this.navCtrl.setRoot(LoginPage)
          return false;
        }, err => {
          console.log("ERROR!: ", err._body);
          this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: 'O campo e-mail precisar ser válido',
            buttons: [{ text: 'Ok' }]
          }).present();
          this.loader.dismiss()

        }
        )
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad EsquecisenhaPage');
  }

}
