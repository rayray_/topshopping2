import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OshoppingPage } from '../oshopping/oshopping'
import { HorariosPage } from '../horarios/horarios'
import { ComochegarPage } from '../comochegar/comochegar'
import { ContatoPage } from '../contato/contato'

import { Page1Page } from '../page1/page1'
import { Page2Page } from '../page2/page2'
import { Page3Page } from '../page3/page3'



import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';

import {SuperTabs,SuperTabsController } from 'ionic2-super-tabs'



@IonicPage()
@Component({
  selector: 'page-list-shopping',
  templateUrl: 'list-shopping.html',
})
export class ListShoppingPage {
  @ViewChild(SuperTabs) superTabs: SuperTabs;


  OshoppingPage=OshoppingPage
  HorariosPage=HorariosPage
  ComochegarPage=ComochegarPage
  ContatoPage=ContatoPage
  Page1Page=Page1Page
  Page2Page=Page2Page
  Page3Page=Page3Page
  menus
  page0=[]
  page1=[]
  page2=[]
  page3=[]
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private superTabsCtrl: SuperTabsController) {
      this.infosPages()
      this.superTabsCtrl.enableTabsSwipe(true);
  }
  infosPages() {
    let headers = new Headers();
    let geranum = Math.random()*9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json?nocache="+nocache).map(res => res.json())
      .subscribe(
      data => {
    this.menus = data.lista;
      for (var z = 0; z < this.menus.length; z++) {
      if(this.menus[z]==this.menus[0]){
        this.page0.push(this.menus[0].titulo.str.lang_1)
        console.log(this.page0, 'Pagina 0')
      }else{
        console.log('Nao veio 0')
        this.page0=undefined
      }
      if(this.menus[z]==this.menus[1]){
        this.page1.push(this.menus[1].titulo.str.lang_1)
        console.log(this.page1, 'Pagina 1')
      }else{
        console.log('nao veio 1')
        this.page2=undefined
      }
      if(this.menus[z]==this.menus[2]){
        this.page2.push(this.menus[2].titulo.str.lang_1)
        console.log(this.page2, 'Pagina 2')
      }else{
        console.log('nao veio o 2')
        this.page2=undefined
      }
      if(this.menus[z]==this.menus[3]){
        this.page3.push(this.menus[3].titulo.str.lang_1)
        console.log(this.page3, 'Pagina 3')
      }else{
        console.log('nao veio o 3')
        this.page3=undefined
      }
    }
    }
      ,
      err => {
        console.log('deuruim lojas', err);
      }
      );
  }
  
  onTabSelect(ev: any) {
    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
  }
  
  ionViewDidLoad() {
  }

}
