import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';

import { SharedService} from '../../providers/shared-service/shared-service';

@IonicPage()
@Component({
  selector: 'page-lojas-interna',
  templateUrl: 'lojas-interna.html',
  providers: [SocialSharing]
})
export class LojasInternaPage {
  Dominio
  vota
  public loja
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    private socialSharing: SocialSharing,
    public SharedService: SharedService) {
      this.Dominio = this.service.config.defaultDomain
      this.loja = this.navParams.get("loja");
      console.log(this.loja);
      this.vota=this.SharedService.checkLike("lojas", this.loja);
      console.log(this.vota, 'vota')
  }
  
  likeVote(loja) {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("lojas", loja);
  }
  subject
  Compartilhar(){
    this.socialSharing.share(
      this.loja.nome.str.lang_1+this.service.config.HashTag+' www.topshopping.com.br', this.subject, this.loja.midia.arquivos.lang_1.arquivo.url
    ).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
      console.log('caiu no erro')
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LojasInternaPage');
  }

}
