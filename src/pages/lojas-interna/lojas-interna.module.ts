import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LojasInternaPage } from './lojas-interna';

@NgModule({
  declarations: [
    LojasInternaPage,
  ],
  imports: [
    IonicPageModule.forChild(LojasInternaPage),
  ],
})
export class LojasInternaPageModule {}
