import { Component, Output, EventEmitter } from '@angular/core';
import { IonicPage, ViewController, NavController, LoadingController, AlertController, NavParams, Events } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { CadastrePage } from '../cadastre/cadastre'
import { HomePage } from '../home/home'
import { EsquecisenhaPage } from '../esquecisenha/esquecisenha'


import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { ServiceProvider } from '../../providers/service/service';
import { FacebookService } from '../../providers/facebook/facebook';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [FacebookService]
})
export class LoginPage {
  // @Output() Logado: EventEmitter<any> = new EventEmitter();

 
  Logado: any
  form: any = {
    Email: '',
    Senha: '',
    Facebook: ''
  }

  Usuario
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public http: Http,
    private Facebook: Facebook,
    public facebookService: FacebookService,
    public events: Events) {
      
  }

  HomePage = HomePage
  CadastrePage = CadastrePage
  EsquecisenhaPage = EsquecisenhaPage

  logar() {
    let loader = this._loadingCtrl.create({
      content: 'Aguarde...',
      dismissOnPageChange: true
    });
    // this.loader.present().then(() => {
    loader.present()
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });

    let body = `Email=${this.form.Email}&Senha=${this.form.Senha}&ClienteCod=${this.service.config.idShopping}`;

    return this.http.post(this.service.config.defaultDomain + '/api/Mobile/Login', body, { headers: headers }).map(res => res.json())
      .subscribe((data) => {
        if (data.Sucess != false) {
          this.Logado = true;
          localStorage.setItem("cacheUsuario", JSON.stringify(data));
          localStorage.removeItem('profile')
          this.events.publish('user:Login', data, this.Logado);
          
          this.navCtrl.setRoot(HomePage, {
            AparecerAlert: 'Apareça',
          
          },
          )
          console.log('caiuno if')
        } else {
          let alert = this._alertCtrl.create({
            title: '<img src="assets/icon/error.png" /> Erro',
            subTitle: data.descricao,
            buttons: ['OK'],
            cssClass: 'alertCustomCssError'
          });
          alert.present();
          console.log('caiu no else')
          loader.dismiss()
          return false;
        }
      }, err => {
        console.log("ERROR!: ", err);
        var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
        let alert = this._alertCtrl.create({
          title: '<img src="assets/icon/error.png" /> Erro',
          subTitle: mensagem,
          buttons: ['OK'],
          cssClass: 'alertCustomCssError'
        });
        alert.present();
      loader.dismiss()
      }
      )
    // })
  }

  InfosFb:any;
  getUserDetail(userid) {
    this.Facebook.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
      .then(res => {
        console.log(res);
        this.InfosFb = res;
        this.http.get(this.service.config.defaultDomain + '/api/Mobile/VerificaFacebook?FacebookId='+this.InfosFb.id+ '&ClienteCod='+this.service.config.idShopping).map(res => res.json())
        .subscribe((data) => {
          console.log(data)
          if(data.Success==true){

            let headers = new Headers({
              'Content-Type': 'application/x-www-form-urlencoded'
            });
            let options = new RequestOptions({ headers: headers });
            let body = `Facebook=${this.InfosFb.id}&ClienteCod=${this.service.config.idShopping}`;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(res => res.json())
            .subscribe((data) => {
              this.Logado = true;
              localStorage.setItem("cacheUsuario", JSON.stringify(data));
              localStorage.removeItem('profile')
              this.events.publish('user:Login', data, this.Logado);
              this.navCtrl.setRoot(HomePage, {
                AparecerAlert: 'Apareça'}
              )
            })
          }else{
            let headers = new Headers({
              'Content-Type': 'application/x-www-form-urlencoded'
            });
            let options = new RequestOptions({ headers: headers });
            let body = `Nome=${this.InfosFb.name}&Email=${this.InfosFb.email}&Facebook=${this.InfosFb.id}&Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}`;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPAdd2', body, { headers: headers }).map(res => res.json())

            .subscribe((data) => {
              console.log(data);
              let headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
              });
              let options = new RequestOptions({ headers: headers });
              let body = `Facebook=${this.InfosFb.id}&ClienteCod=${this.service.config.idShopping}`;
              return this.http.post(this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(res => res.json())
                .subscribe((data) => {
                  this.Logado = true;
                  localStorage.setItem("cacheUsuario", JSON.stringify(data));
                  localStorage.removeItem('profile')
                  this.events.publish('user:Login', data, this.Logado);
                  this.navCtrl.setRoot(HomePage, {
                    AparecerAlert: 'Apareça'}
                  )
                })
            }, err => {

              
              console.log("ERROR!: ", err);
      
            })
          }
        })
      })
      .catch(e => {
        console.log(e);
      });
  }
  AparecerAlert='Apareça'
  public LogarFb() {
    this.Facebook.login(['public_profile', 'user_friends', 'email'])
    .then(res => {
      if(res.status === "connected") {
        this.getUserDetail(res.authResponse.userID);
      } else {
      }
    })
    .catch(e => console.log('Error logging into Facebook', e));
  }

  getLoginFacebook() {
    let loader = this._loadingCtrl.create({
      content: 'Aguarde...',
      dismissOnPageChange: true
    });
    // let infofb = JSON.parse(localStorage.getItem('profile'));
   loader.present()

  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad LoginPage');    
  }
  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
  }

  ionViewWillLeave() {

  }
}
