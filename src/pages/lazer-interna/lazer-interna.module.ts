import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LazerInternaPage } from './lazer-interna';

@NgModule({
  declarations: [
    LazerInternaPage,
  ],
  imports: [
    IonicPageModule.forChild(LazerInternaPage),
  ],
})
export class LazerInternaPageModule {}
