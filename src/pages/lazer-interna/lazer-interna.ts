import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';

import { SocialSharing } from '@ionic-native/social-sharing';

import { SharedService} from '../../providers/shared-service/shared-service';

@IonicPage()
@Component({
  selector: 'page-lazer-interna',
  templateUrl: 'lazer-interna.html',
  providers: [SocialSharing]
})
export class LazerInternaPage {
  public firstParam;
  Dominio
  vota
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    private socialSharing: SocialSharing,
    public SharedService: SharedService) {
      this.Dominio = this.service.config.defaultDomain
      this.firstParam = this.navParams.get("c");
      console.log(this.firstParam);
      this.vota=this.SharedService.checkLike("entretenimento", this.firstParam);
      console.log(this.vota, 'vota')
  }
  likeVote(lazer) {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("entretenimento", lazer);
  }
  Compartilhar(){
    this.socialSharing.share(
      this.firstParam.nome.str.lang_1+this.service.config.HashTag+' www.topshopping.com.br', this.firstParam, this.firstParam.midia.arquivos.lang_1.arquivo_list[0].url
    ).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad LazerInternaPage');
  }

}
