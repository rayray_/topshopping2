import { Component } from '@angular/core';
import { IonicPage, NavController,ViewController,LoadingController, NavParams, AlertController,Alert } from 'ionic-angular';
import {HomePage} from '../home/home'
import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker
 } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

@IonicPage()
@Component({
  selector: 'page-comochegar',
  templateUrl: 'comochegar.html',
  providers: [Geolocation,GoogleMaps,Diagnostic]
})
export class ComochegarPage {


  map: GoogleMap;
  mapElement: HTMLElement;
  Rota
  Dominio
  endereco:any=this.service.config.endereco;
  private loading;
  private _alert: Alert;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private geolocation: Geolocation,
    private googleMaps: GoogleMaps,
    public service: ServiceProvider,
    private _loadingCtrl: LoadingController,
    private _alertCtrl: AlertController,
    public http: Http,
    public viewCtrl: ViewController,
    private diagnostic: Diagnostic) {
      
  }


  montandorota;
  loadJson = false;
  getPositionAtual = false;
  jsonCentral;
  lat
  log
  end

  ionViewDidLoad() {
  }
 
   locAtual(jsonreceb) {
   let _alert = this._alertCtrl.create({
      'title': this.service.config.name,
      'message': 'Para traçar a rota, ative sua localização.',
      buttons: ['OK']
    });
    this.diagnostic.requestLocationAuthorization()
    .then((state) => {
    this.diagnostic.isLocationAvailable()
    .then((state) => {
        if (state==true) {
      // if (state == this.diagnostic.bluetoothState.POWERED_ON){
        let options = {timeout: 120000, enableHighAccuracy: true};
        navigator.geolocation.getCurrentPosition((resp) => {
          console.log(resp)
          resp.coords.latitude
          resp.coords.longitude
          console.log(resp)
          this.lat = this.jsonCentral.lista[0].latitude;
          this.log = this.jsonCentral.lista[0].longitude;
          this.end = this.jsonCentral.lista[0].descricao.str.lang_1;
          var enderecoParse = this.end.split(' ').join('+');
          //var urlRota = "https://www.google.com.br/maps/dir/@lat_o,@lon_o/@end/@@lat_d,@lon_d,10z/?hl=pt-BR";
          var LatAtual = resp.coords.latitude;
          var LongAtual = resp.coords.longitude;
          this.montandorota = "https://www.google.com.br/maps/dir/" + LatAtual + "," + LongAtual + "/" + enderecoParse + "/@" + this.lat + "," + this.log + "/?hl=pt-BR";
          window.open(this.montandorota, '_system', 'location=no');
          console.log(this.montandorota)
          this.loading.dismiss()
        },
        (error) => {
          this.loading.dismiss()
          console.log(error);
        },
        options
      ); 
     }else{
      this.loading.dismiss()
      _alert.present()
     }
    })
  })
  }
  public RotaComoChegar() {
    var headers = new Headers();
    this.http.get(this.service.config.jsonDomain + "jsoncomochegar.json", { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Rota = data,
        this.Dominio = this.service.config.defaultDomain
        console.log(this.Rota)

      }, //this.product = JSON.stringify(data),
      err => {
        console.log('deuruim', err)
      }//this.status.error = JSON.stringify(err)
      );
  }
 tracarRota = function () {
  this.loading = this._loadingCtrl.create({
    content: ' Aguarde...',
    dismissOnPageChange: true
});
    this.loading.present()
        this.http.get(this.service.config.jsonDomain + "jsoncomochegar.json").map(res => res.json())
        .subscribe(
        data => {
          this.jsonCentral = data,
          this.Dominio = this.service.config.defaultDomain
          console.log(this.jsonCentral)
          this.locAtual(this.jsonCentral);
      }, 
      err => {
        console.log('deuruim', err)
      }
      );
    }
}
