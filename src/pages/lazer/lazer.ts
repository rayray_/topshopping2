import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http, Response, Headers } from '@angular/http';
import { ServiceProvider } from '../../providers/service/service';
import { LazerInternaPage } from '../lazer-interna/lazer-interna';

import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-lazer',
  templateUrl: 'lazer.html',
})
export class LazerPage {

  item
  Dominio=this.service.config.defaultDomain
  Pagina = 0
  Token
  nomeShopping = this.service.config.name;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private storage: Storage,) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.Lazer()
      refresher.complete();
    }, 2000);
  }
  public Lazer(){
    var headers = new Headers();
    
    let geranum = Math.random()*9999999;
    let nocache = Math.round(geranum);
   this.http.get(this.service.config.defaultDomain + "/api/mobile/EntretenimentoList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5").map(res => res.json())
    .subscribe(
    data => {
      this.item=data.lista,
      this.Dominio = this.service.config.defaultDomain
      console.log(this.item)
        this.storage.set('lazer', this.item);
    },
    err =>  {
      console.log('deuruim', err)
       this.storage.get('lazer').then((val) => {
          console.log('Your age is', val);
          this.item=val
        });
      }
    );
  }
  IrParaInterna(localNavCtrl: boolean = true, event, c) {
      this.navCtrl.push(LazerInternaPage,{
        c: event
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LazerPage');
    this.Lazer()
  }

}
