import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LazerPage } from './lazer';

@NgModule({
  declarations: [
    LazerPage,
  ],
  imports: [
    IonicPageModule.forChild(LazerPage),
  ],
})
export class LazerPageModule {}
