import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import { ServiceProvider } from '../../providers/service/service';
import { LoginPage } from '../login/login';
import { CuponsInternaPage } from '../cupons-interna/cupons-interna';
@IonicPage()
@Component({
  selector: 'page-cupons',
  templateUrl: 'cupons.html',
})
export class CuponsPage {

  rootNavCtrl: NavController;
  ListaCupons
  Dominio
  Pagina=0
  Data
  Token= JSON.parse(window.localStorage.getItem('cacheUsuario'));
  
  LoginPage=LoginPage
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,) {
      this.rootNavCtrl = navParams.get('rootNavCtrl');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.Cupons()
      refresher.complete();
    }, 2000);
  }
  Cupons(){
    let headers = new Headers();
    headers.append('Authorization', this.Token.Token);
    this.http.get(this.service.config.defaultDomain+"/api/Mobile/CuponsPromocionaisList2?Pagina=" + this.Pagina + "&Items=5", {"headers": headers}).map(res => res.json())
    .subscribe(
    data => {
      this.ListaCupons=data.Cupons,
      this.Dominio = this.service.config.defaultDomain
    },
    err =>  {
      console.log('deuruim', err)
    }
    );
  }
  CountPaginacaoCupons() {
    let headers = new Headers();
    headers.append('Authorization', this.Token.Token);
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain+"/api/Mobile/CuponsPromocionaisList2?Pagina=" + this.Pagina + "&Items=5", {"headers": headers}).map(res => res.json())
      .subscribe(
      data => {
        this.Data = data.Cupons
        if (this.Data != undefined) {
          for (var i = 0; i < 5; i++) {
            this.ListaCupons.push(this.Data[i]);
          }
        }
      },
      err => {
        console.log('deuruim', err)
      }//this.status.error = JSON.stringify(err)
      );
  }
  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      if (this.ListaCupons != undefined) {
        this.CountPaginacaoCupons()
        console.log('Async operation has ended');
        infiniteScroll.complete();
      } else {
        console.log('travo')
      }

    }, 500);
  }

  
  IrParaInterna(localNavCtrl: boolean = true, event, c) {
      this.rootNavCtrl.push(CuponsInternaPage,{
        c: event
      });
  }
  irParaLogin(localNavCtrl: boolean = true, event, s) {
    this.rootNavCtrl.push(LoginPage);
  }
  ionViewDidLoad() {
    if(this.Token!=undefined){
      this.Cupons()
      console.log('VeioTOken')
    }
      console.log('Veio no didload')
    }

}
