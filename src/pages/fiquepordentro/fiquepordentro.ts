import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http, Response, Headers } from '@angular/http';
import { ServiceProvider } from '../../providers/service/service';
import{FiquepordentrointernaPage} from '../fiquepordentrointerna/fiquepordentrointerna'

import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-fiquepordentro',
  templateUrl: 'fiquepordentro.html',
})

export class FiquepordentroPage {

  rootNavCtrl: NavController;
  Novidades
  Dominio=this.service.config.defaultDomain
  Pagina=0
  Data
  NaoDisponivel
  
  constructor(
    public navCtrl: NavController,
     public navParams: NavParams,
     public http: Http,
     public service: ServiceProvider,
    private storage: Storage,
    ) {
      this.rootNavCtrl = navParams.get('rootNavCtrl');
  }
  
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.Pagina=0
      this.FiquePorDentro()
      refresher.complete();
    }, 2000);
  }
  FiquePorDentro(){
    let headers = new Headers();
    this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5", { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Novidades = data.lista,
        this.Dominio = this.service.config.defaultDomain,
        this.storage.set('Novidades', this.Novidades);
        if(this.Novidades.length==0){
          this.NaoDisponivel=true;
        }else{
          this.NaoDisponivel=false
        }
        // console.log(this.Novidades, 'Novidade')
      },
      err => {
        console.log('deuruim', err)
         this.storage.get('Novidades').then((val) => {
          console.log('Your age is', val);
          this.Novidades=val
        });
        }
      );
  }

  CountPaginacaoFiquePorDentro() {
    var headers = new Headers();
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6', { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Data = data.lista
        if (this.Data != undefined) {
          for (var i = 0; i < 6; i++) {
            this.Novidades.push(this.Data[i]);
          }
        }
      },
      err => {
        console.log('deuruim', err)
      }//this.status.error = JSON.stringify(err)
      );
  }
  doInfinite(infiniteScroll) {
    // console.log('Begin async operation');
    setTimeout(() => {
      if(this.Novidades!=undefined){
      // console.log('Async operation has ended');
      this.CountPaginacaoFiquePorDentro()
      infiniteScroll.complete();
    }else{
      ('travo')
    }
    
    }, 500);
  }
  IrParaInterna($event, nov) {
    this.rootNavCtrl.push(FiquepordentrointernaPage, {
      novidade: nov
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FiquepordentroPage');
    this.FiquePorDentro();
    
  }

}
