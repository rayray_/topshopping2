import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiquepordentroPage } from './fiquepordentro';

@NgModule({
  declarations: [
    FiquepordentroPage,
  ],
  imports: [
    IonicPageModule.forChild(FiquepordentroPage),
  ],
})
export class FiquepordentroPageModule {}
