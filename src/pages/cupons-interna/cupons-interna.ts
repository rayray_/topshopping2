import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SharedService} from '../../providers/shared-service/shared-service';

@IonicPage()
@Component({
  selector: 'page-cupons-interna',
  templateUrl: 'cupons-interna.html',
  providers: [SocialSharing]
})
export class CuponsInternaPage {
  public firstParam;
  Dominio
  vota
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    private socialSharing: SocialSharing,
    public SharedService: SharedService) {
      this.Dominio = this.service.config.defaultDomain
      this.firstParam = this.navParams.get("c");
      console.log(this.firstParam);
      this.vota=this.SharedService.checkLike("lojas", this.firstParam);
      console.log(this.vota, 'vota')
  }
  likeVote(loja) {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("lojas", loja);
  }
  subject
  Compartilhar(){
    this.socialSharing.share(this.firstParam.Titulo + this.service.config.HashTag + this.service.config.site, this.subject, this.firstParam.Imagem).then(() => {
      // Sharing via email is possible
      console.log('Entrou no Share TUDO!')
    }).catch(() => {
      console.log()
      // Sharing via email is not possible
    });
  }

  ionViewDidLoad() {

  }
}
