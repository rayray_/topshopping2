import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CuponsInternaPage } from './cupons-interna';

@NgModule({
  declarations: [
    CuponsInternaPage,
  ],
  imports: [
    IonicPageModule.forChild(CuponsInternaPage),
  ],
})
export class CuponsInternaPageModule {}
