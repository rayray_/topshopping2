import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, LoadingController, AlertController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { MyApp} from '../../app/app.component'
import { SharedService } from '../../providers/shared-service/shared-service';



@Component({
  selector: 'social-share',
  templateUrl: 'social-share.html',
  providers: [SharedService]
})
export class SocialSharePage {
    Dominio
    novidade
    vota
  conteudo: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public shared: SharedService,
    public service: ServiceProvider,    
    private socialSharing: SocialSharing,
    private _loadingCtrl: LoadingController,
   public _alertCtrl: AlertController,
    

    public viewCtrl: ViewController) {
        this.Dominio = this.service.config.defaultDomain
        this.novidade = this.navParams.get("novidade");
        console.log(this.novidade)


  }
  

  ionViewDidLoad() {

  }
  facebookShare($event, nov){
    let loader = this._loadingCtrl.create({
      content: 'Aguarde...',
      dismissOnPageChange: true
    });
    loader.present()

    // var msg  = this.compilemsg(nov);
    //  this.socialSharing.shareViaFacebook(nov.nome + this.config.config.HashTag,
    //   nov.midia.arquivos.url);
      this.socialSharing.shareViaFacebook(nov.nome + this.service.config.HashTag,
        nov.midia.arquivos.url).then(() => {
        // Sharing via email is possible
        loader.dismiss()
      }).catch(() => {
        // Sharing via email is not possible
        let alert = this._alertCtrl.create({
          title: '<img src="assets/icon/error.png" /> ',
          subTitle: 'Verifique se você tem o aplicativo instalado.',
          buttons: ['OK'],
          cssClass: 'alertCustomCssError'
          // cssClass: 'alertCustomCssError'
        });
        alert.present()
        loader.dismiss()
      });
   }
   twitterShare($event, nov){
    let loader = this._loadingCtrl.create({
      content: 'Aguarde...',
      dismissOnPageChange: true
    });
    loader.present()
    // var msg  = this.compilemsg(nov);
    // this.socialSharing.shareViaTwitter(nov.nome + this.config.config.HashTag,
    //       nov.midia.arquivos.url);
    this.socialSharing.shareViaTwitter(nov.nome + this.service.config.HashTag,
      nov.midia.arquivos.url).then(() => {
      // Sharing via email is possible
      loader.dismiss()
    }).catch(() => {
      // Sharing via email is not possible
      let alert = this._alertCtrl.create({
        title: '<img src="assets/icon/error.png" /> ',
        subTitle: 'Verifique se você tem o aplicativo instalado.',
        buttons: ['OK'],
        cssClass: 'alertCustomCssError'
        // cssClass: 'alertCustomCssError'
      });
      alert.present() 
       loader.dismiss()
    });
  }
  whatsappShare($event, nov){
    let loader = this._loadingCtrl.create({
      content: 'Aguarde...',
      dismissOnPageChange: true
    });
    loader.present()
    // var msg  = this.compilemsg(nov);
    //  this.socialSharing.shareViaWhatsApp(nov.nome + this.config.config.HashTag,
    //   nov.midia.arquivos.url);
    this.socialSharing.shareViaWhatsApp(nov.nome + this.service.config.HashTag,
      nov.midia.arquivos.url).then(() => {

    loader.dismiss()
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
      let alert = this._alertCtrl.create({
        title: '<img src="assets/icon/error.png" /> ',
        subTitle: 'Verifique se você tem o aplicativo instalado.',
        buttons: ['OK'],
        cssClass: 'alertCustomCssError'
        // cssClass: 'alertCustomCssError'
      });
      alert.present()
      loader.dismiss()
      
    });
   }
  close() {
    this.viewCtrl.dismiss();
  }
}