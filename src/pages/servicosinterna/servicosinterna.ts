import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';

import { SharedService} from '../../providers/shared-service/shared-service';
@IonicPage()
@Component({
  selector: 'page-servicosinterna',
  templateUrl: 'servicosinterna.html',
  providers: [SocialSharing]
})
export class ServicosinternaPage {
  Dominio
  servico
  vota
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    private socialSharing: SocialSharing,
    public SharedService: SharedService
  ) {
    this.Dominio = this.service.config.defaultDomain
    this.servico = this.navParams.get("s");
    console.log(this.servico);
    this.vota=this.SharedService.checkLike("servico", this.servico);
    console.log(this.vota, 'vota')
  }

  likeVote(servico) {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("servico", servico);
  }

  Compartilhar(){
    this.socialSharing.share(
      this.servico.nome.str.lang_1,'Aplicativo Partage São Gonçalo',
    ).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad ServicosinternaPage');
  }

}
