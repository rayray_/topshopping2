import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServicosinternaPage } from './servicosinterna';

@NgModule({
  declarations: [
    ServicosinternaPage,
  ],
  imports: [
    IonicPageModule.forChild(ServicosinternaPage),
  ],
})
export class ServicosinternaPageModule {}
