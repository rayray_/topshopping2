import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, LoadingController } from 'ionic-angular';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { FilmesPage } from '../filmes/filmes';
import { ListFilmesPage } from '../../pages/list-filmes/list-filmes'
import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';
import * as $ from 'jquery'
import { SharedService} from '../../providers/shared-service/shared-service';

@IonicPage()
@Component({
  selector: 'page-filmes-interna',
  templateUrl: 'filmes-interna.html',
  providers: [SocialSharing]
})
export class FilmesInternaPage {
  vota
  Filme;
  Dominio
  listHorarios
  arrDias = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'];
  dataHoje: any = new Date();
  dia: any = this.dataHoje.getDay();
  diaH: any = this.arrDias[this.dia].valueOf();
  dtHoje = this.diaH + ',';
  Cartazeslist
  CartazLink
  slideOptions
  FilmesPage = FilmesPage
  TemIngresso: any = this.service.config.IngressoOnline
  @ViewChild('SlideFilmes') slides1: Slides;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public service: ServiceProvider,
    private _loadingCtrl: LoadingController,
    private socialSharing: SocialSharing,
    public http: Http,
    public SharedService: SharedService

  ) {
    this.Dominio = this.service.config.defaultDomain
    this.Filme = this.navParams.get("f");
    console.log(this.Filme);
    this.Salas()
    this.CinemaCartazes()
    this.dtHoje;
    console.log(this.Filme)
  
    this.vota=this.SharedService.checkLike("cinema", this.Filme);
    console.log(this.vota, 'vota')
    
  }
  likeVote() {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("cinema", this.Filme);
  }
  Salas() {
    this.Filme;
    const SALA = this.Filme.salas
    const jsonLimpo = []
    const arrDias = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'];
    const dataHoje: any = new Date();
    const dia: any = dataHoje.getDay();
    const diaH: any = arrDias[dia].valueOf();
    const dtHoje = diaH + ',';
    for (var x = 0; x < SALA.length; x++) {
      if (jsonLimpo.find(function (data) { return data.sala == SALA[x].sala }) == null) {
        jsonLimpo.push({ sala: SALA[x].sala, horario: SALA[x].horas });
        //  console.log('---------');
        //  console.log(SALA[x]);
      } else {
        var elem = jsonLimpo.find(function (data) { return data.sala == SALA[x].sala });
        elem.horario = elem.horario.concat(SALA[x].horas)
      }

    }
    jsonLimpo.forEach(

      function (e) {
        const DiaDasemana = dtHoje;
        // console.log(DiaDasemana)
        e.horario.forEach(function (p) {

          if (p.dias == DiaDasemana) {
            // p.hora = p.hora.split(",");
            p.hora = p.hora.toString().split(',');
            // console.log(p.hora);
            // console.log(dtHoje);
          };
        });
      }
    );

    this.listHorarios = jsonLimpo;
    // console.log(this.listHorarios, 'Listahorarios')
  }

  CinemaCartazes() {
    var headers = new Headers();
    this.http.get(this.service.config.jsonDomain + "jsonfilmes.json", { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Cartazeslist = data.lista,
          this.CartazLink = this.service.config.pipocaoCartazHome
        for (let i = 0; i < data.lista.length; i++) {
          let findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
          let changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
          data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
        }


      }, //this.product = JSON.stringify(data),
      err => {
        console.log('deuruim', err)
      }
      );
  }

  subject
  Compartilhar() {
    let loader = this._loadingCtrl.create({
      content: 'Aguarde...',
      dismissOnPageChange: true
    });
    loader.present()
    this.socialSharing.share(
      this.Filme.nome.str.lang_1 + this.service.config.HashTag +  ' www.topshopping.com.br', this.subject,this.Filme.midia.arquivos.lang_1.arquivo.url

    ).then(() => {
      
      loader.dismiss()

    }).catch(() => {
      
      loader.dismiss()
    
    });
  }

  IrParaFilmes() {
    this.navCtrl.setRoot(ListFilmesPage);
  }
  IrListaDeFilmes() {
    this.navCtrl.setRoot(FilmesPage);
  }

  IrParaInternaDeOutroFilme(event, f) {
    this.navCtrl.remove(1)
    this.navCtrl.push(FilmesInternaPage, {
      f: f
    });
    
  }

  LinkIngresso() {
    window.open(this.service.config.LinkIngresso, '_system', 'location=no');
  }

  SlideResize() {
    this.slides1.resize()
  }
  
  config: SwiperOptions = {
    slidesPerView: 3.5,
    paginationClickable: true,
    spaceBetween: 15,
    freeMode: true
  };
  ionViewDidLoad() {


}
}
