import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FilmesInternaPage } from './filmes-interna';

@NgModule({
  declarations: [
    FilmesInternaPage,
  ],
  imports: [
    IonicPageModule.forChild(FilmesInternaPage),
  ],
})
export class FilmesInternaPageModule {}
