import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';
import {GastronomiaInternaPage} from '../gastronomia-interna/gastronomia-interna'

import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-gastronomia',
  templateUrl: 'gastronomia.html',
})
export class GastronomiaPage {

  Token
  Dominio
  Data
  Pagina=0
  gastronomia
  allStores
  searching=true;
  categories = []
  letras = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0-9"]
  BuscaVitrine: string = ''
  CategoriaBusca:string = ''
  letraselecionada:string = ''
  activeletra
  nadaEncontrado

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private storage: Storage, ) {
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.Gastronomia()
      refresher.complete();
    }, 2000);
  }
  Gastronomia() {
    this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6').map(res => res.json())
      .subscribe(
      data => {
        this.gastronomia = data.lista;
        this.Dominio = this.service.config.defaultDomain;
        this.allStores = data.lista;
        this.searching=false;
        this.storage.set('gastronomia', this.gastronomia);
      },
      err => {
        console.log('deuruim lojas', err);
        this.searching=false;
        this.storage.get('gastronomia').then((val) => {
          console.log('Your age is', val);
          this.gastronomia=val
        });
      }
      );
  }

  Segmentos() {
    this.http.get(this.service.config.defaultDomain + "/api/mobile/SegmentosGastronomiaList?codCliente="+this.service.config.idShopping).map(res => res.json())
      .subscribe(
      data => {
        this.Segmentos = data.lista;
        this.Dominio = this.service.config.defaultDomain;
        this.searching = false;
        this.storage.set('segmentosgastronomia', this.Segmentos);
      },
      err => {
        console.log('deuruim lojas', err);
        this.storage.get('segmentosgastronomia').then((val) => {
          console.log('Your age is', val);
          this.Segmentos=val
        });
      }
      );
  }
CountPaginacaoGastronomia() {
    var headers = new Headers();
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6', { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Data = data.lista
          for (var i = 0; i < 6; i++) {
            this.gastronomia.push(this.Data[i]);
          }
        
      },
      err => {
        console.log('deuruim', err)
      }//this.status.error = JSON.stringify(err)
      );
  }

  filter
  AbrirFiltro() {
    if (!this.filter) {
      this.filter = true
    } else {
      this.filter = false;
    }
  }

  Filtrar() {
    this.http.get(this.service.config.defaultDomainHomolog + "/api/mobile/GastronomiaList?codCliente="+ this.service.config.idShopping +"&Nome="+this.BuscaVitrine+"&Letra="+this.letraselecionada+"&CodSegmento="+this.CategoriaBusca).map(res => res.json())
      .subscribe(
      data => {
        this.gastronomia = data.lista;
        this.filter = false;
        if(this.gastronomia==''){
        this.nadaEncontrado=true;
        }else{
          this.nadaEncontrado=false;
        }
      },
      err => {
        this.filter = false;
        console.log('deuruim lojas', err);
        this.nadaEncontrado=true;
      }
      );
  }

  selectedItem
  listClick(event, newValue) {
    console.log(newValue);
    this.selectedItem = newValue;
    this.letraselecionada=newValue;
    this.Filtrar()
    this.letraselecionada=''
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      if(this.gastronomia!=undefined){
      this.CountPaginacaoGastronomia()
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }else{
      ('travo')
    }
    
    }, 500);
  }
  IrParaInterna($event, g) {
    this.navCtrl.push(GastronomiaInternaPage,{
      gastr: g
    });
  }
  ionViewDidLoad() {
    this.Gastronomia()
    this.Segmentos()
    console.log('ionViewDidLoad GastronomiaPage');
  }

}
