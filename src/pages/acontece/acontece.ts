import { Component, EventEmitter ,Output} from '@angular/core';
import { IonicPage, NavController, Slides, ViewController,NavParams,Events } from 'ionic-angular';
import { FeedPage } from '../feed/feed';
import { FiquepordentroPage } from '../fiquepordentro/fiquepordentro';
import { EnquetesPage } from '../enquetes/enquetes'
import { LoginPage } from '../login/login';

import { SuperTabsController } from 'ionic2-super-tabs'
import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';

@IonicPage({
  segment: 'acontece/:type'
})
@Component({
  selector: 'page-acontece',
  templateUrl: 'acontece.html',
})
export class AcontecePage {

  showTitles: boolean = true;
  EnquetesPage: any = EnquetesPage;
  FeedPage = FeedPage;
  FiquepordentroPage = FiquepordentroPage

  Dominio
  verEnquete
  enquete
  Token
  NaoDisponivel
  Feed
  Pagina = 5
  respostas = []
  rootNavCtrl
  searching
 
  constructor(
    public navCtrl: NavController,
    public service: ServiceProvider,
    public viewCtrl: ViewController,
    public superTabsCtrl: SuperTabsController,
    public http: Http,
    private navParams: NavParams,
    public events: Events,
    ) {    
  
  }
  
  
  public SocialFeed(){
    var headers = new Headers();
    this.http.get(this.service.config.defaultDomain+"/api/mobile/RedesSociaisList?codCliente="+this.service.config.idShopping+'&Pagina='+this.Pagina+'&Items=5', {"headers": headers}).map(res => res.json())
    .subscribe(
    data => {
      this.Feed=data.lista,
      this.Dominio = this.service.config.defaultDomain
      this.searching=false;

      if(this.Feed.length==0){
        this.NaoDisponivel=true;
      }else{
        this.NaoDisponivel=false
      }
    }, 
    err =>  {
      console.log('deuruim', err)
    }
    );
  } 
  ionViewDidLoad() {
    console.log('ionViewDidLoad AcontecePage');
    this.SocialFeed()
  }

  onTabSelect(ev: any) {
 
  }
  
}
