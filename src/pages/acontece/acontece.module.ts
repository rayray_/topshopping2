import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AcontecePage } from './acontece';

@NgModule({
  declarations: [
    AcontecePage,
  ],
  imports: [
    IonicPageModule.forChild(AcontecePage),
  ],
})
export class AcontecePageModule {}
