import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CadastrePage } from './cadastre';
import { TextMaskModule } from 'angular2-text-mask';

@NgModule({
  declarations: [
    CadastrePage,
  ],
  imports: [
    IonicPageModule.forChild(CadastrePage),
    TextMaskModule,
  ],
})
export class CadastrePageModule {}
