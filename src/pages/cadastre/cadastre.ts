import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController, NavParams, Events } from 'ionic-angular';

import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HomePage } from '../home/home';
import { EsquecisenhaPage } from '../esquecisenha/esquecisenha';

import { NativeStorage } from '@ionic-native/native-storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { ServiceProvider } from '../../providers/service/service';
import { FacebookService } from '../../providers/facebook/facebook';

import { TextMaskModule } from 'angular2-text-mask';
import { FormsModule } from '@angular/forms';
@IonicPage()
@Component({
  selector: 'page-cadastre',
  templateUrl: 'cadastre.html',
  providers: [FacebookService],
  // directives: [FORM_DIRECTIVES]
})
export class CadastrePage {
  Formulario = false;
  Sucesso = false;


  public form = {
    Nome: '',
    Email: '',
    Telefone: '',
    Sexo: '',
    EstadoCivil: '',
    Senha: '',
    ConfirmaSenha: '',
    Facebook: ''
  }

  public maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  selectOptions = {
    title: 'Selecione o sexo',
  };
  SelectEstadoCivil = {
    title: 'Estado Civil'
  }

  HomePage = HomePage
  EsquecisenhaPage = EsquecisenhaPage
  CadastroForm: FormGroup;
  name: string = '';
  profileFB: any
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    public service: ServiceProvider,
    public http: Http,
    private Facebook: Facebook,
    private NativeStorage: NativeStorage,
    public facebookService: FacebookService,
    public events: Events,
    private fb: FormBuilder) {

    this.profileFB = this.navParams.get("profile");
    console.log(this.profileFB)
    if (this.profileFB != undefined) {
      this.form.Nome = this.profileFB.name;
      if (this.profileFB.email != undefined) {
        this.form.Email = this.profileFB.email;
      }
      this.form.Facebook = this.profileFB.id;
    }

    this.CadastroForm = fb.group({
      'name': ['', [Validators.required, Validators.minLength(3)]],
      'email': ['', [Validators.required, this.EmailValidacao.bind(this)]],
      'Telefone': ['', [Validators.required, this.TelefoneValidacao.bind(this)]],
      'Sexo': ['', Validators.required],
      'EstadoCivil': ['', Validators.required],
      'DataNascimento': ['', [Validators.required, this.DataNascimentoValidacao.bind(this)]],
      'Senha': ['', [Validators.required, Validators.minLength(5)]],
      'ConfirmarSenha': ['', [Validators.required, Validators.minLength(5)]],
    });

  }

  NomeValidacao(control: FormControl): { [s: string]: boolean } {
    if (!control.value.match("^[a-zA-Z ,.'-]+$")) {
      return { invalidName: true };
    }
  }

  EmailValidacao(control: FormControl): { [s: string]: boolean } {
    if (!(control.value.match('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'))) {
      return { invalidEmail: true };
    }
  }
  TelefoneValidacao(control: FormControl): { [s: string]: boolean } {
    if (control.value !== '') {
      if (!control.value.match('\\(?\\d{2}\\)?-? *\\d{4}-? *-?\\d{4}')) {
        return { invalidPhone: true };
      }
    }
  }
  DataNascimentoValidacao(control: FormControl): { [s: string]: boolean } {
    if (control.value !== '') {
      if (!control.value.match('[0-9]{2}\/[0-9]{2}\/[0-9]{4}$')) {
        return { invalidPhone: true };
      }
    }
  }
  finalizar() {

    let loader = this._loadingCtrl.create({
      content: 'Aguarde...',
      dismissOnPageChange: true
    });
   loader.present();
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });
    let body = `Nome=${this.form.Nome}&Email=${this.form.Email}&Facebook=${this.form.Facebook}&Telefone=${this.form.Telefone}&Sexo=${this.form.Sexo}&EstadoCivil=${this.form.EstadoCivil}&Senha=${this.form.Senha}&Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}`;
    return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPAdd2', body, { headers: headers }).map(res => res.json())
      .subscribe((data) => {
        if (data.Sucess != false) {
          this.Formulario = true,
            loader.dismiss(),
            this.Sucesso = true,
            console.log(data)
          this.Sucesso = true
          return false;
        } else {
          this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: data.Descricao,
            buttons: [{ text: 'Ok' }]
          }).present();
        }

      }
      , err => {
        console.log("ERROR!: ", err);
        //  this.loader.dismiss();ion
        var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
          if (mensagem.indexOf('encontra cadastrado') != -1) {
            this._alertCtrl.create({
              title: this.service.config.name,
              subTitle: "Esse e-mail já se encontra cadastrado. Informe outro e-mail ou <span (click)='EsqueciPage()'>clique aqui</span> caso tenha esquecido sua senha.",
              buttons: [{ text: 'Ok' }]
            }).present();

            loader.dismiss()
          }
          else {
          this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: mensagem,
            buttons: [{ text: 'Ok' }]
          }).present();
            loader.dismiss()
          }
      }
      );
  }
  createUser2() {
    setTimeout(() => {
      console.log('User Login')
      let profile = JSON.parse(localStorage.getItem('profile'));
      this.events.publish('user:Cadastre', profile, Date.now());
    }, 1000)
  }
  EsqueciPage(){
    this.navCtrl.setRoot(EsquecisenhaPage);
  }
  PegarDadosFb() {
    let permissions = new Array<string>();
    let nav = this.navCtrl;
    permissions = ['public_profile', 'user_friends', 'email'];

    this.Facebook.login(permissions).then((response) => {
      let userId = response.authResponse.userID;
      let params = new Array<string>();
      this.Facebook.api("/me?fields=name,gender,email,birthday", params)
        .then(function (profile) {
          profile.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
          localStorage.setItem('profile', JSON.stringify(profile))
          this.form.Facebook = userId;
        })

      this.createUser2()
    }, (error) => {
      console.log(error);
    })
    this.getDadosFb()
  }
  // infofb = JSON.parse(localStorage.getItem('profile'));

  public getDadosFb() {
    this.events.subscribe('user:Cadastre', (profile) => {
      this.form.Nome = profile.name;
      if (profile.email != undefined) {
        this.form.Email = profile.email;
      }
      this.form.Facebook = profile.id;
    })
  }
  Logado
  VoltarHomeLogado(){
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });

    let body = `Email=${this.form.Email}&Senha=${this.form.Senha}&ClienteCod=${this.service.config.idShopping}`;

    return this.http.post(this.service.config.defaultDomain + '/api/Mobile/Login', body, { headers: headers }).map(res => res.json())
      .subscribe((data) => {
        this.Logado = true;
        localStorage.setItem("cacheUsuario", JSON.stringify(data));
        localStorage.removeItem('profile')
        this.events.publish('user:Login', data, this.Logado);
        this.navCtrl.setRoot(HomePage, {
          AparecerAlert: 'Apareça'}
        )
        console.log('caiuno if')
      }, err => {
        console.log("ERROR!: ", err);

      }
      )
  }

  InfosFb:any;
  getUserDetail(userid) {
    this.Facebook.api("/"+userid+"/?fields=id,email,name,picture,gender",["public_profile"])
      .then(res => {
        console.log(res);
        this.InfosFb = res;
        this.http.get(this.service.config.defaultDomain + '/api/Mobile/VerificaFacebook?FacebookId='+this.InfosFb.id+ '&ClienteCod='+this.service.config.idShopping).map(res => res.json())
        .subscribe((data) => {
          console.log(data)
          if(data.Success==true){

            let headers = new Headers({
              'Content-Type': 'application/x-www-form-urlencoded'
            });
            let options = new RequestOptions({ headers: headers });
            let body = `Facebook=${this.InfosFb.id}&ClienteCod=${this.service.config.idShopping}`;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(res => res.json())
            .subscribe((data) => {
              this.Logado = true;
              localStorage.setItem("cacheUsuario", JSON.stringify(data));
              localStorage.removeItem('profile')
              this.events.publish('user:Login', data, this.Logado);
              this.navCtrl.setRoot(HomePage, {
                AparecerAlert: 'Apareça'}
              )
            })
          }else{
            let headers = new Headers({
              'Content-Type': 'application/x-www-form-urlencoded'
            });
            let options = new RequestOptions({ headers: headers });
            let body = `Nome=${this.InfosFb.name}&Email=${this.InfosFb.email}&Facebook=${this.InfosFb.id}&Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}`;
            return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPAdd2', body, { headers: headers }).map(res => res.json())

            .subscribe((data) => {
              console.log(data);
              let headers = new Headers({
                'Content-Type': 'application/x-www-form-urlencoded'
              });
              let options = new RequestOptions({ headers: headers });
              let body = `Facebook=${this.InfosFb.id}&ClienteCod=${this.service.config.idShopping}`;
              return this.http.post(this.service.config.defaultDomain + '/api/Mobile/LoginFacebook', body, { headers: headers }).map(res => res.json())
                .subscribe((data) => {
                  this.Logado = true;
                  localStorage.setItem("cacheUsuario", JSON.stringify(data));
                  localStorage.removeItem('profile')
                  this.events.publish('user:Login', data, this.Logado);
                  this.navCtrl.setRoot(HomePage, {
                    AparecerAlert: 'Apareça'}
                  )
                })
            }, err => {
              console.log("ERROR!: ", err);
      
            })
          }
        })
      })
      .catch(e => {
        console.log(e);
      });
  }
  AparecerAlert='Apareça'
  public LogarFb() {
    this.Facebook.login(['public_profile', 'user_friends', 'email'])
    .then(res => {
      if(res.status === "connected") {
        this.getUserDetail(res.authResponse.userID);
      } else {
      }
    })
    .catch(e => console.log('Error logging into Facebook', e));
  }
  // myForm:FormGroup;
  ionViewDidLoad() {
    console.log('ionViewDidLoad CadastrePage');


  }
  ionViewWillLeave() {
    localStorage.removeItem('profile')
  }
}
