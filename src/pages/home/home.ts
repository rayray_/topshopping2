import { Component, EventEmitter } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, Events, AlertController,Platform } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { Http, Headers } from '@angular/http';
import { FilmesPage } from '../filmes/filmes'
import { FilmesInternaPage } from '../filmes-interna/filmes-interna';
import{FiquepordentrointernaPage} from '../fiquepordentrointerna/fiquepordentrointerna'
import { ListFilmesPage } from '../../pages/list-filmes/list-filmes'
import { CampanhaPage } from '../campanha/campanha'
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  Usuario: any = localStorage.getItem("cacheUsuario");
  Logado;
  banners
  Novidades
  Filmes
  Dominio =this.service.config.defaultDomain
  CartazLink
  slideOptions
  Header
  Pagina: number = 0
  Data
  Campanha
  ApareceBuscaPage;
  AlertaBoasVindas;

  constructor(
    public navCtrl: NavController,
    public service: ServiceProvider,
    public http: Http,
    public viewCtrl: ViewController,
    private _alertCtrl: AlertController,
    public navParams: NavParams,
    public events: Events,
    private storage: Storage,
    public platform: Platform,
  ) {
    this.slideOptions = {
      initialSlide: 0,
      loop: false,
      direction: 'horizontal',
      pager: true,
      speed: 800
    }

    this.Sliders()
    this.CinemaCartazes()
    this.FiquePorDentro()
    this.Campanhas()

   
  }

  showAlertLoginSucesso() {
    let alert = this._alertCtrl.create({
      title: '<img src="assets/icon/confirmation.png" /> Bem Vindo!',
      subTitle: 'Olá '+this.Usuario.nomeUsuario+', você está logado!',
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });
    alert.present();
  }

  doRefresh(refresher) {
    setTimeout(() => {
      this.Sliders()
      this.Pagina=0
      this.FiquePorDentro()
      refresher.complete();
    }, 2000);
  }
  
  Sliders() {
    let headers = new Headers();
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + "jsonfloatersMobile.json?nocache=" + nocache, { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.banners = data.lista,
          this.Dominio = this.service.config.defaultDomain
        // console.log(this.banners, 'Banner')

        this.storage.set('banners', this.banners);

      }, //this.product = JSON.stringify(data),
      err => {
        // stop disconnect watch
        console.log('deuruim', err)
        this.storage.get('banners').then((val) => {
          console.log('Your age is', val);
          this.banners=val
        });
      }//this.status.error = JSON.stringify(err)
      );
  }
  someAreaCinema
  CinemaCartazes() {
    let headers = new Headers();
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + "jsonfilmes.json?nocache=" + nocache, { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Filmes = data.lista
        if(this.Filmes==0){
          this.someAreaCinema=true
        }else{
          this.someAreaCinema=false
        }
          this.CartazLink = this.service.config.pipocaoCartazHome
        // console.log(this.Filmes),
        // console.log(this.CartazLink)
        this.storage.set('Filmes', this.Filmes);

        for (let i = 0; i < data.lista.length; i++) {
          let findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
          let changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
          data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
        }


      }, //this.product = JSON.stringify(data),
      err => {

        // stop disconnect watch
        console.log('deuruim', err)
        this.storage.get('Filmes').then((val) => {
          console.log('Your age is', val);
          this.Filmes=val
        });
      }
      );
  }
  FiquePorDentro() {
    let headers = new Headers();
    this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5", { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Novidades = data.lista,
        this.Dominio = this.service.config.defaultDomain,
        this.storage.set('Novidades', this.Novidades);
        // console.log(this.Novidades, 'Novidade')
      },
      err => {

        // stop disconnect watch
        console.log('deuruim', err)
        this.storage.get('Novidades').then((val) => {
          console.log('Your age is', val);
          this.Novidades=val
        });
      }
      );
  }
  Campanhas() {
    let headers = new Headers();
    this.http.get(this.service.config.defaultDomain + "/api/mobile/CampanhaList?ClienteCod=" + this.service.config.idShopping, { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Campanha = data.Campanhas,
        this.Dominio = this.service.config.defaultDomain
        // console.log(this.Campanha, 'Campanha')
      },
      err => {
        console.log('deuruim', err)
      }
      );
  }
  CountPaginacaoFiquePorDentro() {
    var headers = new Headers();
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=5', { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.Data = data.lista
        if (this.Data != undefined) {
          for (var i = 0; i < 5; i++) {
            this.Novidades.push(this.Data[i]);
          }
        }
      },
      err => {
        console.log('deuruim', err)
      }//this.status.error = JSON.stringify(err)
      );
  }
  DetailsFiquePorDentro($event, fique) {
    this.navCtrl.push(FiquepordentrointernaPage, {
      novidade: fique
    });
  }
  doInfinite(infiniteScroll) {
    // console.log('Begin async operation');
    setTimeout(() => {
      if(this.Novidades!=undefined){
      // console.log('Async operation has ended');
      this.CountPaginacaoFiquePorDentro()
      infiniteScroll.complete();
    }else{
      ('travo')
    }
    
    }, 500);
  }
  irParaInterna = function ($event, banner) {
    var fiquepordentro=[]

    if(banner.tipo == "Campanha"){
      for (var i = 0; i < this.Campanha.length; i++) {
        console.log(this.Campanha[i].Id);
        if(banner.cod == this.Campanha[i].Id){
         fiquepordentro.push(this.Campanha[i]);
          // console.log('banner')
          this.navCtrl.push(CampanhaPage, {
            campanha: fiquepordentro
          })
        }else{
          console.log('Não Achou')
        }
      }
    }else{
        for (var i = 0; i < this.Novidades.length; i++) {
        if(banner.tipo == "Floater" && banner.url != null){
          window.open(banner.url, '_system', 'location=yes');
          
        }
          if(banner.cod==this.Novidades[i].cod){
          fiquepordentro.push(this.Novidades[i]);
            // console.log('banner')
            this.navCtrl.push(FiquepordentrointernaPage, {
              novidade: fiquepordentro
            })
          }else{
            console.log('Não Achou')
          }
        }
      // }
    }
  } 

  IrListaDeFilmes() {
    this.navCtrl.setRoot(FilmesPage);
  }

  isLogged() {
    console.log('passou na funcao')
    if (this.Usuario != undefined) {
      console.log('entronotrue')
      this.Logado = true
    } else {
      console.log('entronofalse')
      this.Logado = false
      // return false;
    }
  }
  
 

  config: SwiperOptions = {
    slidesPerView: 3.5,
    paginationClickable: true,
    spaceBetween: 15,
    freeMode: true
  };

  IrParaFilmes() {
    this.navCtrl.setRoot(ListFilmesPage);
  }

  IrParaInternaDeOutroFilme(event, filme) {
    this.navCtrl.push(FilmesInternaPage, {
      f: filme
    });
  }
  ionViewWillEnter() {
    this.viewCtrl.showBackButton(false);
    document.getElementById("AparecerHome").style.display = "block";
    document.getElementById("NaoAparecer").style.display = "none";

  } 
  ionViewWillLeave(){
    document.getElementById("AparecerHome").style.display = "none";
    document.getElementById("NaoAparecer").style.display = "block";
    
  }
  ionViewLoaded() {

    // localStorage.removeItem('resultadoDaBuscaHome')
  }
}



