import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GastronomiaInternaPage } from './gastronomia-interna';

@NgModule({
  declarations: [
    GastronomiaInternaPage,
  ],
  imports: [
    IonicPageModule.forChild(GastronomiaInternaPage),
  ],
})
export class GastronomiaInternaPageModule {}
