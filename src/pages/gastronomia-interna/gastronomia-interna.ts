import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';

import { SharedService} from '../../providers/shared-service/shared-service';

@IonicPage()
@Component({
  selector: 'page-gastronomia-interna',
  templateUrl: 'gastronomia-interna.html',
  providers: [SocialSharing]
})
export class GastronomiaInternaPage {
  Dominio
  vota
  public gastronomia
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    private socialSharing: SocialSharing,
    public SharedService: SharedService) {
      this.Dominio = this.service.config.defaultDomain
      this.gastronomia = this.navParams.get("gastr");
      console.log(this.gastronomia);

      this.vota=this.SharedService.checkLike("gastronomia", this.gastronomia);
      console.log(this.vota, 'vota')
  }
  likeVote(gastronomia) {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("gastronomia", gastronomia);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad GastronomiaInternaPage');
  }
  subject
  Compartilhar(){
    this.socialSharing.share(
      this.gastronomia.nome.str.lang_1+this.service.config.HashTag+' www.topshopping.com.br', this.subject, this.gastronomia.midia.arquivos.lang_1.arquivo.url).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }
}

