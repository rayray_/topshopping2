import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NotificacoesInternaPage } from './notificacoes-interna';

@NgModule({
  declarations: [
    NotificacoesInternaPage,
  ],
  imports: [
    IonicPageModule.forChild(NotificacoesInternaPage),
  ],
})
export class NotificacoesInternaPageModule {}
