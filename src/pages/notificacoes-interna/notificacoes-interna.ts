import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events  } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';

import { Storage } from '@ionic/storage';
import { Http, Response, Headers,RequestOptions } from '@angular/http';

import { SocialSharing } from '@ionic-native/social-sharing';


import { LojasPage } from '../lojas/lojas'
import { LojasInternaPage } from '../lojas-interna/lojas-interna'

import { AcontecePage } from '../acontece/acontece';
import { FiquepordentroPage } from '../fiquepordentro/fiquepordentro'
import { FiquepordentrointernaPage } from '../fiquepordentrointerna/fiquepordentrointerna'

import { ListShoppingPage } from '../list-shopping/list-shopping'
import { HomePage } from '../home/home';

import { MeuperfilPage } from '../meuperfil/meuperfil'

import { GastronomiaPage } from '../gastronomia/gastronomia'
import { GastronomiaInternaPage } from '../gastronomia-interna/gastronomia-interna'

import { FilmesPage } from '../filmes/filmes'
import { FilmesInternaPage } from '../filmes-interna/filmes-interna'


import { ServicosPage } from '../servicos/servicos'
import { ServicosinternaPage } from '../servicosinterna/servicosinterna'


import { LazerPage } from '../lazer/lazer'
import { LazerInternaPage } from '../lazer-interna/lazer-interna'

import { OfertasPage } from '../ofertas/ofertas';
import { VitrineInternaPage } from '../vitrine-interna/vitrine-interna';

import { CuponsInternaPage } from '../cupons-interna/cupons-interna'


@IonicPage()
@Component({
  selector: 'page-notificacoes-interna',
  templateUrl: 'notificacoes-interna.html',
  providers: [SocialSharing]
})
export class NotificacoesInternaPage {
  MsgsList
  Msg
  lojas
  lazer
  novidade
  servicos
  vitrine
  gastronomia
  constructor(
    public navCtrl: NavController, 
    private socialSharing: SocialSharing,
    public navParams: NavParams,
    public service: ServiceProvider,
    public http: Http,
   private storage: Storage,
   public events: Events) {
      this.Msg = this.navParams.get("msgInter");
      console.log(this.Msg)

      switch (this.Msg.CodConteudo) {
        case 2:
          //Loja Específica
            this.LojaEspecifica()
            this.LazerEspecifico()
            console.log(this.lojas, 'lojas')
          break;
          case 18:
          // Lazer Interna
            this.LazerEspecifico()
            console.log(this.lazer, 'lazer')
          break;
          case 4:
            //FiqueEspecifico
            this.FiquePorDentroEspecifico()
            console.log(this.novidade, 'novidade')
            break;
            case 22:
            // Lazer Interna
              this.ServicosEspecifico()
              console.log(this.servicos, 'lazer')
            break;
            case 24:
            // Lazer Interna
            this.VitrineEspecifica()
            console.log(this.vitrine, 'vitrine')
            break;
            case 9:
            // Lazer Interna
              this.GastronomiaEspecifica()
              console.log(this.gastronomia, 'gastronomia')
        default:
      }
  }

  LerAmsg(){
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': Token.Token
    });
    let options = new RequestOptions({ headers: headers });
    let body = `Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}&codMensagem=${this.Msg.Cod}`;

    return this.http.post(this.service.config.defaultDomain + '/api/Mobile/Lermensagem', body, { headers: headers }).map(res => res.json())
      .subscribe(
      data => {
        console.log(data)
      },
      err => {
        console.log(err)
        }
      );
  }

  Compartilhar() {
    this.socialSharing.share(
      this.Msg.Titulo+this.service.config.HashTag, this.Msg.Imagem
    ).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }
  
    LojaEspecifica(){
      this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?CodInterna="+this.Msg.CodInterna+"&codCliente=" + this.service.config.idShopping).map(res => res.json())
        .subscribe(
        data => {
          this.lojas = data.lista[0];
        },
        err => {
          console.log('deuruim lojas', err);
        }
      );
    }
    
    LazerEspecifico(){
      this.http.get(this.service.config.defaultDomain + "/api/mobile/EntretenimentoList?CodInterna="+this.Msg.CodInterna+"&codCliente=" + this.service.config.idShopping).map(res => res.json())
        .subscribe(
        data => {
          this.lazer = data.lista[0];
        },
        err => {
          console.log('deuruim lazer', err);
        }
      );
    }
    
    FiquePorDentroEspecifico(){
      this.http.get(this.service.config.defaultDomain + "/api/mobile/DestaquesList?CodInterna="+this.Msg.CodInterna+"&codCliente=" + this.service.config.idShopping).map(res => res.json())
        .subscribe(
        data => {
          this.novidade = data.lista[0];
        },
        err => {
          console.log('deuruim fique', err);
        }
      );
    }

    ServicosEspecifico(){
      this.http.get(this.service.config.defaultDomain + "/api/mobile/ServicosList?CodInterna="+this.Msg.CodInterna+"&codCliente=" + this.service.config.idShopping).map(res => res.json())
        .subscribe(
        data => {
          this.servicos = data.lista[0];
        },
        err => {
          console.log('deuruim serv', err);
        }
      );
    }
    
    VitrineEspecifica(){
      this.http.get(this.service.config.defaultDomain + "/api/mobile/VitrineList?CodInterna="+this.Msg.CodInterna+"&codCliente=" + this.service.config.idShopping).map(res => res.json())
        .subscribe(
        data => {
          this.vitrine = data.lista[0];
        },
        err => {
          console.log('deuruim vitri', err);
        }
      );
    }
    
    GastronomiaEspecifica(){
      this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?CodInterna="+this.Msg.CodInterna+"&codCliente=" + this.service.config.idShopping).map(res => res.json())
        .subscribe(
        data => {
          this.gastronomia = data.lista[0];
        },
        err => {
          console.log('deuruim gastr', err);
        }
      );
    }


  openDetails = function ($event,l) {

    switch (this.Msg.CodConteudo) {
      case 1:
        //Loja Lista
        this.navCtrl.push(LojasPage);
        break;
      case 2:
        //Loja Específica
        console.log(this.lojas, 'lojainterna')
        this.navCtrl.push(LojasInternaPage,
          {
            loja: this.lojas
          });
        break;
        case 3:
          // ListadeFique
          this.navCtrl.setRoot(AcontecePage);
          break;
        case 4:
          //FiqueEspecifico
          this.navCtrl.push(FiquepordentrointernaPage,
            {
              novidade: this.novidade
            });
          break;
          case 5:
            // Sobre
            this.navCtrl.setRoot(ListShoppingPage);
          break;
          case 6:
            // Home
            this.navCtrl.setRoot(HomePage);
          break;
          case 7:
            // Meu Perfil"
            this.navCtrl.setRoot(MeuperfilPage);
          break;
          case 8:
            // Alimentação lista
            this.navCtrl.setRoot(GastronomiaPage);
          break;
          case 9:
            // Alimentação Específico
            this.navCtrl.push(GastronomiaInternaPage,{
              gastr: this.gastronomia
            });
          break;
          case 10:
            // Fale conosco
            this.navCtrl.setRoot(ListShoppingPage);
          break;
          case 11:
            // Filme lista
            this.navCtrl.setRoot(FilmesPage);
          break;
          case 12:
            // Filme Específico
            this.navCtrl.push(FilmesPage,{
              f: l
            });
          break;
          case 13:
            // Como Chegar
            this.navCtrl.setRoot(ListShoppingPage);
          break;
          case 14:
            // Contato
            this.navCtrl.setRoot(ListShoppingPage);
          break;
          case 15:
            // Estacionamento
            this.navCtrl.setRoot(ServicosPage);
          break;
          case 16:
            // Horarios
            this.navCtrl.setRoot(ListShoppingPage);
          break;
          case 17:
            // Lazer lista
            this.navCtrl.setRoot(LazerPage);
          break;
          case 18:
          // Lazer Específico
            this.navCtrl.push(LazerInternaPage,{
              c: this.lazer
            });
          break;
          case 19:
          // O Shopping
            this.navCtrl.setRoot(ListShoppingPage);
          break;
          case 21:
          // Servicos Lista
            this.navCtrl.setRoot(ServicosPage);
          break;
          case 22:
          // Serviços Interna
            this.navCtrl.push(ServicosinternaPage,{
              s: this.servicos
            });
          break;
          case 23:
          // Vitrine Lista
            this.navCtrl.setRoot(OfertasPage);
          break;
          case 24:
          // Vitrine Especifica 
            this.navCtrl.push(VitrineInternaPage,{
              v:this.vitrine
            });
          break;
          case 25:
          // Enquete 
            this.navCtrl.setRoot(AcontecePage);
          break;
          case 26:
            // Enquete Especifica
            this.navCtrl.setRoot(AcontecePage);
          break;
      case "Lojas":
        // localStorage.setItem('store',JSON.stringify(l));
        this.navCtrl.push(LojasInternaPage,
          {
            loja: l
          });
      default:
    }
  }
  ListMensagens(){
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    let headers = new Headers({
      'Authorization': Token.Token
    });
    let options = new RequestOptions({ headers: headers });
  
    this.http.get(this.service.config.defaultDomain + "/api/mobile/MensagensList?Clientecod=" + this.service.config.idShopping, { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.MsgsList = data.Mensagens,
        this.events
        console.log(this.MsgsList, 'Novidade')
      },
      err => {
        console.log('deuruim', err)
        
        }
      );
  }
  ionViewDidLoad() {
    this.LerAmsg()
    
    this.events.publish('user:MensagemLida',  this.MsgsList);

    console.log('ionViewDidLoad NotificacoesInternaPage');
  }
  ionViewWillLeave() {
   this.ListMensagens()
  }
}
