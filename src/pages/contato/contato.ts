import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, NavParams, AlertController } from 'ionic-angular';

import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";

import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HomePage } from '../home/home'
@IonicPage()
@Component({
  selector: 'page-contato',
  templateUrl: 'contato.html',
})
export class ContatoPage {
  loader = this._loadingCtrl.create({
    content: 'Aguarde...',
    dismissOnPageChange: true

  });
  Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
  HomePage = HomePage
  item3
  ContatoForm: FormGroup;
  Formularios
  Dominio
  Sucesso = false
   form2 = {
    nome: '',
    email: '',
    assunto: '',
    mensagem: ''
  }
  rootNavCtrl: NavController;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    private fb: FormBuilder
  ) {
    this.InfoContato()
    if(this.Token!=undefined){
      this.form2 = {
        nome: this.Token.nomeUsuario,
        email: this.Token.emailUsuario,
        assunto: '',
        mensagem: ''
      }
        }else{
        this.form2 = {
            nome: '',
            email: '',
            assunto: '',
            mensagem: ''
          }
        }
      
    this.rootNavCtrl = navParams.get('rootNavCtrl');

    this.ContatoForm = fb.group({
      'nome': ['', [Validators.required, Validators.minLength(3)]],
      'email': ['', [this.EmailValidacao.bind(this)]],
      'assunto': ['', [Validators.required, Validators.minLength(3)]],
      'mensagem': ['', [Validators.required, Validators.minLength(3)]],
    });

  }


  EmailValidacao(control: FormControl): { [s: string]: boolean } {
    if (!(control.value.match('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$'))) {
      return { invalidEmail: true };
    }
  }

  ValidaForm(){
    if (!this.form2.nome) {
      return "Nome";
  } else if (this.ContatoForm.controls.email.invalid) {
      return "E-mail";
  } else if (!this.form2.assunto) {
      return "Assunto";
  } else if (!this.form2.mensagem) {
      return "Mensagem";
  }
  return true
  }
  
  Enviar() {
    let mensagem = this.ValidaForm()
    if(mensagem==true){
    let body = `pemail=${this.form2.email}&pmensagem=${this.form2.mensagem}&pnome=${this.form2.nome}&porigem=AppShopping&ptelefone=00000000`;
    return this.http.get(this.service.config.importacaoDomain + '?' + body, body).map(res => res.json())
      .subscribe(
      data => {
        this.Formularios = data;
        this.Dominio = this.service.config.defaultDomain
        // this.loader.dismiss()
        this.Sucesso = true
        console.log(this.Formularios)
      }
      );
    }else{
      this._alertCtrl.create({
        title: this.service.config.name,
        subTitle: "O campo " + mensagem + " é obrigatório",
        buttons: [{ text: 'Ok' }]
      }).present();
      console.log('erro')
    }
    
  }

  item2
  InfoContato() {
    let headers = new Headers();
    let geranum = Math.random()*9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json?nocache="+nocache).map(res => res.json())
    .subscribe(
    data => {
      // this.item2 = data.lista[0].link_texto;
      let paginas = data.lista;
      
              let filtrarPage=[]
              for (var i = 0; i < paginas.length; i++) {
                if (paginas[i].texto=='ENTRE EM CONTATO') {
                  filtrarPage.push(paginas[i]);
                    //  this.Segmentos=segmentolojas;
                // console.log(filtrarPage, 'entro fi')
                
                }

              this.item3=filtrarPage;
              this.item3 = this.item3[0].link_texto;
      console.log(this.item3)
      
          }
      this.Dominio = this.service.config.defaultDomain
      
    },
    err => {
      console.log('deuruim lojas', err);
    }
    );
  }

  irparaHome(localNavCtrl: boolean = true, event, s) {
    this.rootNavCtrl.push(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ContatoPage');
  }

}