import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';
import {VitrineInternaPage} from '../vitrine-interna/vitrine-interna'

@IonicPage()
@Component({
  selector: 'page-vitrine',
  templateUrl: 'vitrine.html',
})
export class VitrinePage {

  rootNavCtrl: NavController;
  Token
  Dominio
  vitrine
  Data
  filter=false
  Pagina=0;
  allStores=[];
  searching=true;
  vitrineProduto
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,) { 
    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }    
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.Vitrine()
      refresher.complete();
    }, 2000);
  }
  Vitrine() {
    let geranum = Math.random()*9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.defaultDomain+"/api/mobile/VitrineList?codCliente="+this.service.config.idShopping+'&Pagina='+this.Pagina+'&Items=6').map(res => res.json())
      .subscribe(
      data => {
        this.Dominio = this.service.config.defaultDomain
        // console.log(this.vitrine)
      this.allStores=data.lista;
      this.vitrine = data.lista;
      this.searching=false;
      if(this.vitrine.length==0){
        this.vitrineProduto=true;
      }
      },
      err => {
        console.log('deuruim vitrine', err);
      }
      );
  }

  public CountPaginacao(){
    var headers = new Headers();
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain+"/api/mobile/VitrineList?codCliente="+this.service.config.idShopping+'&Pagina='+this.Pagina+'&Items=6', {"headers": headers}).map(res => res.json())
    .subscribe(
    data => {
      this.Data=data.lista
      if(this.Data!=undefined){
        for (var i = 0; i < 6; i++) {
          this.vitrine.push( this.Data[i] );
        }
      }
    }, 
    err =>  {
      console.log('deuruim', err)
    }//this.status.error = JSON.stringify(err)
    );
  } 
  IrParaInterna(localNavCtrl: boolean = true, event, v) {
      this.rootNavCtrl.push(VitrineInternaPage,{
        v: event
      });
  }

doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    setTimeout(() => {
      if(this.vitrine!=undefined){
      this.CountPaginacao()
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }else{
     console.log('travo')
    }
  }, 500);
}
  AbrirFiltro(){
    if(!this.filter){
      this.filter=true
    }else{
      this.filter=false;
    }
  }

  BuscaVitrine:string
  nadaEncontrado
  Filtrar(){
    let auxStores=[];
    console.log('filtro', this.BuscaVitrine)
    if(this.BuscaVitrine!=""){
      for(var i =0 ;i< this.allStores.length;i++){
          if(this.allStores[i].nome.str.lang_1.toUpperCase().indexOf(this.BuscaVitrine.toUpperCase())>-1){
               auxStores.push(this.allStores[i]);
          }else{
            
  this.vitrine=auxStores
  
    this.filter=false;
          }
      }
  }else{
      auxStores=this.allStores;
  }
  this.vitrine=auxStores

  this.filter=false;
  if(this.vitrine.length == 0) {
    this.nadaEncontrado = true;
  }else{
    this.nadaEncontrado=false;
  }
}
  ionViewDidLoad() {
    this.Vitrine()
  }

}
