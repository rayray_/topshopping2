
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';

import { Http, Response, Headers,RequestOptions } from '@angular/http';
import { ServiceProvider } from '../../providers/service/service';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { CuponsInternaPage } from '../cupons-interna/cupons-interna';

@IonicPage()
@Component({
  selector: 'page-enquetes',
templateUrl: 'enquetes.html',
})
export class EnquetesPage {
  loader = this._loadingCtrl.create({
    content: 'Aguarde...'
  });
  Feed:string
  Dominio:string
  verEnquete
  enquete
  Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
  primeiraparte=false
  Sucesso=false
  respostas
  rootNavCtrl: NavController;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    public service: ServiceProvider,
  ) {
    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }
  

enquetes
  public EnqueteUsuario() {
    this.Token = JSON.parse(window.localStorage.getItem('cacheUsuario'));
    var headers = new Headers();
    headers.append('Authorization', this.Token.Token);
    this.http.get(this.service.config.defaultDomain + "/api/Mobile/GetEnqueteUsuario", { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.enquete = data.Enquetes,
        this.verEnquete = data.Sucess;
        this.Dominio = this.service.config.defaultDomain
        console.log(this.verEnquete)

        localStorage.setItem("cachePesquisa", JSON.stringify(data));
        localStorage.setItem("enqueteRespondida", JSON.stringify(data));
        this.enquetes = JSON.parse(localStorage.getItem("cachePesquisa"));
        //listagem de respostas
        this.respostas=[]

        if(this.enquetes.Enquetes!=null){
          for (let i = 0; i < this.enquetes.Enquetes[0].EnqueteOpcoes.length; i++) {
            this.respostas[i] = {
              Id: this.enquetes.Enquetes[0].EnqueteOpcoes[i]["Id"],
              Resposta: this.enquetes.Enquetes[0].EnqueteOpcoes[i]["Resposta"],
              Ordem: i,
              model: false,
              checked: false
            };
          }
        }
      },
      err => {
        console.log('deuruim enquete', err);
      }
      );
  }

 selecionaCheck (checkbox) {
    for (let i = 0; i < this.respostas.length; i++) {
        if (checkbox.Ordem != i) {
          this.respostas[i].model = false;
          this.respostas[i].checked = false;
        } else {          
          this.respostas[i].checked = true;
        }
    }
}
respostaSelecionada
responder () {
    for (let i = 0; i < this.respostas.length; i++) {
        if (this.respostas[i].model == true && this.respostas[i].checked == true) {
            this.respostaSelecionada =this.respostas[i];
        }
    }
    // console.log(this.respostaSelecionada);
    if (this.respostaSelecionada != undefined) {
      this.finalizar()
    } else {
        this._alertCtrl.create({
          title: this.service.config.name,
          subTitle: 'Escolha uma resposta',
          buttons: [{ text: 'Ok' }]
        }).present();
    }
};

CupomSouN
finalizar() {
  this.loader.present();
  
let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.Token.Token
    });

  let body = `ClienteCod=${this.Token.codCliente}&IdCadastroAPP=${this.Token.codUsuario}&IdEnqueteOpcao=${this.respostaSelecionada.Id}`;
  return this.http.post(this.service.config.defaultDomain + '/api/Mobile/ResponderEnquete?'+body,body,  { headers: headers }).map(res => res.json())

  .subscribe((data) => {
    if (data.Sucess!=false) {
      // console.log(data)
      this.CupomSouN=data
      this.primeiraparte=false;
      this.Sucesso=true
    } else
      this._alertCtrl.create({
        title: this.service.config.name,
        subTitle: data.Descricao,
        buttons: [{ text: 'Ok' }]
      }).present();
      this.loader.dismiss()
      return false;
    }
    , err => {
      console.log("ERROR!: ", err);
      //  this.loader.dismiss();ion
      this._alertCtrl.create({
        title: this.service.config.name,
        subTitle: 'Erro',
        buttons: [{ text: 'Ok' }]
      }).present();
      this.loader.dismiss()
    }
    );
}

irParaLogin(localNavCtrl: boolean = true) {
  this.rootNavCtrl.push(LoginPage);
}
idCupom;

irparacupom = function (localNavCtrl: boolean = true, $event, idCupom) {
  this.idCupom=$event;
  var headers = new Headers();
  headers.append('Authorization', this.Token.Token);
  this.http.get(this.service.config.defaultDomain + "/api/Mobile/GetCupomPromocional/?CodCupom=" + this.idCupom, { "headers": headers }).map(res => res.json())
    .subscribe(
    data => {
      console.log(data)
      this.rootNavCtrl.push(CuponsInternaPage,{
        c: data.Cupons[0]
      });
    },
    err => {
      console.log('deuruim enquete', err);
    }
    );
}

irparahome(localNavCtrl: boolean = true) {
  this.rootNavCtrl.push(HomePage);
}
  ionViewDidLoad() {
    if(this.Token!=undefined){
    this.EnqueteUsuario()
  }
  }

}
