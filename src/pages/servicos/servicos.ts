import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ListaservicosPage } from '../listaservicos/listaservicos'
import { EstacionamentoPage } from '../estacionamento/estacionamento';

@IonicPage()
@Component({
  selector: 'page-servicos',
  templateUrl: 'servicos.html',
})
export class ServicosPage {

  ListaservicosPage=ListaservicosPage
  EstacionamentoPage=EstacionamentoPage
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ServicosPage');
  }

}
