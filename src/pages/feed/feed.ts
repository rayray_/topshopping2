import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http, Response, Headers } from '@angular/http';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html',
})
export class FeedPage {
  Dominio:string
  Pagina=0;
  Feed = [];
  Data
  searching=true
  DataFormatada=[]
  NaoDisponivel
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
  ) {
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    setTimeout(() => {
      console.log('Async operation has ended');
      this.SocialFeed()
      refresher.complete();
    }, 2000);
  }
  public SocialFeed(){
    var headers = new Headers();
    this.http.get(this.service.config.defaultDomain+"/api/mobile/RedesSociaisList?codCliente="+this.service.config.idShopping+'&Pagina='+this.Pagina+'&Items=5', {"headers": headers}).map(res => res.json())
    .subscribe(
    data => {
      this.Feed=data.lista,
      this.Dominio = this.service.config.defaultDomain
      this.searching=false;

      if(this.Feed.length==0){
        this.NaoDisponivel=true;
      }else{
        this.NaoDisponivel=false
      }
      // for (var i = 0; i < this.Feed.length; i++) {
      //  this.DataFormatada.push(
      //   this.Feed[i].dataPublicacao.replace(':','h'))
      //   console.log(this.DataFormatada, 'primeirofor')
      // }
    }, 
    err =>  {
      console.log('deuruim', err)
    }//this.status.error = JSON.stringify(err)
    );
  } 

  public CountPaginacao(){
    var headers = new Headers();
    this.Pagina++;
    this.http.get(this.service.config.defaultDomain+"/api/mobile/RedesSociaisList?codCliente="+this.service.config.idShopping+'&Pagina='+this.Pagina+'&Items=5', {"headers": headers}).map(res => res.json())
    .subscribe(
    data => {
      this.Data=data.lista
      if(this.Data!=''){
        for (var i = 0; i < 5; i++) {
          this.Feed.push( this.Data[i] );
        }
      }
    }, 
    err =>  {
      console.log('deuruim', err)
    }//this.status.error = JSON.stringify(err)
    );
  } 

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

      setTimeout(() => {
        this.CountPaginacao()
        // }
        console.log('Async operation has ended');
        infiniteScroll.complete();
      }, 500);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedPage');
    this.SocialFeed()
  }

}
