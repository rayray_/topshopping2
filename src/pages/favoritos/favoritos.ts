import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServicosinternaPage } from '../servicosinterna/servicosinterna'
import { FilmesInternaPage } from '../filmes-interna/filmes-interna'
import { LojasInternaPage } from '../lojas-interna/lojas-interna'
import { VitrineInternaPage } from '../vitrine-interna/vitrine-interna'
import { GastronomiaInternaPage } from '../gastronomia-interna/gastronomia-interna'
import { FiquepordentrointernaPage } from '../fiquepordentrointerna/fiquepordentrointerna'
import { LazerInternaPage } from '../lazer-interna/lazer-interna'
import { CuponsInternaPage } from '../cupons-interna/cupons-interna'



@IonicPage()
@Component({
  selector: 'page-favoritos',
  templateUrl: 'favoritos.html',
})
export class FavoritosPage {
  favorites
  constructor(
    public navCtrl: NavController,
     public navParams: NavParams,
    ) {
      this.getFavorites()
  }
  getFavorites(){
    var favorites=[];
    
              var areas=['servico','cinema','lojas','vitrine','gastronomia','fiquepordentro','entretenimento','cupom'];
    
              for (var i = 0; i < areas.length; i++) {
                    var item = JSON.parse(localStorage.getItem(areas[i]+'-like'));
                    if (item==null)
                        item=[];
    
                    var area='';
                    item.map(function(elem){
                      switch (areas[i]) {
                        case 'lojas':
                              area='Guia de Lojas';
                          break;
                        case 'servico':
                              area='Serviços';
                            break;
                        case 'lojas':
                              area='Guia de Lojas';
                              break;
                        case 'cinema':
                              area='Cinema';
                              break;
                        case 'vitrine':
                              area='Vitrine Virtual';
                              break;
                        case 'gastronomia':
                              area='Gastronomia';
                              break;
                        case 'fiquepordentro':
                              area='Fique Por Dentro';
                              break;
                        case 'entretenimento':
                              area = 'Entretenimento';
                              break;
                        case 'cupom':
                              area = 'Cupom';
                              break;
                      }
                        elem.areaLike=area;
                    });
                    favorites=favorites.concat(item);
              }
    
              this.favorites = favorites;
    
  }
  openFavorite = function (f) {
    var rote = '';
    switch (f.areaLike) {
          case 'Guia de Lojas':
            //     localStorage.setItem('store', JSON.stringify(f))
                this.navCtrl.push(LojasInternaPage,
                {
                  loja: f
                });
                break;
          case 'Serviços':
            //     localStorage.setItem('service', JSON.stringify(f))
                this.navCtrl.push(ServicosinternaPage,
                {
                  s: f
                });
                break;
          case 'Cinema':
            //     localStorage.setItem('movie', JSON.stringify(f))
                this.navCtrl.push(FilmesInternaPage,
                {
                  f: f
                });
                break;
          case 'Vitrine Virtual':
            //     localStorage.setItem('vitrine', JSON.stringify(f))
                this.navCtrl.push(VitrineInternaPage,
                {
                  v: f
                });
                break;
          case 'Gastronomia':
            //     localStorage.setItem('gastronomy', JSON.stringify(f))
                this.navCtrl.push(GastronomiaInternaPage,
                {
                  gastr: f
                });
                break;
          case 'Fique Por Dentro':
            //     localStorage.setItem('fiquepordentro', JSON.stringify(f))
                this.navCtrl.push(FiquepordentrointernaPage,
                {
                  novidade: f
                });
                break;
          case 'Entretenimento':
            //     localStorage.setItem('entretenimentoSelect', JSON.stringify(f))
                this.navCtrl.push(LazerInternaPage,
                {
                  c: f
                });
                break;
          case 'Cupom':
          localStorage.setItem('cupomSelect', JSON.stringify(f))
          this.navCtrl.push(CuponsInternaPage,
          {
            c: f
          });
          break;
    }

}
  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritosPage');
  }

}
