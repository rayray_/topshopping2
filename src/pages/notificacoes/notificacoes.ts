import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { NotificacoesInternaPage } from '../notificacoes-interna/notificacoes-interna'
import { ServiceProvider } from '../../providers/service/service';

import { Storage } from '@ionic/storage';
import { Http, Response, Headers,RequestOptions } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-notificacoes',
  templateUrl: 'notificacoes.html',
})
export class NotificacoesPage {
  Dominio
  MsgsList
  NaoDisponivel
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
   private storage: Storage,) {
  }


  ViewDetails($event, m){
    this.navCtrl.push(NotificacoesInternaPage,
    {
      msgInter: m
    })
  }

  ListMensagens(){
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    let headers = new Headers({
      'Authorization': Token.Token
    });
    let options = new RequestOptions({ headers: headers });
  
    this.http.get(this.service.config.defaultDomain + "/api/mobile/MensagensList?Clientecod=" + this.service.config.idShopping, { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.MsgsList = data.Mensagens,
        this.Dominio = this.service.config.defaultDomain,
        this.storage.set('Mensagens', this.MsgsList);
        if(this.MsgsList.length==0){
          this.NaoDisponivel=true;
        }else{
          this.NaoDisponivel=false
        }
        console.log(this.MsgsList, 'Novidade')
      },
      err => {
        console.log('deuruim', err)
         this.storage.get('Mensagens').then((val) => {
          console.log('Your age is', val);
          this.MsgsList=val
        });
        }
      );
  }
  ionViewDidLoad() {
    this.ListMensagens()
    console.log('ionViewDidLoad NotificacoesPage');
  }
  ionViewWillLeave() {
    this.ListMensagens()
   }
   ionViewCanEnter(){

    this.ListMensagens()
   }

}
