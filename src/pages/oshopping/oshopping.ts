import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { Http,Headers } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-oshopping',
  templateUrl: 'oshopping.html',
})
export class OshoppingPage {
  Dominio
  item
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,) {
  }
  
  InfoShopping() {
    let headers = new Headers();
    let geranum = Math.random()*9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json?nocache="+nocache).map(res => res.json())
      .subscribe(
      data => {
        let paginas = data.lista;

        let filtrarPage=[]
        for (var i = 0; i < paginas.length; i++) {
          if (paginas[i].texto=='TopShopping') {
            filtrarPage.push(paginas[i]);
              //  this.Segmentos=segmentolojas;
          console.log(filtrarPage, 'entro fi')
          
          }
        this.item=filtrarPage;
        }
        this.Dominio = this.service.config.defaultDomain

        console.log(this.item[0])
        console.log(this.Dominio);
      },
      err => {
        console.log('deuruim lojas', err);
      }
      );
  }
  ionViewDidLoad() {
    this.InfoShopping()
    console.log('ionViewDidLoad OshoppingPage');
  }

}