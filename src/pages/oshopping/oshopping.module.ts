import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OshoppingPage } from './oshopping';

@NgModule({
  declarations: [
    OshoppingPage,
  ],
  imports: [
    IonicPageModule.forChild(OshoppingPage),
  ],
})
export class OshoppingPageModule {}
