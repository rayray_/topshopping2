import { Component } from '@angular/core';
import { IonicPage, Events, NavController, NavParams,AlertController } from 'ionic-angular';

import { ServicosinternaPage } from '../servicosinterna/servicosinterna'
import { FilmesInternaPage } from '../filmes-interna/filmes-interna'
import { LojasInternaPage } from '../lojas-interna/lojas-interna'
import { VitrineInternaPage } from '../vitrine-interna/vitrine-interna'
import { GastronomiaInternaPage } from '../gastronomia-interna/gastronomia-interna'
import { FiquepordentrointernaPage } from '../fiquepordentrointerna/fiquepordentrointerna'
import { LazerInternaPage } from '../lazer-interna/lazer-interna'
import { CuponsInternaPage } from '../cupons-interna/cupons-interna'


import { ServiceProvider } from '../../providers/service/service';


@IonicPage()
@Component({
  selector: 'page-busca',
  templateUrl: 'busca.html',
})
export class BuscaPage {
  nadaEncontrado
  stores
  quantidade
  txtValue
  storesQntd
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private _alertCtrl: AlertController,
    public service: ServiceProvider,
   ) {
    this.txtValue = localStorage.getItem('campoPesquisa');
    let resultadoLoja = JSON.parse(localStorage.getItem('resultadoDaBuscaHome'));
    let fieldLoja = '';
    if (resultadoLoja != undefined) {
      for (var k = 0; k < resultadoLoja.length; k++) {
        if (resultadoLoja[k].categoria == "Filmes") {
          var urlPipocao = resultadoLoja[k].midia.arquivos.lang_1.arquivo.url;
          resultadoLoja[k].midia.arquivos.lang_1.arquivo.url = urlPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
        }
      }
    }

    let allStores = resultadoLoja;
    this.stores = allStores;
    if (allStores.length == 0) {
      this.nadaEncontrado = true;
      // this._alertCtrl.create({
      //   title: this.service.config.name,
      //   subTitle: 'Nada Encontrado',
      //   buttons: [{ text: 'Ok' }]
      // }).present();
    }else{
      this.nadaEncontrado = false;
    }
    this.quantidade = this.stores.length > 0 ? 10 : 0;
    this.storesQntd = allStores.length;


  }
  openDetails = function (l) {
    switch (l.categoria) {
      case "Acontece":
        // localStorage.setItem('fiquedentro',JSON.stringify(l));
        // $state.go('app.fiquepordentroDetails');
        this.navCtrl.push(FiquepordentrointernaPage,
          {
            novidade: l
          });
        break;
      case "Filmes":
        // localStorage.setItem('movie',JSON.stringify(l));
        this.navCtrl.push(FilmesInternaPage,
          {
            f: l
          });
        break;
        case "Alimentação":
          // localStorage.setItem('gastronomy',JSON.stringify(l));
          // $state.go('app.gastronomiainterna');
  
          this.navCtrl.push(GastronomiaInternaPage,
            {
              gastr: l
            });
          break;
        case "Lazer":
          // localStorage.setItem('gastronomy',JSON.stringify(l));
          // $state.go('app.gastronomiainterna');
  
          this.navCtrl.push(LazerInternaPage,
            {
              c: l
            });
          break;
        case "Serviços":
          // localStorage.setItem('gastronomy',JSON.stringify(l));
          // $state.go('app.gastronomiainterna');
          this.navCtrl.push(ServicosinternaPage,
            {
              s: l
            });
          break;
      case "Lojas":
        // localStorage.setItem('store',JSON.stringify(l));
        this.navCtrl.push(LojasInternaPage,
          {
            loja: l
          });
      default:
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BuscaPage');
  }

}
