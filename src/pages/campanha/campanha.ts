import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { TextMaskModule } from 'angular2-text-mask';
import {CampanhaCadastroPage} from '../campanha-cadastro/campanha-cadastro'
@IonicPage()
@Component({
  selector: 'page-campanha',
  templateUrl: 'campanha.html',
  providers: [SocialSharing]
})
export class CampanhaPage {
  public campanha;
  Dominio
  params:object
  CampanhaCadastroPage=CampanhaCadastroPage
  constructor(
    public navCtrl: NavController,
     public navParams: NavParams,
     public service: ServiceProvider,
     private socialSharing: SocialSharing) {
    this.Dominio = this.service.config.defaultDomain
    this.campanha = this.navParams.get("campanha");
  
  }

  IrParaCadastro(){
    this.navCtrl.push(CampanhaCadastroPage,{
      cadastrocampanha: this.campanha
    })
  }
  subject
  Compartilhar(){
    var titulo = this.campanha[0].Titulo;
    var desc = this.campanha[0].Descricao;
    console.log(this.campanha[0].Imagem, 'imgs')
    this.socialSharing.share(
      titulo+' #AppPartageBetim', desc, this.campanha[0].Imagem
      

    ).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CampanhaPage');
  }

}
