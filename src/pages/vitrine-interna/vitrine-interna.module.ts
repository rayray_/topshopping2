import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VitrineInternaPage } from './vitrine-interna';

@NgModule({
  declarations: [
    VitrineInternaPage,
  ],
  imports: [
    IonicPageModule.forChild(VitrineInternaPage),
  ],
})
export class VitrineInternaPageModule {}
