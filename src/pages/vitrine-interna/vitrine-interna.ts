import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';

import { SharedService} from '../../providers/shared-service/shared-service';

@IonicPage()
@Component({
  selector: 'page-vitrine-interna',
  templateUrl: 'vitrine-interna.html',
  providers: [SocialSharing]
})
export class VitrineInternaPage {
  Dominio
  vota
  public firstParam
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    private socialSharing: SocialSharing,
    public SharedService: SharedService) {
      this.Dominio = this.service.config.defaultDomain
      this.firstParam = this.navParams.get("v");
      console.log(this.firstParam);
      this.vota=this.SharedService.checkLike("vitrine", this.firstParam);
      console.log(this.vota, 'vota')

  } 
  likeVote(vitrine) {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("vitrine", vitrine);
  }
  subject
  Compartilhar(){
    this.socialSharing.share(
      this.firstParam.nome.str.lang_1+this.service.config.HashTag+' www.topshopping.com.br', this.subject, this.Dominio+'/'+this.firstParam.midia.arquivos.lang_1.arquivo.url

    ).then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad VitrineInternaPage');
  }

}
