import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams  } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SharedService} from '../../providers/shared-service/shared-service';
@IonicPage()
@Component({
  selector: 'page-fiquepordentrointerna',
  templateUrl: 'fiquepordentrointerna.html'
})
export class FiquepordentrointernaPage {
  public novidade;
  vota
  Dominio
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    private socialSharing: SocialSharing,
    public SharedService: SharedService) {
      this.Dominio = this.service.config.defaultDomain
      this.novidade = this.navParams.get("novidade");
      console.log(this.novidade)
      this.vota=this.SharedService.checkLike("fiquepordentro", this.novidade);
      console.log(this.vota, 'vota')
  }
  
  likeVote(novidade) {
    this.vota =this.vota ? false : true;
    this.SharedService.setLike("fiquepordentro", novidade);
  }
  abrirExterno(link){
    window.open(link, '_system', 'location=no');
  }
  subject
  Compartilhar(){
    this.socialSharing.share(
      this.novidade.nome.str.lang_1+this.service.config.HashTag+' www.topshopping.com.br', this.subject, this.Dominio+'/'+this.novidade.midia.arquivos.lang_1.arquivo.url
    ).then(() => {

    }).catch(() => {

    });
  }
 
   ionViewDidLoad() {
   
  }

}
