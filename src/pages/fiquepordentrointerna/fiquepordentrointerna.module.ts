import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiquepordentrointernaPage } from './fiquepordentrointerna';

@NgModule({
  declarations: [
    FiquepordentrointernaPage,
  ],
  imports: [
    IonicPageModule.forChild(FiquepordentrointernaPage),
  ],
})
export class FiquepordentrointernaPageModule {}
