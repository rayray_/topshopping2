import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HomePage } from '../home/home';
import { ServiceProvider } from '../../providers/service/service';
import { LoginPage } from '../login/login';
import { MomentModule } from 'angular2-moment';
declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-campanha-cadastro',
  templateUrl: 'campanha-cadastro.html',
})
export class CampanhaCadastroPage {
  public campanha
  form:any ={
    Nome: '',
    Email: '',
    Telefone: '',
    Celular: '',
    CPF: '',
    RG: '',
    DatadeNascimento: '',
    nomeArquivo: '',
    Texto: '',
    DataNascimento: ''
  }
  Sucesso
  Token= JSON.parse(window.localStorage.getItem('cacheUsuario'));
  
  public maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];  
  public maskcel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
  public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public maskcpfnovo = [/[0-9]/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /[0-9]/];

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
     private _alertCtrl: AlertController,
    public service: ServiceProvider,
    public moment: MomentModule,
    public http: Http,
  ) {

    this.campanha = this.navParams.get("cadastrocampanha");
    console.log(this.campanha , 'cadastro')
  }

  ValidaForm(){
    if (this.campanha[0].HabilitarNome == 2 && !this.form.Nome) {
      return "Nome";
  } else if (this.campanha[0].HabilitarEmail == 2 && !this.form.Email) {
      return "E-mail";
  }  else if (this.campanha[0].HabilitarDataNascimento == 2 && !this.form.DataNascimento) {
    return "Data de Nascimento";
  } else if (this.campanha[0].HabilitarTelefone == 2 && !this.form.Telefone) {
      return "Telefone";
  } else if (this.campanha[0].HabilitarCelular == 2 && !this.form.Celular) {
      return "Celular";
  } else if (this.campanha[0].HabilitarCPF == 2 && !this.form.CPF) {
      return "CPF";
  } else if (this.campanha[0].HabilitarRG == 2 && !this.form.RG) {
      return "RG";
  } else if (this.campanha[0].HabilitarArquivo == 2 && !this.form.Arquivo) {
      return "Arquivo";
  } else if (this.campanha[0].HabilitarTexto == 2 && !this.form.Texto) {
      return "Descrição";
  }
  return true
  }
  changeFile (arquivo) {
    this.form.nomeArquivo = arquivo.path[0].files[0].name;
    this.form.Arquivo = arquivo.path[0].files[0];
    var fotoSize = 0;
    fotoSize = arquivo.path[0].files[0].size / 1024;
    if(fotoSize > 4096){
      // $scope.alertNativo();
      let alert = this._alertCtrl.create({
        title: '<img src="assets/icon/error.png" /> Erro',
        subTitle: "O tamanho máximo da foto é de 4M.",
        buttons: ['OK'],
        cssClass: 'alertCustomCssError'
      });
      alert.present();
    }
};

  enviar(){
    var str = this.form.DataNascimento
    let res = str.split('/');
    let dataTratada = res[1] + '/' + res[0] + '/' + res[2];
    console.log(dataTratada)



    // this.form.DataNascimento = moment(this.form.DataNascimento).format("MM-DD-YYYY HH:mm");
    // console.log(this.form.DataNascimento);
    
    let mensagem = this.ValidaForm()
    if(mensagem==true){
    if (this.campanha[0].HabilitarArquivo != 0){

    var formData = new FormData();
    formData.append("Arquivo", this.form.Arquivo);

    return this.http.post(this.service.config.defaultDomain + '/api/Mobile/UploadFile?ClienteCod=' + this.service.config.idShopping + '&Area=campanhas', formData).map(res => res.json())

    .subscribe((data) => {
      
      if (data.Sucesso != false) {
      
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    });
    let options = new RequestOptions({ headers: headers });

    let body = `Nome=${this.form.Nome}&Email=${this.form.Email}&Telefone=${this.form.Telefone}&Celular=${this.form.Celular}&CPF=${this.form.CPF}&RG=${this.form.RG}&Texto=${this.form.Texto}&ClienteCod=${this.service.config.idShopping}&IdCampanha=${this.campanha[0].Id}&Arquivo=${data.Arquivo}&DataNascimento=${dataTratada}`;
    
    return this.http.post(this.service.config.defaultDomain + '/api/Mobile/ParticiparCampanha2', body, { headers: headers }).map(res => res.json())
      .subscribe((data) => {
        console.log(data)
        if (data.Sucess != false) {
          this.Sucesso=true

        } else{
          this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: data.Descricao,
            buttons: [{ text: 'Ok' }]
          }).present();
        }
      }
      , err => {
        console.log("ERROR!: ", err);
        //  this.loader.dismiss();ion
        this._alertCtrl.create({
          title: this.service.config.name,
          subTitle: err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', ''),
          buttons: [{ text: 'Ok' }]
        }).present();
        // this.loader.dismiss()
      }
      
      );
    }else{
      this._alertCtrl.create({
        title: this.service.config.name,
        subTitle: data.Descricao,
        buttons: [{ text: 'Ok' }]
      }).present();
    }
  })
    } else{
      let headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded'
      });
 
      let body = `Nome=${this.form.Nome}&Email=${this.form.Email}&Telefone=${this.form.Telefone}&Celular=${this.form.Celular}&CPF=${this.form.CPF}&RG=${this.form.RG}&Texto=${this.form.Texto}&ClienteCod=${this.service.config.idShopping}&IdCampanha=${this.campanha[0].Id}&Arquivo=${''}&DataNascimento=${this.form.DataNascimento}`;
      
      return this.http.post(this.service.config.defaultDomain + '/api/Mobile/ParticiparCampanha2', body, { headers: headers }).map(res => res.json())
        .subscribe((data) => {
          console.log(data)
          if (data.Sucess != false) {
            this.Sucesso=true
           
  
          } else{
            this._alertCtrl.create({
              title: this.service.config.name,
              subTitle: data.Descricao,
              buttons: [{ text: 'Ok' }]
            }).present();
          }
        }
        , err => {
          console.log("ERROR!: ", err);
          //  this.loader.dismiss();ion
          this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', ''),
            buttons: [{ text: 'Ok' }]
          }).present();
          // this.loader.dismiss()
        }
      )}
    
  }
    else{
      this._alertCtrl.create({
        title: this.service.config.name,
        subTitle: "O campo " + mensagem + " é obrigatório",
        buttons: [{ text: 'Ok' }]
      }).present();
    }
  
  }
  irParaLogin() {
    this.navCtrl.setRoot(LoginPage);
  }
  irParaHome() {
    this.navCtrl.setRoot(HomePage);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad CampanhaCadastroPage');
  }
  
  // setUploadFile(formData, function(response){        
  //   $scope.form.ArquivoNomeNovo = response["data"]["Arquivo"];
  //   setParticiparCampanha($scope.form, function(data){
  //       console.log(data);
  //       if(data["data"]["Sucess"]){
  //           $state.go('app.campanhaSucesso');
  //       }else{
  //            $rootScope.alertNativo(data.data.Descricao);
  //       }
  //   })
  // });

}
