import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CampanhaCadastroPage } from './campanha-cadastro';

@NgModule({
  declarations: [
    CampanhaCadastroPage,
  ],
  imports: [
    IonicPageModule.forChild(CampanhaCadastroPage),
  ],
})
export class CampanhaCadastroPageModule {}
