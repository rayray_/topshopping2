import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaservicosPage } from './listaservicos';

@NgModule({
  declarations: [
    ListaservicosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaservicosPage),
  ],
})
export class ListaservicosPageModule {}
