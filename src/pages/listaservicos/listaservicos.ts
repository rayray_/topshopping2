import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';
import {ServicosinternaPage} from '../servicosinterna/servicosinterna'
@IonicPage()
@Component({
  selector: 'page-listaservicos',
  templateUrl: 'listaservicos.html',
})
export class ListaservicosPage {
  rootNavCtrl: NavController;
  Token
  Dominio
  servicos
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,) { 
    this.rootNavCtrl = navParams.get('rootNavCtrl');
  }    

  ListServicos() {
    this.http.get(this.service.config.jsonDomain + "jsonservicos.json").map(res => res.json())
      .subscribe(
      data => {
        this.servicos = data.lista;
        this.Dominio = this.service.config.defaultDomain
        
        console.log(this.servicos)
      },
      err => {
        console.log('deuruim listaservicos', err);
      }
      );
  }
  IrParaInterna(localNavCtrl: boolean = true, event, s) {
      this.rootNavCtrl.push(ServicosinternaPage,{
        s: event
      });
  }
  ionViewDidLoad() {
    this.ListServicos()
    // console.log('ionViewDidLoad Vit  rinePage');
  }
}
