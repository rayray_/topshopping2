import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrecoCinemaPage } from './preco-cinema';

@NgModule({
  declarations: [
    PrecoCinemaPage,
  ],
  imports: [
    IonicPageModule.forChild(PrecoCinemaPage),
  ],
})
export class PrecoCinemaPageModule {}
