import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Http, Response, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service/service';

@IonicPage()
@Component({
  selector: 'page-preco-cinema',
  templateUrl: 'preco-cinema.html',
})
export class PrecoCinemaPage {

  searching=true
  precos
  someAreaCinema
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private storage: Storage,
    ) {
  }
  PrecoCinema(){
    var geranum = Math.random() * 9999999;
    var nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + "jsonPrecosCinemaMobile.json?nocache=" + nocache).map(res => res.json())
    .subscribe(
    
      data => {
        console.log(data)
        this.precos=data.lista
        this.searching=false
        if(this.precos==0){
          this.someAreaCinema=true
        }else{
          this.someAreaCinema=false
        }
      }
    )
  }
  ionViewDidLoad() {
    this.PrecoCinema()
    console.log('ionViewDidLoad PrecoCinemaPage');
  }

}
