import { Component, Pipe, PipeTransform } from '@angular/core';
import { IonicPage, NavController, ActionSheetController, LoadingController, ToastController, AlertController, NavParams, Events, Loading, Platform } from 'ionic-angular';

import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { HomePage } from '../home/home';
import { EsquecisenhaPage } from '../esquecisenha/esquecisenha';

import { NativeStorage } from '@ionic-native/native-storage';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { ServiceProvider } from '../../providers/service/service';
import { FacebookService } from '../../providers/facebook/facebook';

import { TextMaskModule } from 'angular2-text-mask';
import { FormsModule } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { MomentModule } from 'angular2-moment';
import * as moment from 'moment';

import * as locales from 'moment/min/locales';
// import { File } from '@ionic-native/file';
// import { Transfer, TransferObject } from '@ionic-native/transfer';
// import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Base64 } from '@ionic-native/base64';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';

declare var cordova: any;
@IonicPage()
@Component({
  selector: 'page-meuperfil',
  templateUrl: 'meuperfil.html',
  providers: [FacebookService, Base64, Base64ToGallery,
    Camera,],
})
@Pipe({
  name: 'formatDate'
})

export class MeuperfilPage {

  lastImage: string = null;
  loading: Loading;

  public maskdate = [/[0-9]/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
  public masktel = ['(', /[0-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  selectOptions = {
    title: 'Selecione o sexo',
  };
  SelectEstadoCivil = {
    title: 'Estado Civil'
  }


  Usuario
  Formulario = false
  form
  meusdados
  Dominio
  email
  telefone
  sexo

  estadocivil
  datanascimento
  datacadastro
  FotoApp
  ModeFace
  ModeFace2
  ModoFace3
  formSenha = {
    Senha: '',
    NovaSenha: '',
    ConfirmaSenha: ''
  }

  constructor(
    public navCtrl: NavController,
    public platform: Platform,
    public navParams: NavParams,
    private _alertCtrl: AlertController,
    public facebookService: FacebookService,
    private Facebook: Facebook,
    public events: Events,
    public service: ServiceProvider,
    private storage: Storage,
    public toastCtrl: ToastController,
    public moment: MomentModule,
    public http: Http,
    private camera: Camera,
    // private transfer: Transfer, 
    // private file: File, 
    // private filePath: FilePath, 
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    private base64: Base64,
    private base64ToGallery: Base64ToGallery) {
    this.Usuario = JSON.parse(window.localStorage.getItem('cacheUsuario'));
    console.log(this.Usuario);
    this.infoPerfil()

  }

  alterarSenhar
  editarPerfil() {
    let DataFormatada2 = moment(this.meusdados.datanascimentoUsuario).format("DD/MM/YYYY");
    console.log(this.meusdados);
    this.Formulario = true;
    this.form = {
      Nome: this.meusdados.nomelUsuario,
      Email: this.meusdados.emailUsuario,
      Telefone: this.meusdados.telefone,
      Sexo: this.meusdados.sexoUsuario,
      EstadoCivil: this.meusdados.estadocivilUsuario,
      datanascimento: DataFormatada2
    }

  }
  changePassword() {
    this.alterarSenhar = true;
  }

  atualizar() {
    var str = this.form.datanascimento
    let res = str.split('/');
    let dataTratada = res[1] + '/' + res[0] + '/' + res[2];
    console.log(dataTratada)


    if (dataTratada == "undefined//undefined") {
      dataTratada = ""
      console.log(dataTratada)

      let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
      let headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': Token.Token
      });
      let options = new RequestOptions({ headers: headers });

      let body = `Nome=${this.form.Nome}&Email=${this.form.Email}&Telefone=${this.form.Telefone}&Sexo=${this.form.Sexo}&EstadoCivil=${this.form.EstadoCivil}&DataNascimento=${dataTratada}&Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}`;
      return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPEdit', body, { headers: headers }).map(res => res.json())

        .subscribe((data) => {

          console.log(data)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: data.Descricao,
            buttons: [{
              text: 'Ok', handler: () => {
                //TODO: Your logic here
                // this.navCtrl.push(SomeComponent);

                this.Formulario = false,
                  this.infoPerfil()
              }
            }]
          })
          alerta.present()

        }
        , err => {
          console.log("ERROR!: ", err);
          console.log(err)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: err.Descricao,
            buttons: [{ text: 'Ok' }],


          })
          alerta.present()
        }
        );
    } else {

      let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
      let headers = new Headers({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': Token.Token
      });
      let options = new RequestOptions({ headers: headers });

      let body = `Nome=${this.form.Nome}&Email=${this.form.Email}&Telefone=${this.form.Telefone}&Sexo=${this.form.Sexo}&EstadoCivil=${this.form.EstadoCivil}&DataNascimento=${dataTratada}&Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}`;
      return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastrosAPPEdit', body, { headers: headers }).map(res => res.json())

        .subscribe((data) => {

          console.log(data)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: data.Descricao,
            buttons: [{
              text: 'Ok', handler: () => {
                //TODO: Your logic here
                // this.navCtrl.push(SomeComponent);

                this.Formulario = false,
                  this.infoPerfil()
              }
            }]
          })
          alerta.present()

        }
        , err => {
          console.log("ERROR!: ", err);
          console.log(err)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: err.Descricao,
            buttons: [{ text: 'Ok' }],


          })
          alerta.present()
        }
        );
    }

  }


  TrocarSenha() {
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': Token.Token
    });
    let options = new RequestOptions({ headers: headers });
    if (this.Usuario.fbUsuario == null) {

      let body = `SenhaAtual=${this.formSenha.Senha}&NovaSenha=${this.formSenha.NovaSenha}&ConfirmarSenha=${this.formSenha.ConfirmaSenha}&Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}`;
      return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastroAppAlterarSenha', body, { headers: headers }).map(res => res.json())


        .subscribe((data) => {

          console.log(data)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: data.Descricao,
            buttons: [{
              text: 'Ok', handler: () => {
                this.alterarSenhar = false,
                  this.infoPerfil()
              }
            }]
          })
          alerta.present()

        }
        , err => {
          var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
          console.log("ERROR!: ", err);
          console.log(err)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: mensagem,
            buttons: [{ text: 'Ok' }],


          })
          alerta.present()
        }
        );
    } else {
      let body = `NovaSenha=${this.formSenha.NovaSenha}&ConfirmarSenha=${this.formSenha.ConfirmaSenha}&Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}`;
      return this.http.post(this.service.config.defaultDomain + '/api/Mobile/CadastroAppAlterarSenha', body, { headers: headers }).map(res => res.json())

        .subscribe((data) => {

          console.log(data)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: data.Descricao,
            buttons: [{
              text: 'Ok', handler: () => {
                this.alterarSenhar = false,
                  this.infoPerfil()
              }
            }]
          })
          alerta.present()

        }
        , err => {
          var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');
          console.log("ERROR!: ", err);
          console.log(err)
          let alerta = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: mensagem,
            buttons: [{ text: 'Ok' }],


          })
          alerta.present()
        }
        );
    }
  }

  createUser2() {
    setTimeout(() => {
      console.log('User Login')
      let profile = JSON.parse(localStorage.getItem('profile'));
      this.events.publish('user:PegarDados', profile, Date.now());
    }, 1000)
  }

  facebook() {
    let permissions = new Array<string>();
    let nav = this.navCtrl;
    permissions = ['public_profile', 'user_friends', 'email'];

    this.Facebook.login(permissions).then((response) => {
      let userId = response.authResponse.userID;
      let params = new Array<string>();
      this.Facebook.api("/me?fields=name,gender,email,birthday", params)
        .then(function (profile) {
          profile.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
          localStorage.setItem('profile', JSON.stringify(profile))
          this.form.Facebook = userId;
        })
      this.createUser2()
    }, (error) => {
      console.log(error);
    });
  }
  VincularConta() {
    this.facebook()
    this.events.subscribe('user:PegarDados', (profile) => {

      let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
      let headers = new Headers();
      headers.append('Authorization', Token.Token);
      headers.append('Content-Type', 'application/json')


      let options = new RequestOptions({ headers: headers });
      let body = {
        Origem: "Aplicativo",
        ClienteCod: this.service.config.idShopping,
        UsuarioCod: Token.codUsuario,
        FacebookId: profile.id
      };

      return this.http.post(this.service.config.defaultDomain + '/api/Mobile/VincularFacebook', body, { headers: headers }).map(res => res.json())
        .subscribe((data) => {
          console.log(data)
          let alert = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: data.Descricao,
            buttons: [{ text: 'Ok', }]
          })
          alert.present();
        }, err => {
          console.log("ERROR!: ", err);
          var mensagem = err._body.replace('{', '').replace('"Message"', '').replace(':', '').replace('}', '').replace('"', '').replace('"', '');

          let alert = this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: mensagem,
            buttons: [{ text: 'Ok' }]
          })
          alert.present();
        })
    })
  }

  facebooktem
  infoPerfil() {
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    let headers = new Headers();
    headers.append('Authorization', Token.Token);
    this.http.get(this.service.config.defaultDomain + "/api/mobile/CadastrosAPPDetalhes?ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario, { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.meusdados = data,
          this.Dominio = this.service.config.defaultDomain,
          this.storage.set('meusdados', this.meusdados);
        console.log(this.meusdados, 'Meus Dados')

        this.email = this.meusdados.emailUsuario
        this.telefone = this.meusdados.telUsuario
        this.sexo = this.meusdados.sexoUsuario
        this.FotoApp = this.meusdados.fotoUsuario
        this.facebooktem = this.Usuario.fbUsuario
        if (this.sexo == '1') {
          this.sexo = 'Feminino'
        } else if (this.sexo == '2') {
          this.sexo = 'Masculino'
        } else {
          this.sexo = 'Sexo'
        }
        this.estadocivil = this.meusdados.estadocivilUsuario
        if (this.estadocivil == '1') {
          this.estadocivil = 'Feminino'
        } else if (this.estadocivil == '2') {
          this.estadocivil = 'Masculino'
        } else if (this.estadocivil == '3') {
          this.estadocivil = 'Masculino'
        } else if (this.estadocivil == '4') {
          this.estadocivil = 'Masculino'
        }
        this.datanascimento = this.meusdados.datanascimentoUsuario
        this.datacadastro = this.meusdados.datacadastroUsuario
        if (this.Usuario.fbUsuario != null && this.meusdados.fotoUsuario != null) {
          this.ModeFace = false;
          this.ModeFace2 = false;
          this.ModoFace3 = false
        } else if (this.Usuario.fbUsuario == null && this.meusdados.fotoUsuario == null) {
          this.ModeFace = false;
          this.ModeFace2 = true;
          this.ModoFace3 = true;
        } else if (this.Usuario.fbUsuario != null && this.meusdados.fotoUsuario == null) {
          this.ModeFace = true;
          this.ModeFace2 = false;
          this.ModoFace3 = true
        } else if (this.Usuario.fbUsuario == null && this.meusdados.fotoUsuario == null) {
          this.ModeFace = false;
          this.ModeFace2 = true;
          this.ModoFace3 = true
        }

      },
      err => {
        console.log('deuruim', err)
        this.storage.get('meusdados').then((val) => {
          console.log('Your age is', val);
          this.meusdados = val
        });
      }
      );
  }
  FotoEnviar
  public presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Escolha uma imagem',
      buttons: [
        {
          text: 'Abrir Galeria',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Usar a Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 50,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
    };
    this.camera.getPicture(options).then((imagePath) => {
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.ImagemBase64 = imagePath;
      } else {

        this.ImagemBase64 = imagePath;
      }
      this.uploadImage()
    }, (err) => {
      this.presentToast('Erro ao selecionar a imagem');
    });
  }
  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  // private copyFileToLocalDir(namePath, currentName, newFileName) {
  //   this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
  //     this.lastImage = newFileName;
  //   }, error => {
  //     this.presentToast('Erro ao ler o arquivo.');
  //   });
  // }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  };
  ImagemBase64: string;

  uint8ToString(Uint8Array) {
    var i, length, out = '';
    for (i = 0, length = Uint8Array.length; i < length; i += 1) {
      out += String.fromCharCode(Uint8Array[i]);
    }
    return out;
  }
  public uploadImage() {
    // btoa(this.ImagemBase64)
    // var base64 = btoa(this.uint8ToString(this.ImagemBase64));

    this.loading = this.loadingCtrl.create({
      content: 'Aguarde...',
    });
    this.loading.present();
    var base64 = this.ImagemBase64;

    var data = new FormData();
    data.append("image_data", base64);

    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    var url = "http://am4mall.com.br/api/mobile/CadastroAppUploadFoto";

    // const fileTransfer: TransferObject = this.transfer.create();


    let headers = new Headers({
      'Content-Type': 'application/json'
    });
    let options = new RequestOptions({ headers: headers });
    let body = {
      ClienteCod: this.service.config.idShopping,
      codUsuario: Token.codUsuario,
      Base64: 'data:image/jpeg;base64,' + base64
    }
    // let body = `ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}&Base64=${'data:image/jpeg;base64,'+base64}`;

    return this.http.post('http://am4mall.com.br/api/mobile/CadastroAppUploadFoto', body, { headers: headers }

    ).map(res => res.json())
      .subscribe(response => {
        console.log(response)
        // this.path=response;
        this.infoPerfil()
        this.loading.dismiss()

        this.events.publish('FotoPerfil', response, Date.now());
      },
      err => {
        console.log(err)
        this.presentToast(err);
        this.loading.dismiss()
      })
  }

  ionViewDidLoad() {




  }

}

