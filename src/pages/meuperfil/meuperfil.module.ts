import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MeuperfilPage } from './meuperfil';

@NgModule({
  declarations: [
    MeuperfilPage,
  ],
  imports: [
    IonicPageModule.forChild(MeuperfilPage),
  ],
})
export class MeuperfilPageModule {}
