import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {
  public Header=false;
  public Footer=true;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

ionViewWillEnter() {
  document.getElementById("ion-header").style.display = "none";
}  
ionViewWillLeave(){
  document.getElementById("ion-header").style.display = "block";
}

ionViewDidLoad() {
  console.log('ionViewDidLoad TutorialPage');
}
HomePage=HomePage
irparaHome(){
  localStorage.setItem('Primeiravez', 'true')
}

}
