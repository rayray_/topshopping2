import { Component,ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FilmesPage } from '../filmes/filmes'
import { PrecoCinemaPage } from '../preco-cinema/preco-cinema'

import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';
import {SuperTabs,SuperTabsController } from 'ionic2-super-tabs'
@IonicPage()
@Component({
  selector: 'page-list-filmes',
  templateUrl: 'list-filmes.html',
})


export class ListFilmesPage {
  @ViewChild(SuperTabs) superTabs: SuperTabs;
  FilmesPage=FilmesPage
  PrecoCinemaPage=PrecoCinemaPage
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,
    private superTabsCtrl: SuperTabsController) {
  }

  onTabSelect(ev: any) {
    console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ListFilmesPage');
  }

}
