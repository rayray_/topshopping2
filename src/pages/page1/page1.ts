import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html',
})
export class Page1Page {
  Dominio
  page1
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,) {
  }
  InfoShopping() {
    this.http.get(this.service.config.jsonDomain + "jsonTelasMobile.json").map(res => res.json())
      .subscribe(
      data => {
        this.page1 = data.lista;
        this.Dominio = this.service.config.defaultDomain
          
        console.log(this.page1, 'Pagina 1')
      },
      err => {
        console.log('deuruim lojas', err);
      }
      );
  }
  ionViewDidLoad() {
    this.InfoShopping()
    console.log('ionViewDidLoad Page1Page');
  }

}
