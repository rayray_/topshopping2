import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { ServiceProvider } from '../../providers/service/service';
import { Http, Response, Headers } from '@angular/http';

@IonicPage()
@Component({
  selector: 'page-horarios',
  templateUrl: 'horarios.html',
})
export class HorariosPage {
  Dominio
  horarios
  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public http: Http,
    public service: ServiceProvider,) {
  }
  
  InfoShopping() {
    this.http.get(this.service.config.jsonDomain + "jsonHorariosMobile.json").map(res => res.json())
      .subscribe(
      data => {
        this.horarios = data.lista;
        this.Dominio = this.service.config.defaultDomain
        
        console.log(this.horarios)
      },
      err => {
        console.log('deuruim lojas', err);
      }
      );
  }
  ionViewDidLoad() {
    this.InfoShopping()
    console.log('ionViewDidLoad OshoppingPage');
  }

}
