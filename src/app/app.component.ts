import { Component, ViewChild,EventEmitter,Output,ElementRef,Directive,  Host, Renderer2 ,ChangeDetectorRef,HostBinding } from '@angular/core';
import { Platform, IonicApp, ViewController, Nav, Events,AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { TutorialPage } from '../pages/tutorial/tutorial';
import { LoginPage } from '../pages/login/login'
import { AcontecePage } from '../pages/acontece/acontece';
import { OfertasPage } from '../pages/ofertas/ofertas';
import { LojasPage } from '../pages/lojas/lojas';
import { GastronomiaPage } from '../pages/gastronomia/gastronomia'
import { LazerPage } from '../pages/lazer/lazer'
import { ListFilmesPage } from '../pages/list-filmes/list-filmes'
import { OshoppingPage } from '../pages/oshopping/oshopping'
import { ServicosPage } from '../pages/servicos/servicos'
import { FavoritosPage } from '../pages/favoritos/favoritos'
import { ListShoppingPage } from '../pages/list-shopping/list-shopping'
import { MeuperfilPage } from '../pages/meuperfil/meuperfil'
import { NotificacoesPage } from '../pages/notificacoes/notificacoes'
import { NotificacoesInternaPage } from '../pages/notificacoes-interna/notificacoes-interna'
import { BuscaPage } from '../pages/busca/busca';
import { ServiceProvider } from '../providers/service/service';
// import { PushProvider } from '../providers/push/push';
import { SharedService} from '../providers/shared-service/shared-service';
import { BuscaProvider } from '../providers/busca/busca';
import { AppAvailability } from '@ionic-native/app-availability';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DomSanitizer, SafeHtml, SafeUrl, SafeStyle} from '@angular/platform-browser';
import { Network } from '@ionic-native/network';

import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { ImageLoaderConfig } from 'ionic-image-loader';

@Component({
  templateUrl: 'app.html',
  providers: [AppAvailability,InAppBrowser,SharedService,BuscaPage],
})

@Directive({
  selector: '[m-ripple-effect]',
  host: {
      'tappable': '',
      'role': 'button',
      'style': 'position: relative; overflow: hidden'
  }
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  // @ViewChild('Like') likey: ElementRef;

  rootPage: any = TutorialPage;

  public HomePage: any = HomePage
  public LoginPage: any = LoginPage
  public OfertasPage: any = OfertasPage
  public AcontecePage: any = AcontecePage
  public LojasPage: any = LojasPage
  public GastronomiaPage: any = GastronomiaPage
  public LazerPage:any = LazerPage
  public ListFilmesPage:any = ListFilmesPage
  public ServicosPage:any = ServicosPage
  public FavoritosPage:any = FavoritosPage
  public ListShoppingPage:any = ListShoppingPage;
  public MeuperfilPage:any = MeuperfilPage
  public NotificacoesPage:any = NotificacoesPage

  Logado: any
  Usuario: any
  NomePessoa: string
  EmailPessoa: string
  NumeroNotificacoes: any
  appExistStatus:any
  twitter;
  facebook;
  instagram
  public disconnect: boolean = false;
  Token
  MensagemInterna
  ArmazenamentoEstacionamento:any
  ModeFace
  ModeFace2
  ModoFace3
  FotoApp
  constructor(
    public platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public service: ServiceProvider,
    private appAvailability: AppAvailability,
    private iab: InAppBrowser,
    public events: Events,
    public busca:BuscaProvider,
    private sharedService:SharedService,
    public sanitization: DomSanitizer,
    private _alertCtrl: AlertController,
    private network: Network,
    private element: ElementRef,
    private cdRef:ChangeDetectorRef,
    public push: Push,
    public http: Http,
    private imageLoaderConfig: ImageLoaderConfig,
    private localNotifications: LocalNotifications
    // private localNotification: PhonegapLocalNotification
    ) {
      this.VerificaLogin();
      imageLoaderConfig.enableSpinner(true);
      
      // set the maximum concurrent connections to 10
      imageLoaderConfig.setConcurrency(10000);
       this.network.onDisconnect().subscribe(() => {
        this.disconnect = true;
        this.onDisconnect();
      });
    // this.sanitization=sanitization;

    
    const Primeiravez = JSON.parse(localStorage.getItem('Primeiravez'));
    if (Primeiravez == 'true' || Primeiravez != undefined) {
      this.rootPage = HomePage;
     
    } else {
      this.rootPage = TutorialPage;
    }
 
    this.events.subscribe('user:Login', (data, Logado, AparecerAlert) => {
      this.Usuario = data,
      this.Logado = Logado,
      this.infoPerfil()
      
      console.log(this.Logado, 'status logado')
  
     //LOGIN PELO FACEBOOK CAI NO 3
      if(this.Usuario.fbUsuario!=null && this.Usuario.fotoUsuario!=null){
        this.ModeFace=false;
        this.ModeFace2=false;
        this.ModoFace3=false
      console.log("caiu 1");
        
      }else if(this.Usuario.fbUsuario==null && this.Usuario.fotoUsuario==null){
        this.ModeFace=false;
        this.ModeFace2=true;
        this.ModoFace3=true;
      console.log("caiu 2");
        
      }else if(this.Usuario.fbUsuario!=null && this.Usuario.fotoUsuario==null){
        this.ModeFace=true;
        this.ModeFace2=false;
        this.ModoFace3=true
      console.log("caiu 3");
        
      }else if(this.Usuario.fbUsuario==null && this.Usuario.fotoUsuario!=null){
        this.ModeFace=false;
        this.ModeFace2=false;
        this.ModoFace3=false
      console.log("caiu 4");
        
      }
      this.MsgsNaoLidas() 
      setInterval(() => { 
        this.MsgsNaoLidas() 
      }, 5000);
      this.showAlertLoginSucesso()
      this.initPushNotification()
      // this.AtrelandoUsuarioAoPush()
    });


    
    this.events.subscribe('FotoPerfil', (response) => {
      this.infoPerfil()
    })
    
    platform.ready().then(() => {
      this.initPushNotification();
      splashScreen.hide();
    // this.push;
      if (this.platform.is('ios')) {
        statusBar.hide();
      }else{
        statusBar.show();
      }
    });
    this.events.subscribe('user:MensagemLida', (MsgsList) => {
      this.MsgsNaoLidas() 
    });
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    this.VerificaLogin()
    if(Token){
    // this.MsgsNaoLidas() 
    this.infoPerfil()   

     
   

    setInterval(() => { 
      this.MsgsNaoLidas() 
   

    }, 5000);
    }else{
      console.log('To Deslogado')
    }
  }


  Deslogar(page) {
    
    localStorage.removeItem('cacheUsuario')
    this.events.unsubscribe('user:Login', (data,Logado) => {
      console.log('Login', data, Logado);
      this.Usuario=data,
      this.Logado=Logado
    });
    this.Logado = false;
    this.nav.setRoot(page)
  }

  initPushNotification() {
    if (!this.platform.is('cordova')) {
      console.warn('Push notifications not initialized. Cordova is not available - Run in physical device');
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: this.service.config.SenderID
      },
      ios: {
        alert: 'true',
        badge: false,
        sound: 'true'
      },
      windows: {}
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on('registration').subscribe((data: any) => {
      console.log('device token -> ' + data.registrationId);
      localStorage.setItem('Token', JSON.stringify(data.registrationId))
      let plataforma = '';
      if (navigator.userAgent.match(/Android/i)) {
          plataforma = 'android';
      } else {
          plataforma = 'ios';
      }
      // this.http.get(this.service.config.defaultDomain + '/Admin/PushRegister/Index/'+this.service.config.idShopping+'?token='+data.registrationId+'&plataforma='+plataforma).map(res => res.json())
      if(!this.Usuario){
        this.http.get(this.service.config.defaultDomain + ':128/Admin/PushRegister/Index/'+this.service.config.idShopping+'?token='+data.registrationId+'&plataforma='+plataforma).map(res => res.json())
        //TODO - send device token to server
        .subscribe(
          data => {
           console.log(data, `retorno do token`)
          },
          err => {
            // stop disconnect watch
            console.log('deuruim', err)
          }
          );
      }else{
        this.http.get(this.service.config.defaultDomain + ':128/Admin/PushRegister/Index/'+this.service.config.idShopping+'?token='+data.registrationId+'&plataforma='+plataforma+'&usuarioid='+this.Usuario.codUsuario).map(res => res.json())
        //TODO - send device token to server
        .subscribe(
          data => {
           console.log(data, `retorno do token`)
          },
          err => {
            // stop disconnect watch
            console.log('deuruim', err)
          }
          );
      }
     
    });

    pushObject.on('notification').subscribe((notificacao: any) => {
      this.Token = JSON.parse(localStorage.getItem('cacheUsuario'))
        console.log('message -> ' + notificacao);  []
      
      if (notificacao.additionalData.foreground==true) {
        this.localNotifications.schedule({
          title: this.service.config.name,
          text: notificacao.message,
          icon: 'ic_notifications',
          smallIcon: 'ic_notification_small',
          id: notificacao.additionalData.MensagemCod
        });
        this.localNotifications.on("click", (notification, state) => {
          if(!this.Token){
            this.nav.setRoot(HomePage)
          }else{
            console.log('clique do push', notification, state)
            this.http.get(this.service.config.defaultDomain + "/api/mobile/MensagensList?ClienteCod=" + this.service.config.idShopping+'&MensagemCod='+notification.id, { "headers": new Headers({'Authorization': this.Token.Token}) }).map(res => res.json())
            .subscribe(
              data => {
                this.MensagemInterna=data.Mensagens[0]
    // this.navCtrl.remove(1)
                console.log(data, 'InternaNotificacao')
                this.nav.push(NotificacoesInternaPage, { 
                  msgInter: this.MensagemInterna
                });
              }            
          )
          }
        });
      }else if(notificacao.additionalData.foreground==false){
        // if(!this.Token){
          this.http.get(this.service.config.defaultDomain + "/api/mobile/MensagensList?ClienteCod=" + this.service.config.idShopping+'&MensagemCod='+notificacao.additionalData.MensagemCod, { "headers": new Headers({'Authorization': this.Token.Token}) }).map(res => res.json())
            .subscribe(
              data => {
                this.MensagemInterna=data.Mensagens[0]
                console.log(data, 'InternaNotificacao')
                this.nav.push(NotificacoesInternaPage, { 
                msgInter: this.MensagemInterna });
                console.log('Push notification clicked', this.MensagemInterna);
              }            
          )
        // }
      }
    });
    pushObject.on('error').subscribe(error => console.error('Error with Push plugin' + error));
  }


  onDisconnect() {
    console.log('Sem conexão com internet. O conteúdo mais recente não poderá ser exibido. :-(');
    this._alertCtrl.create({
      // title: this.service.config.name,
      // subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
      // buttons: [{ text: 'Ok' }]
      title: '<img src="assets/icon/error.png" />'+this.service.config.name,
      subTitle: 'Sem conexão com internet. O conteúdo mais recente não poderá ser exibido.',
      buttons: ['OK'],
      cssClass: 'alertCustomCssError'
    }).present();
    this.VerificaLogin()
    
  }
  showAlertLoginSucesso() {
    let alert = this._alertCtrl.create({
      title: '<img src="assets/icon/confirmation.png" /> Bem Vindo!',
      subTitle: 'Olá '+this.Usuario.nomeUsuario+', você está logado!',
      buttons: ['OK'],
      cssClass: 'alertCustomCss'
    });
    alert.present();
  }
  
  frasemenu=this.service.config.FraseMenu;
  Filtro;
  VerificaLogin(){
    if (this.service.isLogged()) {
      this.Logado = true;
      this.Usuario = JSON.parse(window.localStorage.getItem('cacheUsuario'));
    } else {
      this.Logado = false;
    }
  }
  pushPage(page) {
    this.nav.push(page).then(response => {
      console.log(response);
    }).catch(e => {
      console.log(e);
    });
  }

  setPage(page) {
    this.nav.setRoot(page).then(response => {
      console.log(response);
      // this.events.publish('user:pagesClass', page, this.ClassAcontece);
      this.Filtro=false
    }).catch(e => {
      console.log(e);
    });
  }
 
  setPage2(page) {
    this.nav.setRoot(page).then(response => {

      // localStorage.setItem('1', 'contato')
      console.log(response);
      // this.events.publish('user:pagesClass', page, this.ClassAcontece);
      
      this.events.publish('user:Pagina', 1);
      this.Filtro=false
    }).catch(e => {
      console.log(e);
    });
  }

  app:any
  CheckForApp(app){
    // let app;
    console.log(app)
      if (this.platform.is('ios')) {
        if(app=='facebook'){
          this.fbCheck('fb://', 'fb://profile/566270846722846');
        }else if(app=='instagram'){
          this.instaCheck('instagram://');
        }else if(app=='twitter'){
          this.twitterCheck('twitter://');
        }else if(app=='youtube'){
          this.youtubeCheck('youtube://');
        } else if (app=="topshopping"){
          window.open('http://www.ehtopshopping.com.br/', '_system', 'location=no');
        }
      } else if(this.platform.is('android')){
        if(app=='facebook'){
          this.fbCheck('com.facebook.katana', 'fb://page/'+this.service.config.IdFacebook);
        }else if(app=='instagram'){
          this.instaCheck('com.instagram.android');
        }else if(app=='twitter'){
          this.twitterCheck('com.twitter.android');
        }else if(app=='youtube'){
          this.youtubeCheck('com.youtube');
        } else if (app=="topshopping"){
          window.open('http://www.ehtopshopping.com.br/', '_system', 'location=no');
        }
      }
    }

    fbCheck(facebook, urlscheme){
     this.appAvailability.check(facebook)
     .then(
       (yes) => {
         console.log(facebook + ' is available')
         window.open(urlscheme, '_system', 'location=no');
       },
       (no) => {
         console.log(facebook + ' is NOT available')
         window.open('https://www.facebook.com/'+this.service.config.NomePagina, '_system', 'location=no');
         console.log("náo tem instalado");
       }
     );
   }
   
  instaCheck(instagram){
    this.appAvailability.check(instagram)
    .then(
      (yes) => {
        console.log(instagram + ' is available')
        this.appExistStatus = instagram + 'is available'
        window.open('instagram://user?username='+this.service.config.usernameInstagram, '_system', 'location=no');
      },
      (no) => {
        console.log(instagram + ' is NOT available')
        window.open('https://www.instagram.com/'+this.service.config.usernameInstagram, '_system', 'location=no');
      }
    );
  }
  
  youtubeCheck(youtube){
    this.appAvailability.check(youtube)
    .then(
      (yes) => {
        console.log(youtube + ' is available')
        this.appExistStatus = youtube + 'is available'
        window.open('youtube://'+this.service.config.youtube, '_system', 'location=no');
      },
      (no) => {
        console.log(youtube + ' is NOT available')
        window.open('https://www.youtube.com/'+this.service.config.youtube, '_system', 'location=no');
      }
    );
  }

    twitterCheck(twitter){
      this.appAvailability.check(this.app)
      .then(
        (yes) => {
          console.log(twitter + ' is available')
          this.appExistStatus = this.app + 'is available'
          {window.open('twitter://user?screen_name='+this.service.config.usernameTwitter, '_system', 'location=no');}

        },
        (no) => {
          console.log("veio no else do availabity");
          window.open('https://twitter.com/'+this.service.config.usernameTwitter, '_system', 'location=no');
          console.log("náo tem instalado");
        }
      );
    }
  MensagensDisponiveis
  numeroMsgs
  MsgsNaoLidas(){
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    if(Token){
    let headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': Token.Token
    });
    let options = new RequestOptions({ headers: headers });
    // let body = `Origem=${'Aplicativo'}&ClienteCod=${this.service.config.idShopping}&codUsuario=${Token.codUsuario}&codMensagem=${this.Msg.Cod}`;

    return this.http.get(this.service.config.defaultDomain + '/api/Mobile/MensagensNaoLidas?ClienteCod='+this.service.config.idShopping+'&codUsuario='+Token.codUsuario, { headers: headers }).map(res => res.json())
      .subscribe(
      data => {
        // console.log(data)
        this.MensagensDisponiveis=data
        this.numeroMsgs = this.MensagensDisponiveis.NaoLidas
      },
      err => {
        console.log(err)
        // localStorage.removeItem('cacheUsuario')
        this.Logado = false;
        }
      );
    }
  }
  FieldBusca
  onClick(){
    if(!this.Filtro){
      this.Filtro=true
    }else{
      this.Filtro=false
    }
  }
  public page=BuscaPage
  BuscarPesquisa () {
    this.events.publish('user:Pesquisa',
      this.FieldBusca,       
      this.page
    );
    this.busca
    this.nav.setRoot(BuscaPage)
    // this.nav.remove(-1)
    this.Filtro=false
  }
  
  
  meusdados
  infoPerfil(){
    let Token = JSON.parse(localStorage.getItem('cacheUsuario'))
    let headers = new Headers();
    headers.append('Authorization', Token.Token);
    this.http.get(this.service.config.defaultDomain + "/api/mobile/CadastrosAPPDetalhes?ClienteCod=" + this.service.config.idShopping + "&codUsuario=" + Token.codUsuario, { "headers": headers }).map(res => res.json())
      .subscribe(
      data => {
        this.meusdados = data,
        this.FotoApp = this.meusdados.fotoUsuario
        if(this.Usuario.fbUsuario!=null && this.meusdados.fotoUsuario!=null){
          this.ModeFace=false;
          this.ModeFace2=false;
          this.ModoFace3=false;
        }else if(this.Usuario.fbUsuario==null && this.meusdados.fotoUsuario==null){
          this.ModeFace=false;
          this.ModeFace2=true;
          this.ModoFace3=true;
          
    
        }else if(this.Usuario.fbUsuario!=null && this.meusdados.fotoUsuario==null){
          this.ModeFace=true;
          this.ModeFace2=false;
          this.ModoFace3=true;
          
        }else if(this.Usuario.fbUsuario==null && this.meusdados.fotoUsuario==null){
          this.ModeFace=false;
          this.ModeFace2=true;
          this.ModoFace3=true;
          
        }
      },
      err => {
        console.log('deuruim', err)
       
    this.VerificaLogin()
        }
      );
  }

}
