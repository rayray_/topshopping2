import { NgModule, ErrorHandler } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular'
import { MyApp } from './app.component'
import { ServiceProvider } from '../providers/service/service'
import { TutorialPage } from '../pages/tutorial/tutorial'
import { HomePage } from '../pages/home/home'
import { LoginPage } from '../pages/login/login'
import { CadastrePage } from '../pages/cadastre/cadastre'
import { EsquecisenhaPage } from '../pages/esquecisenha/esquecisenha'
import { AcontecePage } from '../pages/acontece/acontece'
import { FeedPage } from '../pages/feed/feed'
import { FiquepordentroPage } from '../pages/fiquepordentro/fiquepordentro'
import { FiquepordentrointernaPage } from '../pages/fiquepordentrointerna/fiquepordentrointerna'
import { EnquetesPage } from '../pages/enquetes/enquetes'
import { OfertasPage } from '../pages/ofertas/ofertas'
import { CuponsPage } from '../pages/cupons/cupons'
import {CuponsInternaPage} from '../pages/cupons-interna/cupons-interna'
import { VitrinePage } from '../pages/vitrine/vitrine'
import { VitrineInternaPage } from '../pages/vitrine-interna/vitrine-interna'
import { LojasPage } from '../pages/lojas/lojas'
import { LojasInternaPage } from '../pages/lojas-interna/lojas-interna'
import { GastronomiaPage } from '../pages/gastronomia/gastronomia'
import { GastronomiaInternaPage } from '../pages/gastronomia-interna/gastronomia-interna'
import { LazerPage } from '../pages/lazer/lazer'
import { LazerInternaPage } from '../pages/lazer-interna/lazer-interna'
import { ListFilmesPage } from '../pages/list-filmes/list-filmes'
import { PrecoCinemaPage } from '../pages/preco-cinema/preco-cinema'
import { FilmesPage } from '../pages/filmes/filmes'
import { FilmesInternaPage } from '../pages/filmes-interna/filmes-interna'
import { OshoppingPage } from '../pages/oshopping/oshopping'
import { ServicosPage } from '../pages/servicos/servicos'
import { ListShoppingPage } from '../pages/list-shopping/list-shopping'

import { ListaservicosPage } from '../pages/listaservicos/listaservicos'
import { EstacionamentoPage } from '../pages/estacionamento/estacionamento'
import { ServicosinternaPage } from '../pages/servicosinterna/servicosinterna'
import { FavoritosPage } from '../pages/favoritos/favoritos'
import { HorariosPage } from '../pages/horarios/horarios'
import { ComochegarPage } from '../pages/comochegar/comochegar'
import { ContatoPage } from '../pages/contato/contato'
import { MeuperfilPage } from '../pages/meuperfil/meuperfil'
import { NotificacoesPage } from '../pages/notificacoes/notificacoes'
import { NotificacoesInternaPage } from '../pages/notificacoes-interna/notificacoes-interna'

import { Page1Page } from '../pages/page1/page1'
import { Page2Page } from '../pages/page2/page2'
import { Page3Page } from '../pages/page3/page3'
import { CampanhaPage } from '../pages/campanha/campanha'
import { CampanhaCadastroPage } from '../pages/campanha-cadastro/campanha-cadastro'
import { BuscaPage } from '../pages/busca/busca';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import { TruncateModule } from 'ng2-truncate';
import { TextMaskModule } from 'angular2-text-mask';
import { Facebook } from '@ionic-native/facebook';
import { FacebookService } from '../providers/facebook/facebook';
import { NativeStorage } from '@ionic-native/native-storage';
import { SuperTabsModule,SuperTabsController } from 'ionic2-super-tabs'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SwiperModule } from 'angular2-useful-swiper';
import { BuscaProvider } from '../providers/busca/busca';
import { SharedService } from '../providers/shared-service/shared-service';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { Footer } from '../components/footer/footer';
import { Push} from '@ionic-native/push';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { MomentModule } from 'angular2-moment';
import { IonicImageLoader } from 'ionic-image-loader';
@NgModule({
  
  declarations: [
    MyApp,
    TutorialPage,
    HomePage,
    LoginPage,
    CadastrePage,
    EsquecisenhaPage,
    AcontecePage,FeedPage,FiquepordentroPage,EnquetesPage,FiquepordentrointernaPage,
    OfertasPage,VitrinePage,VitrineInternaPage ,CuponsPage,CuponsInternaPage,
    LojasPage,LojasInternaPage,
    GastronomiaPage, GastronomiaInternaPage,
    LazerPage, LazerInternaPage,
    ListFilmesPage,FilmesPage, FilmesInternaPage,PrecoCinemaPage,
    OshoppingPage,ListShoppingPage,HorariosPage,ComochegarPage,ContatoPage,Page1Page,Page2Page,Page3Page,
    ServicosPage,ListaservicosPage,EstacionamentoPage,ServicosinternaPage,
    FavoritosPage,
    CampanhaPage, CampanhaCadastroPage,
    BuscaPage,
    MeuperfilPage,
    NotificacoesPage,NotificacoesInternaPage,
    Footer
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp,{
     mode: 'md'
    }),
    HttpModule,
    TruncateModule,
    FormsModule,
    TextMaskModule,
    SuperTabsModule,
    ReactiveFormsModule,
    SwiperModule,
    MomentModule,
    IonicStorageModule.forRoot(),
    IonicImageLoader.forRoot(),
    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TutorialPage,
    HomePage,
    LoginPage,
    CadastrePage,
    EsquecisenhaPage,
    AcontecePage,FeedPage,FiquepordentroPage,EnquetesPage,FiquepordentrointernaPage,
    OfertasPage,VitrinePage,VitrineInternaPage ,CuponsPage,CuponsInternaPage,
    LojasPage, LojasInternaPage,
    GastronomiaPage, GastronomiaInternaPage,
    LazerPage, LazerInternaPage,
    ListFilmesPage,FilmesPage, FilmesInternaPage,PrecoCinemaPage,
    ListShoppingPage,OshoppingPage,HorariosPage,ComochegarPage,ContatoPage,Page1Page,Page2Page,Page3Page,
    ServicosPage,ListaservicosPage,EstacionamentoPage,ServicosinternaPage,
    FavoritosPage,
    CampanhaPage,CampanhaCadastroPage,
    BuscaPage,
    MeuperfilPage,
    NotificacoesPage,NotificacoesInternaPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiceProvider,
    Facebook,
    SuperTabsModule,
    FacebookService,
    NativeStorage,
    SuperTabsController,
    BuscaProvider,
    SharedService,
    Network,
    SocialSharing,
    Footer,
    Push,
    LocalNotifications  ]
})
export class AppModule {
}
