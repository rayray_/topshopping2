import { Output,Component,EventEmitter,Input} from '@angular/core';
import { IonicPage, NavController, NavParams,Nav ,Events} from 'ionic-angular';

import { AcontecePage } from '../../pages/acontece/acontece';
import { LojasPage } from '../../pages/lojas/lojas';
import { GastronomiaPage } from '../../pages/gastronomia/gastronomia'
import { LazerPage } from '../../pages/lazer/lazer'
import { ListFilmesPage } from '../../pages/list-filmes/list-filmes'
@Component({
  selector: 'app-footer',
  templateUrl: 'footer.html',
})
export class Footer {

@Input('ClassAcontece')
  public AcontecePage: any = AcontecePage
  public LojasPage: any = LojasPage
  public GastronomiaPage: any = GastronomiaPage
  public LazerPage:any = LazerPage
  public ListFilmesPage:any = ListFilmesPage


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events) {


  }

  
  setPage(page) {
    this.navCtrl.setRoot(page).then(response => {
      console.log(response);
    }).catch(e => {
      console.log(e);
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad Footer');
  }

}
