import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Footer } from './footer';

@NgModule({
  declarations: [
    Footer,
  ],
  imports: [
    IonicPageModule.forChild(Footer),
  ],
})
export class FooterPageModule {}
