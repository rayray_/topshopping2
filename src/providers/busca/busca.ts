import { Injectable, ViewChild } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import * as $ from 'jquery'
import { IonicPage, Nav,  NavParams, Events,LoadingController,AlertController } from 'ionic-angular';
import { ServiceProvider } from '../service/service';
import { BuscaPage } from '../../pages/busca/busca';

@Injectable()
export class BuscaProvider {
  
  @ViewChild(Nav) nav: Nav;
  constructor(
    public http: Http,
    public service: ServiceProvider,
    public events: Events, private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController,
    // private navCtrl: NavController, 
    ) {
    this.JsonLojas()
    this.JsonGastronomia()
    this.JsonDestaque()
    this.JsonServicos()
    this.JsonLazer()
    this.JsonFilme()
    this.events.subscribe('user:Pesquisa', (FieldBusca, page ) => {
        // console.log(FieldBusca, page, 'tA DENTRO DO CONSTRUCTOR')
        let auxList = [];
        let store;
        let nadaEncontrado;
        let txtBusca
        if(FieldBusca==''){
          this._alertCtrl.create({
            title: this.service.config.name,
            subTitle: 'Digite o que procura.',
            buttons: [{ text: 'Ok' }]
          }).present();
        }
        FieldBusca;
        if(this.lojas !=undefined && this.filmes && this.gastronomia !=undefined && this.destaque !=undefined){
        for (var z = 0; z < this.lojas.length; z++) {
          this.lojas[z].categoria = "Lojas";
          store = this.lojas[z];
          if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
            auxList.push(store);
          }
        }
        for (var z = 0; z < this.servicos.length; z++) {
          this.servicos[z].categoria = "Serviços";
          store = this.servicos[z];
          if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
            auxList.push(store);
          }
        }
        for (var z = 0; z < this.lazer.length; z++) {
          this.lazer[z].categoria = "Lazer";
          store = this.lazer[z];
          if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
            auxList.push(store);
          }
        }
        for (var v = 0; v < this.gastronomia.length; v++) {
          this.gastronomia[v].categoria = "Alimentação";
          store = this.gastronomia[v];
          if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
            auxList.push(store);
          }
        }
        for (var b = 0; b < this.destaque.length; b++) {
          this.destaque[b].categoria = "Acontece";
          store = this.destaque[b];
          if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
            auxList.push(store);
          }
        }
        for (var h = 0; h < this.filmes.length; h++) {
          this.filmes[h].categoria = "Filmes";
          store = this.filmes[h];
          if (store.nome.str.lang_1.toUpperCase().indexOf(FieldBusca.toUpperCase()) > -1) {
            auxList.push(store);
          }
        }
        if (auxList.toString() == "") {
          console.log('veiovazio')
          // localStorage.setItem('campoPesquisa', 'Não Encontrou '+FieldBusca);
          nadaEncontrado=true
          localStorage.setItem('resultadoDaBuscaHome', JSON.stringify(auxList));
          localStorage.setItem('campoPesquisa', FieldBusca);
        } else {
          localStorage.setItem('resultadoDaBuscaHome', JSON.stringify(auxList));
          nadaEncontrado=false
          localStorage.setItem('campoPesquisa', FieldBusca);
         return true
        }
        
      }else{
        console.log('Nao Carrego os Jsons')
        // localStorage.setItem('campoPesquisa', 'Não Encontrou '+FieldBusca);
        return false
      }
    })
    console.log('Hello BuscaProvider Provider');
  }
  lojas
  gastronomia
  filmes
  destaque
  lazer
  servicos
  CartazLink

  public JsonLojas() {
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.defaultDomain + "/api/mobile/LojasList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=1000").map(res => res.json())
      .subscribe(
      data => {
        this.lojas = data.lista;
      },
    );
  }

  public JsonGastronomia() {
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.defaultDomain + "/api/mobile/GastronomiaList?codCliente=" + this.service.config.idShopping + '&Pagina=' + this.Pagina + '&Items=6').map(res => res.json())
    
      .subscribe(
      data => {
        this.gastronomia = data.lista;
      },
    );
  }

  public JsonDestaque() {
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + 'jsonDestaquesMobile.json?nocache=' + nocache).map(res => res.json())
      .subscribe(
      data => {
        this.destaque = data.lista;
      },
    );
  }
  Pagina=0
  public JsonLazer() {
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    // this.http.get(this.service.config.jsonDomain + 'jsonEntretenimento.json?nocache=' + nocache).map(res => res.json())
    this.http.get(this.service.config.defaultDomain + "/api/mobile/EntretenimentoList?codCliente=" + this.service.config.idShopping + "&Pagina=" + this.Pagina + "&Items=5").map(res => res.json())
      .subscribe(
      data => {
        this.lazer = data.lista;
      },
    );
  }

  public JsonServicos() {
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + 'jsonservicos.json?nocache=' + nocache).map(res => res.json())
      .subscribe(
      data => {
        this.servicos = data.lista;
      },
    );
  }

  public JsonFilme() {
    let geranum = Math.random() * 9999999;
    let nocache = Math.round(geranum);
    this.http.get(this.service.config.jsonDomain + "jsonfilmes.json?nocache=" + nocache).map(res => res.json())
      .subscribe(
      data => {
        this.filmes = data.lista;
        this.CartazLink = this.service.config.pipocaoCartazHome
        for (let i = 0; i < data.lista.length; i++) {
          let findPipocao = data.lista[i].midia.arquivos.lang_1.arquivo.url;
          let changeFindPipocao = findPipocao.split('pipocao/').join('http://www.pipocao.com.br/images/cinema/340x480/');
          data.lista[i].midia.arquivos.lang_1.arquivo.url = changeFindPipocao;
        }
      },
      err => {
        console.log('deuruim lojas', err);
      }
      );
  }
  loader = this._loadingCtrl.create({
    content: 'Aguarde...',
    dismissOnPageChange: true

  });

}
