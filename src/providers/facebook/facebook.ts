import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Facebook } from '@ionic-native/facebook';
@Injectable()
export class FacebookService {
 APP_ID: number = 142735572987072;
 
 constructor(
     private fb: Facebook) {     
 }
 
 init() {
    this.fb.browserInit(this.APP_ID, "v2.8");
 }
}