import { Injectable } from '@angular/core';
import { Http, Response, Headers,RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ServiceProvider {

  constructor(
    public http: Http,
  ) {
  }

  config = new function(){

    this.SenderID='3985457908',
      this.shopping="topshopping"
      this.idShopping = 19,

    //shopping teste
    // this.idShopping = 17,
    // this.shopping="shopping-de-teste",
    this.name="TopShopping",
    this.telefone="(21) 2667-1787",
    this.email="sac@psbetim.com.br"
    this.site="www.topshopping.com.br",
    this.IdFacebook="558893457475582"
    this.NomePagina="PartageShoppingBetim/"
    this.usernameInstagram="partageshoppingbetim"
    this.usernameTwitter=""
    // this.youtube="user/ShoppingMetropolitan",
    this.swarm="",
    this.defaultDomain="http://www.am4mall.com.br";
    // this.defaultDomainHomolog="http://www.am4mall.com.br:128";
    this.domain=this.defaultDomain+"/conteudo/clientes/",
    this.jsonDomain=this.domain+this.shopping+"/jsons/"
    this.contentDomain=this.domain;
    this.wiseitDomain=""
    this.importacaoDomain=this.defaultDomain+"/Admin/Importacao/ImpFaleConosco/"+this.shopping;
    this.importacaoNovidades=this.defaultDomain+"/Admin/Importacao/ImpCadastros/am4";
    this.getImagesDomain="http://www.am4mall.com.br";
    this.pipocaoCartazHome = "http://www.pipocao.com.br/images/cinema/340x480/"
    this.IngressoOnline=true
    this.LinkIngresso='https://www.kinoplex.com.br/cinema/kinoplex-topshopping-1-a-6/49'
    this.FraseMenu='Cadastre-se e receba as últimas novidades do TopShopping!'
    this.HashTag='#AppTopShopping'
    this.endereco='Av. Gov. Roberto Silveira, 540, Centro, Nova Iguaçu - RJ'

   
  }
  

  Usuario = JSON.parse(window.localStorage.getItem('cacheUsuario'));
  Logado: any
  isLogged(){
    if(this.Usuario!=undefined){
      return true
    }else{
      return false;
    }
  }

  
}
