import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import * as _ from "lodash";
// import * as _ from 'underscore';
import * as $ from 'jquery';


@Injectable()
export class SharedService {

  constructor(public http: Http) {
    console.log('Hello SharedServiceProvider Provider');
  }
  checkLike = function (area: any, objeto: any) {
      // gera o nome do localStorage padrao area-like
      var localStorageName = area + "-like";
      //busca os likes da area
      var likes = JSON.parse(localStorage.getItem(localStorageName));
      if (likes == null)
        likes = [];
      //verifica se o objeto em questão ja foi curtido
      // let like
      var curtiu = _.filter(likes, function (like: any) {
        return like.cod == objeto.cod;
      });

     
      if (curtiu == null)
        curtiu = [];
      //retorna true se ja foi curtido e false se não
      return curtiu.length > 0;
    }

  setLike = function (area: any, objeto: any) {
    var localStorageName = area + "-like";
    //busca os likes da area
    var likes = JSON.parse(localStorage.getItem(localStorageName));
    //se nao existir nenhum like ele gera um array vazio
    if (likes == null)
      likes = [];

    //verifica se o objeto em questão ja foi curtido
    var curtiu = _.filter(likes, function (like:any) {
      return like.cod == objeto.cod;
    });

    if (curtiu == null)
      curtiu = [];

    //se ja curtiu ele ira descurtir, senao ira curtir
    if (curtiu.length > 0) {
      likes = _.difference(likes, curtiu);
    } else {
      likes.push(objeto);
    }
    //salva no localStorage de curtidas da area
    localStorage.setItem(localStorageName, JSON.stringify(likes));
  };
  
}
